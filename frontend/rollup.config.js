
import typescript from "rollup-plugin-typescript2";
import resolve from "@rollup/plugin-node-resolve";
import commonjs from "@rollup/plugin-commonjs";
import { terser } from "rollup-plugin-terser";
import replace from "@rollup/plugin-replace";
import babel from "@rollup/plugin-babel";
import globals from "rollup-plugin-node-globals";
import builtins from "rollup-plugin-node-builtins";

const NODE_ENV = process.env.NODE_ENV || "development";

export default {
    input: ["src/tree.tsx"],
    output: [{
        file: "lib/tree.js",
        format: "iife",
        sourcemap: NODE_ENV === "development",
    }],
    plugins: [
        replace({
            "process.env.NODE_ENV": JSON.stringify(NODE_ENV),
        }),
        resolve({preferBuiltins: true}),
        commonjs(),
        globals(),
        builtins(),
        typescript(),
        babel({
            extensions: [".ts", ".tsx", ".js", ".jsx"],
            babelrc: false,
            babelHelpers: "bundled",
            presets: [[
                '@babel/preset-env',
                { modules: false, useBuiltIns: "usage", corejs: 3, targets: {esmodules: true} }
            ]],
            exclude: [/node_modules\/core-js\//, /node_modules\/react-dom\//, /node_modules\/react\//],
        }),
        NODE_ENV === "production" ? terser() : undefined,
    ],
}
