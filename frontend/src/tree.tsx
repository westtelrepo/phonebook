/** @jsx jsx */
import { css, jsx } from "@emotion/core";
import styled from "@emotion/styled";
import { IHistoryEvent } from "@w/shared/dist/db_phonebook";
import { pdp_allowed } from "@w/shared/dist/pdp";
import * as Phonebook from "@w/shared/dist/phonebook";
import Immutable from "immutable";
import $ from "jquery";
import React, {
    Fragment,
    useCallback,
    useEffect,
    useRef,
    useState,
} from "react";
import ReactDOM from "react-dom";

import { AddUndo } from "./constants";
import { ContactsEditor } from "./contacts";
import { DBWidget } from "./db";
import { ExportEditor } from "./export";
import { History } from "./history";
import { post, useUndoStack } from "./hooks";
import { ImportEditor } from "./import";
import { Editor } from "./ng911/Editor";
import {
    AccountLogoutIcon,
    INLINE_ICON_STYLE,
    RedoIcon,
    UndoIcon,
} from "./open_iconic";
import { PhoneEditor } from "./phone";
import { SpeedDialEditor } from "./speeddials";
import { UsersEditor } from "./users";
import { useServerState } from "./useServerState";

enum State {
    contacts,
    db,
    export,
    import,
    phone,
    sd,
    users,
    history,
    ng911,
}

const Button = styled.button`
    font-size: inherit;
    margin-left: 1px;
    margin-right: 1px;
`;

const current_button = css`
    color: blue;
`;

const topTabStyle = css`
    margin-left: 10px;
    margin-right: 10px;
`;

function App() {
    async function logout() {
        await post("/api/logout");
        document.location.reload(true);
    }

    const phonebook_list = useServerState(
        "/api/phonebook/list",
        (o): { id: string }[] => o as any,
    );

    let phonebook_uri: string | null = null;
    if (phonebook_list && phonebook_list.length === 1) {
        phonebook_uri = "/api/phonebook/" + phonebook_list[0].id;
    }

    const phonebook = useServerState(
        phonebook_uri,
        o => new Phonebook.Phonebook(o as any),
    );

    async function exec_undo(change_id: number) {
        if (phonebook) {
            const ret = await post("/api/phonebook/" + phonebook.id + "/undo", {
                change_id,
            });
            if (
                typeof ret === "object" &&
                ret !== null &&
                typeof ret.change_id === "number"
            ) {
                return ret.change_id;
            }
        }
        return null;
    }

    const [history_events, set_history_events] = useState<
        Immutable.Map<number, IHistoryEvent>
    >(Immutable.Map());
    /**
     * change_id's that are currently being acquired from the server.
     * Note that we never refresh; an IHistoryEvent is immutable.
     */
    const acquiring = useRef<Set<number>>(new Set());

    // we want this callback to change when either history_events or the phonebook changes,
    // as those are the two situations when the doability changes
    const can_undo_f = useCallback(
        (change_id: number) => {
            if (phonebook === null) {
                return false;
            }

            async function get() {
                // if we are acquiring it don't bother trying again
                if (acquiring.current.has(change_id)) {
                    return;
                }

                try {
                    acquiring.current.add(change_id);
                    const req = await fetch(
                        "/api/phonebook/" +
                            // eslint-disable-next-line @typescript-eslint/no-non-null-assertion
                            phonebook!.id +
                            "/history/" +
                            String(change_id),
                        {
                            credentials: "same-origin",
                            headers: {
                                Accept: "application/json",
                            },
                        },
                    );

                    if (!req.ok) {
                        throw new Error("TODO: " + req.statusText);
                    }

                    const val = await req.json();

                    set_history_events(old => old.set(change_id, val));
                } finally {
                    acquiring.current.delete(change_id);
                }
            }

            const event = history_events.get(change_id);
            if (event === undefined) {
                // acquire the event async
                get().catch(e =>
                    console.error("ERROR IN App call to get()", e),
                );
                return false;
            }

            try {
                // if the inv_patch applies and validates then it is undoable
                if (event.inv_patch.length === 0) {
                    throw new Error("refuse zero length operation");
                }
                const op = new Phonebook.Operation({
                    patch: event.inv_patch,
                    inv_patch: [],
                });
                const ph = phonebook._apply(op);
                ph.validate();
                return true;
            } catch (_error) {
                return false;
            }
        },
        [phonebook, history_events],
    );

    const [
        add_undo,
        undo,
        can_undo,
        redo,
        can_redo,
        undoing,
        redoing,
    ] = useUndoStack<number>(exec_undo, can_undo_f);

    const icon_list =
        useServerState(
            phonebook_uri === null ? null : phonebook_uri + "/icon_list",
            (o): string[] => o as string[],
        ) ?? [];

    const [error_msg, set_error_msg] = useState(
        undefined as string | undefined,
    );
    const [state, set_state] = useState(State.contacts);
    const [search_text, set_search_text] = useState("");

    const api_call = async (command: any): Promise<any> => {
        try {
            const val = await new Promise((resolve, reject) => {
                // eslint-disable-next-line @typescript-eslint/no-floating-promises
                $.post("/api", {
                    command: JSON.stringify(command),
                })
                    .then(data => {
                        if (data.error) {
                            if (data.code === "UNAUTHORIZED") {
                                document.location.reload(true);
                            } else {
                                reject(data);
                            }
                        } else {
                            resolve(data);
                        }
                    })
                    .fail((_, textStatus) =>
                        reject({
                            error: "Could not contact server: " + textStatus,
                        }),
                    );
            });
            set_error_msg(undefined);
            return val;
        } catch (e) {
            set_error_msg(e.error);
            return Promise.reject(e);
        }
    };

    async function onUpdate() {
        if (!phonebook) {
            throw new Error("updating unknown phonebook");
        }

        try {
            const ret = await post(
                "/api/phonebook/" + phonebook.id + "/update",
            );
            window.alert("Phonebook updated!");
            if (
                typeof ret === "object" &&
                ret !== null &&
                typeof ret.change_id === "number"
            ) {
                add_undo(ret.change_id);
            }
        } catch (e) {
            window.alert("Error updating phonebook: " + e.message);
        }
    }

    const whoami = useServerState<{
        permissions: any;
        is_superadmin: boolean;
        uid: string;
    }>("/api/whoami", o => o as any) ?? {
        permissions: {},
        is_superadmin: false,
        uid: "",
    };

    let central_widget: React.ReactNode;
    switch (state) {
        case State.contacts:
            central_widget =
                phonebook !== null ? (
                    <ContactsEditor
                        phonebook={phonebook}
                        icon_list={icon_list}
                        search_text={search_text}
                        whoami={whoami}
                    />
                ) : null;
            break;
        case State.sd:
            central_widget = phonebook ? (
                <SpeedDialEditor phonebook={phonebook} icon_list={icon_list} />
            ) : null;
            break;
        case State.import:
            central_widget = phonebook ? (
                <ImportEditor phonebook={phonebook} />
            ) : null;
            break;
        case State.export:
            central_widget = phonebook ? (
                <ExportEditor phonebook={phonebook} />
            ) : null;
            break;
        case State.users:
            central_widget = <UsersEditor whoami={whoami} />;
            break;
        case State.phone:
            central_widget = <PhoneEditor api_call={api_call} />;
            break;
        case State.db:
            central_widget = (
                <DBWidget
                    api_call={api_call}
                    pid={phonebook ? phonebook.id : undefined}
                />
            );
            break;
        case State.history:
            central_widget = phonebook ? (
                <History
                    phonebook={phonebook}
                    show_patches={whoami.is_superadmin}
                />
            ) : null;
            break;
        case State.ng911:
            central_widget = (
                <ErrorBoundary>
                    <Editor />
                </ErrorBoundary>
            );
            break;
        default:
            throw new Error("unknown state");
    }

    // do Ctrl+Z and Ctrl+Y
    useEffect(() => {
        document.addEventListener("keydown", onKeyDown);
        function onKeyDown(e: KeyboardEvent) {
            if (e.ctrlKey && e.key === "z") {
                undo().catch(e =>
                    console.error("ERROR calling undo in App", e),
                );
                e.preventDefault();
                e.stopPropagation();
            } else if (e.ctrlKey && e.key === "y") {
                redo().catch(e =>
                    console.error("ERROR calling redo in App", e),
                );
                e.preventDefault();
                e.stopPropagation();
            }
        }

        return () => {
            document.removeEventListener("keydown", onKeyDown);
        };
    }, [undo, redo]);

    return (
        <AddUndo.Provider value={add_undo}>
            <div>
                <div style={{ fontSize: 14 }}>
                    {whoami.is_superadmin ? (
                        <Button
                            css={state === State.db ? current_button : null}
                            onClick={() => {
                                set_state(State.db);
                            }}
                            disabled={state === State.db}
                        >
                            DB Management
                        </Button>
                    ) : null}
                    {whoami.is_superadmin ? (
                        <Button
                            css={state === State.ng911 ? current_button : null}
                            onClick={() => {
                                set_state(State.ng911);
                            }}
                            disabled={state === State.ng911}
                        >
                            ng911.cfg
                        </Button>
                    ) : null}
                    {pdp_allowed({
                        user: whoami,
                        target: { type: "USER_LIST" },
                        action: { type: "READ" },
                    }) ? (
                        <Button
                            css={state === State.users ? current_button : null}
                            onClick={() => {
                                set_state(State.users);
                            }}
                            disabled={state === State.users}
                        >
                            User Management
                        </Button>
                    ) : null}
                    <Button
                        css={state === State.contacts ? current_button : null}
                        onClick={() => {
                            set_state(State.contacts);
                        }}
                        disabled={state === State.contacts}
                    >
                        Contacts
                    </Button>
                    <Button
                        css={state === State.sd ? current_button : null}
                        onClick={() => {
                            set_state(State.sd);
                        }}
                        disabled={state === State.sd}
                    >
                        Speed Dials
                    </Button>
                    {pdp_allowed({
                        user: whoami,
                        target: { type: "PHONEBOOK" },
                        action: { type: "IMPORT_EXPORT" },
                    }) ? (
                        <Fragment>
                            <Button
                                css={
                                    state === State.import
                                        ? current_button
                                        : null
                                }
                                onClick={() => {
                                    set_state(State.import);
                                }}
                                disabled={state === State.import}
                            >
                                Import
                            </Button>
                            <Button
                                css={
                                    state === State.export
                                        ? current_button
                                        : null
                                }
                                onClick={() => {
                                    set_state(State.export);
                                }}
                                disabled={state === State.export}
                            >
                                Export
                            </Button>
                        </Fragment>
                    ) : null}
                    {pdp_allowed({
                        user: whoami,
                        target: { type: "PHONEBOOK" },
                        action: { type: "READ_HISTORY" },
                    }) ? (
                        <Button
                            css={
                                state === State.history ? current_button : null
                            }
                            onClick={() => {
                                set_state(State.history);
                            }}
                            disabled={state === State.history}
                        >
                            History
                        </Button>
                    ) : null}
                    {state === State.contacts ||
                    state === State.sd ||
                    state === State.import ||
                    state === State.export ? (
                        <Button
                            css={topTabStyle}
                            onClick={onUpdate}
                            disabled={
                                !pdp_allowed({
                                    user: whoami,
                                    target: { type: "PHONEBOOK" },
                                    action: { type: "UPDATE" },
                                })
                            }
                        >
                            Update
                        </Button>
                    ) : null}
                    {state === State.contacts ? (
                        <input
                            type="search"
                            placeholder="Search..."
                            value={search_text}
                            css={css`
                                font-size: inherit;
                            `}
                            onChange={e => set_search_text(e.target.value)}
                        />
                    ) : null}
                    <span css={[topTabStyle, { color: "red" }]}>
                        {error_msg}
                    </span>
                    <button disabled={!can_undo} onClick={undo}>
                        Undo <UndoIcon css={INLINE_ICON_STYLE} />
                        {undoing ? "..." : ""}
                    </button>
                    <button disabled={!can_redo} onClick={redo}>
                        Redo <RedoIcon css={INLINE_ICON_STYLE} />
                        {redoing ? "..." : ""}
                    </button>
                    <button onClick={logout}>
                        Logout <AccountLogoutIcon css={INLINE_ICON_STYLE} />
                    </button>
                </div>
                {central_widget}
            </div>
        </AddUndo.Provider>
    );
}

class ErrorBoundary extends React.Component<
    Readonly<{ children: React.ReactNode }>,
    Readonly<{ hasError: boolean; error?: any }>
> {
    constructor(props: Readonly<{ children: React.ReactNode }>) {
        super(props);
        this.state = { hasError: false };
    }

    public static getDerivedStateFromError(error: any) {
        return { hasError: true, error };
    }

    public componentDidCatch(error: any, errorInfo: any) {
        console.log("componentDidCatch", error, errorInfo);
    }

    public render() {
        if (this.state.hasError) {
            return (
                <div
                    css={css`
                        color: red;
                    `}
                >
                    Error in application: {String(this.state.error)}
                    <br />
                    <code style={{ whiteSpace: "pre" }}>
                        {this.state.error instanceof Error
                            ? this.state.error.stack
                            : null}
                    </code>
                </div>
            );
        } else {
            return this.props.children;
        }
    }
}

ReactDOM.render(
    <ErrorBoundary>
        <App />
    </ErrorBoundary>,
    document.getElementById("root"),
);
