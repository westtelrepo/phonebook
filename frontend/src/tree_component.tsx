/* eslint-disable @typescript-eslint/no-non-null-assertion */

/** @jsx jsx */
import { css, jsx } from "@emotion/core";
import Immutable from "immutable";
import React from "react";

import {
    BACKGROUND_TREE_COLOR,
    HOVER_ITEM_COLOR,
    SELECTED_COLOR,
    UNSELECTED_ITEM_COLOR,
} from "./constants";

const ROW_HEIGHT = 20;

export interface TreeEntry {
    down: () => void;
    is_selected: boolean;
    key: string;
    select: () => void;
    text: React.ReactNode;
    trash: () => void;
    unselect: () => void;
    up: () => void;
}

export interface TreeGroup {
    add_item?: () => void;
    close: () => void;
    down: () => void;
    is_open: boolean;
    is_selected: boolean;
    items: Immutable.List<TreeEntry>;
    key: string;
    open: () => void;
    select: () => void;
    sort?: () => void;
    text: React.ReactNode;
    trash: () => void;
    unselect: () => void;
    up: () => void;
}

export function Tree(props: {
    readonly groups: Immutable.List<Readonly<TreeGroup>>;
    readonly new_group: React.ReactNode;
    readonly new_item: React.ReactNode;
    readonly onGroupAdd?: () => void;
}) {
    let groupList = props.groups.map(g => {
        const itemBlockStyle = {
            transition: "height 0.25s",
            overflow: "hidden",
            height: g.is_open
                ? ROW_HEIGHT * (g.items.size + (g.add_item ? 1 : 0))
                : 0,
        };
        const contents = (
            <div style={itemBlockStyle}>
                {g.items.map(item => (
                    <ItemRow
                        key={item.key}
                        onSelect={item.select}
                        onUnselect={item.unselect}
                        onUp={item.up}
                        onDown={item.down}
                        onTrash={item.trash}
                        selected={item.is_selected}
                    >
                        {item.text}
                    </ItemRow>
                ))}
                {g.add_item ? (
                    <ItemRow selected={false} onSelect={g.add_item}>
                        {props.new_item}
                    </ItemRow>
                ) : null}
            </div>
        );

        return (
            <div key={g.key}>
                <GroupRow
                    selected={g.is_selected}
                    open={g.is_open}
                    onOpen={g.open}
                    onClose={g.close}
                    onSelect={g.select}
                    onTrash={g.trash}
                    onUp={g.up}
                    onDown={g.down}
                    onSort={g.sort}
                    onUnselect={g.unselect}
                >
                    {g.text}
                </GroupRow>
                {contents}
            </div>
        );
    });

    if (props.onGroupAdd) {
        groupList = groupList.push(
            <GroupRow
                open={false}
                key="new"
                onSelect={props.onGroupAdd}
                onOpen={props.onGroupAdd}
                selected={false}
            >
                {props.new_group}
            </GroupRow>,
        );
    }

    const style = {
        backgroundColor: BACKGROUND_TREE_COLOR,
        height: "100%",
    };
    return <div style={style}>{groupList}</div>;
}

const buttonStyle = css({
    boxSizing: "border-box",
    height: 0.75 * ROW_HEIGHT,
    marginLeft: 0.1 * ROW_HEIGHT,
    marginRight: 0.1 * ROW_HEIGHT,
    padding: 0.05 * ROW_HEIGHT,
    verticalAlign: "middle",
    width: 0.75 * ROW_HEIGHT,
});

export function GroupRow(props: {
    readonly open: boolean;
    readonly children: React.ReactNode;
    readonly onClose?: () => void;
    readonly onDown?: () => void;
    readonly onOpen: () => void;
    readonly onSelect: () => void;
    readonly onSort?: () => void;
    readonly onTrash?: () => void;
    readonly onUnselect?: () => void;
    readonly onUp?: () => void;
    readonly selected: boolean;
}) {
    const style = css({
        backgroundColor: props.selected
            ? SELECTED_COLOR
            : BACKGROUND_TREE_COLOR,
        boxSizing: "border-box",
        height: ROW_HEIGHT,
        overflow: "hidden",
        padding: 2,
        transition: "background-color 0.25s ease-out",
    });

    // we make it so that clicking anywhere that don't stopPropagation() will
    // cause a select/unselect event.
    return (
        <div
            css={style}
            onClick={() => {
                if (props.selected) {
                    props.onUnselect!();
                } else {
                    props.onSelect();
                }
            }}
        >
            <img
                css={buttonStyle}
                src={
                    props.open ? "/static/minus-8x.png" : "/static/plus-8x.png"
                }
                onClick={e => {
                    e.stopPropagation();
                    if (props.open) {
                        props.onClose!();
                    } else {
                        props.onOpen();
                    }
                }}
            />
            {props.children}
            <span
                css={css`
                    float: right;
                `}
            >
                {props.onSort ? (
                    <img
                        css={buttonStyle}
                        src="/static/sort-ascending-8x.png"
                        onClick={e => {
                            e.stopPropagation();
                            props.onSort!();
                        }}
                    />
                ) : null}
                {props.onUp ? (
                    <img
                        css={buttonStyle}
                        src="/static/arrow-thick-top-8x.png"
                        onClick={e => {
                            e.stopPropagation();
                            props.onUp!();
                        }}
                    />
                ) : null}
                {props.onDown ? (
                    <img
                        css={buttonStyle}
                        src="/static/arrow-thick-bottom-8x.png"
                        onClick={e => {
                            e.stopPropagation();
                            props.onDown!();
                        }}
                    />
                ) : null}
                {props.onTrash ? (
                    <img
                        css={buttonStyle}
                        src="/static/trash-8x.png"
                        onClick={e => {
                            e.stopPropagation();
                            props.onTrash!();
                        }}
                    />
                ) : null}
            </span>
        </div>
    );
}

export function ItemRow(props: {
    readonly children: React.ReactNode;
    readonly onDown?: () => void;
    readonly onSelect: () => void;
    readonly onTrash?: () => void;
    readonly onUnselect?: () => void;
    readonly onUp?: () => void;
    readonly selected: boolean;
}) {
    const style = css({
        backgroundColor: props.selected
            ? SELECTED_COLOR
            : UNSELECTED_ITEM_COLOR,
        paddingBottom: "2px",
        paddingLeft: "40px",
        paddingRight: "2px",
        paddingTop: "2px",
        boxSizing: "border-box",
        height: ROW_HEIGHT,
        transition: "background-color 0.25s ease-out",
        "&:hover": {
            backgroundColor: props.selected ? undefined : HOVER_ITEM_COLOR,
        },
    });

    // we make it so that clicking anywhere that doesn't call stopPropagation() will
    // cause a select/unselect event.
    return (
        <div
            css={style}
            onClick={() => {
                if (props.selected) {
                    props.onUnselect!();
                } else {
                    props.onSelect();
                }
            }}
        >
            <span
                style={{
                    maxWidth: 280,
                    overflow: "hidden",
                    boxSizing: "border-box",
                    display: "inline-block",
                    maxHeight: ROW_HEIGHT,
                }}
            >
                {props.children}
            </span>
            <span style={{ float: "right" }}>
                {props.onUp ? (
                    <img
                        css={buttonStyle}
                        src="/static/arrow-thick-top-8x.png"
                        onClick={e => {
                            e.stopPropagation();
                            props.onUp!();
                        }}
                    />
                ) : null}
                {props.onDown ? (
                    <img
                        css={buttonStyle}
                        src="/static/arrow-thick-bottom-8x.png"
                        onClick={e => {
                            e.stopPropagation();
                            props.onDown!();
                        }}
                    />
                ) : null}
                {props.onTrash ? (
                    <img
                        css={buttonStyle}
                        src="/static/trash-8x.png"
                        onClick={e => {
                            e.stopPropagation();
                            props.onTrash!();
                        }}
                    />
                ) : null}
            </span>
        </div>
    );
}
