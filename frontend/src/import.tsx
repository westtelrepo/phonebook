/** @jsx jsx */
import { css, jsx, SerializedStyles } from "@emotion/core";
import {
    bulk_import_precheck,
    BulkImportReturn,
} from "@w/shared/dist/bulk_import_precheck";
import { IUpsertContact } from "@w/shared/dist/db_phonebook";
import * as Phonebook from "@w/shared/dist/phonebook";
import { formatPhoneNumber } from "@w/shared/dist/validation";
import Immutable from "immutable";
import React, { useContext, useState } from "react";

import { AddUndo, editorStyle } from "./constants";
import { post } from "./hooks";

const style = css`
    border: 2px solid;
    height: 600px;
    width: 1000px;
`;

const table_style = css`
    border-collapse: collapse;
    width: 1000px;

    th,
    td {
        border: 1px solid black;
        font-size: 14px;
    }
    thead,
    tbody {
        display: block;
        width: 100%;
        table-layout: fixed;
    }
    tbody {
        display: block;
        height: 380px;
        overflow-y: scroll;
    }

    th:nth-child(1),
    td:nth-child(1) {
        min-width: 40px;
        max-width: 40px;
    }

    th:nth-child(2),
    td:nth-child(2) {
        min-width: 250px;
        max-width: 250px;
    }

    th:nth-child(3),
    td:nth-child(3) {
        min-width: 250px;
        max-width: 250px;
    }

    th:nth-child(4),
    td:nth-child(4) {
        min-width: 100px;
        max-width: 100px;
    }

    th:nth-child(5),
    td:nth-child(5) {
        min-width: 60px;
        max-width: 60px;
    }

    th:nth-child(6),
    td:nth-child(6) {
        min-width: 60px;
        max-width: 60px;
    }

    th:nth-child(7),
    td:nth-child(7) {
        min-width: 60px;
        max-width: 60px;
    }

    th:nth-child(8),
    td:nth-child(8) {
        min-width: 80px;
        max-width: 80px;
    }

    th:nth-child(9),
    td:nth-child(9) {
        width: 80px;
    }
`;

const add_style = css`
    color: green;
`;
const change_style = css`
    color: blue;
`;
const error_style = css`
    color: red;
`;

const import_button_style = css`
    font-size: inherit;
    margin-top: 10px;
`;

export function ImportEditor(props: { phonebook: Phonebook.Phonebook }) {
    const [text, setText] = useState(null as string | null);
    const [importing, setImporting] = useState(false);
    const [import_error, set_import_error] = useState(null as string | null);

    const add_undo = useContext(AddUndo);

    const readFile = (event: React.ChangeEvent<HTMLInputElement>) => {
        // eslint-disable-next-line @typescript-eslint/no-non-null-assertion
        const file = event.target.files![0];
        const reader = new FileReader();
        reader.onload = _ => {
            if (typeof reader.result === "string") {
                setText(reader.result);
            } else {
                throw new Error("reader.result is not a string");
            }
        };
        reader.readAsText(file);
    };

    const onImport = async (
        contacts: Immutable.List<IUpsertContact>,
    ): Promise<void> => {
        const ret = await post(
            "/api/phonebook/" + props.phonebook.id + "/import",
            contacts,
        );
        if (
            typeof ret === "object" &&
            ret !== null &&
            typeof ret.change_id === "number"
        ) {
            add_undo(ret.change_id);
        }
    };

    const onImportFunc = async () => {
        if (typeof text !== "string") {
            throw new Error("text is not a string in onImportFunc");
        }
        setImporting(true);

        try {
            await onImport(
                bulk_import_precheck(props.phonebook, text).row_status.map(
                    e => e.contact,
                ),
            );

            window.alert("CSV Successfully imported!");
            setImporting(false);
            set_import_error(null);
        } catch (e) {
            setImporting(false);
            if (e instanceof Error) {
                set_import_error(e.message);
            } else {
                set_import_error("CSV file could not be imported");
            }
        }
    };

    let precheck: BulkImportReturn | undefined;
    let precheck_error: string | undefined;
    try {
        if (text !== null) {
            precheck = bulk_import_precheck(props.phonebook, text);
        }
    } catch (e) {
        console.log(e);
        precheck_error = "Could not parse CSV file";
    }

    let inside_the_box: JSX.Element | undefined;

    if (precheck) {
        let errors = 0;
        let changes = 0;
        let additions = 0;

        precheck.row_status.forEach(e => {
            if (e.contact_state === "error" || e.number_state === "error") {
                errors += 1;
            } else if (e.contact_state === "change") {
                changes += 1;
            } else if (e.contact_state === "add") {
                additions += 1;
            }
        });

        inside_the_box = (
            <div>
                <div>
                    {errors} contacts with errors, {changes} contacts with
                    changes, {additions} new contacts
                </div>
                <div
                    css={css`
                        color: red;
                        max-height: 100px;
                        overflow-y: auto;
                    `}
                >
                    {precheck.row_status.map(e => {
                        const ret: JSX.Element[] = [];
                        if (e.contact_state === "error") {
                            ret.push(
                                <div key={"contact_state_" + e.index}>
                                    #{e.index}: {e.contact_tooltip}
                                </div>,
                            );
                        }
                        if (e.number_state === "error") {
                            ret.push(
                                <div key={"number_state_" + e.index}>
                                    #{e.index}: {e.number_tooltip}
                                </div>,
                            );
                        }
                        return ret;
                    })}
                </div>
                <table css={table_style}>
                    <thead>
                        <tr>
                            <th>#</th>
                            <th>Contact Name</th>
                            <th>Group</th>
                            <th>Phone Number</th>
                            <th>9-1-1 Transfer</th>
                            <th>Blind Transfer</th>
                            <th>Attended Transfer</th>
                            <th>Conference Transfer</th>
                            <th>Outbound</th>
                        </tr>
                    </thead>
                    <tbody>
                        {precheck.row_status.map(e => {
                            let contact_style: SerializedStyles;
                            switch (e.contact_state) {
                                case "add":
                                    contact_style = add_style;
                                    break;
                                case "change":
                                    contact_style = change_style;
                                    break;
                                case "error":
                                    contact_style = error_style;
                                    break;
                                default:
                                    throw Error("contact_state has bad state");
                            }

                            return (
                                <tr key={e.index}>
                                    <td>{e.index}</td>
                                    <td
                                        css={contact_style}
                                        title={e.contact_tooltip}
                                    >
                                        {e.contact.name}
                                    </td>
                                    <td
                                        css={
                                            e.group_state === "change"
                                                ? change_style
                                                : add_style
                                        }
                                        title={e.group_tooltip}
                                    >
                                        {e.contact.gname}
                                    </td>
                                    <td
                                        css={
                                            e.number_state === "error"
                                                ? error_style
                                                : undefined
                                        }
                                        title={e.number_tooltip}
                                    >
                                        {formatPhoneNumber(e.contact.contact)}
                                    </td>
                                    <td>{e.contact.xfer_ng911 ? "Y" : "N"}</td>
                                    <td>{e.contact.xfer_blind ? "Y" : "N"}</td>
                                    <td>
                                        {e.contact.xfer_attended ? "Y" : "N"}
                                    </td>
                                    <td>
                                        {e.contact.xfer_conference ? "Y" : "N"}
                                    </td>
                                    <td>
                                        {e.contact.xfer_outbound ? "Y" : "N"}
                                    </td>
                                </tr>
                            );
                        })}
                    </tbody>
                </table>
                <button
                    css={import_button_style}
                    onClick={onImportFunc}
                    disabled={!precheck.allow_import || importing}
                >
                    {importing ? "Importing..." : "Import"}
                </button>
                <span
                    css={css`
                        color: red;
                    `}
                >
                    {import_error}
                </span>
            </div>
        );
    } else if (precheck_error !== undefined) {
        inside_the_box = <div>{precheck_error}</div>;
    }

    return (
        <div css={[style, editorStyle]}>
            <input
                type="file"
                accept=".csv,text/csv"
                onChange={readFile}
                css={css`
                    font-size: inherit;
                `}
            />
            {inside_the_box}
        </div>
    );
}
