import assert from "assert";
import canonify from "canonical-json";
import jsSHA from "jssha/dist/sha256";
import { useEffect, useRef, useState } from "react";
import { applyPatch } from "rfc6902";

function hash_str(text: string): string {
    const hasher = new jsSHA("SHA-256", "TEXT");
    hasher.update(text);
    return hasher.getHash("HEX");
}

export function useServerState<T>(
    uri: string | null,
    parse: (obj: unknown) => T,
): T | null {
    const [, rerender] = useState({});

    const state = useRef({
        uri,
        obj: null as unknown,
    });

    useEffect(() => {
        state.current = { uri, obj: null };
    }, [uri]);

    useEffect(() => {
        if (typeof uri !== "string") {
            return;
        }

        let cancel = false;

        const dispatch = (uri: string, obj: unknown) => {
            if (!cancel && uri === state.current.uri) {
                state.current.obj = obj;
                rerender({});
            }
        };

        void (async () => {
            while (!cancel) {
                try {
                    const previous =
                        uri === state.current.uri && state.current.obj
                            ? canonify(state.current.obj)
                            : null;

                    const url = new URL(uri, document.baseURI);
                    if (typeof previous === "string") {
                        url.searchParams.set("h", hash_str(previous));
                    }

                    const obj = await fetch(url.toString(), {
                        cache: "no-cache",
                        headers: {
                            Accept:
                                "application/json-patch+json, application/json",
                            "Cache-Control": "no-cache",
                        },
                    });
                    if (obj.ok) {
                        const o = await obj.json();

                        if (
                            obj.headers.get("content-type") ===
                            "application/json-patch+json"
                        ) {
                            assert(typeof previous === "string");
                            const clone = JSON.parse(previous);
                            applyPatch(clone, o);
                            dispatch(uri, clone);
                        } else {
                            dispatch(uri, o);
                        }

                        const short_poll = obj.headers.get("X-Short-Poll");
                        if (typeof short_poll === "string") {
                            await new Promise(resolve =>
                                setTimeout(resolve, Number(short_poll)),
                            );
                        }
                    } else if (obj.status === 403) {
                        // NOTE: reload window when unauthorized
                        window.location.reload();
                    } else {
                        console.error(
                            "error from server in useServerState",
                            obj.status,
                            obj.statusText,
                        );
                    }
                } catch (e) {
                    console.error("error in useServerState", e);
                    // wait 2 seconds on error
                    await new Promise(resolve => setTimeout(resolve, 2000));
                }

                await new Promise(resolve => setTimeout(resolve, 100));
            }
        })();

        return () => {
            cancel = true;
        };
    }, [uri]);

    return state.current.obj === null || state.current.uri !== uri
        ? null
        : parse(state.current.obj);
}
