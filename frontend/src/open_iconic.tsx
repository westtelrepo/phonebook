/** @jsx jsx */
import { css, InterpolationWithTheme, jsx } from "@emotion/core";

// icons are from https://useiconic.com/open/
// icons are undo MIT license

export const INLINE_ICON_STYLE = css`
    width: 1em;
    height: 1em;
    vertical-align: middle;
`;

export function UndoIcon(props: { css?: InterpolationWithTheme<any> }) {
    return (
        <svg
            xmlns="http://www.w3.org/2000/svg"
            width="8"
            height="8"
            viewBox="0 0 8 8"
            {...props}
        >
            <path
                d="M4.5 0c-1.93 0-3.5 1.57-3.5 3.5v.5h-1l2 2 2-2h-1v-.5c0-1.38 1.12-2.5 2.5-2.5s2.5 1.12 2.5 2.5c0-1.93-1.57-3.5-3.5-3.5z"
                transform="translate(0 1)"
            />
        </svg>
    );
}

export function RedoIcon(props: { css?: InterpolationWithTheme<any> }) {
    return (
        <svg
            xmlns="http://www.w3.org/2000/svg"
            width="8"
            height="8"
            viewBox="0 0 8 8"
            {...props}
        >
            <path
                d="M3.5 0c-1.93 0-3.5 1.57-3.5 3.5 0-1.38 1.12-2.5 2.5-2.5s2.5 1.12 2.5 2.5v.5h-1l2 2 2-2h-1v-.5c0-1.93-1.57-3.5-3.5-3.5z"
                transform="translate(0 1)"
            />
        </svg>
    );
}

export function AccountLogoutIcon(props: {
    css?: InterpolationWithTheme<any>;
}) {
    return (
        <svg
            xmlns="http://www.w3.org/2000/svg"
            width="8"
            height="8"
            viewBox="0 0 8 8"
            {...props}
        >
            <path d="M3 0v1h4v5h-4v1h5v-7h-5zm-1 2l-2 1.5 2 1.5v-1h4v-1h-4v-1z" />
        </svg>
    );
}
