/** @jsx jsx */
import { css, jsx } from "@emotion/core";
import { IHistoryEvent } from "@w/shared/dist/db_phonebook";
import { Operation, Phonebook } from "@w/shared/dist/phonebook";
import { desc } from "@w/shared/dist/phonebook_desc";
import React, { Fragment, useMemo } from "react";

import { post } from "./hooks";
import { useServerState } from "./useServerState";

const table_css = css`
    border-collapse: collapse;
    width: 100%;
    td,
    th {
        border: 1px solid black;
    }
    height: 500px;
    overflow-y: scroll;

    .patch {
        max-height: 200px;
        overflow-y: auto;
    }

    .desc {
        max-height: 200px;
        overflow-y: auto;
        white-space: pre;
    }
`;

export function History(props: {
    phonebook: Phonebook;
    show_patches: boolean;
}) {
    const history_obj: {
        [index: number]: IHistoryEvent;
    } | null = useServerState(
        "/api/phonebook/" + props.phonebook.id + "/history",
        x => x as any,
    );

    const history_list = useMemo(() => {
        if (history_obj === null) {
            return null;
        }

        const list: IHistoryEvent[] = [];

        for (const prop in history_obj) {
            if (Object.prototype.hasOwnProperty.call(history_obj, prop)) {
                list.push(history_obj[prop]);
            }
        }

        // sort in reverse change_id order
        return list.sort((a, b) => {
            if (a.change_id < b.change_id) {
                return 1;
            } else if (a.change_id > b.change_id) {
                return -1;
            } else {
                return 0;
            }
        });
    }, [history_obj]);

    async function undo(change_id: number) {
        await post("/api/phonebook/" + props.phonebook.id + "/undo", {
            change_id,
        });
    }

    async function restore(change_id: number) {
        await post("/api/phonebook/" + props.phonebook.id + "/restore_to", {
            change_id,
        });
    }

    let table: React.ReactNode[] = [];

    if (history_list) {
        table = history_list.map(e => {
            let undoable = true;
            try {
                if (e.inv_patch.length === 0) {
                    throw new Error("refuse zero length operation");
                }
                const op = new Operation({
                    patch: e.inv_patch,
                    inv_patch: e.patch,
                });
                const ph = props.phonebook.apply(op);
                ph.validate();
            } catch (_error) {
                // console.log(e);
                undoable = false;
            }

            return (
                <tr key={e.change_id}>
                    <td>{e.change_id}</td>
                    <td title={e.meta?.user?.uid}>
                        {e.meta?.user?.username ?? "SYSTEM"}
                    </td>
                    <td>{new Date(e.utc * 1000).toLocaleString()}</td>
                    <td>
                        <div className="desc">{desc(e, props.phonebook)}</div>
                    </td>
                    {props.show_patches ? (
                        <Fragment>
                            <td>
                                <pre className="patch">
                                    {JSON.stringify(e.patch, null, 2)}
                                </pre>
                            </td>
                            <td>
                                <pre className="patch">
                                    {JSON.stringify(e.inv_patch, null, 2)}
                                </pre>
                            </td>
                        </Fragment>
                    ) : null}
                    <td>
                        <button
                            onClick={undo.bind(null, e.change_id)}
                            disabled={!undoable}
                        >
                            Undo
                        </button>
                        <button onClick={restore.bind(null, e.change_id)}>
                            Restore
                        </button>
                    </td>
                </tr>
            );
        });
    }

    return (
        <div style={{ height: 750, overflowY: "auto", width: 1500 }}>
            <table css={table_css}>
                <tbody>{table}</tbody>
            </table>
        </div>
    );
}
