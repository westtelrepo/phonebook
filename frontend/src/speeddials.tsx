/* eslint-disable @typescript-eslint/no-non-null-assertion */
/** @jsx jsx */
import { jsx } from "@emotion/core";
import * as Phonebook from "@w/shared/dist/phonebook";
import {
    IPatchDirectory,
    IPatchSpeedDial,
    IPostDirectory,
    IPostSpeedDial,
} from "@w/shared/dist/phonebook_interfaces";
import { formatPhoneNumber } from "@w/shared/dist/validation";
import Immutable from "immutable";
import React, { useContext, useEffect, useReducer } from "react";
import { useMemoOne } from "use-memo-one";

import {
    AddUndo,
    DEBOUNCE_TIME,
    divStyle,
    editorStyle,
    inline_icon,
    inputStyle,
} from "./constants";
import { DualPane } from "./dualpane";
import { json_delete, post, useDebounce, usePatch } from "./hooks";
import { IconField } from "./iconfield";
import { Tree, TreeEntry, TreeGroup } from "./tree_component";

interface ISDEditorState {
    readonly open_groups: Immutable.Set<string>;
    readonly selected_group: string | null;
    readonly selected_number: string | null;
}

type SDEditorAction =
    | {
          op: "select_group";
          id: string;
      }
    | {
          op: "unselect_group";
      }
    | {
          op: "open_group";
          id: string;
      }
    | {
          op: "close_group";
          id: string;
      }
    | {
          op: "select_item";
          id: string;
      }
    | {
          op: "unselect_item";
      };

export function SpeedDialEditor(props: {
    readonly phonebook: Phonebook.Phonebook;
    readonly icon_list: string[];
}) {
    function reducer(
        reducer_state: ISDEditorState,
        action: SDEditorAction,
    ): ISDEditorState {
        switch (action.op) {
            case "select_group":
                return {
                    ...reducer_state,
                    selected_number: null,
                    selected_group: action.id,
                };
            case "unselect_group":
                return {
                    ...reducer_state,
                    selected_group: null,
                    selected_number: null,
                };
            case "open_group":
                return {
                    ...reducer_state,
                    open_groups: reducer_state.open_groups.add(action.id),
                };
            case "close_group":
                return {
                    ...reducer_state,
                    open_groups: reducer_state.open_groups.delete(action.id),
                };
            case "select_item":
                return {
                    ...reducer_state,
                    selected_group: null,
                    selected_number: action.id,
                };
            case "unselect_item":
                return {
                    ...reducer_state,
                    selected_group: null,
                    selected_number: null,
                };
        }
    }

    const add_undo = useContext(AddUndo);

    const [state, dispatch] = useReducer(reducer, {
        open_groups: Immutable.Set(),
        selected_group: null,
        selected_number: null,
    });

    function groupSelect(id: string) {
        dispatch({ op: "select_group", id });
    }

    function groupOpen(id: string) {
        dispatch({ op: "open_group", id });
    }

    function itemSelect(id: string) {
        dispatch({ op: "select_item", id });
    }

    async function addSDGroup() {
        try {
            let name = "New Group";
            if (props.phonebook.sd_dirs.find(e => e.name === name)) {
                for (let i = 1; i < 100; i++) {
                    name = "New Group " + i;
                    if (!props.phonebook.sd_dirs.find(e => e.name === name)) {
                        break;
                    }
                }
            }
            const post_directory: IPostDirectory = {
                name,
            };
            const ret: {
                id: string;
                change_id?: number;
            } = await post(
                "/api/phonebook/" + phonebook.id + "/sd_dirs",
                post_directory,
            );
            if (typeof ret.change_id === "number") {
                add_undo(ret.change_id);
            }
            groupSelect(ret.id);
            groupOpen(ret.id);
        } catch (e) {
            if (e instanceof Error) {
                window.alert("Error adding directory: " + e.message);
            } else {
                throw e;
            }
        }
    }

    async function onSDGroupDelete(id: string) {
        try {
            if (!phonebook.sd_children_of(id).isEmpty()) {
                window.alert("Can not delete directory with speed dials!");
                return;
            }
            const ret = await json_delete(
                "/api/phonebook/" + phonebook.id + "/sd_dirs/" + id,
            );
            if (
                typeof ret === "object" &&
                ret !== null &&
                typeof ret.change_id === "number"
            ) {
                add_undo(ret.change_id);
            }
        } catch (e) {
            if (e instanceof Error) {
                window.alert("Error deleting directory: " + e.message);
            } else {
                throw e;
            }
        }
    }

    async function groupUp(id: string) {
        try {
            const ret = await post(
                "/api/phonebook/" + phonebook.id + "/sd_dirs/" + id + "/moveup",
            );
            if (
                typeof ret === "object" &&
                ret !== null &&
                typeof ret.change_id === "number"
            ) {
                add_undo(ret.change_id);
            }
        } catch (e) {
            if (e instanceof Error) {
                window.alert("Error moving directory: " + e.message);
            } else {
                throw e;
            }
        }
    }

    async function groupDown(id: string) {
        try {
            const ret = await post(
                "/api/phonebook/" +
                    phonebook.id +
                    "/sd_dirs/" +
                    id +
                    "/movedown",
            );
            if (
                typeof ret === "object" &&
                ret !== null &&
                typeof ret.change_id === "number"
            ) {
                add_undo(ret.change_id);
            }
        } catch (e) {
            if (e instanceof Error) {
                window.alert("Error moving directory: " + e.message);
            } else {
                throw e;
            }
        }
    }

    async function sdUp(id: string) {
        try {
            const ret = await post(
                "/api/phonebook/" +
                    phonebook.id +
                    "/speed_dials/" +
                    id +
                    "/moveup",
            );
            if (
                typeof ret === "object" &&
                ret !== null &&
                typeof ret.change_id === "number"
            ) {
                add_undo(ret.change_id);
            }
        } catch (e) {
            if (e instanceof Error) {
                window.alert("Error moving speed dial: " + e.message);
            } else {
                throw e;
            }
        }
    }

    async function sdDown(id: string) {
        try {
            const ret = await post(
                "/api/phonebook/" +
                    phonebook.id +
                    "/speed_dials/" +
                    id +
                    "/movedown",
            );
            if (
                typeof ret === "object" &&
                ret !== null &&
                typeof ret.change_id === "number"
            ) {
                add_undo(ret.change_id);
            }
        } catch (e) {
            if (e instanceof Error) {
                window.alert("Error moving speed dial: " + e.message);
            } else {
                throw e;
            }
        }
    }

    async function onSDAdd(gid: string) {
        try {
            const to_post: IPostSpeedDial = {
                parent: gid,
                contact_id: props.phonebook.contacts.findKey(() => true)!,
            };
            const ret = await post(
                "/api/phonebook/" + phonebook.id + "/speed_dials",
                to_post,
            );
            itemSelect(ret.id);
            if (typeof ret.change_id === "number") {
                add_undo(ret.change_id);
            }
        } catch (e) {
            if (e instanceof Error) {
                window.alert("Error adding speed dial: " + e.message);
            } else {
                throw e;
            }
        }
    }

    async function onSDDelete(id: string) {
        try {
            const ret = await json_delete(
                "/api/phonebook/" + phonebook.id + "/speed_dials/" + id,
            );
            if (
                typeof ret === "object" &&
                ret !== null &&
                typeof ret.change_id === "number"
            ) {
                add_undo(ret.change_id);
            }
        } catch (e) {
            if (e instanceof Error) {
                window.alert("Error deleting speed dial: " + e.message);
            } else {
                throw e;
            }
        }
    }

    const { phonebook } = props;

    const groups: Immutable.List<TreeGroup> = props.phonebook
        .sd_children_of_only_dirs(undefined)
        .map(
            (g): TreeGroup => ({
                key: g[0],
                text: g[1].name,
                items: props.phonebook.sd_children_of_only_sds(g[0]).map(
                    (e): TreeEntry => ({
                        text: (
                            <span>
                                {inline_icon(
                                    props.phonebook.id,
                                    e[1].icon ??
                                        props.phonebook.contacts.get(
                                            e[1].contact_id,
                                        )!.icon,
                                )}{" "}
                                {phonebook.contacts.get(e[1].contact_id)!.name}
                            </span>
                        ),
                        key: e[0],
                        trash: onSDDelete.bind(null, e[0]),
                        up: sdUp.bind(null, e[0]),
                        down: sdDown.bind(null, e[0]),
                        select: itemSelect.bind(null, e[0]),
                        unselect: () => dispatch({ op: "unselect_item" }),
                        is_selected: state.selected_number === e[0],
                    }),
                ),
                is_open: state.open_groups.has(g[0]),
                is_selected: g[0] === state.selected_group,
                select: groupSelect.bind(null, g[0]),
                unselect: () => dispatch({ op: "unselect_group" }),
                open: groupOpen.bind(null, g[0]),
                close: () => dispatch({ op: "close_group", id: g[0] }),
                trash: onSDGroupDelete.bind(null, g[0]),
                up: groupUp.bind(null, g[0]),
                down: groupDown.bind(null, g[0]),
                add_item: onSDAdd.bind(null, g[0]),
            }),
        );

    let editor: React.ReactNode;
    if (state.selected_number !== null) {
        editor = (
            <SDEditor
                phonebook={phonebook}
                icon_list={props.icon_list}
                sid={state.selected_number}
            />
        );
    } else if (state.selected_group !== null) {
        editor = (
            <SDGroupEditor
                gid={state.selected_group}
                phonebook={props.phonebook}
            />
        );
    }

    return (
        <DualPane>
            <Tree
                groups={groups}
                new_item={<i>New Item</i>}
                new_group={<i>New Group</i>}
                onGroupAdd={addSDGroup}
            />
            {editor}
        </DualPane>
    );
}

interface ISDGroupEditorState {
    pid: string;
    gid: string;
    to_send: IPatchDirectory | null;
}

type ISDGroupEditorAction =
    | {
          op: "set_gid";
          pid: string;
          gid: string;
      }
    | {
          op: "update_name";
          name: string;
      };

function group_reducer(
    state: ISDGroupEditorState,
    action: ISDGroupEditorAction,
): ISDGroupEditorState {
    switch (action.op) {
        case "set_gid":
            return { pid: action.pid, gid: action.gid, to_send: null };
        case "update_name":
            return {
                ...state,
                to_send: { ...state.to_send, name: action.name },
            };
    }
}

export function SDGroupEditor(props: {
    phonebook: Phonebook.Phonebook;
    gid: string;
}) {
    const [state, dispatch] = useReducer(group_reducer, {
        pid: props.phonebook.id,
        gid: props.gid,
        to_send: null,
    });

    useEffect(() => {
        dispatch({ op: "set_gid", gid: props.gid, pid: props.phonebook.id });
    }, [props.phonebook.id, props.gid]);

    const debounced = useDebounce(state, DEBOUNCE_TIME);

    const to_send = useMemoOne(() => {
        if (debounced.to_send === null) {
            return null;
        }

        return {
            ...debounced.to_send,
            name:
                typeof debounced.to_send.name === "string"
                    ? debounced.to_send.name.trim()
                    : undefined,
        };
    }, [debounced.to_send]);

    usePatch(
        "/api/phonebook/" + debounced.pid + "/sd_dirs/" + debounced.gid,
        to_send,
    );

    const current = props.phonebook.sd_dirs.get(props.gid);

    if (!current) {
        return null;
    }

    const name = [(state.to_send ?? {}).name, current.name].find(
        x => typeof x === "string",
    );

    const name_error =
        state.to_send?.name !== undefined &&
        props.phonebook
            .sd_children_of_only_dirs(undefined)
            .some(
                x => x[0] !== props.gid && x[1].name === state.to_send!.name,
            ) ? (
            <span style={{ color: "red" }}>Duplicate Name</span>
        ) : null;

    return (
        <div css={editorStyle}>
            <div css={divStyle}>
                <label>
                    Name {name_error}
                    <br />
                    <input
                        css={inputStyle}
                        type="text"
                        value={name}
                        onChange={e =>
                            dispatch({
                                op: "update_name",
                                name: e.target.value,
                            })
                        }
                    />
                </label>
            </div>
        </div>
    );
}

interface IEditorState {
    readonly pid: string;
    readonly sid: string;
    readonly to_send: IPatchSpeedDial | null;
}

type IEditorAction =
    | {
          op: "set_sid";
          pid: string;
          sid: string;
      }
    | {
          op: "update_icon";
          icon: string | null;
      }
    | {
          op: "update_action";
          action: Phonebook.sd_action;
      }
    | {
          op: "update_parent";
          parent: string | null;
      }
    | {
          op: "update_contact_id";
          contact_id: string;
      };

function editor_reducer(
    state: IEditorState,
    action: IEditorAction,
): IEditorState {
    switch (action.op) {
        case "set_sid":
            return { pid: action.pid, sid: action.sid, to_send: null };
        case "update_icon":
            return {
                ...state,
                to_send: { ...state.to_send, icon: action.icon },
            };
        case "update_action":
            return {
                ...state,
                to_send: { ...state.to_send, action: action.action },
            };
        case "update_parent":
            return {
                ...state,
                to_send: { ...state.to_send, parent: action.parent },
            };
        case "update_contact_id":
            return {
                ...state,
                to_send: { ...state.to_send, contact_id: action.contact_id },
            };
    }
}

export function SDEditor(props: {
    readonly phonebook: Phonebook.Phonebook;
    readonly icon_list: string[];
    readonly sid: string;
}) {
    const { phonebook } = props;

    const [state, dispatch] = useReducer(editor_reducer, {
        pid: props.phonebook.id,
        sid: props.sid,
        to_send: null,
    });

    const debounced = useDebounce(state, DEBOUNCE_TIME);

    useEffect(() => {
        dispatch({ op: "set_sid", sid: props.sid, pid: props.phonebook.id });
    }, [props.phonebook.id, props.sid]);

    usePatch(
        "/api/phonebook/" + debounced.pid + "/speed_dials/" + debounced.sid,
        debounced.to_send,
    );

    const current = phonebook.speed_dials.get(props.sid);

    if (!current) {
        return null;
    }

    const compare = (x: Phonebook.Contact, y: Phonebook.Contact) => {
        const cmp = props.phonebook.dirs
            .get(x.parent!)!
            .ord.cmp(props.phonebook.dirs.get(y.parent!)!.ord);
        if (cmp === 0) {
            return x.ord.cmp(y.ord);
        } else {
            return cmp;
        }
    };

    const num = phonebook.contacts.get(current.contact_id)!;

    const contact_id = [
        (state.to_send ?? {}).contact_id,
        current.contact_id,
    ].find(x => typeof x === "string");
    const parent =
        [(state.to_send ?? {}).parent, current.parent].find(
            x => x !== undefined,
        ) ?? "unknown"; // TODO: undefined is root directory
    const action = (state.to_send ?? {}).action ?? current.action;
    const icon =
        [(state.to_send ?? {}).icon, current.icon].find(
            x => typeof x === "string" || x === null,
        ) ?? null;

    return (
        <div css={editorStyle}>
            <div css={divStyle}>
                <label>
                    Original Contact
                    <br />
                    <select
                        css={inputStyle}
                        value={contact_id}
                        onChange={e =>
                            dispatch({
                                op: "update_contact_id",
                                contact_id: e.target.value,
                            })
                        }
                    >
                        {props.phonebook.contacts
                            .sort(compare)
                            .map((c, id) => (
                                <option key={id} value={id}>
                                    {props.phonebook.dirs.get(c.parent!)!.name}{" "}
                                    → {c.name}
                                </option>
                            ))
                            .toList()}
                    </select>
                </label>
            </div>

            <div css={divStyle}>
                Phone number: {formatPhoneNumber(num.contact)}
            </div>

            <div css={divStyle}>
                <label>
                    Group
                    <br />
                    <select
                        css={inputStyle}
                        value={parent}
                        onChange={e =>
                            dispatch({
                                op: "update_parent",
                                parent: e.target.value,
                            })
                        }
                    >
                        {phonebook.sd_dirs
                            .sortBy(
                                v => v.ord,
                                (x, y) => x.cmp(y),
                            )
                            .map((g, id) => (
                                <option key={id} value={id}>
                                    {g.name}
                                </option>
                            ))
                            .toList()}
                    </select>
                </label>
            </div>

            <div css={divStyle}>
                <label>
                    Action
                    <br />
                    <select
                        css={inputStyle}
                        value={action}
                        onChange={e =>
                            dispatch({
                                op: "update_action",
                                action: e.target.value as Phonebook.sd_action,
                            })
                        }
                    >
                        <option value="dial_out">Dial Out</option>
                        <option value="blind_transfer">Blind Transfer</option>
                        <option value="attended_transfer">
                            Attended Transfer
                        </option>
                        <option value="conference_transfer">
                            Conference Transfer
                        </option>
                        <option value="tandem_transfer">Tandem Transfer</option>
                    </select>
                </label>
            </div>

            <IconField
                value={icon}
                icon_list={props.icon_list}
                pid={phonebook.id}
                onChange={e => dispatch({ op: "update_icon", icon: e })}
            />
            <br />
        </div>
    );
}
