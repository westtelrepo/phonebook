/** @jsx jsx */
import { css, jsx } from "@emotion/core";
import { Stats } from "fs";
import React, { Fragment, useState } from "react";

import { post, post_xml } from "./hooks";
import { useServerState } from "./useServerState";

interface StatusError {
    error: string;
}

interface Peer {
    hostname: string;
    listen_id: string;
    version: string;
    received_utc: string;
}

interface StatusReal {
    db: {
        desired_schema: string;
        current_schema: string;
        migs:
            | false
            | { name: string; description: string; state_uuid: string }[];
    };
    version: string;
    peers: Peer[];
    hostname: string;
    listen_id: string;
    datadir_stat: null | Stats;
    datadir: string;
    user: {
        uid: number;
        gid: number;
    };
}

type Status = StatusReal | StatusError | Record<string, unknown>;

function isReal(status: Status): status is StatusReal {
    return (status as StatusReal).db !== undefined;
}

function isError(status: Status): status is StatusError {
    return (status as StatusError).error !== undefined;
}

export function DBWidget(props: {
    api_call: (obj: any) => Promise<any>;
    pid?: string;
}) {
    const [password, setPassword] = useState("");

    const server_status = useServerState(
        "/api/db_status",
        (obj): Status => obj as Status,
    );
    const status = server_status === null ? {} : server_status;
    const [text, setText] = useState(null as string | null);
    const [backup, setBackup] = useState<string | null>(null);
    const [restoring, setRestoring] = useState(false);

    const readFile = (event: React.ChangeEvent<HTMLInputElement>) => {
        // eslint-disable-next-line @typescript-eslint/no-non-null-assertion
        const file = event.target.files![0];
        const reader = new FileReader();
        reader.onload = _ => {
            if (typeof reader.result === "string") {
                setText(reader.result);
            } else {
                throw new Error("reader.result is not a string");
            }
        };
        reader.readAsText(file);
    };

    const readBackup = (event: React.ChangeEvent<HTMLInputElement>) => {
        // eslint-disable-next-line @typescript-eslint/no-non-null-assertion
        const file = event.target.files![0];
        const reader = new FileReader();
        reader.onload = _ => {
            if (typeof reader.result === "string") {
                setBackup(reader.result);
            } else {
                throw new Error("reader.result is not a string");
            }
        };
        reader.readAsText(file);
    };

    async function importFile(disk: boolean) {
        try {
            if (props.pid === undefined) {
                return;
            }
            const to_type = "replace the contacts";
            const prompt = window.prompt(
                `This operation will replace the contacts that appear in the phonebook.

    Type "${to_type}" to continue`,
                "",
            );

            if (prompt !== to_type) {
                return;
            }

            if (disk) {
                await post("/api/phonebook/" + props.pid + "/xml_import_disk");
            } else {
                if (typeof text !== "string") {
                    throw new Error("programmer error, text is not a string");
                }
                await post_xml(
                    "/api/phonebook/" + props.pid + "/xml_import",
                    text,
                );
            }
            window.alert("Successful import!");
            setText(null);
        } catch (e) {
            window.alert("Failed to import: " + e.message);
        }
    }

    async function restoreDB() {
        if (backup === null) {
            return;
        }

        const to_type = "replace all database data";
        const prompt = window.prompt(
            `This operation will replace ALL DATA in the Configurator database.

Type "${to_type}" to continue`,
            "",
        );

        if (prompt !== to_type) {
            return;
        }

        try {
            setRestoring(true);
            await post("/api/restore", JSON.parse(backup));

            window.alert("Successful restore!");
            window.location.reload();
        } catch (e) {
            console.log(e);
            if (e instanceof Error) {
                window.alert(e.message);
            }
        } finally {
            setRestoring(false);
        }
    }

    async function migrate() {
        await props.api_call({ code: "update_schema", password });
        window.alert("Migration successful!");
        // this is temporary, much of the UI assumes the database was already set
        document.location.reload(true);
    }

    const table_css = css`
        border-collapse: collapse;

        th,
        td {
            border: 1px solid black;
            font-size: 14px;
            padding: 3px;
        }
        thead {
            font-weight: bold;
        }
    `;

    const migration_required =
        isReal(status) && status.db.desired_schema !== status.db.current_schema;

    const can_do =
        isReal(status) && status.peers.every(e => e.version === status.version);

    let migration_widget: React.ReactNode;

    if (migration_required && !can_do) {
        migration_widget = (
            <div style={{ color: "red" }}>
                A migration is necessary but one or more instances need to be
                updated, so a database migration is not permissible. Please
                upgrade all phonebook instances before upgrading the database.
                <br />
                If you just did, you may need to wait 60 seconds for earlier
                instances to timeout.
                <br />
                <br />
            </div>
        );
    } else if (migration_required && isReal(status)) {
        migration_widget = (
            <div style={{ border: "2px solid black", width: 800, padding: 5 }}>
                {status.db.migs === false ? (
                    <span style={{ color: "red" }}>
                        A database migration is required but is impossible to
                        achieve
                    </span>
                ) : (
                    <Fragment>
                        A database migration is required: <br />
                        Insert Password:{" "}
                        <input
                            type="password"
                            value={password}
                            onChange={e => setPassword(e.target.value)}
                        />
                        <br />
                        <button
                            css={css`
                                font-size: inherit;
                            `}
                            onClick={migrate}
                        >
                            Migrate
                        </button>
                        <br />
                        <br />
                        <table css={table_css}>
                            <thead>
                                <tr>
                                    <td>Name</td>
                                    <td>Migration Description</td>
                                    <td>State UUID</td>
                                </tr>
                            </thead>
                            <tbody>
                                {status.db.migs.map((e, i) => (
                                    <tr key={i}>
                                        <td>{e.name}</td>
                                        <td>{e.description}</td>
                                        <td>
                                            <code>{e.state_uuid}</code>
                                        </td>
                                    </tr>
                                ))}
                            </tbody>
                        </table>
                    </Fragment>
                )}
            </div>
        );
    }

    let directory_error: React.ReactNode;

    if (isReal(status) && status.datadir_stat === null) {
        directory_error = (
            <div style={{ color: "red" }}>
                The directory <code>{status.datadir}</code> does not exist and
                needs to be created. Please run the following programs to
                create:
                <br />
                <code>sudo mkdir {status.datadir}</code>
                <br />
                <code>
                    sudo chown {status.user.uid}:{status.user.gid}{" "}
                    {status.datadir}
                </code>
                <br />
            </div>
        );
    } else if (
        isReal(status) &&
        status.datadir_stat !== null &&
        (status.datadir_stat.uid !== status.user.uid ||
            status.datadir_stat.gid !== status.user.gid)
    ) {
        directory_error = (
            <div style={{ color: "red" }}>
                The directory <code>{status.datadir}</code> does not have the
                correct owner. Please run the following to rectify:
                <br />
                <code>
                    sudo chown {status.user.uid}:{status.user.gid}{" "}
                    {status.datadir}
                </code>
                <br />
            </div>
        );
    }

    return (
        <div
            css={css`
                font-size: 14px;
            `}
        >
            {isError(status) ? (
                <div style={{ color: "red" }}>{status.error}</div>
            ) : null}
            {migration_widget}
            {directory_error}
            <div>
                Instance Hostname:{" "}
                <code>{isReal(status) ? status.hostname : "unknown"}</code>{" "}
                (Listen ID:{" "}
                <code>{isReal(status) ? status.listen_id : "unknown"}</code>)
            </div>
            <div>
                Instance Version:{" "}
                <code>{isReal(status) ? status.version : "unknown"}</code>
            </div>
            <div>
                {"Schema Version: "}
                <code>
                    {isReal(status) ? status.db.current_schema : "Unknown"}
                </code>
                {" (Desired: "}{" "}
                <code>
                    {isReal(status) ? status.db.desired_schema : "Unknown"}
                </code>
                )
            </div>
            <table css={table_css}>
                <thead>
                    <tr>
                        <td>Hostname</td>
                        <td>Version</td>
                        <td>Last UTC heard</td>
                        <td>Listen ID</td>
                    </tr>
                </thead>
                <tbody>
                    {isReal(status)
                        ? status.peers.map(e => (
                              <tr
                                  key={e.listen_id}
                                  style={{
                                      color:
                                          e.version !== status.version
                                              ? "red"
                                              : undefined,
                                  }}
                              >
                                  <td>
                                      <code>{e.hostname}</code>
                                  </td>
                                  <td>{e.version}</td>
                                  <td>{e.received_utc}</td>
                                  <td>
                                      <code>{e.listen_id}</code>
                                  </td>
                              </tr>
                          ))
                        : null}
                </tbody>
            </table>
            <br />
            <br />

            {typeof props.pid === "string" ? (
                <div style={{ border: "2px solid black", width: 750 }}>
                    <h1>Import XML</h1>
                    <div>
                        Warning: this replaces all contact data in the database
                    </div>
                    <input
                        type="file"
                        accept=".xml,text/xml"
                        onChange={readFile}
                        css={css`
                            font-size: inherit;
                        `}
                    />
                    <button
                        disabled={typeof text !== "string"}
                        onClick={importFile.bind(null, false)}
                    >
                        Import
                    </button>
                    <button onClick={importFile.bind(null, true)}>
                        Import from Disk
                    </button>
                </div>
            ) : null}

            <div style={{ border: "2px solid black", width: 750 }}>
                <h1>Restore DB from Backup</h1>
                <div>
                    Warning: this replaces ALL DATA in the database (contacts,
                    users, etc)
                </div>
                <input
                    type="file"
                    accept=".json,application/json"
                    onChange={readBackup}
                    css={css`
                        font-size: inherit;
                    `}
                />
                <button
                    disabled={typeof backup !== "string" || restoring}
                    onClick={restoreDB}
                >
                    {restoring ? "Restoring..." : "Restore"}
                </button>
            </div>
            <pre style={{ width: 800, height: 600, overflow: "auto" }}>
                {JSON.stringify(status, null, 2)}
            </pre>
        </div>
    );
}
