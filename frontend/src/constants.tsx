import { css } from "@emotion/core";
import React from "react";

export const INPUT_WIDTH = 300;
export const SELECTED_COLOR = "#2196F3"; // Blue 500
export const BACKGROUND_TREE_COLOR = "#BBDEFB"; // Blue 100
export const HOVER_ITEM_COLOR = "#BBDEFB"; // Blue 100
export const UNSELECTED_ITEM_COLOR = "#E3F2FD"; // Blue 50

export function icon_url(pid: string, icon: string): string {
    return (
        "/icon?pid=" +
        encodeURIComponent(pid) +
        "&icon=" +
        encodeURIComponent(icon)
    );
}

export function inline_icon(
    pid: string,
    icon: string | undefined,
): JSX.Element {
    const style = {
        display: "inline-block",
        height: "0.9em",
        verticalAlign: "-0.1em",
        width: "0.9em",
    };

    if (icon !== undefined) {
        return <img src={icon_url(pid, icon)} style={style} />;
    } else {
        return <span style={style}></span>;
    }
}

export const editorStyle = css({
    padding: "4px",
    fontSize: "16px",
});

export const inputStyle = css({
    width: INPUT_WIDTH,
    boxSizing: "border-box",
    fontSize: "inherit",
});

export const divStyle = css({
    marginBottom: "5px",
});

export const AddUndo = React.createContext<(obj: number) => void>(
    _ => undefined,
);

export const DEBOUNCE_TIME: number = 300;
