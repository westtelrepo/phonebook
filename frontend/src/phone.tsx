/* eslint-disable @typescript-eslint/no-non-null-assertion, @typescript-eslint/explicit-member-accessibility, @typescript-eslint/no-unsafe-call */

/** @jsx jsx */
import { jsx } from "@emotion/core";
import { css } from "@emotion/core";
import { PolycomTemplate } from "@w/shared/dist/phone_config";
import React, { Fragment } from "react";

import {
    divStyle,
    editorStyle,
    HOVER_ITEM_COLOR,
    INPUT_WIDTH,
    inputStyle,
    SELECTED_COLOR,
    UNSELECTED_ITEM_COLOR,
} from "./constants";
import { DualPane } from "./dualpane";

export class PhoneEditor extends React.Component<
    { api_call: any },
    {
        allowed: any;
        editor_state: any;
        location: any;
        page: number;
        selected_template: any;
        template: any;
        template_allowed: any;
        template_edit: any;
        template_list: any[];
        editor_allowed?: any;
    }
> {
    constructor(props: { api_call: any }) {
        super(props);

        this.state = {
            allowed: {},
            editor_state: null,
            location: null,
            page: 0,
            selected_template: "nothing",
            template: null,
            template_allowed: {},
            template_edit: {},
            template_list: [],
        };
    }

    refreshList() {
        this.props.api_call({ code: "get_polycom_list" }).then((data: any) => {
            this.setState({ template_list: data.polycom_list });
        });

        this.props.api_call({ code: "pdp_polycom_list" }).then((data: any) => {
            this.setState({ allowed: data.allowed });
        });
    }

    componentDidMount() {
        this.refreshList();
    }

    render() {
        const {
            allowed,
            editor_state,
            location,
            page,
            selected_template,
            template,
            template_allowed,
            template_edit,
            template_list,
        } = this.state;

        const tbody = template
            ? [0, 1, 2, 3, 4, 5, 6, 7, 8, 9, 10, 11, 12, 13].map(e => {
                  const represent = (linekey: any) => {
                      if (linekey === null) {
                          return { str: "Unassigned" };
                      } else if (linekey.category === "SpeedDial") {
                          return {
                              str: "SD " + linekey.lb,
                              tooltip: linekey.ct,
                          };
                      } else if (linekey.category === "BLF") {
                          return { str: "BLF" };
                      } else if (linekey.category === "Line") {
                          return { str: "Line " + linekey.explicit_index };
                      }
                  };

                  const style = (selected: boolean) =>
                      css({
                          backgroundColor: selected
                              ? SELECTED_COLOR
                              : UNSELECTED_ITEM_COLOR,
                          transition: "background-color 0.25s ease-out",
                          "&:hover": {
                              backgroundColor: selected
                                  ? undefined
                                  : HOVER_ITEM_COLOR,
                          },
                      });

                  let left_selected = false;
                  let right_selected = false;

                  if (
                      location &&
                      location.page === page &&
                      location.entry === e
                  ) {
                      // left side is selected
                      if (location.column === 0) {
                          left_selected = true;
                      }
                      // right side is selected
                      else if (location.column === 1) {
                          right_selected = true;
                      }
                  }

                  const left = represent(
                      template.getLineKeySideCar({ page, column: 0, entry: e }),
                  )!;
                  const right = represent(
                      template.getLineKeySideCar({ page, column: 1, entry: e }),
                  )!;
                  return (
                      <tr key={e}>
                          <td
                              onClick={this.clickLocation.bind(this, {
                                  page,
                                  column: 0,
                                  entry: e,
                              })}
                              css={style(left_selected)}
                              title={left.tooltip}
                          >
                              {left.str}
                          </td>
                          <td
                              onClick={this.clickLocation.bind(this, {
                                  page,
                                  column: 1,
                                  entry: e,
                              })}
                              css={[
                                  style(right_selected),
                                  css`
                                      text-align: right;
                                  `,
                              ]}
                              title={right.tooltip}
                          >
                              {right.str}
                          </td>
                      </tr>
                  );
              })
            : null;

        const buttonStyle = css`
            font-size: inherit;
        `;

        return (
            <DualPane>
                <div style={{ padding: 5 }}>
                    <div css={divStyle}>
                        <label>
                            Template
                            <br />
                            <select
                                css={inputStyle}
                                value={selected_template}
                                onChange={e => this.setTemplate(e.target.value)}
                            >
                                <option key="nothing" value="nothing">
                                    No Template
                                </option>
                                {template_list.map(e => (
                                    <option key={e.id} value={e.id}>
                                        {e.name}
                                    </option>
                                ))}
                            </select>
                        </label>
                    </div>
                    <div css={divStyle}>
                        <button
                            disabled={!allowed.create}
                            css={buttonStyle}
                            onClick={this.addTemplate.bind(this)}
                        >
                            Add Template
                        </button>
                        {selected_template !== "nothing" ? (
                            <Fragment>
                                <button
                                    disabled={!template_allowed.delete}
                                    css={buttonStyle}
                                    onClick={this.deleteTemplate.bind(
                                        this,
                                        selected_template,
                                    )}
                                >
                                    Delete Template
                                </button>
                                <button
                                    disabled={
                                        !template_allowed.update ||
                                        !(
                                            template &&
                                            typeof template.tftp_directory ===
                                                "string"
                                        )
                                    }
                                    css={buttonStyle}
                                    onClick={this.templateCommit.bind(
                                        this,
                                        selected_template,
                                    )}
                                >
                                    Update Template
                                </button>
                            </Fragment>
                        ) : null}
                    </div>
                    {template ? (
                        <Fragment>
                            <div css={divStyle}>
                                <label>
                                    Name
                                    <br />
                                    <input
                                        disabled={!template_allowed.name}
                                        css={inputStyle}
                                        type="text"
                                        value={
                                            template_edit.name ||
                                            template.name ||
                                            ""
                                        }
                                        onChange={e =>
                                            this.updateTemplate({
                                                name: e.target.value,
                                            })
                                        }
                                    />
                                </label>
                            </div>

                            <div css={divStyle}>
                                <label>
                                    Directory
                                    <br />
                                    <input
                                        disabled={
                                            !template_allowed.tftp_directory
                                        }
                                        css={inputStyle}
                                        type="text"
                                        value={
                                            template_edit.tftp_directory ||
                                            template.tftp_directory ||
                                            ""
                                        }
                                        onChange={e =>
                                            this.updateTemplate({
                                                tftp_directory: e.target.value,
                                            })
                                        }
                                    />
                                </label>
                            </div>

                            <table
                                css={[
                                    css({ width: INPUT_WIDTH }),
                                    css`
                                        & td {
                                            width: 50%;
                                        }
                                    `,
                                ]}
                            >
                                <tbody>{tbody}</tbody>
                            </table>
                            <div
                                css={[
                                    css`
                                        text-align: center;
                                        & button {
                                            margin-left: 10px;
                                            margin-right: 10px;
                                            font-size: 120%;
                                        }
                                    `,
                                    { width: INPUT_WIDTH },
                                    divStyle,
                                ]}
                            >
                                <button
                                    disabled={page === 0}
                                    onClick={_ => this.setState({ page: 0 })}
                                >
                                    1
                                </button>
                                <button
                                    disabled={page === 1}
                                    onClick={_ => this.setState({ page: 1 })}
                                >
                                    2
                                </button>
                                <button
                                    disabled={page === 2}
                                    onClick={_ => this.setState({ page: 2 })}
                                >
                                    3
                                </button>
                            </div>
                        </Fragment>
                    ) : null}
                </div>
                {location ? (
                    <LineKeyEditor
                        state={editor_state}
                        onChange={this.onLineKeyEditorChange.bind(this)}
                        allowed={this.state.editor_allowed}
                    />
                ) : null}
            </DualPane>
        );
    }

    templateCommit(id: string) {
        this.props.api_call({ code: "polycom_commit", id }).then(() => {
            window.alert("Polycom Template Updated!");
        });
    }

    updateTemplate(val: any) {
        this.setState(prev => {
            return {
                template_edit: Object.assign({}, prev.template_edit, val),
            };
        });
        this.props
            .api_call({
                code: "update_polycom",
                id: this.state.selected_template,
                changes: val,
            })
            .then(() => {
                this.refreshList();
                this.props
                    .api_call({
                        code: "get_polycom",
                        id: this.state.selected_template,
                    })
                    .then((data: any) => {
                        this.setState({
                            template: new PolycomTemplate(data.polycom),
                        });
                    });
            });
    }

    onLineKeyEditorChange(e: any) {
        this.setState({ editor_state: e });
        this.props
            .api_call({
                code: "upsert_linekey",
                linekey: e.category === "Unassigned" ? null : e,
                location: this.state.location,
                id: this.state.selected_template,
            })
            .then(() => {
                this.props
                    .api_call({
                        code: "get_polycom",
                        id: this.state.selected_template,
                    })
                    .then((data: any) => {
                        this.setState({
                            template: new PolycomTemplate(data.polycom),
                        });
                    });
            });
    }

    clickLocation(location: any) {
        this.setState(prev => {
            if (
                prev.location &&
                prev.location.page === location.page &&
                prev.location.column === location.column &&
                prev.location.entry === location.entry
            ) {
                return { location: null, editor_state: null };
            } else {
                const line_key =
                    prev.template.getLineKeySideCar(location) || {};
                this.props
                    .api_call({
                        code: "pdp_polycom_location",
                        location: location,
                        id: prev.template.id,
                    })
                    .then((ret: any) => {
                        this.setState({ editor_allowed: ret.allowed });
                    });
                return {
                    location: location,
                    editor_state: {
                        category: line_key.category || "Unassigned",
                        lb: line_key.lb || "",
                        ct: line_key.ct || "",
                        explicit_index: line_key.explicit_index || 0,
                    },
                };
            }
        });
    }

    setTemplate(id: string) {
        this.setState({
            selected_template: id,
            template: null,
            editor_state: null,
            location: null,
            page: 0,
            template_edit: {},
        });
        if (id !== "nothing") {
            this.props
                .api_call({ code: "get_polycom", id })
                .then((data: any) => {
                    this.setState({
                        template: new PolycomTemplate(data.polycom),
                    });
                });
            this.props
                .api_call({ code: "pdp_polycom_template", id })
                .then((data: any) => {
                    this.setState({ template_allowed: data.allowed });
                });
        }
    }

    addTemplate() {
        this.props.api_call({ code: "add_polycom" }).then((data: any) => {
            this.setTemplate(data.id);
            this.refreshList();
        });
    }

    deleteTemplate(id: string) {
        if (
            !window.confirm(
                `Delete polycom template ${
                    this.state.template_list.find(x => x.id === id).name
                }?`,
            )
        ) {
            return;
        }
        this.props.api_call({ code: "del_polycom", id }).then(() => {
            this.setTemplate("nothing");
            this.refreshList();
        });
    }
}

class LineKeyEditor extends React.Component<any, any> {
    render() {
        const { category, lb, ct, explicit_index } = this.props.state;

        const perm = this.props.allowed || { category: {} };

        let edit_type;
        switch (category) {
            case "Unassigned":
                edit_type = null;
                break;
            case "SpeedDial":
                edit_type = (
                    <Fragment>
                        <div css={divStyle}>
                            <label>
                                Label: <br />
                                <input
                                    css={inputStyle}
                                    type="text"
                                    value={lb}
                                    disabled={!perm.lb}
                                    onChange={e =>
                                        this.updateState({ lb: e.target.value })
                                    }
                                />
                            </label>
                        </div>
                        <div css={divStyle}>
                            <label>
                                Contact: <br />
                                <input
                                    css={inputStyle}
                                    type="text"
                                    value={ct}
                                    disabled={!perm.ct}
                                    onChange={e =>
                                        this.updateState({ ct: e.target.value })
                                    }
                                />
                            </label>
                        </div>
                    </Fragment>
                );
                break;
            case "BLF":
                edit_type = null;
                break;
            case "Line":
                edit_type = (
                    <div css={divStyle}>
                        <label>
                            Line Index: <br />
                            <input
                                css={inputStyle}
                                type="text"
                                value={explicit_index}
                                disabled={!perm.explicit_index}
                                onChange={e =>
                                    this.updateState({
                                        explicit_index: e.target.value,
                                    })
                                }
                            />
                        </label>
                    </div>
                );
                break;
        }

        return (
            <div css={editorStyle}>
                <div css={divStyle}>
                    <label>
                        Button Type
                        <br />
                        <select
                            css={inputStyle}
                            value={category}
                            onChange={e =>
                                this.updateState({ category: e.target.value })
                            }
                            disabled={
                                !(
                                    perm.category.Unassigned ||
                                    perm.category.SpeedDial ||
                                    perm.category.Line ||
                                    perm.category.BLF
                                )
                            }
                        >
                            <option
                                value="Unassigned"
                                disabled={!perm.category.Unassigned}
                            >
                                Unassigned
                            </option>
                            <option
                                value="SpeedDial"
                                disabled={!perm.category.SpeedDial}
                            >
                                Speed Dial
                            </option>
                            <option value="Line" disabled={!perm.category.Line}>
                                Line
                            </option>
                            <option value="BLF" disabled={!perm.category.BLF}>
                                BLF
                            </option>
                        </select>
                    </label>
                </div>
                {edit_type}
            </div>
        );
    }

    updateState(state: any) {
        this.props.onChange(Object.assign({}, this.props.state, state));
    }
}
