/** @jsx jsx */
import { css, jsx } from "@emotion/core";

import { HOVER_ITEM_COLOR, icon_url, SELECTED_COLOR } from "./constants";

export function IconField(props: {
    icon_list: string[];
    disabled?: boolean;
    pid: string;
    value: string | null;
    onChange: (v: string | null) => void;
}) {
    const { icon_list, pid, value } = props;
    const disabled = props.disabled ?? false;

    function onClick(icon: string | null) {
        let new_value = icon;
        if (props.value === new_value) {
            new_value = null;
        }
        props.onChange(new_value);
    }

    return (
        <div style={{ maxHeight: 200, overflowY: "auto" }}>
            {icon_list.map(e => {
                const style = css({
                    display: "inline-block",
                    padding: 6,
                    backgroundColor: e === value ? SELECTED_COLOR : undefined,
                    transition: "background-color 0.25s ease-out",
                    "&:hover": {
                        backgroundColor:
                            e === value ? undefined : HOVER_ITEM_COLOR,
                    },
                });

                return (
                    <span
                        key={e}
                        onClick={
                            disabled ? () => undefined : onClick.bind(null, e)
                        }
                        css={style}
                    >
                        <img
                            title={e}
                            alt={e}
                            style={{
                                width: "16px",
                                height: "16px",
                                verticalAlign: "middle",
                            }}
                            src={icon_url(pid, e)}
                        />
                    </span>
                );
            })}
        </div>
    );
}
