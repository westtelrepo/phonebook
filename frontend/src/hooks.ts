import Immutable from "immutable";
import {
    useCallback,
    useContext,
    useEffect,
    useReducer,
    useState,
} from "react";

import { AddUndo } from "./constants";

export async function patch(uri: string, body: any): Promise<any> {
    const req = await fetch(uri, {
        method: "PATCH",
        body: JSON.stringify(body),
        credentials: "same-origin",
        cache: "no-store",
        headers: {
            "Content-Type": "application/merge-patch+json",
            "Cache-Control": "no-cache",
            Pragma: "no-cache",
        },
    });

    if (!req.ok) {
        throw new Error(await req.text());
    }

    return req.json();
}

export async function post(uri: string, body?: any): Promise<any> {
    const req = await fetch(uri, {
        method: "POST",
        body: body ? JSON.stringify(body) : undefined,
        credentials: "same-origin",
        cache: "no-store",
        headers: {
            "Content-Type": "application/json",
            "Cache-Control": "no-cache",
            Pragma: "no-cache",
        },
    });

    if (!req.ok) {
        throw new Error(await req.text());
    }

    if (req.status === 204) {
        return null;
    }

    return req.json();
}

export async function post_xml(uri: string, body: string): Promise<any> {
    const req = await fetch(uri, {
        method: "POST",
        body,
        credentials: "same-origin",
        cache: "no-store",
        headers: {
            "Content-Type": "text/xml",
            "Cache-Control": "no-cache",
            Pragma: "no-cache",
            Accept: "application/json",
        },
    });

    if (!req.ok) {
        throw new Error(await req.text());
    }

    // if the code is 204 there is no body
    if (req.status === 204) {
        return null;
    }

    return req.json();
}

export async function json_delete(uri: string): Promise<any> {
    const req = await fetch(uri, {
        method: "DELETE",
        credentials: "same-origin",
        cache: "no-store",
        headers: {
            "Content-Type": "application/json",
            "Cache-Control": "no-cache",
            Pragma: "no-cache",
        },
    });

    if (!req.ok) {
        throw new Error(await req.text());
    }

    // if the code is 204 there is no body
    if (req.status === 204) {
        return null;
    }

    return req.json();
}

export function useDebounce<T>(value: T, delay: number): T {
    const [debounced, set_debounced] = useState(value);

    useEffect(() => {
        const id = setTimeout(() => {
            set_debounced(value);
        }, delay);

        return () => {
            clearTimeout(id);
        };
    }, [value, delay]);

    return debounced;
}

type IPatchState =
    | {
          state: "error";
          message: string;
      }
    | {
          state: "pending";
      }
    | {
          state: "empty";
      }
    | {
          state: "saved";
      };

interface IPatchReducerState<T> {
    state: "error" | "pending" | "empty" | "saved";
    obj: T | null;
    uri: string | null;
    message?: string; // only exists in state === "error"
}

type IPatchReducerAction<T> =
    | {
          op: "set_obj";
          obj: T;
          uri: string;
      }
    | {
          op: "set_error";
          uri: string;
          obj: T | null;
          message: string;
      }
    | {
          op: "set_save";
          uri: string;
          obj: T;
      }
    | {
          op: "set_empty";
      };

function patch_reducer<T>(
    state: IPatchReducerState<T>,
    action: IPatchReducerAction<T>,
): IPatchReducerState<T> {
    switch (action.op) {
        case "set_obj":
            return { state: "pending", obj: action.obj, uri: action.uri };
        case "set_error": {
            if (state.uri === action.uri && Object.is(state.obj, action.obj)) {
                return {
                    state: "error",
                    obj: action.obj,
                    uri: action.uri,
                    message: action.message,
                };
            } else {
                return state;
            }
        }
        case "set_save": {
            if (state.uri === action.uri && Object.is(state.obj, action.obj)) {
                return { state: "saved", obj: action.obj, uri: action.uri };
            } else {
                return state;
            }
        }
        case "set_empty": {
            return { state: "empty", obj: null, uri: null };
        }
    }
}

// THIS ASSUMES PATCH IS IDEMPOTENT
// TODO: DOES NOT DO DEBOUNCING
export function usePatch<T>(uri: string | null, obj: T | null): IPatchState {
    const [state, dispatch] = useReducer(patch_reducer, {
        state: "empty",
        uri: null,
        obj: null,
    });

    const add_undo = useContext(AddUndo);

    useEffect(() => {
        if (uri === null || obj === null) {
            dispatch({ op: "set_empty" });
            return;
        }

        const f = async () => {
            dispatch({ op: "set_obj", obj, uri });
            try {
                // do fetch
                const req = await fetch(uri, {
                    method: "PATCH",
                    body: JSON.stringify(obj),
                    credentials: "same-origin",
                    cache: "no-store",
                    headers: {
                        "Content-Type": "application/merge-patch+json",
                        "Cache-Control": "no-cache",
                        Pragma: "no-cache",
                    },
                });

                if (!req.ok) {
                    throw new Error("TODO: " + req.statusText);
                }

                const ret = await req.json();
                if (
                    typeof ret === "object" &&
                    ret !== null &&
                    typeof ret.change_id === "number"
                ) {
                    add_undo(ret.change_id);
                }
                dispatch({ op: "set_save", obj, uri });
            } catch (e) {
                console.log(e);
                dispatch({ op: "set_error", message: e.message, uri, obj });
            }
        };

        f().catch(e => console.error("ERROR IN usePatch f", e));
        // eslint-disable-next-line react-hooks/exhaustive-deps
    }, [uri, obj]);

    return state as IPatchState;
}

export function useUndoStack<T>(
    exec_undo: (obj: T) => Promise<T | null>,
    can_undo: (obj: T) => boolean,
): [
    (obj: T) => void,
    () => Promise<void>,
    boolean,
    () => Promise<void>,
    boolean,
    boolean,
    boolean,
] {
    const [state, set_state] = useState({
        undo: Immutable.Stack<T>(),
        redo: Immutable.Stack<T>(),
    });

    const [undoing, set_undoing] = useState(false);
    const [redoing, set_redoing] = useState(false);

    const add_undo = useCallback(
        (obj: T) => {
            set_state(s => ({
                undo: s.undo.push(obj),
                redo: Immutable.Stack(),
            }));
        },
        [set_state],
    );

    const undo = useCallback(async () => {
        if (!state.undo.isEmpty() && can_undo(state.undo.first())) {
            try {
                set_undoing(true);
                const ret = await exec_undo(state.undo.first());
                if (ret !== null) {
                    set_state(s => ({
                        undo: s.undo.pop(),
                        redo: s.redo.push(ret),
                    }));
                } else {
                    set_state(s => ({ undo: s.undo.pop(), redo: s.redo }));
                }
            } catch (e) {
                if (e instanceof Error) {
                    window.alert("Error in undo: " + e.message);
                } else {
                    throw e;
                }
            } finally {
                set_undoing(false);
            }
        }
    }, [state, exec_undo, can_undo]);

    const redo = useCallback(async () => {
        if (!state.redo.isEmpty() && can_undo(state.redo.first())) {
            try {
                set_redoing(true);
                const ret = await exec_undo(state.redo.first());
                if (ret !== null) {
                    set_state(s => ({
                        undo: s.undo.push(ret),
                        redo: s.redo.pop(),
                    }));
                } else {
                    set_state(s => ({ undo: s.undo, redo: s.redo.pop() }));
                }
            } catch (e) {
                if (e instanceof Error) {
                    window.alert("Error in redo: " + e.message);
                } else {
                    throw e;
                }
            } finally {
                set_redoing(false);
            }
        }
        // eslint-disable-next-line react-hooks/exhaustive-deps
    }, [exec_undo, can_undo]);

    return [
        add_undo,
        undo,
        !undoing &&
            !redoing &&
            !state.undo.isEmpty() &&
            can_undo(state.undo.first()),
        redo,
        !undoing &&
            !redoing &&
            !state.redo.isEmpty() &&
            can_undo(state.redo.first()),
        undoing,
        redoing,
    ];
}
