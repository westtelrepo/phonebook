import assert from "assert";
import React from "react";

import { is_string_array, NGTopLevelValue } from "./ng911ini";
import { LSchema } from "./schema/InterfaceSchemas";
import { TopLevelArrayEdit } from "./TopLevelEdits/TopLevelArrayEdit";
import { TopLevelBooleanEdit } from "./TopLevelEdits/TopLevelBooleanEdit";
import { TopLevelComplexArrayEdit } from "./TopLevelEdits/TopLevelComplexArrayEdit";
import { TopLevelOptionalSubsectionEdit } from "./TopLevelEdits/TopLevelOptionalSubsectionEdit";
import { TopLevelSimpleStringArrayEdit } from "./TopLevelEdits/TopLevelSimpleStringArrayEdit";
import { TopLevelStringEdit } from "./TopLevelEdits/TopLevelStringEdit";

export function Edit({
    name,
    schema,
    state,
    onChange,
}: {
    name: string;
    schema: LSchema;
    state: NGTopLevelValue;
    onChange: (newValue: NGTopLevelValue) => void;
}) {
    switch (schema.t) {
        case "string": {
            assert(typeof state === "string" || state === undefined);
            return (
                <TopLevelStringEdit
                    name={name}
                    schema={schema}
                    state={state}
                    onChange={onChange}
                />
            );
        }
        case "boolean": {
            assert(typeof state === "boolean" || state === undefined);
            return (
                <TopLevelBooleanEdit
                    name={name}
                    state={state}
                    onChange={onChange}
                    schema={schema}
                />
            );
        }
        case "simple-string-array": {
            assert(is_string_array(state) || state === undefined);
            return (
                <TopLevelSimpleStringArrayEdit
                    name={name}
                    onChange={onChange}
                    state={state}
                    schema={schema}
                />
            );
        }
        case "numbered-array": {
            assert(Array.isArray(state) || state === undefined);
            return (
                <TopLevelArrayEdit
                    name={name}
                    schema={schema}
                    onChange={onChange}
                    state={state as any}
                />
            );
        }
        case "array": {
            assert(Array.isArray(state) || state === undefined);
            return (
                <TopLevelArrayEdit
                    name={name}
                    schema={schema}
                    onChange={onChange}
                    state={state as any}
                />
            );
        }
        case "complex-array-in-array": {
            assert(Array.isArray(state) || state === undefined);
            return (
                <TopLevelComplexArrayEdit
                    name={name}
                    schema={schema}
                    onChange={onChange}
                    state={state as any}
                />
            );
        }
        case "optional-subsection": {
            assert(
                (typeof state === "object" || state === undefined) &&
                    !Array.isArray(state),
            );
            return (
                <TopLevelOptionalSubsectionEdit
                    name={name}
                    schema={schema}
                    onChange={onChange}
                    state={state}
                />
            );
        }
        default:
            throw new Error("IMPLEMENT THIS, PROGRAMMER!");
    }
}
