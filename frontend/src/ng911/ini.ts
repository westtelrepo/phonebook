import assert from "assert";

export interface INIValue {
    readonly value: string;
    readonly comment: string;
}
export interface INISection {
    readonly comment: string;
    readonly contents: Map<string, INIValue[]>;
}
export type INIFile = Map<string, INISection>;

export function parseINI(content: string, trimEnd: boolean): INIFile {
    const ret: INIFile = new Map();
    let current_section: INISection | undefined;
    let current_comment = "";
    let section_name: string = "";

    for (const line of content.split(/\r?\n/)) {
        let m: RegExpExecArray | null = null;
        if (/^\s*$/.test(line)) {
            // ignore empty lines
        } else if ((m = /^\s*#(.*)$/.exec(line))) {
            current_comment +=
                (trimEnd ? m[1].trim() : m[1].trimStart()) + "\n";
        } else if ((m = /^\s*\[([A-Z]+)\]\s*/.exec(line))) {
            current_section = { comment: current_comment, contents: new Map() };
            section_name = m[1];
            current_comment = "";
            ret.set(m[1], current_section);
        } else if ((m = /^\s*(\S+)\s*=(.*)$/.exec(line))) {
            assert(current_section);
            if (!current_section.contents.has(m[1])) {
                current_section.contents.set(m[1], []);
            }
            // eslint-disable-next-line @typescript-eslint/no-non-null-assertion
            current_section.contents.get(m[1])!.push({
                comment: current_comment,
                value: trimEnd ? m[2].trim() : m[2].trimStart(),
            });
            current_comment = "";
        } else {
            throw new Error(
                `INVALID LINE IN SECTION ${section_name}: ` +
                    JSON.stringify(line),
            );
        }
    }

    return ret;
}
