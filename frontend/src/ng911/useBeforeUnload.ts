import { useEffect, useRef } from "react";

export function useBeforeUnload(cb: () => boolean) {
    const current = useRef(cb);
    current.current = cb;

    useEffect(() => {
        function listen(event: BeforeUnloadEvent) {
            if (current.current()) {
                event.preventDefault();
            }
        }

        window.addEventListener("beforeunload", listen);
        return () => {
            window.removeEventListener("beforeunload", listen);
        };
    }, []);
}
