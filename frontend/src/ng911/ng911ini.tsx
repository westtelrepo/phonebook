/* eslint-disable @typescript-eslint/strict-boolean-expressions, @typescript-eslint/no-dynamic-delete */
import assert from "assert";

import { INIFile } from "./ini";
import { little_schema } from "./schema/schema";
import {
    COMMA_SEPARATED_INTEGERS_PATTERN,
    DEFAULT_PATTERN,
    EMAIL_PATTERN,
    IPV4_PATTERN,
    IPV4_PORT_PATTERN,
    NUMBER_PATTERN,
    PORT_PATTERN,
} from "./schema/schema";

function pattern_message(pattern: string) {
    switch (pattern) {
        case DEFAULT_PATTERN:
            return "contains non ASCII characters.";
        case IPV4_PATTERN:
            return "is not a valid IPV4 address.";
        case PORT_PATTERN:
            return "is not a valid port number.";
        case IPV4_PORT_PATTERN:
            return "should be an IPV4 address and a port number separated by a comma.";
        case NUMBER_PATTERN:
            return "should be a number with no commas.";
        case EMAIL_PATTERN:
            return "is not a valid email.";
        case COMMA_SEPARATED_INTEGERS_PATTERN:
            return "should be comma separated integers.";
        default:
            return `does not follow the pattern "${pattern}".`;
    }
}

export type NGOptionalSubsection = Record<
    string,
    undefined | string | boolean | string[]
>;
export type NGArray = Record<string, undefined | string | boolean>[];
export type NGNumberedArray = Record<
    string,
    | undefined
    | string
    | boolean
    | string[]
    | NGSimpleNumberedArray
    | NGOptionalSubsection
>[];
export type NGSimpleNumberedArray = string[];
export type NGComplexArray = Record<
    string,
    undefined | string | Record<string, undefined | string | boolean>[]
>[];
export type NGTopLevelValue =
    | undefined
    | boolean
    | string
    | string[]
    | NGOptionalSubsection
    | NGArray
    | NGNumberedArray
    | NGSimpleNumberedArray
    | NGComplexArray;

export type NG911File = Record<string, Record<string, NGTopLevelValue>>;

export function is_string_array(val: NGTopLevelValue): val is string[] {
    if (!Array.isArray(val)) {
        return false;
    }
    return val.every((v: unknown) => typeof v === "string");
}

export function parse_ini_file(ini: INIFile): [NG911File, string[]] {
    const file: INIFile = new Map(
        [...ini].map(([k, v]) => [k, { ...v, contents: new Map(v.contents) }]),
    );
    const ret: NG911File = {};

    let errormsg = "";
    const errors: string[] = [];

    for (const section_name of Object.keys(little_schema)) {
        const sec_schema = little_schema[section_name];
        const sec = file.get(section_name);

        if (sec) {
            const ret_sec: Record<string, NGTopLevelValue> = {};
            ret[section_name] = ret_sec;

            for (const key of Object.keys(sec_schema)) {
                const key_schema = sec_schema[key];

                switch (key_schema.t) {
                    case "boolean": {
                        const value = sec.contents.get(key);
                        if (value) {
                            if (value.length !== 1) {
                                errors.push(
                                    `${key} in section ${section_name} has multiple values. Must be assigned a value only once or it will be "${value[0].value.trim()}".`,
                                );
                            } else if (
                                value[0].value.trim() !== "true" &&
                                value[0].value.trim() !== "false"
                            ) {
                                errors.push(
                                    `${key} in section ${section_name}: expected "true" or "false", received "${value[0].value.trim()}".`,
                                );
                            }

                            ret_sec[key] = value[0].value.trim() === "true";
                            sec.contents.delete(key);
                        } else if (key_schema.required) {
                            errors.push(
                                `${key} is required in section ${section_name}.`,
                            );
                        }
                        break;
                    }
                    case "string": {
                        const value = sec.contents.get(key);
                        if (value) {
                            if (value.length !== 1) {
                                errors.push(
                                    `${key} in section ${section_name} has multiple values. Must be assigned a value only once or it will be "${value[0].value.trim()}".\n`,
                                );
                            }
                            if (
                                key_schema.min &&
                                Number(value[0].value) < key_schema.min
                            ) {
                                errors.push(
                                    `${key} = "${value[0].value}" is less than the minimum of ${key_schema.min}.`,
                                );
                            } else if (
                                key_schema.max &&
                                Number(value[0].value) > key_schema.max
                            ) {
                                errors.push(
                                    `${key} = "${value[0].value}" is greater than the maximum of ${key_schema.max}.`,
                                );
                            } else if (
                                key_schema.pattern &&
                                !new RegExp(key_schema.pattern).exec(
                                    value[0].value.trim(),
                                )
                            ) {
                                errors.push(
                                    `${key} = "${
                                        value[0].value
                                    }" in section ${section_name} ${pattern_message(
                                        key_schema.pattern,
                                    )}`,
                                );
                            }
                            ret_sec[key] = value[0].value;
                            sec.contents.delete(key);
                        } else if (key_schema.required) {
                            errors.push(
                                `${key} is required in section ${section_name}.`,
                            );
                        }
                        break;
                    }
                    case "simple-string-array": {
                        const value = sec.contents.get(key);
                        if (value) {
                            for (const v of value) {
                                if (
                                    key_schema.min &&
                                    Number(v.value) < key_schema.min
                                ) {
                                    errors.push(
                                        `${key} = "${v.value}" is less than the minimum of ${key_schema.min}.`,
                                    );
                                    break;
                                } else if (
                                    key_schema.max &&
                                    Number(v.value) > key_schema.max
                                ) {
                                    errors.push(
                                        `${key} = "${v.value}" is greater than the maximum of ${key_schema.max}.`,
                                    );
                                    break;
                                } else if (
                                    key_schema.pattern &&
                                    !new RegExp(key_schema.pattern).exec(
                                        v.value.trim(),
                                    )
                                ) {
                                    errors.push(
                                        `${key} = "${
                                            v.value
                                        }" in section ${section_name} ${pattern_message(
                                            key_schema.pattern,
                                        )}`,
                                    );
                                    break;
                                }
                            }
                            ret_sec[key] = value.map(e => e.value);
                            sec.contents.delete(key);
                        } else if (key_schema.required) {
                            errors.push(
                                `${key} is required in section ${section_name}.`,
                            );
                        }
                        break;
                    }
                    case "numbered-array": {
                        const array: NGNumberedArray = [];
                        for (const [key, value] of sec.contents) {
                            if (key.startsWith(key_schema.prefix)) {
                                const post = key.substring(
                                    key_schema.prefix.length,
                                );

                                const split = /^(\d+)(.*)$/.exec(post);
                                assert(
                                    split,
                                    `Did not find a number in value ${key}`,
                                );

                                const index = Number(split[1]) - 1;
                                assert(
                                    index >= 0,
                                    `${key} cannot have a number of 0.`,
                                );

                                const subkey = split[2];

                                if (/^_Range\d+$/.test(subkey)) {
                                    const m = /^_Range(\d+)$/.exec(subkey);
                                    assert(m);
                                    const ri = Number(m[1]) - 1;
                                    assert(ri >= 0);

                                    if (array[index] === undefined) {
                                        array[index] = {};
                                    }
                                    if (array[index]._Range === undefined) {
                                        array[index]._Range = [];
                                    }
                                    if (
                                        !/^[0-9]{10}, ?[0-9]{10}, ?[0-9]{1,2}$/.exec(
                                            value[0].value.trim(),
                                        )
                                    ) {
                                        errors.push(
                                            `${key}="${value[0].value}" should be two 10-digit numbers separated by a comma then a number separated by another comma.`,
                                        );
                                    }
                                    {
                                        const a = array[index]._Range;
                                        assert(Array.isArray(a));
                                        a[ri] = value[0].value;
                                    }
                                } else {
                                    const subkey_schema = key_schema.schema[
                                        subkey
                                    ]
                                        ? key_schema.schema[subkey]
                                        : key_schema.schema.COS;
                                    sec.contents.delete(
                                        `Database${index + 1}COS`,
                                    );

                                    assert(
                                        subkey_schema,
                                        `${key} is not a known value in numbered array ${key_schema.prefix}`,
                                    );
                                    switch (subkey_schema.t) {
                                        case "string": {
                                            if (value.length > 1) {
                                                errors.push(
                                                    `${key} in numbered array ${
                                                        key_schema.prefix
                                                    } in section ${section_name} has multiple values. Must be assigned a value only once or it will be "${value[0].value.trim()}".`,
                                                );
                                            }

                                            if (
                                                subkey_schema.min &&
                                                Number(value[0].value) <
                                                    subkey_schema.min
                                            ) {
                                                errors.push(
                                                    `${key} = "${value[0].value}" in numbered array ${key_schema.prefix} in section ${section_name} is less than the minimum of ${subkey_schema.min}.`,
                                                );
                                            } else if (
                                                subkey_schema.max &&
                                                Number(value[0].value) >
                                                    subkey_schema.max
                                            ) {
                                                errors.push(
                                                    `${key} = "${value[0].value}" in numbered array ${key_schema.prefix} in section ${section_name} is greater than the maximum of ${subkey_schema.max}.`,
                                                );
                                            } else if (
                                                subkey_schema.pattern &&
                                                !new RegExp(
                                                    subkey_schema.pattern,
                                                ).exec(value[0].value.trim())
                                            ) {
                                                errors.push(
                                                    `${key} in section ${section_name} ${pattern_message(
                                                        subkey_schema.pattern,
                                                    )}`,
                                                );
                                            }

                                            if (array[index] === undefined) {
                                                array[index] = {};
                                            }
                                            array[index][subkey] =
                                                value[0].value;
                                            break;
                                        }
                                        case "boolean": {
                                            if (value.length > 1) {
                                                errors.push(
                                                    `${key} in numbered array ${
                                                        key_schema.prefix
                                                    } in section ${section_name} has multiple values. Must be assigned a value only once or it will be "${value[0].value.trim()}".`,
                                                );
                                            }

                                            if (value.length > 0) {
                                                if (
                                                    value[0].value.trim() !==
                                                        "true" &&
                                                    value[0].value.trim() !==
                                                        "false" &&
                                                    value[0].value.trim() !== ""
                                                ) {
                                                    errors.push(
                                                        `${key} in section ${section_name}: expected "true" or "false", received "${value[0].value.trim()}".`,
                                                    );
                                                }

                                                if (
                                                    array[index] === undefined
                                                ) {
                                                    array[index] = {};
                                                }

                                                if (
                                                    value[0].value.trim() ===
                                                    "true"
                                                ) {
                                                    array[index][subkey] = true;
                                                } else if (
                                                    value[0].value.trim() ===
                                                    "false"
                                                ) {
                                                    array[index][
                                                        subkey
                                                    ] = false;
                                                } else {
                                                    array[index][
                                                        subkey
                                                    ] = undefined;
                                                }
                                            }
                                            break;
                                        }
                                        case "simple-string-array": {
                                            if (array[index] === undefined) {
                                                array[index] = {};
                                            }
                                            for (const v of value) {
                                                if (
                                                    subkey_schema.min &&
                                                    Number(v.value) <
                                                        subkey_schema.min
                                                ) {
                                                    errors.push(
                                                        `${subkey} = "${v.value}" is less than the minimum of ${subkey_schema.min}.`,
                                                    );
                                                    break;
                                                } else if (
                                                    subkey_schema.max &&
                                                    Number(v.value) >
                                                        subkey_schema.max
                                                ) {
                                                    errors.push(
                                                        `${key} = "${v.value}" is greater than the maximum of ${subkey_schema.max}.`,
                                                    );
                                                    break;
                                                } else if (
                                                    subkey_schema.pattern &&
                                                    !new RegExp(
                                                        subkey_schema.pattern,
                                                    ).exec(v.value.trim())
                                                ) {
                                                    errors.push(
                                                        `${key} = "${
                                                            v.value
                                                        }" in section ${section_name} ${pattern_message(
                                                            subkey_schema.pattern,
                                                        )}`,
                                                    );
                                                    break;
                                                }
                                            }
                                            array[index][subkey] = value.map(
                                                e => e.value,
                                            );
                                            break;
                                        }
                                        case "optional-subsection": {
                                            let subsection:
                                                | NGOptionalSubsection
                                                | undefined = undefined;
                                            {
                                                const o = array[index].COS;
                                                assert(
                                                    (typeof o === "object" &&
                                                        !Array.isArray(o)) ||
                                                        o === undefined,
                                                );
                                                subsection = o ? o : {};
                                            }

                                            const s =
                                                subkey_schema.schema[subkey];
                                            assert(
                                                s,
                                                `Unknown subkey ${subkey} in COS`,
                                            );
                                            const f = (arg: string) => {
                                                switch (s.t) {
                                                    case "string":
                                                        if (arg.trim()) {
                                                            if (
                                                                s.min &&
                                                                Number(arg) <
                                                                    s.min
                                                            ) {
                                                                errors.push(
                                                                    `${subkey} = "${arg}" in subsection ${key} in section ${section_name} is less than the minimum of ${s.min}.`,
                                                                );
                                                            } else if (
                                                                s.max &&
                                                                Number(arg) >
                                                                    s.max
                                                            ) {
                                                                errors.push(
                                                                    `${subkey} = "${arg}" in subsection ${key} in section ${section_name} is greater than the maximum of ${s.max}.`,
                                                                );
                                                            } else if (
                                                                s.pattern &&
                                                                !new RegExp(
                                                                    s.pattern,
                                                                ).exec(
                                                                    arg.trim(),
                                                                )
                                                            ) {
                                                                errors.push(
                                                                    `${subkey} = "${arg}" in subsection ${key} in section ${section_name} ${pattern_message(
                                                                        s.pattern,
                                                                    )}`,
                                                                );
                                                            }
                                                        }
                                                        return arg;
                                                    case "boolean":
                                                        if (
                                                            arg.trim() !==
                                                                "false" &&
                                                            arg.trim() !==
                                                                "true"
                                                        ) {
                                                            errors.push(
                                                                `${subkey} in subsection ${key} in section ${section_name}: expected "true" or "false", received "${arg.trim()}"`,
                                                            );
                                                        }
                                                        if (
                                                            arg.trim() ===
                                                            "true"
                                                        ) {
                                                            return true;
                                                        }
                                                        if (
                                                            arg.trim() ===
                                                            "false"
                                                        ) {
                                                            return false;
                                                        }
                                                        return undefined;
                                                    case "simple-string-array":
                                                        if (arg.trim()) {
                                                            if (
                                                                s.min &&
                                                                Number(arg) <
                                                                    s.min
                                                            ) {
                                                                errors.push(
                                                                    `${subkey} = "${arg}" in subsection ${key} in section ${section_name} is less than the minimum of ${s.min}.`,
                                                                );
                                                            } else if (
                                                                s.max &&
                                                                Number(arg) >
                                                                    s.max
                                                            ) {
                                                                errors.push(
                                                                    `${subkey} = "${arg}" in subsection ${key} in section ${section_name} is greater than the maximum of ${s.max}.`,
                                                                );
                                                            } else if (
                                                                s.pattern &&
                                                                !new RegExp(
                                                                    s.pattern,
                                                                ).exec(
                                                                    arg.trim(),
                                                                )
                                                            ) {
                                                                errors.push(
                                                                    `${subkey} = "${arg}" in subsection ${key} in section ${section_name} ${pattern_message(
                                                                        s.pattern,
                                                                    )}`,
                                                                );
                                                            }
                                                        }
                                                        return arg;
                                                    default:
                                                        throw new Error(
                                                            `Unhandled optional subsection in numbered array type: ${JSON.stringify(
                                                                s,
                                                            )}`,
                                                        );
                                                }
                                            };
                                            if (value) {
                                                if (!subsection) {
                                                    subsection = {};
                                                } else if (
                                                    value[0].value ===
                                                        undefined &&
                                                    !s.deprecated
                                                ) {
                                                    errors.push(
                                                        `All values must be filled out in subsection ${key} in section ${section_name} or the subsection must be unchecked.`,
                                                    );
                                                }
                                                if (
                                                    s.t ===
                                                    "simple-string-array"
                                                ) {
                                                    if (!subsection[subkey]) {
                                                        subsection[subkey] = [];
                                                    }
                                                    for (const v of value) {
                                                        const a =
                                                            subsection[subkey];
                                                        assert(
                                                            Array.isArray(a),
                                                        );
                                                        const b = f(v.value);
                                                        assert(
                                                            typeof b ===
                                                                "string",
                                                        );
                                                        subsection[subkey] = [
                                                            ...a,
                                                            b,
                                                        ];
                                                    }
                                                } else {
                                                    subsection[subkey] = f(
                                                        value[0].value,
                                                    );
                                                }
                                                array[index].COS = subsection;
                                            }
                                            sec.contents.delete(subkey);

                                            break;
                                        }
                                        default:
                                            throw new Error(
                                                `TODO THE SUBKEY: ${subkey}`,
                                            );
                                    }
                                }
                                sec.contents.delete(key);
                            }
                        }
                        if (array.length === 0 && key_schema.required) {
                            errors.push(
                                `Numbered array ${key} is required in section ${section_name}.`,
                            );
                        }
                        ret_sec[key] = array;
                        break;
                    }
                    case "array": {
                        let array: NGArray | undefined;
                        for (const subkey of Object.keys(key_schema.schema)) {
                            const arr = sec.contents.get(subkey);
                            const s = key_schema.schema[subkey];
                            const f = (arg: string, index: number) => {
                                switch (s.t) {
                                    case "boolean":
                                        if (
                                            arg.trim() !== "false" &&
                                            arg.trim() !== "true" &&
                                            arg.trim() !== ""
                                        ) {
                                            errors.push(
                                                `${subkey} in array ${key} ${
                                                    index + 1
                                                } in section ${section_name}: expected "true" or "false", received "${arg.trim()}".`,
                                            );
                                        }

                                        if (arg.trim() === "true") {
                                            return true;
                                        }
                                        if (arg.trim() === "false") {
                                            return false;
                                        }
                                        return undefined;
                                    case "string":
                                        if (s.min && Number(arg) < s.min) {
                                            errors.push(
                                                `${subkey} = "${arg}" in array ${key} ${
                                                    index + 1
                                                } in section ${section_name} is less than the minimum of ${
                                                    s.min
                                                }.`,
                                            );
                                        } else if (
                                            s.max &&
                                            Number(arg) > s.max
                                        ) {
                                            errors.push(
                                                `${subkey} = "${arg}" in array ${key} ${
                                                    index + 1
                                                } in section ${section_name} is greater than the maximum of ${
                                                    s.max
                                                }.`,
                                            );
                                        } else if (
                                            s.pattern &&
                                            !new RegExp(s.pattern).exec(
                                                arg.trim(),
                                            )
                                        ) {
                                            errors.push(
                                                `${subkey} = "${arg}" in array ${key} ${
                                                    index + 1
                                                } in section ${section_name} ${pattern_message(
                                                    s.pattern,
                                                )}`,
                                            );
                                        }
                                        return arg;
                                }
                            };
                            if (arr) {
                                if (array) {
                                    if (array.length !== arr.length) {
                                        errors.push(
                                            `Uneven array in section ${section_name}. ${subkey} has too many values.`,
                                        );
                                        errormsg += `Uneven array in section ${section_name}. ${subkey} has too many values.\n`;
                                    }
                                    for (let i = 0; i < array.length; i++) {
                                        array[i][subkey] = f(arr[i].value, i);
                                    }
                                } else {
                                    array = arr.map(e => ({
                                        [subkey]: f(e.value, 0),
                                    }));
                                }
                            } else if (array && s.required) {
                                errors.push(
                                    `${subkey} is required for array ${key} in section ${section_name}.`,
                                );
                            }
                            sec.contents.delete(subkey);
                        }
                        if (!array && key_schema.required) {
                            errors.push(
                                `${key} is required in section ${section_name}.`,
                            );
                        }
                        ret_sec[key] = array;
                        break;
                    }
                    case "complex-array-in-array": {
                        let array: NGComplexArray | undefined;
                        for (const subkey of [
                            ...Object.keys(key_schema.outer_schema),
                            key_schema.inner_length_name,
                        ]) {
                            const arr = sec.contents.get(subkey);
                            if (arr) {
                                if (array) {
                                    for (let i = 0; i < array.length; i++) {
                                        if (arr[i] === undefined) {
                                            errors.push(
                                                `${subkey} is missing from a phone.`,
                                            );
                                        } else {
                                            if (
                                                key_schema.outer_schema[subkey]
                                            ) {
                                                const s =
                                                    key_schema.outer_schema[
                                                        subkey
                                                    ];
                                                if (
                                                    s.min &&
                                                    Number(arr[i].value) < s.min
                                                ) {
                                                    errors.push(
                                                        `${subkey} = "${
                                                            arr[i].value
                                                        }" in Phone ${
                                                            i + 1
                                                        } is less than the minimum of ${
                                                            s.min
                                                        }`,
                                                    );
                                                } else if (
                                                    s.max &&
                                                    Number(arr[i].value) > s.max
                                                ) {
                                                    errors.push(
                                                        `${subkey} = "${
                                                            arr[i].value
                                                        }" in Phone ${
                                                            i + 1
                                                        } is greater than the maximum of ${
                                                            s.max
                                                        }`,
                                                    );
                                                } else if (
                                                    s.pattern !== undefined &&
                                                    !new RegExp(s.pattern).exec(
                                                        arr[i].value.trim(),
                                                    )
                                                ) {
                                                    errors.push(
                                                        `${subkey} = "${
                                                            arr[i].value
                                                        }" in Phone ${
                                                            i + 1
                                                        } ${pattern_message(
                                                            s.pattern,
                                                        )}`,
                                                    );
                                                }
                                            }
                                            array[i][subkey] = arr[i].value;
                                        }
                                    }
                                } else {
                                    for (let i = 0; i < arr.length; i++) {
                                        if (
                                            !arr[i].value &&
                                            key_schema.outer_schema[subkey] &&
                                            key_schema.outer_schema[subkey]
                                                .required
                                        ) {
                                            errors.push(
                                                `${subkey} is absent from Phone ${
                                                    i + 1
                                                }. All phones must have this value.`,
                                            );
                                        }
                                    }
                                    array = arr.map(e => ({
                                        [subkey]: e.value,
                                    }));
                                }
                            } else {
                                if (
                                    !array &&
                                    key_schema.outer_schema[subkey] &&
                                    key_schema.outer_schema[subkey].required
                                ) {
                                    errors.push(
                                        `${subkey} is a required value for phones. Make sure all phones have this value.`,
                                    );
                                }
                            }
                            sec.contents.delete(subkey);
                        }

                        if (array) {
                            for (const member of array) {
                                member[key_schema.inner_array_name] = new Array(
                                    Number(
                                        member[key_schema.inner_length_name],
                                    ),
                                ).fill(null);
                                delete member[key_schema.inner_length_name];
                            }
                        }

                        /** line number -> nth time line i appears -> line */
                        const lines_transposed: Record<
                            string,
                            string | boolean | undefined
                        >[][] = [];
                        for (const [key, value] of sec.contents) {
                            if (key.startsWith(key_schema.inner_prefix)) {
                                const post = key.substring(
                                    key_schema.inner_prefix.length,
                                );

                                const split = /^(\d+)(.*)$/.exec(post);
                                assert(
                                    split,
                                    `Did not find a number for ${key} within a phone in TELEPHONE`,
                                );

                                const index = Number(split[1]) - 1;
                                assert(index >= 0, "Cannot have a Line_0");

                                const subkey = split[2];
                                const s = key_schema.inner_schema[subkey];
                                const f = (arg: string, index: number) => {
                                    if (!s) {
                                        errors.push(
                                            `${key} is not a known key for a phone.`,
                                        );
                                    }
                                    switch (s.t) {
                                        case "boolean":
                                            if (
                                                arg.trim() !== "false" &&
                                                arg.trim() !== "true" &&
                                                arg.trim() !== ""
                                            ) {
                                                errors.push(
                                                    `${key} in Phone ${
                                                        index + 1
                                                    } in section ${section_name} expected "true" or "false", received "${arg.trim()}".`,
                                                );
                                            }
                                            if (arg.trim() === "true") {
                                                return true;
                                            }
                                            if (arg.trim() === "false") {
                                                return false;
                                            }
                                            return undefined;
                                        case "string":
                                            if (arg.trim()) {
                                                if (
                                                    s.min &&
                                                    Number(arg) < s.min
                                                ) {
                                                    errors.push(
                                                        `${key} = "${arg}" in Phone ${
                                                            index + 1
                                                        } in section ${section_name} is less than the minimum of ${
                                                            s.min
                                                        }.`,
                                                    );
                                                } else if (
                                                    s.max &&
                                                    Number(arg) > s.max
                                                ) {
                                                    errors.push(
                                                        `${key} = "${arg}" in Phone ${
                                                            index + 1
                                                        } in section ${section_name} is greater than the maximum of ${
                                                            s.max
                                                        }.`,
                                                    );
                                                } else if (
                                                    s.pattern &&
                                                    !new RegExp(s.pattern).exec(
                                                        arg.trim(),
                                                    )
                                                ) {
                                                    errors.push(
                                                        `${key} = "${arg}" in Phone ${
                                                            index + 1
                                                        } in section ${section_name} ${pattern_message(
                                                            s.pattern,
                                                        )}`,
                                                    );
                                                }
                                            }
                                            return arg;
                                    }
                                };
                                if (!lines_transposed[index]) {
                                    lines_transposed[index] = [];
                                }

                                for (let i = 0; i < value.length; i++) {
                                    if (!lines_transposed[index][i]) {
                                        lines_transposed[index][i] = {};
                                    }
                                    lines_transposed[index][i][subkey] = f(
                                        value[i].value,
                                        i,
                                    );
                                }

                                sec.contents.delete(key);
                            }
                        }

                        if (array) {
                            for (const member of array) {
                                const inner_array =
                                    member[key_schema.inner_array_name];
                                assert(
                                    Array.isArray(inner_array),
                                    "Inner array is not an array",
                                );
                                for (let i = 0; i < inner_array.length; i++) {
                                    if (!lines_transposed[i]) {
                                        errormsg +=
                                            "Line numbering error within a phone in section TELEPHONE.\n";
                                    } else {
                                        for (const innerkey of Object.keys(
                                            key_schema.inner_schema,
                                        )) {
                                            if (
                                                lines_transposed[i][0] ===
                                                undefined
                                            ) {
                                                errormsg += `${innerkey} error.\n`;
                                            } else if (
                                                lines_transposed[i][0][
                                                    innerkey
                                                ] === undefined ||
                                                lines_transposed[i][0][
                                                    innerkey
                                                ] === ""
                                            ) {
                                                errors.push(
                                                    `Line_${
                                                        i + 1
                                                    }${innerkey} is missing from Line_${
                                                        i + 1
                                                    }.`,
                                                );
                                            }
                                        }
                                        let front;
                                        [
                                            // eslint-disable-next-line prefer-const
                                            front,
                                            ...lines_transposed[i]
                                        ] = lines_transposed[i];
                                        if (!front) {
                                            errors.push(
                                                `Error parsing ${key_schema.inner_array_name} within a phone in section TELEPHONE.`,
                                            );
                                            errormsg += `Error parsing ${key_schema.inner_array_name} within a phone in section TELEPHONE.\n`;
                                        }
                                        inner_array[i] = front;
                                    }
                                }
                            }

                            for (const member of lines_transposed) {
                                if (!member) {
                                    errormsg +=
                                        "Line numbering error. Double check integers.\n";
                                } else if (member.length !== 0) {
                                    errormsg += `A value has been set more than once or the lines are wrong in count. Member: ${JSON.stringify(
                                        member,
                                    )}\n`;
                                }
                            }
                        } else if (key_schema.required) {
                            errors.push(
                                `${key} are required in section ${section_name}.`,
                            );
                        }
                        ret_sec[key] = array;
                        break;
                    }
                    case "optional-subsection": {
                        let subsection: NGOptionalSubsection | undefined;
                        for (const subkey of Object.keys(key_schema.schema)) {
                            const s = key_schema.schema[subkey];
                            const value = sec.contents.get(subkey);

                            const f = (arg: string) => {
                                switch (s.t) {
                                    case "string":
                                        if (arg.trim()) {
                                            if (s.min && Number(arg) < s.min) {
                                                errors.push(
                                                    `${subkey} = "${arg}" in subsection ${key} in section ${section_name} is less than the minimum of ${s.min}.`,
                                                );
                                            } else if (
                                                s.max &&
                                                Number(arg) > s.max
                                            ) {
                                                errors.push(
                                                    `${subkey} = "${arg}" in subsection ${key} in section ${section_name} is greater than the maximum of ${s.max}.`,
                                                );
                                            } else if (
                                                s.pattern &&
                                                !new RegExp(s.pattern).exec(
                                                    arg.trim(),
                                                )
                                            ) {
                                                errors.push(
                                                    `${subkey} = "${arg}" in subsection ${key} in section ${section_name} ${pattern_message(
                                                        s.pattern,
                                                    )}`,
                                                );
                                            }
                                        }
                                        return arg;
                                    case "boolean":
                                        if (
                                            arg.trim() !== "false" &&
                                            arg.trim() !== "true"
                                        ) {
                                            errors.push(
                                                `${subkey} in subsection ${key} in section ${section_name}: expected "true" or "false", received "${arg.trim()}"`,
                                            );
                                        }
                                        if (arg.trim() === "true") {
                                            return true;
                                        }
                                        if (arg.trim() === "false") {
                                            return false;
                                        }
                                        return undefined;
                                    case "simple-string-array":
                                        if (arg.trim()) {
                                            if (s.min && Number(arg) < s.min) {
                                                errors.push(
                                                    `${subkey} = "${arg}" in subsection ${key} in section ${section_name} is less than the minimum of ${s.min}.`,
                                                );
                                            } else if (
                                                s.max &&
                                                Number(arg) > s.max
                                            ) {
                                                errors.push(
                                                    `${subkey} = "${arg}" in subsection ${key} in section ${section_name} is greater than the maximum of ${s.max}.`,
                                                );
                                            } else if (
                                                s.pattern &&
                                                !new RegExp(s.pattern).exec(
                                                    arg.trim(),
                                                )
                                            ) {
                                                errors.push(
                                                    `${subkey} = "${arg}" in subsection ${key} in section ${section_name} ${pattern_message(
                                                        s.pattern,
                                                    )}`,
                                                );
                                            }
                                        }
                                        return arg;
                                    default:
                                        throw new Error(
                                            `Unhandled optional subsection type: ${JSON.stringify(
                                                s,
                                            )}`,
                                        );
                                }
                            };
                            if (value) {
                                if (!subsection) {
                                    subsection = {};
                                } else if (
                                    value[0].value === undefined &&
                                    !s.deprecated
                                ) {
                                    errors.push(
                                        `All values must be filled out in subsection ${key} in section ${section_name} or the subsection must be unchecked.`,
                                    );
                                }
                                if (s.t === "simple-string-array") {
                                    if (!subsection[subkey]) {
                                        subsection[subkey] = [];
                                    }
                                    for (const v of value) {
                                        const a = subsection[subkey];
                                        assert(Array.isArray(a));
                                        const b = f(v.value);
                                        assert(typeof b === "string");
                                        subsection[subkey] = [...a, b];
                                    }
                                } else {
                                    subsection[subkey] = f(value[0].value);
                                }
                            }
                            sec.contents.delete(subkey);
                        }

                        for (const subkey of Object.keys(key_schema.schema)) {
                            if (subsection) {
                                if (
                                    subsection[subkey] === undefined &&
                                    !key_schema.schema[subkey].deprecated
                                ) {
                                    errors.push(
                                        `All values must be filled out in subsection ${key} in section ${section_name} or the subsection must be unchecked.`,
                                    );
                                    break;
                                }
                            }
                        }
                        ret_sec[key] = subsection;
                        break;
                    }
                    default:
                        throw new Error("EXPLOSION!");
                }
            }

            if (sec.contents.size > 0) {
                errormsg += "Unknown key";
                if (sec.contents.size > 1) {
                    errormsg += "s";
                }
                errormsg += ": ";

                const entries = sec.contents.entries();
                for (let i = 0; i < sec.contents.size; i++) {
                    errormsg += `${entries.next().value[0]}`;
                    if (i < sec.contents.size - 1) {
                        errormsg += ", ";
                    }
                }
                errormsg += ` in section ${section_name}.`;

                if (
                    errormsg.includes("Phone_Device_Caller_ID_Name") ||
                    errormsg.includes("Phone_Device_Caller_ID_Number")
                ) {
                    errormsg += " Use Phone_Device_Caller_N... instead.";
                }
                if (errormsg.includes("Show_Dial_Button")) {
                    errormsg += " Use Show_Dial_Pad_Button";
                }
                if (errormsg.includes("LocalNXXprefix")) {
                    errormsg += " Use LocalNXXPrefix instead.";
                }
                errormsg += "\n";
            }
        }

        file.delete(section_name);
    }

    if (file.size > 0) {
        errormsg += "Unknown section";
        if (file.size > 1) {
            errormsg += "s";
        }
        errormsg += ": ";
        const entries = file.entries();
        for (let i = 0; i < file.size; i++) {
            errormsg += `${entries.next().value[0]}`;
            if (i < file.size - 1) {
                errormsg += ", ";
            }
        }
        errormsg += "\n";
    }
    if (errormsg) {
        console.error(errormsg);
        throw new Error(errormsg);
    }
    return [ret, errors];
}

export function gen_ini_file(input: NG911File): string {
    // TODO: set type of obj to NG911File
    const obj = JSON.parse(JSON.stringify(input));
    let ret = "";

    for (const section_name of Object.keys(little_schema)) {
        const sec_schema = little_schema[section_name];
        const sec = obj[section_name];

        if (sec) {
            ret += `[${section_name}]\n`;
            for (const key of Object.keys(sec_schema)) {
                const key_schema = sec_schema[key];

                switch (key_schema.t) {
                    case "boolean": {
                        const v = sec[key];
                        assert(
                            v === undefined || typeof v === "boolean",
                            `${key} is not a boolean`,
                        );
                        if (typeof v === "boolean") {
                            ret += `${key} = ${v ? "true" : "false"}\n\n`;
                        }
                        delete sec[key];
                        break;
                    }
                    case "string": {
                        const v = sec[key];
                        assert(
                            v === undefined || typeof v === "string",
                            `${key} is not a string`,
                        );
                        if (typeof v === "string" && v !== "") {
                            ret += `${key} = ${v}\n\n`;
                        }
                        delete sec[key];
                        break;
                    }
                    case "simple-string-array": {
                        const v = sec[key];
                        assert(
                            v === undefined || Array.isArray(v),
                            `${key} is not an array`,
                        );
                        if (v) {
                            for (const e of v) {
                                assert(
                                    typeof e === "string",
                                    `${key} has a non-string entry "${e}"`,
                                );
                                ret += `${key} = ${e}\n`;
                            }
                            ret += "\n";
                        }
                        delete sec[key];
                        break;
                    }
                    case "numbered-array": {
                        const v = sec[key];
                        assert(
                            v === undefined || Array.isArray(v),
                            `${key} is not an array`,
                        );
                        if (v) {
                            for (let i = 0; i < v.length; i++) {
                                const e = v[i];
                                assert(
                                    typeof e === "object",
                                    "index of numbered array is absent",
                                );
                                if (e) {
                                    for (const subkey of Object.keys(e)) {
                                        // console.log(subkey);
                                        const subkey_schema =
                                            key_schema.schema[subkey];
                                        assert(
                                            subkey_schema,
                                            "Unknown numbered array subkey schema",
                                        );

                                        switch (subkey_schema.t) {
                                            case "string": {
                                                assert(
                                                    typeof e[subkey] ===
                                                        "string" ||
                                                        e[subkey] === undefined,
                                                    `${key} in numbered array is not a string`,
                                                );
                                                if (
                                                    typeof e[subkey] ===
                                                    "string"
                                                ) {
                                                    ret += `${
                                                        key_schema.prefix
                                                    }${i + 1}${subkey} = ${
                                                        e[subkey]
                                                    }\n`;
                                                }
                                                break;
                                            }
                                            case "boolean": {
                                                assert(
                                                    typeof e[subkey] ===
                                                        "boolean" ||
                                                        e[subkey] === undefined,
                                                    `${key} is numbered array is not a boolean nor undefined.`,
                                                );
                                                if (e[subkey] !== undefined) {
                                                    ret += `${
                                                        key_schema.prefix
                                                    }${i + 1}${subkey} = ${
                                                        e[subkey]
                                                            ? "true"
                                                            : "false"
                                                    }\n`;
                                                }
                                                break;
                                            }
                                            case "simple-string-array": {
                                                assert(
                                                    e[subkey] === undefined ||
                                                        Array.isArray(
                                                            e[subkey],
                                                        ),
                                                    `${subkey} is not an array`,
                                                );
                                                if (e[subkey]) {
                                                    for (const v of e[subkey]) {
                                                        assert(
                                                            typeof v ===
                                                                "string",
                                                            `${subkey} has a non string value`,
                                                        );
                                                        ret += `${
                                                            key_schema.prefix
                                                        }${
                                                            i + 1
                                                        }${subkey} = ${v}\n`;
                                                    }
                                                }
                                                break;
                                            }
                                            case "simple-numbered-array": {
                                                assert(
                                                    e[subkey] === undefined ||
                                                        Array.isArray(
                                                            e[subkey],
                                                        ),
                                                    `${subkey} is not an array`,
                                                );
                                                if (e[subkey]) {
                                                    for (
                                                        let j = 0;
                                                        j < e[subkey].length;
                                                        j++
                                                    ) {
                                                        const v = e[subkey][j];
                                                        assert(
                                                            typeof v ===
                                                                "string",
                                                            `${subkey} has non string (simple-numbered-array)`,
                                                        );
                                                        ret += `${
                                                            key_schema.prefix
                                                        }${i + 1}${subkey}${
                                                            j + 1
                                                        } = ${v}\n`;
                                                    }
                                                }
                                                break;
                                            }
                                            case "optional-subsection": {
                                                // console.log("e", e[subkey]);
                                                for (const subbykey of Object.keys(
                                                    subkey_schema.schema,
                                                )) {
                                                    // console.log(subbykey, e[subkey][subbykey]);
                                                    switch (
                                                        subkey_schema.schema[
                                                            subbykey
                                                        ].t
                                                    ) {
                                                        case "string":
                                                            if (
                                                                e[subkey][
                                                                    subbykey
                                                                ]
                                                            ) {
                                                                ret += `${
                                                                    key_schema.prefix
                                                                }${
                                                                    i + 1
                                                                }${subbykey} = ${
                                                                    e[subkey][
                                                                        subbykey
                                                                    ]
                                                                }\n`;
                                                            }
                                                            break;
                                                        case "simple-string-array":
                                                            assert(
                                                                Array.isArray(
                                                                    e[subkey][
                                                                        subbykey
                                                                    ],
                                                                ) ||
                                                                    e[subkey][
                                                                        subbykey
                                                                    ] ===
                                                                        undefined,
                                                                `Optional subsection ${subkey}'s ${subbykey} is not an array.`,
                                                            );
                                                            if (
                                                                e[subkey][
                                                                    subbykey
                                                                ]
                                                            ) {
                                                                for (const val of e[
                                                                    subkey
                                                                ][subbykey]) {
                                                                    assert(
                                                                        typeof val ===
                                                                            "string",
                                                                        `${subkey} in optional subsection has a non string value.`,
                                                                    );
                                                                    if (
                                                                        val ||
                                                                        (v[i][
                                                                            subkey
                                                                        ][
                                                                            subbykey
                                                                        ]
                                                                            .length >
                                                                            1 &&
                                                                            v[
                                                                                i
                                                                            ][
                                                                                subkey
                                                                            ][
                                                                                subbykey
                                                                            ][0])
                                                                    ) {
                                                                        ret += `${
                                                                            key_schema.prefix
                                                                        }${
                                                                            i +
                                                                            1
                                                                        }${subbykey} = ${val}\n`;
                                                                    }
                                                                }
                                                            }
                                                            break;
                                                        default:
                                                            throw new Error(
                                                                `Unhandled subbykey type in numbered array ${JSON.stringify(
                                                                    subkey_schema
                                                                        .schema[
                                                                        subbykey
                                                                    ],
                                                                )}`,
                                                            );
                                                    }
                                                }
                                                break;
                                            }
                                            default:
                                                throw new Error(
                                                    `TODO: GEN THE SUBKEY ${subkey}`,
                                                );
                                        }
                                    }
                                    ret += "\n";
                                }
                            }
                        }
                        delete sec[key];
                        break;
                    }
                    case "array": {
                        const v = sec[key];
                        if (v) {
                            for (const f of v) {
                                for (const k of Object.keys(
                                    key_schema.schema,
                                )) {
                                    switch (key_schema.schema[k].t) {
                                        case "boolean":
                                            assert(
                                                typeof f[k] === "boolean" ||
                                                    f[k] === undefined,
                                                `${key} in array ${key} is not a boolean nor undefined`,
                                            );
                                            if (typeof f[k] === "boolean") {
                                                ret += `${k} = ${
                                                    f[k] ? "true" : "false"
                                                }\n`;
                                            } else {
                                                ret += `${k} = \n`;
                                            }
                                            break;
                                        case "string":
                                            ret += `${k} = ${f[k]}\n`;
                                            break;
                                    }
                                }
                                ret += "\n";
                            }
                        }
                        delete sec[key];
                        break;
                    }
                    case "complex-array-in-array": {
                        const v = sec[key];
                        if (v) {
                            for (const f of v) {
                                if (v.length === 1) {
                                    for (const k of Object.keys(
                                        key_schema.outer_schema,
                                    )) {
                                        if (f[k]) {
                                            ret += `${k} = ${f[k]}\n`;
                                        }
                                    }
                                } else {
                                    for (const k of Object.keys(v[0])) {
                                        if (k !== key_schema.inner_array_name) {
                                            ret += `${k} = ${f[k]}\n`;
                                        }
                                    }
                                }

                                const arr = f[key_schema.inner_array_name];
                                assert(
                                    Array.isArray(arr),
                                    "that arr is not an array for the complex-array-in-array",
                                );
                                ret += `${key_schema.inner_length_name} = ${arr.length}\n`;
                                for (let i = 0; i < arr.length; i++) {
                                    for (const k of Object.keys(
                                        key_schema.inner_schema,
                                    )) {
                                        switch (key_schema.inner_schema[k].t) {
                                            case "string":
                                                ret += `${
                                                    key_schema.inner_prefix
                                                }${i + 1}${k} = ${arr[i][k]}\n`;
                                                break;
                                            case "boolean":
                                                assert(
                                                    typeof arr[i][k] ===
                                                        "boolean" ||
                                                        arr[i][k] === undefined,
                                                );
                                                if (arr[i][k] !== undefined) {
                                                    ret += `${
                                                        key_schema.inner_prefix
                                                    }${i + 1}${k} = ${
                                                        arr[i][k]
                                                            ? "true"
                                                            : "false"
                                                    }\n`;
                                                } else {
                                                    ret += `${
                                                        key_schema.inner_prefix
                                                    }${i + 1}${k} = \n`;
                                                }
                                                break;
                                        }
                                    }
                                }

                                ret += "\n";
                            }
                        }
                        delete sec[key];
                        break;
                    }
                    case "optional-subsection": {
                        const v = sec[key];
                        if (v) {
                            assert(
                                typeof v === "object" && v !== null,
                                `${key} is not object`,
                            );
                            for (const subkey of Object.keys(
                                key_schema.schema,
                            )) {
                                switch (key_schema.schema[subkey].t) {
                                    case "string":
                                        if (v[subkey]) {
                                            ret += `${subkey} = ${v[subkey]}\n`;
                                        }
                                        break;
                                    case "boolean":
                                        assert(
                                            typeof v[subkey] === "boolean" ||
                                                v[subkey] === undefined,
                                            `${subkey} in optional subsection is not a boolean nor undefined.`,
                                        );
                                        if (typeof v[subkey] === "boolean") {
                                            ret += `${subkey} = ${
                                                v[subkey] ? "true" : "false"
                                            }\n`;
                                        }
                                        break;
                                    case "simple-string-array":
                                        assert(
                                            Array.isArray(v[subkey]) ||
                                                v[subkey] === undefined,
                                            `${subkey} is not an array in optional subsection ${key}.`,
                                        );
                                        if (v[subkey]) {
                                            for (const val of v[subkey]) {
                                                assert(
                                                    typeof val === "string",
                                                    `${subkey} in optional subsection has a non string value.`,
                                                );
                                                if (
                                                    val ||
                                                    (v[subkey].length > 1 &&
                                                        v[subkey][0])
                                                ) {
                                                    ret += `${subkey} = ${val}\n`;
                                                }
                                            }
                                        }
                                        break;
                                    default:
                                        throw new Error(
                                            `Optional subsection had unhandeled type ${key_schema.schema[subkey].t}`,
                                        );
                                }
                            }
                            ret += "\n";
                        }
                        delete sec[key];
                        break;
                    }
                    default:
                        throw new Error(
                            `Unknown schema ${JSON.stringify(key_schema)}\n`,
                        );
                }
            }

            const keys = Object.keys(sec);
            if (keys.length > 0) {
                throw new Error("unknown key " + keys[0] + "\n");
            }
        }

        delete obj[section_name];
    }

    const keys = Object.keys(obj);
    if (keys.length > 0) {
        throw new Error(
            keys[0] === "0"
                ? `There was an error parsing. Make sure all values are correct and there are no errors in the editor.\n`
                : "unknown section " + keys[0],
        );
    }

    return ret;
}
