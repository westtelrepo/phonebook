import {
    COMMA_SEPARATED_INTEGERS_PATTERN,
    DEFAULT_PATTERN,
    EMAIL_PATTERN,
    IPV4_PATTERN,
    IPV4_PORT_PATTERN,
    NUMBER_PATTERN,
    PORT_PATTERN,
} from "./schema/schema";

const default_regexp = new RegExp(DEFAULT_PATTERN);

function pattern_message(pattern: string | RegExp) {
    switch (pattern) {
        case IPV4_PATTERN:
            return `Must be a valid IPv4 address, for example 192.0.2.42`;
        case PORT_PATTERN:
            return `Must be a valid port number 0-65535`;
        case IPV4_PORT_PATTERN:
            return `Must be an IPv4 address and port, for example: "192.0.2.37, 43212`;
        case NUMBER_PATTERN:
            return `Must be a non-negative integer`;
        case EMAIL_PATTERN:
            return `Must be an email address`;
        case COMMA_SEPARATED_INTEGERS_PATTERN:
            return `Must be a list of non-negative integers seperated by commas, for example: "42, 5, 3, 777"`;
        default:
            return `Field does not match ${
                typeof pattern === "string" ? pattern : pattern.source
            }`;
    }
}

export function verify_field(
    field: string | undefined,
    options: {
        pattern?: string | RegExp;
        enum?: (string | { readonly value: string; readonly name: string })[];
        required?: boolean;
    },
): { validityMessage: string } | null {
    if (typeof field === "string" && !default_regexp.test(field)) {
        return {
            validityMessage: "Contains non printable or non-ASCII characters.",
        };
    }

    if (typeof field === "string" && options.pattern !== undefined) {
        const pattern =
            typeof options.pattern === "string"
                ? new RegExp(options.pattern)
                : options.pattern;
        if (!pattern.test(field)) {
            return {
                validityMessage: pattern_message(options.pattern),
            };
        }
    }

    if (typeof field === "string" && options.enum !== undefined) {
        if (
            !options.enum.some(x =>
                typeof x === "string" ? x === field : x.value === field,
            )
        ) {
            return {
                validityMessage: `Must be one of ${JSON.stringify(
                    options.enum.map(x =>
                        typeof x === "string" ? x : x.value,
                    ),
                )}`,
            };
        }
    }

    if (options.required ?? false) {
        if (field === undefined || field === "") {
            return {
                validityMessage: "Is required and must be non-empty.",
            };
        }
    }

    return null;
}
