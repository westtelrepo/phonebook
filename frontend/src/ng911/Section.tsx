/** @jsx jsx */
import { css, jsx } from "@emotion/core";

import { Edit } from "./Edit";
import { NGTopLevelValue } from "./ng911ini";
import { little_schema } from "./schema/schema";

const table_style = css`
    display: table;

    .row {
        display: table-row;
    }

    .item_name {
        display: table-cell;
        padding-top: 5px;
        font-weight: bold;
    }

    .item {
        display: table-cell;
        padding-bottom: 5px;
        white-space: pre-wrap;
    }

    .item:nth-child(1) {
        padding-right: 5px;
        width: 465px;
    }

    .item:nth-child(2) {
        padding-left: 5px;
    }

    .array_chunk {
        margin-top: 20px;
        margin-bottom: 20px;
        padding-left: 30px;
    }
`;

export function Section({
    schema,
    state,
    onChange,
    add,
    remove,
}: {
    schema: typeof little_schema["TELEPHONE"];
    state: Record<string, NGTopLevelValue> | undefined;
    onChange: (key: string, newValue: NGTopLevelValue) => void;
    add: () => void;
    remove: () => void;
}) {
    return (
        <div style={{ marginTop: 75 }}>
            {state === undefined ? (
                <button onClick={() => add()}>Add</button>
            ) : (
                <button
                    onClick={() => {
                        if (
                            window.confirm(
                                "Do you want to delete this section?",
                            )
                        ) {
                            remove();
                        }
                    }}
                >
                    Delete!
                </button>
            )}
            <div css={table_style}>
                {state
                    ? Object.keys(schema).map(e => (
                          <Edit
                              key={e}
                              name={e}
                              schema={schema[e]}
                              state={state[e]}
                              onChange={n => onChange(e, n)}
                          />
                      ))
                    : undefined}
            </div>
        </div>
    );
}
