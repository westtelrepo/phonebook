import Immutable from "immutable";
import React, { ReactNode, useContext, useState } from "react";

const CollapseStateContext = React.createContext<Immutable.Map<
    string,
    boolean
> | null>(null);
const UpdateStateContext = React.createContext<React.Dispatch<
    React.SetStateAction<Immutable.Map<string, boolean>>
> | null>(null);

export function CollapseProvider(props: { children: ReactNode }) {
    const [collapse, set_collapse] = useState(Immutable.Map<string, boolean>());

    return (
        <CollapseStateContext.Provider value={collapse}>
            <UpdateStateContext.Provider value={set_collapse}>
                {props.children}
            </UpdateStateContext.Provider>
        </CollapseStateContext.Provider>
    );
}

export function useCollapse(
    name: string,
): {
    collapsed: boolean;
    flip_collapsed: () => void;
    set_collapsed: (v: boolean) => void;
} {
    const collapse = useContext(CollapseStateContext);
    const set_collapse = useContext(UpdateStateContext);

    if (!collapse || !set_collapse) {
        throw new Error("useCollapse used without a CollapseProvider");
    }

    return {
        collapsed: collapse.get(name) ?? false,
        flip_collapsed: () =>
            set_collapse(v => v.set(name, !v.get(name, false))),
        set_collapsed: (n: boolean) => set_collapse(v => v.set(name, n)),
    };
}
