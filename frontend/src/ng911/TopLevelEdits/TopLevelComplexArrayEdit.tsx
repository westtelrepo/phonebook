import assert from "assert";
// eslint-disable-next-line emotion/no-vanilla
import { css, cx } from "emotion";
import React, { useState } from "react";

import { NGComplexArray } from "../ng911ini";
import { SchemaComplexArray } from "../schema/InterfaceSchemas";
import { useCollapse } from "../useCollapse";
import { StringInArrayEdit, TopLevelArrayEdit } from "./TopLevelArrayEdit";

export function TopLevelComplexArrayEdit(props: {
    name: string;
    schema: SchemaComplexArray;
    onChange: (newValue: NGComplexArray) => void;
    state: NGComplexArray | undefined;
}) {
    const { name, schema, onChange, state } = props;
    const { collapsed, set_collapsed, flip_collapsed } = useCollapse(name);
    const the_style = css`
        button {
            margin-left: 5px;
            margin-right: 5px;
        }
        input {
            margin-top: 2px;
            margin-bottom: 2px;
        }
    `;
    const [currentIndex, set_currentIndex] = useState(0);
    const tempschema: SchemaComplexArray = {
        ...schema,
        outer_schema:
            state !== undefined
                ? Object.fromEntries(
                      Object.entries(schema.outer_schema).filter(
                          ([subkey]) => state[0][subkey],
                      ),
                  )
                : {},
    };
    return (
        <>
            <div className="row">
                <div className={cx(the_style, "item_name")}>
                    <button onClick={flip_collapsed}>
                        {collapsed ? "⯅" : "⯆"}
                    </button>
                    {name}
                    {schema.required ? <span className="required">*</span> : ""}
                    <button
                        onClick={() => {
                            if (state === undefined) {
                                const obj: Record<
                                    string,
                                    | undefined
                                    | string
                                    | Record<string, string | undefined>[]
                                > = {};
                                for (const m of Object.keys(
                                    schema.outer_schema,
                                )) {
                                    const s = schema.outer_schema[m];
                                    switch (s.t) {
                                        case "string":
                                            obj[m] = "";
                                            break;
                                        default:
                                            throw new Error(
                                                "programming error initializing the array individual object",
                                            );
                                    }
                                }
                                const arr: Record<
                                    string,
                                    string | undefined
                                >[] = [{}];
                                for (const m of Object.keys(
                                    schema.inner_schema,
                                )) {
                                    const s = schema.inner_schema[m];
                                    switch (s.t) {
                                        case "string":
                                            arr[0][m] = "";
                                            break;
                                        case "boolean":
                                            arr[0][m] = undefined;
                                            break;
                                        default:
                                            throw new Error(
                                                "Unhandled inner array type",
                                            );
                                    }
                                }
                                obj[schema.inner_array_name] = arr;
                                onChange([obj]);
                            } else {
                                const obj: Record<
                                    string,
                                    | undefined
                                    | string
                                    | Record<string, string | undefined>[]
                                > = {};
                                for (const m of Object.keys(state[0])) {
                                    const s = state[0][m];
                                    switch (typeof s) {
                                        case "string":
                                            if (s) {
                                                obj[m] = " ";
                                            }
                                            break;
                                        case "object": {
                                            const arr: Record<
                                                string,
                                                string | undefined
                                            >[] = [{}];
                                            for (const m of Object.keys(
                                                schema.inner_schema,
                                            )) {
                                                const s =
                                                    schema.inner_schema[m];
                                                switch (s.t) {
                                                    case "string":
                                                        arr[0][m] = "";
                                                        break;
                                                    case "boolean":
                                                        arr[0][m] = undefined;
                                                        break;
                                                    default:
                                                        throw new Error(
                                                            "Unhandled inner array type",
                                                        );
                                                }
                                            }
                                            obj[schema.inner_array_name] = arr;
                                            break;
                                        }
                                        default:
                                            throw new Error(
                                                "programming error initializing the array individual object",
                                            );
                                    }
                                }
                                onChange([...state, obj]);
                            }
                            set_currentIndex(state?.length ?? 0);
                            set_collapsed(false);
                        }}
                    >
                        +
                    </button>
                    {state !== undefined && state.length > 0 ? (
                        <button
                            onClick={() => {
                                if (schema.required && state.length === 1) {
                                    const obj: any = {};
                                    for (const m of Object.keys(
                                        schema.outer_schema,
                                    )) {
                                        const s = schema.outer_schema[m];
                                        switch (s.t) {
                                            case "string":
                                                obj[m] = " ";
                                                break;
                                            default:
                                                throw new Error(
                                                    "programming error initializing the array individual object",
                                                );
                                        }
                                    }
                                    const arr: any = [{}];
                                    for (const m of Object.keys(
                                        schema.inner_schema,
                                    )) {
                                        const s = schema.inner_schema[m];
                                        switch (s.t) {
                                            case "string":
                                                arr[0][m] = " ";
                                                break;
                                            case "boolean":
                                                arr[0][m] = undefined;
                                                break;
                                            default:
                                                throw new Error(
                                                    "Unhandled inner array type",
                                                );
                                        }
                                    }
                                    obj[schema.inner_array_name] = arr;
                                    onChange([obj]);
                                } else {
                                    state.splice(currentIndex, 1);
                                    onChange(state);
                                }

                                if (state.length <= 1) {
                                    set_currentIndex(0);
                                } else if (currentIndex >= state.length) {
                                    set_currentIndex(state.length - 1);
                                }

                                set_collapsed(false);
                            }}
                        >
                            Del
                        </button>
                    ) : null}
                </div>
            </div>
            {collapsed || state === undefined ? null : (
                <>
                    <h4 style={{ margin: 0 }}>{`Phone ${currentIndex + 1}`}</h4>
                    {state.map((item, index: number) => {
                        return (
                            <button
                                key={index}
                                onClick={() => {
                                    set_currentIndex(index);
                                    set_collapsed(false);
                                }}
                                disabled={currentIndex === index}
                            >
                                {`Phone ${index + 1}`}
                            </button>
                        );
                    })}
                </>
            )}
            {collapsed || state === undefined ? null : (
                <>
                    {
                        // eslint-disable-next-line @typescript-eslint/strict-boolean-expressions, no-nested-ternary
                        state[currentIndex] ? (
                            <React.Fragment key={currentIndex}>
                                {currentIndex === 0
                                    ? Object.keys(schema.outer_schema).map(
                                          e => {
                                              const a_schema =
                                                  schema.outer_schema[e];
                                              switch (a_schema.t) {
                                                  case "string": {
                                                      const v =
                                                          state[currentIndex][
                                                              e
                                                          ];
                                                      assert(
                                                          typeof v ===
                                                              "string" ||
                                                              v === undefined,
                                                      );

                                                      return (
                                                          <div
                                                              className={cx(
                                                                  "row",
                                                              )}
                                                              key={e}
                                                          >
                                                              <div
                                                                  className={cx(
                                                                      "item",
                                                                      "keyvalue",
                                                                  )}
                                                              >
                                                                  <StringInArrayEdit
                                                                      key={e}
                                                                      name={e}
                                                                      onChange={newValue => {
                                                                          const new_state = [
                                                                              ...state,
                                                                          ];
                                                                          new_state[
                                                                              currentIndex
                                                                          ] = {
                                                                              ...state[
                                                                                  currentIndex
                                                                              ],
                                                                              [e]: newValue,
                                                                          };
                                                                          onChange(
                                                                              new_state,
                                                                          );
                                                                      }}
                                                                      a_schema={
                                                                          a_schema
                                                                      }
                                                                      value={v}
                                                                  />
                                                              </div>
                                                              <div className="item">
                                                                  {
                                                                      a_schema.documentation
                                                                  }
                                                              </div>
                                                          </div>
                                                      );
                                                  }
                                                  default:
                                                      throw new Error(
                                                          `Unhandled complex array type`,
                                                      );
                                              }
                                          },
                                      )
                                    : Object.keys(tempschema.outer_schema).map(
                                          e => {
                                              const a_schema =
                                                  tempschema.outer_schema[e];
                                              switch (a_schema.t) {
                                                  case "string": {
                                                      const v =
                                                          state[currentIndex][
                                                              e
                                                          ];
                                                      assert(
                                                          typeof v ===
                                                              "string" ||
                                                              v === undefined,
                                                      );
                                                      return (
                                                          <div
                                                              className={cx(
                                                                  "row",
                                                              )}
                                                              key={e}
                                                          >
                                                              <div
                                                                  className={cx(
                                                                      "item",
                                                                      "keyvalue",
                                                                  )}
                                                              >
                                                                  <StringInArrayEdit
                                                                      key={e}
                                                                      name={e}
                                                                      onChange={newValue => {
                                                                          const new_state = [
                                                                              ...state,
                                                                          ];
                                                                          new_state[
                                                                              currentIndex
                                                                          ] = {
                                                                              ...state[
                                                                                  currentIndex
                                                                              ],
                                                                              [e]: newValue,
                                                                          };
                                                                          onChange(
                                                                              new_state,
                                                                          );
                                                                      }}
                                                                      a_schema={
                                                                          a_schema
                                                                      }
                                                                      value={v}
                                                                  />
                                                              </div>
                                                              <div className="item">
                                                                  {
                                                                      a_schema.documentation
                                                                  }
                                                              </div>
                                                          </div>
                                                      );
                                                  }
                                                  default:
                                                      throw new Error(
                                                          `Unhandled complex array type`,
                                                      );
                                              }
                                          },
                                      )}
                                <TopLevelArrayEdit
                                    name={schema.inner_array_name}
                                    schema={{
                                        t: "numbered-array",
                                        schema: schema.inner_schema,
                                        prefix: schema.inner_prefix,
                                        required: schema.required,
                                    }}
                                    onChange={newValue => {
                                        const new_state = [...state];
                                        new_state[currentIndex] = {
                                            ...state[currentIndex],
                                            [schema.inner_array_name]: newValue as any,
                                        };
                                        onChange(new_state);
                                    }}
                                    state={
                                        state[currentIndex][
                                            schema.inner_array_name
                                        ] as any
                                    }
                                />
                            </React.Fragment>
                        ) : schema.required ? (
                            <>
                                {Object.keys(schema.outer_schema).map(e => {
                                    const a_schema = schema.outer_schema[e];
                                    switch (a_schema.t) {
                                        case "string":
                                            return (
                                                <div
                                                    className={cx("row")}
                                                    key={e}
                                                >
                                                    <div
                                                        className={cx(
                                                            "item",
                                                            "keyvalue",
                                                        )}
                                                    >
                                                        <StringInArrayEdit
                                                            key={e}
                                                            name={e}
                                                            onChange={newValue => {
                                                                onChange([
                                                                    {
                                                                        [e]: newValue,
                                                                    },
                                                                ]);
                                                            }}
                                                            a_schema={a_schema}
                                                            value={""}
                                                        />
                                                    </div>
                                                    <div className="item">
                                                        {a_schema.documentation}
                                                    </div>
                                                </div>
                                            );
                                        default:
                                            throw new Error(
                                                `Unhandled complex array type`,
                                            );
                                    }
                                })}
                                <TopLevelArrayEdit
                                    name={schema.inner_array_name}
                                    schema={{
                                        t: "numbered-array",
                                        schema: schema.inner_schema,
                                        prefix: schema.inner_prefix,
                                        required: schema.required,
                                    }}
                                    onChange={newValue => {
                                        onChange([
                                            {
                                                [schema.inner_array_name]: newValue as any,
                                            },
                                        ]);
                                    }}
                                    state={[]}
                                />
                            </>
                        ) : null
                    }
                </>
            )}
        </>
    );
}
