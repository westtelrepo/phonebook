// eslint-disable-next-line emotion/no-vanilla
import { css, cx } from "emotion";
import React from "react";

import { DatalistInput } from "../inputs/DatalistInput";
import { SchemaSimpleStringArray } from "../schema/InterfaceSchemas";
import { DEFAULT_PATTERN } from "../schema/schema";

export function TopLevelSimpleStringArrayEdit(props: {
    name: string;
    onChange: (newValue: string[]) => void;
    schema: SchemaSimpleStringArray;
    state: string[] | undefined;
}) {
    const { name, onChange, schema } = props;
    const state = props.state ?? [];

    const the_style = css`
        button {
            margin-left: 5px;
            margin-right: 5px;
        }
        input {
            margin-top: 2px;
            margin-bottom: 2px;
        }
    `;
    return (
        <>
            <div className="row">
                <div
                    className={cx(the_style, "item_name")}
                    style={{ fontFamily: "Consolas,monospace" }}
                >
                    {schema.deprecated ?? false ? (
                        <span className="deprecated">{name}</span>
                    ) : (
                        name
                    )}
                    {schema.required ? <span className="required">*</span> : ""}
                    <button
                        onClick={() => {
                            onChange([...state, ""]);
                        }}
                    >
                        +
                    </button>
                    {state.length > 0 ? (
                        <button
                            onClick={() => {
                                onChange(state.slice(0, -1));
                            }}
                        >
                            -
                        </button>
                    ) : null}
                </div>
            </div>
            <div className="row">
                <div className={cx(the_style, "keyvalue", "item")}>
                    {state.map((e: string, i: number) => (
                        <React.Fragment key={i}>
                            <DatalistInput
                                type="text"
                                value={e ?? ""}
                                pattern={schema.pattern ?? DEFAULT_PATTERN}
                                schema={schema}
                                onChange={event => {
                                    const n = [...state];
                                    n[i] = event.target.value;
                                    onChange(n);
                                }}
                            />
                            <br />
                        </React.Fragment>
                    ))}
                </div>
                <div className="item">{schema.documentation}</div>
            </div>
        </>
    );
}
