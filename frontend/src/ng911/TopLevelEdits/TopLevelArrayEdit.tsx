/* eslint-disable @typescript-eslint/no-non-null-assertion, no-nested-ternary */
import assert from "assert";
// eslint-disable-next-line emotion/no-vanilla
import { css, cx } from "emotion";
import React from "react";

import { DatalistInput } from "../inputs/DatalistInput";
import { FancySelect } from "../inputs/FancySelect";
import { TriStateCheckbox } from "../inputs/TriStateCheckbox";
import {
    is_string_array,
    NGArray,
    NGNumberedArray,
    NGOptionalSubsection,
} from "../ng911ini";
import {
    SchemaArray,
    SchemaBoolean,
    SchemaNumberedArray,
    SchemaOptionalSubsection,
    SchemaSimpleNumberedArray,
    SchemaSimpleStringArray,
    SchemaString,
} from "../schema/InterfaceSchemas";
import { DEFAULT_PATTERN } from "../schema/schema";
import { useCollapse } from "../useCollapse";
import { useUniqueID } from "../useUniqueID";

export function TopLevelArrayEdit(props: {
    name: string;
    schema: SchemaNumberedArray | SchemaArray;
    onChange: (newValue: NGArray | NGNumberedArray) => void;
    state: NGNumberedArray | undefined;
}) {
    const { name, schema, onChange, state } = props;
    const { collapsed, set_collapsed, flip_collapsed } = useCollapse(name);
    const the_style = css`
        button {
            margin-left: 5px;
            margin-right: 5px;
        }
        input {
            margin-top: 2px;
            margin-bottom: 2px;
        }
    `;
    return (
        <>
            <div className="row">
                <div className={cx(the_style, "item_name")}>
                    <button onClick={flip_collapsed}>
                        {collapsed ? "⯅" : "⯆"}
                    </button>
                    {name}
                    {schema.required ? <span className="required">*</span> : ""}
                    <button
                        onClick={() => {
                            const obj: Record<
                                string,
                                undefined | boolean | string | string[]
                            > = {};
                            for (const m of Object.keys(schema.schema)) {
                                const s = schema.schema[m];
                                switch (s.t) {
                                    case "boolean":
                                        obj[m] = undefined;
                                        break;
                                    case "string":
                                        obj[m] = "";
                                        break;
                                    case "simple-numbered-array":
                                        obj[m] = [];
                                        break;
                                    case "simple-string-array":
                                        obj[m] = [""];
                                        break;
                                    case "optional-subsection":
                                        obj[m] = undefined;
                                        break;
                                    default:
                                        throw new Error(
                                            "programming error initializing the array individual object",
                                        );
                                }
                            }
                            onChange(state ? [...state, obj] : [obj]);
                            set_collapsed(false);
                        }}
                    >
                        +
                    </button>
                    {state && state.length > 0 ? (
                        <button
                            onClick={() => {
                                if (schema.required && state.length === 1) {
                                    const obj: Record<
                                        string,
                                        boolean | string | string[] | undefined
                                    > = {};
                                    for (const m of Object.keys(
                                        schema.schema,
                                    )) {
                                        const s = schema.schema[m];
                                        switch (s.t) {
                                            case "boolean":
                                                obj[m] = undefined;
                                                break;
                                            case "string":
                                                obj[m] = "";
                                                break;
                                            case "simple-numbered-array":
                                                obj[m] = [];
                                                break;
                                            case "simple-string-array":
                                                obj[m] = [""];
                                                break;
                                            case "optional-subsection":
                                                obj[m] = undefined;
                                                break;
                                            default:
                                                throw new Error(
                                                    "programming error initializing the array individual object",
                                                );
                                        }
                                    }
                                    onChange([obj]);
                                } else {
                                    onChange(state.slice(0, -1));
                                }
                                set_collapsed(false);
                            }}
                        >
                            -
                        </button>
                    ) : null}
                </div>
            </div>
            {collapsed ? null : (
                <>
                    {state && state.length > 0 ? (
                        state.map((item, index) => (
                            <React.Fragment key={index}>
                                {Object.keys(schema.schema).map(e => {
                                    const a_schema = schema.schema[e];
                                    switch (a_schema.t) {
                                        case "string": {
                                            const v = item[e];
                                            assert(
                                                typeof v === "string" ||
                                                    v === undefined,
                                            );
                                            return (
                                                <div
                                                    className={cx("row")}
                                                    key={e}
                                                >
                                                    <div
                                                        className={cx(
                                                            "item",
                                                            "keyvalue",
                                                            "array_chunk",
                                                        )}
                                                    >
                                                        <StringInArrayEdit
                                                            key={e}
                                                            name={
                                                                schema.t ===
                                                                "numbered-array"
                                                                    ? `${
                                                                          schema.prefix
                                                                      }${
                                                                          index +
                                                                          1
                                                                      }${e}`
                                                                    : e
                                                            }
                                                            onChange={newValue => {
                                                                const new_state = [
                                                                    ...state,
                                                                ];
                                                                new_state[
                                                                    index
                                                                ] = {
                                                                    ...state[
                                                                        index
                                                                    ],
                                                                    [e]: newValue,
                                                                };
                                                                onChange(
                                                                    new_state,
                                                                );
                                                            }}
                                                            a_schema={a_schema}
                                                            value={v}
                                                        />
                                                    </div>
                                                    <div className="item">
                                                        {a_schema.documentation}
                                                    </div>
                                                </div>
                                            );
                                        }
                                        case "boolean": {
                                            const v = item[e];
                                            assert(
                                                typeof v === "boolean" ||
                                                    v === undefined,
                                            );
                                            return (
                                                <div
                                                    className={cx("row")}
                                                    key={e}
                                                >
                                                    <div
                                                        className={cx(
                                                            "item",
                                                            "keyvalue",
                                                            "array_chunk",
                                                        )}
                                                    >
                                                        <BooleanInArrayEdit
                                                            key={e}
                                                            name={
                                                                schema.t ===
                                                                "numbered-array"
                                                                    ? `${
                                                                          schema.prefix
                                                                      }${
                                                                          index +
                                                                          1
                                                                      }${e}`
                                                                    : e
                                                            }
                                                            onChange={newValue => {
                                                                const new_state = [
                                                                    ...state,
                                                                ];
                                                                new_state[
                                                                    index
                                                                ] = {
                                                                    ...state[
                                                                        index
                                                                    ],
                                                                    [e]: newValue,
                                                                };
                                                                onChange(
                                                                    new_state,
                                                                );
                                                            }}
                                                            value={v}
                                                            a_schema={a_schema}
                                                        />
                                                    </div>
                                                    <div className="item">
                                                        {a_schema.documentation}
                                                    </div>
                                                </div>
                                            );
                                        }
                                        case "simple-string-array": {
                                            const v = item[e];
                                            assert(
                                                is_string_array(v) ||
                                                    v === undefined,
                                            );
                                            return (
                                                <div
                                                    className={cx("row")}
                                                    key={e}
                                                >
                                                    <div
                                                        className={cx(
                                                            "item",
                                                            "keyvalue",
                                                            "array_chunk",
                                                        )}
                                                    >
                                                        <SimpleStringArrayInArrayEdit
                                                            key={e}
                                                            name={
                                                                schema.t ===
                                                                "numbered-array"
                                                                    ? `${
                                                                          schema.prefix
                                                                      }${
                                                                          index +
                                                                          1
                                                                      }${e}`
                                                                    : e
                                                            }
                                                            onChange={nv => {
                                                                const new_state = [
                                                                    ...state,
                                                                ];
                                                                new_state[
                                                                    index
                                                                ] = {
                                                                    ...state[
                                                                        index
                                                                    ],
                                                                    [e]: nv,
                                                                };
                                                                onChange(
                                                                    new_state,
                                                                );
                                                            }}
                                                            a_schema={a_schema}
                                                            value={v}
                                                        />
                                                    </div>
                                                    <div className="item">
                                                        {a_schema.documentation}
                                                    </div>
                                                </div>
                                            );
                                        }
                                        case "simple-numbered-array": {
                                            const v = item[e];
                                            assert(
                                                is_string_array(v) ||
                                                    v === undefined,
                                            );
                                            return (
                                                <div
                                                    className={cx("row")}
                                                    key={e}
                                                >
                                                    <div
                                                        className={cx(
                                                            "item",
                                                            "keyvalue",
                                                            "array_chunk",
                                                        )}
                                                    >
                                                        <SimpleNumberedArrayInArrayEdit
                                                            key={e}
                                                            prefix={
                                                                schema.t ===
                                                                "numbered-array"
                                                                    ? `${
                                                                          schema.prefix
                                                                      }${
                                                                          index +
                                                                          1
                                                                      }${e}`
                                                                    : e
                                                            }
                                                            onChange={nv => {
                                                                const new_state = [
                                                                    ...state,
                                                                ];
                                                                new_state[
                                                                    index
                                                                ] = {
                                                                    ...state[
                                                                        index
                                                                    ],
                                                                    [e]: nv,
                                                                };
                                                                onChange(
                                                                    new_state,
                                                                );
                                                            }}
                                                            a_schema={a_schema}
                                                            value={v}
                                                        />
                                                    </div>
                                                    <div className="item">
                                                        {a_schema.documentation}
                                                    </div>
                                                </div>
                                            );
                                        }
                                        case "optional-subsection": {
                                            const v = item[e];
                                            assert(
                                                (typeof v === "object" ||
                                                    v === undefined) &&
                                                    !Array.isArray(v),
                                            );
                                            return (
                                                <React.Fragment key={e}>
                                                    <div
                                                        className={cx(
                                                            "item",
                                                            "array_chunk",
                                                        )}
                                                        key={e}
                                                    >
                                                        <OptionalSubSectionInArrayEdit
                                                            key={e}
                                                            schema={a_schema}
                                                            onChange={nv => {
                                                                const new_state = [
                                                                    ...state,
                                                                ];
                                                                new_state[
                                                                    index
                                                                ] = {
                                                                    ...state[
                                                                        index
                                                                    ],
                                                                    [e]: nv,
                                                                };
                                                                onChange(
                                                                    new_state,
                                                                );
                                                            }}
                                                            state={v}
                                                        />
                                                    </div>
                                                    <div
                                                        className={cx(
                                                            "item",
                                                            "row",
                                                        )}
                                                    >
                                                        {v
                                                            ? a_schema.documentation
                                                            : null}
                                                    </div>
                                                </React.Fragment>
                                            );
                                        }
                                        default:
                                            throw new Error(
                                                `Unhandled numbered array type: ${JSON.stringify(
                                                    a_schema,
                                                )}`,
                                            );
                                    }
                                })}
                            </React.Fragment>
                        ))
                    ) : schema.required ? (
                        <>
                            {Object.keys(schema.schema).map(e => {
                                const a_schema = schema.schema[e];
                                switch (a_schema.t) {
                                    case "string":
                                        return (
                                            <div className={cx("row")} key={e}>
                                                <div
                                                    className={cx(
                                                        "item",
                                                        "keyvalue",
                                                        "array_chunk",
                                                    )}
                                                >
                                                    <StringInArrayEdit
                                                        key={e}
                                                        name={
                                                            schema.t ===
                                                            "numbered-array"
                                                                ? `${
                                                                      schema.prefix
                                                                  }${1}${e}`
                                                                : e
                                                        }
                                                        onChange={newValue => {
                                                            const new_state = [
                                                                ...state!,
                                                            ];
                                                            new_state[0] = {
                                                                ...state![0],
                                                                [e]: newValue,
                                                            };
                                                            onChange(new_state);
                                                        }}
                                                        a_schema={a_schema}
                                                        value={""}
                                                    />
                                                </div>
                                                <div className="item">
                                                    {a_schema.documentation}
                                                </div>
                                            </div>
                                        );
                                    case "boolean":
                                        return (
                                            <div className={cx("row")} key={e}>
                                                <div
                                                    className={cx(
                                                        "item",
                                                        "keyvalue",
                                                        "array_chunk",
                                                    )}
                                                >
                                                    <BooleanInArrayEdit
                                                        key={e}
                                                        name={
                                                            schema.t ===
                                                            "numbered-array"
                                                                ? `${
                                                                      schema.prefix
                                                                  }${1}${e}`
                                                                : e
                                                        }
                                                        onChange={newValue => {
                                                            const new_state = [
                                                                ...state!,
                                                            ];
                                                            new_state[0] = {
                                                                ...state![0],
                                                                [e]: newValue,
                                                            };
                                                            onChange(new_state);
                                                        }}
                                                        value={undefined}
                                                        a_schema={a_schema}
                                                    />
                                                </div>
                                                <div className="item">
                                                    {a_schema.documentation}
                                                </div>
                                            </div>
                                        );
                                    case "simple-string-array":
                                        return (
                                            <div className={cx("row")} key={e}>
                                                <div
                                                    className={cx(
                                                        "item",
                                                        "keyvalue",
                                                        "array_chunk",
                                                    )}
                                                >
                                                    <SimpleStringArrayInArrayEdit
                                                        key={e}
                                                        name={
                                                            schema.t ===
                                                            "numbered-array"
                                                                ? `${
                                                                      schema.prefix
                                                                  }${1}${e}`
                                                                : e
                                                        }
                                                        onChange={nv => {
                                                            const new_state = [
                                                                ...state!,
                                                            ];
                                                            new_state[0] = {
                                                                ...state![0],
                                                                [e]: nv,
                                                            };
                                                            onChange(new_state);
                                                        }}
                                                        a_schema={a_schema}
                                                        value={[""]}
                                                    />
                                                </div>
                                                <div className="item">
                                                    {a_schema.documentation}
                                                </div>
                                            </div>
                                        );
                                    case "simple-numbered-array":
                                        return (
                                            <div className={cx("row")} key={e}>
                                                <div
                                                    className={cx(
                                                        "item",
                                                        "keyvalue",
                                                        "array_chunk",
                                                    )}
                                                >
                                                    <SimpleNumberedArrayInArrayEdit
                                                        key={e}
                                                        prefix={
                                                            schema.t ===
                                                            "numbered-array"
                                                                ? `${
                                                                      schema.prefix
                                                                  }${1}${e}`
                                                                : e
                                                        }
                                                        onChange={nv => {
                                                            const new_state = [
                                                                ...state!,
                                                            ];
                                                            new_state[0] = {
                                                                ...state![0],
                                                                [e]: nv,
                                                            };
                                                            onChange(new_state);
                                                        }}
                                                        a_schema={a_schema}
                                                        value={[]}
                                                    />
                                                </div>
                                                <div className="item">
                                                    {a_schema.documentation}
                                                </div>
                                            </div>
                                        );
                                    case "optional-subsection":
                                        return (
                                            <React.Fragment key={e}>
                                                <div
                                                    className={cx(
                                                        "item",
                                                        "array_chunk",
                                                    )}
                                                    key={e}
                                                >
                                                    <OptionalSubSectionInArrayEdit
                                                        key={e}
                                                        schema={a_schema}
                                                        onChange={nv => {
                                                            const new_state = [
                                                                ...state!,
                                                            ];
                                                            new_state[0] = {
                                                                ...state![0],
                                                                [e]: nv,
                                                            };
                                                            onChange(new_state);
                                                        }}
                                                        state={undefined}
                                                    />
                                                </div>
                                                <div
                                                    className={cx(
                                                        "item",
                                                        "row",
                                                    )}
                                                ></div>
                                            </React.Fragment>
                                        );
                                    default:
                                        throw new Error(
                                            `Unhandled numbered array type`,
                                        );
                                }
                            })}
                        </>
                    ) : null}
                </>
            )}
        </>
    );
}

function SimpleNumberedArrayInArrayEdit(props: {
    prefix: string;
    onChange: (newValue: string[]) => void;
    a_schema: SchemaSimpleNumberedArray;
    value: string[] | undefined;
}): JSX.Element {
    const { prefix, onChange, a_schema, value } = props;
    return (
        <div
            style={{
                marginTop: 10,
                marginBottom: 10,
                marginLeft: 10,
            }}
        >
            <span>
                Ranges
                {a_schema.required ? <span className="required">*</span> : ""}
            </span>
            <button
                onClick={() => {
                    if (!value && a_schema.required) {
                        onChange(["", ""]);
                    } else {
                        onChange([...(value ?? []), ""]);
                    }
                }}
            >
                +
            </button>
            {value && value.length > 0 ? (
                <button
                    onClick={() => {
                        if (a_schema.required && value.length === 1) {
                            onChange([""]);
                        } else {
                            onChange(value.slice(0, -1));
                        }
                    }}
                >
                    -
                </button>
            ) : null}
            {value ? (
                value.map((item, index) => (
                    <div key={index}>
                        <div
                            style={{
                                fontFamily: "Consolas, monospace",
                            }}
                        >
                            {prefix}
                            {index + 1}
                        </div>
                        <input
                            type="text"
                            value={item ?? ""}
                            pattern={a_schema.pattern ?? DEFAULT_PATTERN}
                            required={a_schema.required}
                            onChange={event => {
                                if (
                                    event.target.value &&
                                    !new RegExp(a_schema.pattern!).exec(
                                        event.target.value,
                                    )
                                ) {
                                    event.target.setCustomValidity(
                                        `${event.target.value} should be two 10-digit numbers separated by a comma then a number separated by another comma. For example: 0000000000, 9999999999, 10`,
                                    );
                                } else {
                                    event.target.setCustomValidity("");
                                }

                                const new_arr = [...value];
                                new_arr[index] = event.target.value;
                                onChange(new_arr);
                            }}
                        />
                    </div>
                ))
            ) : a_schema.required ? (
                <div>
                    <div
                        style={{
                            fontFamily: "Consolas, monospace",
                        }}
                    >
                        {prefix}
                        {1}
                    </div>
                    <input
                        type="text"
                        value={""}
                        pattern={a_schema.pattern ?? DEFAULT_PATTERN}
                        required={a_schema.required}
                        onChange={event => onChange([event.target.value])}
                    />
                </div>
            ) : null}
        </div>
    );
}
function OptionalSubSectionInArrayEdit(props: {
    schema: SchemaOptionalSubsection;
    onChange: (newValue: NGOptionalSubsection | undefined) => void;
    state: NGOptionalSubsection | undefined;
}) {
    const { schema, onChange, state } = props;

    return (
        <>
            <div>
                {schema.name}
                <input
                    type="checkbox"
                    onChange={() => {
                        if (!state) {
                            const obj: NGOptionalSubsection = {};
                            for (const m of Object.keys(schema.schema)) {
                                const s = schema.schema[m];
                                switch (s.t) {
                                    case "string":
                                        obj[m] = " ";
                                        break;
                                    case "boolean":
                                        obj[m] = undefined;
                                        break;
                                    case "simple-string-array":
                                        obj[m] = [""];
                                        break;
                                    default:
                                        throw new Error(
                                            `Programming error initializing optional subsection type`,
                                        );
                                }
                            }
                            onChange(obj);
                        } else {
                            onChange(undefined);
                        }
                    }}
                    checked={state !== undefined}
                />
            </div>
            {state ? (
                <>
                    {Object.keys(schema.schema).map(e => {
                        const a_schema = schema.schema[e];
                        switch (a_schema.t) {
                            case "string": {
                                const v = state[e];
                                assert(
                                    typeof v === "string" || v === undefined,
                                );
                                return (
                                    <div
                                        className={cx("row", "keyvalue")}
                                        key={e}
                                    >
                                        <StringInArrayEdit
                                            key={e}
                                            name={e}
                                            onChange={newValue => {
                                                state[e] = newValue;
                                                const new_state = state;
                                                onChange(new_state);
                                            }}
                                            a_schema={a_schema}
                                            value={v}
                                        />
                                    </div>
                                );
                            }
                            case "simple-string-array": {
                                const v = state[e];
                                assert(is_string_array(v) || v === undefined);
                                return (
                                    <div
                                        className={cx("row", "keyvalue")}
                                        key={e}
                                    >
                                        <SimpleStringArrayInArrayEdit
                                            key={e}
                                            name={e}
                                            onChange={newValue => {
                                                state[e] = newValue;
                                                const new_state = state;
                                                onChange(new_state);
                                            }}
                                            a_schema={a_schema}
                                            value={v}
                                        />
                                    </div>
                                );
                            }
                            default:
                                throw new Error(
                                    `Unimplemented Optional Subsection schema ${JSON.stringify(
                                        a_schema,
                                    )}`,
                                );
                        }
                    })}
                </>
            ) : null}
        </>
    );
}
export function SimpleStringArrayInArrayEdit(props: {
    name: string;
    onChange: (newValue: string[]) => void;
    a_schema: SchemaSimpleStringArray;
    value: string[] | undefined;
}): JSX.Element {
    const { name, onChange, a_schema } = props;
    const value = props.value ?? [];

    return (
        <>
            <div className="row">
                <span style={{ fontFamily: "Consolas,monospace" }}>
                    {name}
                    {a_schema.required ? (
                        <span className="required">*</span>
                    ) : (
                        ""
                    )}
                </span>
                <button
                    onClick={() => {
                        onChange([...value, ""]);
                    }}
                >
                    +
                </button>
                {value.length > 0 ? (
                    <button
                        onClick={() => {
                            onChange(value.slice(0, -1));
                        }}
                    >
                        -
                    </button>
                ) : null}
            </div>
            <div className="row">
                {value.map((item, index) => (
                    <div key={index}>
                        <DatalistInput
                            type="text"
                            value={item ?? ""}
                            pattern={a_schema.pattern ?? DEFAULT_PATTERN}
                            schema={a_schema}
                            onChange={event => {
                                const new_arr = [...value];
                                new_arr[index] = event.target.value;
                                onChange(new_arr);
                            }}
                        />
                    </div>
                ))}
            </div>
        </>
    );
}
export function BooleanInArrayEdit(props: {
    name: string;
    onChange: (newValue: boolean | undefined) => void;
    value: boolean | undefined;
    a_schema: SchemaBoolean;
}): JSX.Element {
    const { name, onChange, value, a_schema } = props;
    return (
        <div
            className={
                a_schema.deprecated ?? false
                    ? cx("row", "keyvalue", "deprecated")
                    : cx("row", "keyvalue")
            }
        >
            <label style={{ fontFamily: "Consolas,monospace" }}>
                {name}
                {a_schema.required ? <span className="required">*</span> : ""}
                <TriStateCheckbox
                    value={value}
                    required={a_schema.required}
                    onChange={nv => {
                        onChange(nv);
                    }}
                    name={name}
                />
            </label>
        </div>
    );
}
export function StringInArrayEdit(props: {
    name: string;
    onChange: (newValue: string) => void;
    a_schema: SchemaString;
    value: string | undefined;
}): JSX.Element {
    const { name, onChange, a_schema, value } = props;
    const id = useUniqueID();
    return (
        <>
            <label htmlFor={id} style={{ fontFamily: "Consolas,monospace" }}>
                {a_schema.deprecated ?? false ? (
                    <span className="deprecated">{name}</span>
                ) : (
                    name
                )}
                {a_schema.required ? <span className="required">*</span> : ""}
            </label>
            <br />

            {a_schema.enum ? (
                <FancySelect
                    value={value ?? ""}
                    onChange={onChange}
                    enum={a_schema.enum}
                    required={a_schema.required}
                />
            ) : (
                <DatalistInput
                    id={id}
                    type="text"
                    value={value ?? ""}
                    schema={a_schema}
                    pattern={a_schema.pattern ?? DEFAULT_PATTERN}
                    onChange={event => onChange(event.target.value)}
                />
            )}
        </>
    );
}
