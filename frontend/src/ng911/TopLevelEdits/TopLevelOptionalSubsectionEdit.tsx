import assert from "assert";
// eslint-disable-next-line emotion/no-vanilla
import { css, cx } from "emotion";
import React from "react";

import { NGOptionalSubsection } from "../ng911ini";
import { SchemaOptionalSubsection } from "../schema/InterfaceSchemas";
import {
    BooleanInArrayEdit,
    SimpleStringArrayInArrayEdit,
    StringInArrayEdit,
} from "./TopLevelArrayEdit";

export function TopLevelOptionalSubsectionEdit(props: {
    name: string;
    schema: SchemaOptionalSubsection;
    onChange: (newValue: NGOptionalSubsection | undefined) => void;
    state: NGOptionalSubsection | undefined;
}) {
    const { schema, onChange, state } = props;
    const the_style = css`
        button {
            margin-left: 5px;
            margin-right: 5px;
        }
        input {
            margin-top: 2px;
            margin-bottom: 2px;
        }
    `;
    return (
        <>
            <div className="row">
                <div className={cx(the_style, "item_name")}>
                    {schema.name}
                    <input
                        type="checkbox"
                        onChange={() => {
                            if (!state) {
                                const obj: NGOptionalSubsection = {};
                                for (const m of Object.keys(schema.schema)) {
                                    const s = schema.schema[m];
                                    switch (s.t) {
                                        case "string":
                                            obj[m] = " ";
                                            break;
                                        case "boolean":
                                            obj[m] = undefined;
                                            break;
                                        case "simple-string-array":
                                            obj[m] = [""];
                                            break;
                                        default:
                                            throw new Error(
                                                `Programming error initializing optional subsection type`,
                                            );
                                    }
                                }
                                onChange(obj);
                            } else {
                                onChange(undefined);
                            }
                        }}
                        checked={state !== undefined}
                    />
                </div>
                {
                    // eslint-disable-next-line @typescript-eslint/strict-boolean-expressions
                    !schema.documentation ? null : (
                        <div className="item">{schema.documentation}</div>
                    )
                }
            </div>
            {state ? (
                <>
                    <hr />
                    {Object.keys(schema.schema).map(e => {
                        const a_schema = schema.schema[e];
                        switch (a_schema.t) {
                            case "string": {
                                const v = state[e];
                                assert(
                                    typeof v === "string" || v === undefined,
                                );
                                return (
                                    <div className={cx("row")} key={e}>
                                        <div
                                            className={cx(
                                                "item",
                                                "keyvalue",
                                                "array_chunk",
                                            )}
                                        >
                                            <StringInArrayEdit
                                                key={e}
                                                name={e}
                                                onChange={newValue => {
                                                    state[e] = newValue;
                                                    const new_state = state;
                                                    onChange(new_state);
                                                }}
                                                a_schema={a_schema}
                                                value={v}
                                            />
                                        </div>
                                        <div className="item">
                                            {a_schema.documentation}
                                        </div>
                                    </div>
                                );
                            }
                            case "boolean": {
                                const v = state[e];
                                assert(
                                    typeof v === "boolean" || v === undefined,
                                );
                                return (
                                    <div className={cx("row")} key={e}>
                                        <div
                                            className={cx(
                                                "item",
                                                "keyvalue",
                                                "array_chunk",
                                            )}
                                        >
                                            <BooleanInArrayEdit
                                                key={e}
                                                name={e}
                                                onChange={newValue => {
                                                    state[e] = newValue;
                                                    const new_state = state;
                                                    onChange(new_state);
                                                }}
                                                value={v}
                                                a_schema={a_schema}
                                            />
                                        </div>
                                        <div className="item">
                                            {a_schema.documentation}
                                        </div>
                                    </div>
                                );
                            }
                            case "simple-string-array": {
                                const v = state[e];
                                assert(Array.isArray(v) || v === undefined);
                                return (
                                    <div className={cx("row")} key={e}>
                                        <div
                                            className={cx(
                                                "item",
                                                "keyvalue",
                                                "array_chunk",
                                            )}
                                        >
                                            <SimpleStringArrayInArrayEdit
                                                key={e}
                                                name={e}
                                                onChange={newValue => {
                                                    state[e] = newValue;
                                                    const new_state = state;
                                                    onChange(new_state);
                                                }}
                                                a_schema={a_schema}
                                                value={v}
                                            />
                                        </div>
                                        <div className="item">
                                            {a_schema.documentation}
                                        </div>
                                    </div>
                                );
                            }
                            default:
                                throw new Error(
                                    `Unimplemented Optional Subsection schema ${JSON.stringify(
                                        a_schema,
                                    )}`,
                                );
                        }
                    })}
                    <hr />
                </>
            ) : null}
        </>
    );
}
