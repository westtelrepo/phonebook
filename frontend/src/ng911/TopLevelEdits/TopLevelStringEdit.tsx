// eslint-disable-next-line emotion/no-vanilla
import { cx } from "emotion";
import React from "react";

import { DatalistInput } from "../inputs/DatalistInput";
import { FancySelect } from "../inputs/FancySelect";
import { SchemaString } from "../schema/InterfaceSchemas";
import { DEFAULT_PATTERN } from "../schema/schema";
import { useUniqueID } from "../useUniqueID";
import { verify_field } from "../verify_field";

export function TopLevelStringEdit(props: {
    name: string;
    schema: SchemaString;
    state: string | undefined;
    onChange: (newValue: string) => void;
}) {
    const { name, schema, state, onChange } = props;
    const id = useUniqueID();

    const verify = verify_field(state, schema);

    return (
        <>
            <div className="row">
                <label
                    htmlFor={id}
                    className="item_name"
                    style={{ fontFamily: "Consolas,monospace" }}
                >
                    {schema.deprecated ?? false ? (
                        <span className="deprecated">{name}</span>
                    ) : (
                        name
                    )}
                    {schema.required ? <span className="required">*</span> : ""}
                </label>
            </div>
            <div className={cx("row", "keyvalue")}>
                <div className="item">
                    {schema.enum ? (
                        <FancySelect
                            value={state ?? ""}
                            onChange={onChange}
                            enum={schema.enum}
                            required={schema.required}
                            customValidity={verify?.validityMessage}
                        />
                    ) : (
                        <DatalistInput
                            id={id}
                            type="text"
                            value={state ?? ""}
                            schema={schema}
                            onChange={event => onChange(event.target.value)}
                            pattern={schema.pattern ?? DEFAULT_PATTERN}
                            customValidity={verify?.validityMessage}
                        />
                    )}
                </div>
                <div className="item">{schema.documentation}</div>
            </div>
        </>
    );
}
