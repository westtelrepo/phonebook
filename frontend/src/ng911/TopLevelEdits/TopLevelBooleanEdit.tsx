// eslint-disable-next-line emotion/no-vanilla
import { cx } from "emotion";
import React from "react";

import { TriStateCheckbox } from "../inputs/TriStateCheckbox";
import { SchemaBoolean } from "../schema/InterfaceSchemas";

export function TopLevelBooleanEdit(props: {
    name: string;
    state: boolean | undefined;
    onChange: (newValue: boolean | undefined) => void;
    schema: SchemaBoolean;
}) {
    const { name, state, onChange, schema } = props;

    return (
        <div
            className={
                schema.deprecated ?? false
                    ? cx("row", "keyvalue", "deprecated")
                    : cx("row", "keyvalue")
            }
        >
            <label
                className="item_name"
                style={{ fontFamily: "Consolas,monospace" }}
            >
                {name}
                {schema.required ? <span className="required">*</span> : ""}
                <TriStateCheckbox
                    value={state}
                    required={schema.required}
                    onChange={e => onChange(e)}
                    name={name}
                />
            </label>
            <div className="item">{schema.documentation}</div>
        </div>
    );
}
