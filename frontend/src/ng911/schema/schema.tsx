import React from "react";

import { LittleSchema } from "./InterfaceSchemas";

export const DEFAULT_PATTERN = "[ -~]*";
const IPV4_SUBPATTERN =
    "((25[0-5]|2[0-4][0-9]|1[0-9][0-9]|[1-9]?[0-9])\\.(25[0-5]|2[0-4][0-9]|1[0-9][0-9]|[1-9]?[0-9])\\.(25[0-5]|2[0-4][0-9]|1[0-9][0-9]|[1-9]?[0-9])\\.(25[0-5]|2[0-4][0-9]|1[0-9][0-9]|[1-9]?[0-9]))";
export const IPV4_PATTERN = `^${IPV4_SUBPATTERN}$`;
const PORT_SUBPATTERN =
    "([0-9]{1,4}|[1-5][0-9]{4}|6[0-4][0-9]{3}|65[0-4][0-9]{2}|655[0-2][0-9]|6553[0-5])";
export const PORT_PATTERN = `^${PORT_SUBPATTERN}$`;
export const IPV4_PORT_PATTERN = `^${IPV4_SUBPATTERN}, ?${PORT_SUBPATTERN}$`;
export const NUMBER_PATTERN = "^[0-9]+$";
export const EMAIL_PATTERN = "^[a-zA-Z0-9._%+-]+@[a-zA-Z0-9.-]+.[a-zA-Z]{2,}$";
export const COMMA_SEPARATED_INTEGERS_PATTERN = "^([0-9]+, ?)*[0-9]+$";
export const CONNECTION_TYPE = ["TCP", "UDP"];
export const COLORS = [
    "black",
    "blue",
    "brown",
    "gray",
    "green",
    "orange",
    "purple",
    "red",
];
/*
 potential links for the below description:
 https://tools.ietf.org/html/rfc6056
 https://www.iana.org/assignments/service-names-port-numbers/service-names-port-numbers.xhtml
 https://www.iana.org/assignments/service-names-port-numbers/service-names-port-numbers.xml
*/
const IANA_PORT = (
    <>
        Port number should be between 49,152 and 65,535 which are considered
        dynamic/private ports by the{" "}
        <abbr title="Internet Assigned Numbers Authority">IANA</abbr> and can be
        used by anyone. See{" "}
        <a
            href="https://www.iana.org/assignments/service-names-port-numbers/service-names-port-numbers.xhtml"
            target="_blank"
            rel="noopener noreferrer"
        >
            IANA
        </a>{" "}
        for more info.
    </>
);

export const little_schema: LittleSchema = {
    GENERAL: {
        // required: true,
        Config_Version: {
            t: "string",
            required: true,
            documentation: "Version of ng911.cfg file.",
            datalist: ["2.730"],
        },
        PSAP_Name: {
            t: "string",
            required: true,
            documentation: "Name of the PSAP.",
        },
        PSAP_ID: {
            t: "string",
            required: true,
            pattern: "^[A-Z]{2}-[0-9]{3}$",
            documentation: "Identification code of PSAP.",
        },
        Product_Name: {
            t: "string",
            required: false,
            documentation: "Name used to identify the program.",
        },
        Company_Name: {
            t: "string",
            required: false,
            documentation: "Name of the company.",
            datalist: ["WestTel"],
        },
        ESME_Identification: {
            t: "string",
            required: false,
            documentation:
                "Unique name assigned by NENA for E2 Communications.",
            deprecated: true,
        },
        Support_Number: {
            t: "string",
            required: false,
            datalist: ["(303) 695-5000"],
            documentation: (
                <>
                    Phone number for tech support on splash screen of PTK. Use{" "}
                    <code>/r/n</code> to make it 2 lines.
                </>
            ),
        },
        About_Splash_File: {
            t: "string",
            required: false,
            documentation: (
                <>
                    Change the file of the <abbr title="PSAP Toolkit">PTK</abbr>{" "}
                    about button. The HTTP address of the file. For alternate
                    branding of the <abbr title="PSAP Toolkit">PTK</abbr>.
                </>
            ),
        },
        About_Splash_Color: {
            t: "string",
            required: false,
            documentation: (
                <>
                    RGB color of the bottom half of the{" "}
                    <abbr title="PSAP Toolkit">PTK</abbr> splash screen. 6
                    hexadecimal values. For alternate branding of the{" "}
                    <abbr title="PSAP Toolkit">PTK</abbr>.
                </>
            ),
            pattern: "^[0-9a-fA-F]{6}$",
        },
        About_Splash_Font: {
            t: "string",
            required: false,
            documentation: (
                <>
                    Font of the text in the{" "}
                    <abbr title="PSAP Toolkit">PTK</abbr> about window. For
                    alternate branding of the{" "}
                    <abbr title="PSAP Toolkit">PTK</abbr>.
                </>
            ),
            datalist: ["Roboto"],
        },
        About_Splash_Font_Pt: {
            t: "string",
            required: false,
            documentation: (
                <>
                    Font size of text in <abbr title="PSAP Toolkit">PTK</abbr>{" "}
                    about window. For alternate branding of the{" "}
                    <abbr title="PSAP Toolkit">PTK</abbr>.
                </>
            ),
            datalist: ["8", "10", "12"],
            pattern: NUMBER_PATTERN,
        },
        Default_Ethernet_Device: {
            t: "string",
            required: true,
            pattern: "^[!-.0-~]{1,16}$",
            datalist: ["eth0"],
            documentation: (
                // TODO: better ask Matt
                <>
                    Ethernet card used for controller communications.
                    <br />
                    In Debian, found in the file{" "}
                    <code>/etc/network/interfaces</code>.
                </>
            ),
        },
        Server1_IP_Address: {
            t: "string",
            required: true,
            pattern: IPV4_PATTERN,
            documentation: (
                <>
                    IP address of <code>Default_Ethernet_Device</code>.
                </>
            ),
        },
        Server1_RAC_IP_Address: {
            t: "simple-string-array",
            required: false,
            documentation: "IP address of server remote access card.",
            pattern: IPV4_PATTERN,
            deprecated: true,
        },
        Server2_RAC_IP_Address: {
            t: "simple-string-array",
            required: false,
            documentation: "IP address of server remote access card.",
            pattern: IPV4_PATTERN,
            deprecated: true,
        },
        Port_Alarm_Delay_Sec: {
            t: "string",
            required: false,
            documentation:
                "Number of seconds elapsed since communication failure trigger an initial sync port alarm.",
            pattern: NUMBER_PATTERN,
            min: 0,
        },
        Port_Alarm_Reminder_Sec: {
            t: "string",
            required: false,
            documentation:
                "Number of seconds elapsed since the last alarm to trigger another sync port alarm.",
            pattern: NUMBER_PATTERN,
            min: 0,
        },
        NPD0: {
            t: "string",
            required: true,
            pattern: "^[0-9]{3}$",
            documentation: "Numbering plan digits.",
        },
        NPD1: { t: "string", required: true, pattern: "^[0-9]{3}$" },
        NPD2: { t: "string", required: true, pattern: "^[0-9]{3}$" },
        NPD3: { t: "string", required: true, pattern: "^[0-9]{3}$" },
        PSAP_Monitor_IP_Address: {
            t: "string",
            required: true,
            documentation:
                "IP address of the central server that monitors all PSAP servers.",
            pattern: IPV4_PATTERN,
            datalist: ["23.24.152.49"],
        },
        PSAP_Monitor_Port_Number: {
            t: "string",
            required: false,
            documentation: "UDP port number used for the beacon.",
            pattern: PORT_PATTERN,
            datalist: ["59000"],
            min: 0,
            max: 65536,
        },
    },
    LOG: {
        // required: true,
        ECATS: {
            t: "optional-subsection",
            name: "ECATS",
            schema: {
                ECATS_Alarm_Delay_Sec: {
                    t: "string",
                    required: true,
                    documentation:
                        "Number of seconds elapsed since communication failure to trigger an initial ECATS port alarm.",
                    pattern: NUMBER_PATTERN,
                    min: 0,
                },
                ECATS_Alarm_Reminder_Sec: {
                    t: "string",
                    required: true,
                    documentation:
                        "Number of seconds elapsed since the last alarm to trigger another ECATS port alarm.",
                    pattern: NUMBER_PATTERN,
                    min: 0,
                },
                ECATS_IP_Address: {
                    t: "string",
                    required: true,
                    documentation: "IP Address of the ECATS server.",
                    pattern: IPV4_PATTERN,
                },
                ECATS_Port_Number: {
                    t: "string",
                    required: true,
                    documentation:
                        "Port number used for the remote ECATS server.",
                    pattern: PORT_PATTERN,
                    min: 0,
                    max: 65536,
                },
                ECATS_Connection_Type: {
                    t: "string",
                    required: true,
                    enum: CONNECTION_TYPE,
                },
                ECATS_MSG_Buffer_Size: {
                    t: "string",
                    required: true,
                    documentation:
                        "Max size of the queue for ECATS messages waiting to be sent. Exceeding this number results in lost messages.",
                    pattern: NUMBER_PATTERN,
                    min: 0,
                },
            },
        },
        Email_TechSupport_Address: {
            t: "string",
            required: true,
            documentation: "Email address to send all emails.",
            datalist: ["psap@westtel.com"],
            pattern: EMAIL_PATTERN,
        },
        Email_Alarm_Address: {
            t: "string",
            required: true,
            documentation: "Email address to send all alarm emails.",
            datalist: ["alarm@westtel.com"],
            pattern: EMAIL_PATTERN,
        },
        Disk_Space_Warning: {
            t: "string",
            required: false,
            documentation:
                "A daily warning message is activated when the remaining available gigabytes of disk space falls below this value.",
            pattern: NUMBER_PATTERN,
            min: 0,
        },
        Number_Months_To_Keep_Logs: {
            t: "string",
            required: false,
            documentation: (
                <>
                    Number of months to retain Controller log files.{" "}
                    <code>0</code> means that log files are never deleted.
                </>
            ),
            pattern: NUMBER_PATTERN,
            min: 0,
        },
        JSON_Logging: {
            t: "boolean",
            required: false,
            documentation: (
                <>
                    Generate JSON logs in{" "}
                    <code>/datadisk1/ng911/logs/json/</code>.
                </>
            ),
        },
    },
    DSB: {
        // required: false,
        UseDashboard: {
            t: "boolean",
            required: false,
            documentation: "Enable the dashboard client protocol.",
        },
        Dashboard_Alarm_Delay_Sec: {
            t: "string",
            required: false,
            documentation:
                "Number of seconds elapsed since communication failure to trigger an initial DSB port alarm.",
            pattern: NUMBER_PATTERN,
            min: 0,
        },
        Dashboard_Alarm_Reminder_Sec: {
            t: "string",
            required: false,
            documentation:
                "Number of seconds elapsed since the last alarm to trigger another DSB port alarm.",
            pattern: NUMBER_PATTERN,
            min: 0,
        },
        Dashboard_IP_Address: {
            t: "string",
            required: true,
            documentation: "IP address of the Dashboard service.",
            pattern: IPV4_PATTERN,
        },
        Dashboard_Port_Number: {
            t: "string",
            required: false,
            documentation: "Port number used for the remote Dashboard service.",
            pattern: PORT_PATTERN,
            min: 0,
            max: 65536,
        },
        Dashboard_API_Key: {
            t: "string",
            required: true,
            documentation: "Key given to log into the Dashboard service.",
        },
    },
    ALI: {
        // required: false,
        Legacy_ALI: {
            t: "boolean",
            required: false,
            documentation: "Whether legacy ALI modem pairs are used.",
        },
        Legacy_ALI_Only: {
            t: "boolean",
            required: false,
            documentation:
                "Whether to only display legacy ALI information even in a NG9-1-1 environment.",
        },
        Port_Alarm_Delay_Sec: {
            t: "string",
            required: false,
            documentation:
                "Number of seconds elapsed since communication failure to trigger an initial ANI port alarm.",
            pattern: NUMBER_PATTERN,
            min: 0,
        },
        Port_Alarm_Reminder_Sec: {
            t: "string",
            required: false,
            documentation:
                "Number of seconds elapsed since the last alarm to trigger another ALI port alarm.",
            pattern: NUMBER_PATTERN,
            min: 0,
        },
        Auto_Wireless_Phase_One_Only: {
            t: "boolean",
            required: false,
            documentation: (
                <>
                    Automatic rebids will only be for Phase I ALI records when
                    set to <code>true</code>. Phase II records will not be bid.
                </>
            ),
        },
        Auto_Wireless_Rebid_Count: {
            t: "string",
            required: false,
            documentation: "Number of automatic wireless/VoIP rebids.",
            pattern: NUMBER_PATTERN,
            min: 0,
        },
        Auto_Wireless_Rebid_Sec: {
            t: "string",
            required: false,
            documentation: "Number of seconds between auto rebids.",
            min: 0,
            deprecated: false,
        },
        Auto_WireLess_Rebid_Sec: {
            t: "string",
            required: false,
            documentation: (
                <>
                    Use <code>Auto_Wireless_Rebid_Sec</code> instead.
                </>
            ),
            min: 0,
            deprecated: true,
        },
        Auto_Rebid_NG911: {
            t: "boolean",
            required: false,
            documentation: (
                <>
                    Auto bid NG9-1-1 records. Note: Utilizes value from{" "}
                    <code>Auto_Wireless_Rebid_Count</code> and{" "}
                    <code>Auto_Wireless_Rebid_Sec</code>.
                </>
            ),
        },
        Allow_Manual_Bids: {
            t: "boolean",
            required: false,
            documentation:
                "Activate or suppress manual ALI bids as allowed by state laws. In some states it is illegal to conduct manual ALI bids.",
        },
        Show_Google_Map: {
            t: "boolean",
            required: false,
            documentation:
                "Enable the Google MAP button to map ALI x/y coordinates.",
        },
        Verify_ALI_Records: {
            t: "boolean",
            required: false,
            documentation:
                "Verify ALI records by finding a match to the pANI or callback number.",
        },
        Ignore_Bad_ALI: {
            t: "boolean",
            required: false,
            documentation:
                "Ignore invalid ALI data coming through the port (do not send warnings or tally the bad record count).",
        },
        Use_ALI_Service_Server: {
            t: "optional-subsection",
            name: "ALI Service Server",
            schema: {
                Use_ALI_Service_Server: {
                    t: "boolean",
                    required: true,
                    documentation:
                        "Utilize the ALI service. The service is used to share modems with multiple PSAPs.",
                },
                ALIservice_IP_Address: {
                    t: "string",
                    required: true,
                    documentation: "IP address of the ALI service.",
                    pattern: IPV4_PATTERN,
                },
                ALIservice_Port: {
                    t: "string",
                    required: true,
                    documentation: IANA_PORT,
                    pattern: PORT_PATTERN,
                },
                ALIservice_Connection: {
                    t: "string",
                    required: true,
                    documentation: "Connection type of the ALI service.",
                    enum: ["TCP"],
                },
            },
            ini_key: "Use_ALI_Service_Server",
            documentation:
                "Utilize the ALI service. The service is used to share modems with multiple PSAPs.",
        },
        Pairs: {
            t: "numbered-array",
            prefix: "Pair",
            required: false,
            schema: {
                _IP_Address_A: {
                    t: "string",
                    required: true,
                    pattern: IPV4_PORT_PATTERN,
                    documentation:
                        "IP address and port number of ALI port pair or serial converter box",
                },
                _IP_Address_B: {
                    t: "string",
                    required: true,
                    pattern: IPV4_PORT_PATTERN,
                },
                _Connection_Type_A: {
                    t: "string",
                    required: false,
                    enum: CONNECTION_TYPE,
                    documentation: "Connection type of ALI port",
                },
                _Connection_Type_B: {
                    t: "string",
                    required: false,
                    enum: CONNECTION_TYPE,
                },
                _Connection_Protocol: {
                    t: "string",
                    required: false,
                    enum: ["Modem", "E2", "Service"],
                    documentation: (
                        <>
                            {" "}
                            Determines which type of connection protocols are
                            allowed. Error checking.
                            <ul>
                                <li>Modem: TCP and UDP</li>
                                <li>E2: TCP only</li>
                                <li>Service: TCP Only</li>
                            </ul>
                        </>
                    ),
                },
                _Connection_Note_A: {
                    t: "string",
                    required: false,
                    documentation:
                        "Notes to be displayed with port alarm messages",
                },
                _Connection_Note_B: {
                    t: "string",
                    required: false,
                },
            },
        },
        Databases: {
            t: "numbered-array",
            prefix: "Database",
            required: false,
            schema: {
                _Pairs: {
                    t: "string",
                    required: true,
                    pattern: COMMA_SEPARATED_INTEGERS_PATTERN,
                    documentation:
                        "Which ALI port pairs are assigned to the database",
                },
                _Range: {
                    t: "simple-numbered-array",
                    required: true,
                    pattern: "^[0-9]{10}, ?[0-9]{10}, ?[0-9]{1,2}$",
                    documentation:
                        "Two 10-digit telephone numbers and rebid delays contained within the database.",
                },
                _Request_Key_Format: {
                    t: "string",
                    required: false,
                    enum: ["8", "10", "E2"],
                    documentation: "Digit format of ALI request.",
                },
                _Trunk_Type: {
                    t: "string",
                    required: true,
                    enum: ["All", "Landline", "Wireless"],
                    documentation: "Type of trunks accepted by the database.",
                },
                _Sends_Single_ALI: {
                    t: "boolean",
                    required: false,
                    documentation:
                        "Database returns a single ALI record response on one link.",
                },
                _LF_AS_CR: {
                    t: "boolean",
                    required: false,
                    documentation:
                        "Database treats line feeds as a carriage return.",
                },
                COS: {
                    t: "optional-subsection",
                    name: "COS",
                    documentation:
                        "\n\nThe row inside the ALI record where Class of Service is found.\n\nEach Class of Service may have different callback locations. List each Class of Service and the corresponding row that the callback is located.",
                    schema: {
                        _COS_Row: {
                            t: "string",
                            required: true,
                            pattern: NUMBER_PATTERN,
                            min: 0,
                            documentation:
                                "The row inside the ALI record where Class of Service is found.",
                        },
                        _COS_Callback: {
                            t: "simple-string-array",
                            required: true,
                            datalist: [
                                "BOPX , ",
                                "RESD , ",
                                "BUSN , ",
                                "RPXB , ",
                                "BPBX , ",
                                "CNTX , ",
                                "PAY1 , ",
                                "PAY2 , ",
                                "MOBL , ",
                                "ROPX , ",
                                "COCT , ",
                                "     , ",
                                "VRES , ",
                                "VBUS , ",
                                "VPAY , ",
                                "VMBL , ",
                                "WHP1 , ",
                                "WHP2 , ",
                                "VNOM , ",
                                "VENT , ",
                                "VOIP , ",
                                "WRLS , ",
                            ],
                            pattern: "^[0-9A-Z$]+ ?, ?[0-9]+$",
                            documentation:
                                "Each Class of Service may have different callback locations. List each Class of Service and the corresponding row that the callback is located.",
                        },
                    },
                },
                _Callback_LocationA: {
                    t: "string",
                    required: true,
                    pattern: "^[0-9]+(, ?[0-9]+)?$",
                    documentation:
                        "Row and column within ALI record that contains the callback number. Column number is optional if the phone number appears first or is the only phone number contained within the row.",
                },
                _Callback_LocationB: {
                    t: "string",
                    required: false,
                    pattern: "^[0-9]+(, ?[0-9]+)?$",
                    documentation:
                        "Used in databases where there are two formats. This is the second place to look for the Callback number location A fails to find a number. Row and column within ALI record that contains the callback number. Column number is optional if the phone number appears first or is the only phone number contained within the row. If the row number is set to zero the search is suppressed.",
                },
                _CC_Alarm_Email_Address: {
                    t: "string",
                    required: false,
                    pattern: EMAIL_PATTERN,
                    documentation:
                        "Email address to carbon copy all ALI alarm emails from ALI port pairs assigned to the database.",
                },
            },
        },
        ALI_Noise_Threshold: {
            t: "string",
            required: false,
            documentation:
                "Ignore and do not log ALI character noise below this threshold.",
            pattern: NUMBER_PATTERN,
            min: 0,
        },
        CLID_Bid_ALI_Rule_Code: {
            t: "string",
            required: false,
            documentation: (
                <>
                    How the system handles ALI Bids on CLID Trunks.
                    <ul>
                        <li>0 = Allow CLID ALI Bids</li>
                        <li>1 = Bid test key to generate record not found</li>
                        <li>2 = Prohibit</li>
                    </ul>
                </>
            ),
            enum: [
                { value: "0", name: "Allow CLID ALI bids" },
                {
                    value: "1",
                    name: "Bid test key to generate record not found",
                },
                { value: "2", name: "Prohibit" },
            ],
            pattern: "^[0-2]$",
        },
        MAX_ALI_Record_Size: {
            t: "string",
            required: false,
            documentation:
                "NENA standard ALI record Size is 516. Some systems ignore this rule.",
            datalist: ["516"],
            pattern: NUMBER_PATTERN,
            min: 0,
        },
    },
    LIS: {
        // required: false,
        Next_Generation_ALI: {
            t: "boolean",
            required: false,
            documentation: "Enables processing of NG911 data from a LIS.",
        },
        MSRP_LIS_SERVER: {
            t: "string",
            required: false,
            documentation: "MSRP LIS Server.",
        },
        MSRP_LIS_USE_POST_ONLY: {
            t: "boolean",
            required: false,
            documentation:
                "Use the HTTP POST method insated of the HTTP GET method for MSRP LIS.",
        },
        NG911_LIS_Server: {
            t: "string",
            required: false,
            documentation: "NG911 LIS Server.",
        },
        NG911_LIS_USE_POST_ONLY: {
            t: "boolean",
            required: false,
            documentation:
                "Use the HTTP POST method instead of the HTTP GET method for NG911 LIS.",
        },
        Enable_Rapid_Lite_Button: {
            t: "boolean",
            required: false,
            documentation: "Enable the RapidSOS button.",
        },
        Convert_I3_XML_To: {
            t: "string",
            required: false,
            documentation:
                // PidfLo location documentation from phones.
                "Converts i3 Data to ALI30W or NEXTGEN.\nBoth PidfLo and i3 get read into an i3 record. The record is then mapped into a legacy format (ALI30W) or line generated values (NEXTGEN)",
            enum: ["ALI30W", "NEXTGEN"],
        },
    },
    ANI: {
        // required: true,
        Port_Alarm_Delay_Sec: {
            t: "string",
            required: false,
            documentation:
                "Number of seconds elapsed since communication failure to trigger an initial ANI port alarm.",
            pattern: NUMBER_PATTERN,
            min: 0,
        },
        Port_Alarm_Reminder_Sec: {
            t: "string",
            required: false,
            documentation:
                "Number of seconds elapsed since the last alarm to trigger another ANI port alarm.",
            pattern: NUMBER_PATTERN,
            min: 0,
        },
        Max_Incoming_Trunk_Number: {
            t: "string",
            required: true,
            // TODO: verification
            documentation:
                "Highest trunk number used. Any trunk number in the comma separated lists below must not exceed this number.\nDo not add a trunk to more than one list.",
            pattern: NUMBER_PATTERN,
            min: 0,
        },
        CAMA_Trunks: {
            t: "string",
            required: false,
            documentation: "List which trunks are CAMA trunks.",
            pattern: COMMA_SEPARATED_INTEGERS_PATTERN,
        },
        Landline_Trunks: {
            t: "string",
            required: false,
            documentation: "List which CAMA trunks are landline only.",
            pattern: COMMA_SEPARATED_INTEGERS_PATTERN,
        },
        Wireless_Trunks: {
            t: "string",
            required: false,
            documentation: "List which CAMA trunks are wireless only.",
            pattern: COMMA_SEPARATED_INTEGERS_PATTERN,
        },
        CLID_Trunks: {
            t: "string",
            required: false,
            documentation: "List which trunks are Caller-ID trunks.",
            pattern: COMMA_SEPARATED_INTEGERS_PATTERN,
        },
        SIP_Trunks: {
            t: "string",
            required: false,
            documentation: "List which trunks are SIP trunks.",
            pattern: COMMA_SEPARATED_INTEGERS_PATTERN,
        },
        NG911_Trunks: {
            t: "string",
            required: false,
            documentation: "List which SIP trunks are NG911.",
            pattern: COMMA_SEPARATED_INTEGERS_PATTERN,
        },
        INTRADO_Trunks: {
            t: "string",
            required: false,
            documentation: "List which SIP trunks are Intrado.",
            pattern: COMMA_SEPARATED_INTEGERS_PATTERN,
        },
        MSRP_Trunks: {
            t: "string",
            required: false,
            documentation: "List which SIP trunks are MSRP.",
            pattern: COMMA_SEPARATED_INTEGERS_PATTERN,
        },
        INdigital_Wireless_Trunks: {
            t: "string",
            required: false,
            documentation: "List which SIP trunks are INdigital.",
            pattern: COMMA_SEPARATED_INTEGERS_PATTERN,
        },
        Trunk_Rotate_Groups: {
            t: "array",
            required: true,
            schema: {
                Trunk_Rotate_Group_Members: {
                    t: "string",
                    required: true,
                    pattern: COMMA_SEPARATED_INTEGERS_PATTERN,
                    documentation:
                        "List of trunks that rotate together. A trunk can not be in more than one group.",
                },
                Trunk_Rotate_Group_Rule: {
                    t: "string",
                    required: true,
                    enum: ["Multiplier", "GroupCount", "Days"],
                    documentation: (
                        <>
                            Rule that defines when an error shall be displayed
                            if a member of the rotate group fails to log a call
                            within the specified number of calls within the
                            type.
                            <ul>
                                <li>
                                    Multiplier: if after (size of Group X
                                    Multiplier) Calls.
                                </li>
                                <li>
                                    GroupCount: if after GroupCount number of
                                    calls.
                                </li>
                                <li>Days: if after X number of days</li>
                            </ul>
                        </>
                    ),
                },
                Trunk_Rotate_Group_Rule_Value: {
                    t: "string",
                    required: true,
                    pattern: NUMBER_PATTERN,
                    min: 0,
                    documentation: (
                        <>
                            Value associated with{" "}
                            <code>Trunk_Rotate_Group_Rule</code>
                        </>
                    ),
                },
            },
        },
        ConferenceNumber_Prefix: {
            t: "string",
            required: false,
            documentation: (
                <>
                    Used to track conferences. Omit if the system does not
                    utilize 3 digit extensions. Use <code>00</code> for 5 digit,{" "}
                    <code>000</code> for 6 digit, and <code>000000000000</code>{" "}
                    for 15 digit, etc.
                    <br />
                    Note: the conference number is a number from{" "}
                    <code>100</code> to <code>910</code> this string will be
                    prepended for dialing
                </>
            ),
            pattern: "^0*$",
            datalist: ["00", "000", "000000000000"],
        },
        ANI_20_Digit_Valid_Callback: {
            t: "boolean",
            required: false,
            documentation:
                "Used to identify ANI providers that are unable to provide a valid callback number when sending 20 digit ANI.",
        },
        TDD_Auto_Detect: {
            t: "boolean",
            required: false,
            documentation: "Auto display the TDD window when TDD is detected.",
        },
        Delay_After_Flash_Hook_ms: {
            t: "string",
            required: false,
            documentation:
                "Delay in milliseconds to wait after the flash hook before dialing a 911 CAMA transfer.",
            pattern: NUMBER_PATTERN,
            min: 0,
        },
        Delay_Between_DTMF_ms: {
            t: "string",
            required: false,
            documentation: (
                <>
                    Delay between dialing{" "}
                    <abbr title="dual-tone multi-frequency">DTMF</abbr> digits
                    during a 9-1-1 CAMA transfer.
                </>
            ),
            pattern: NUMBER_PATTERN,
            min: 0,
        },
        INdigital: {
            t: "optional-subsection",
            name: "INdigital",
            schema: {
                Delay_After_Star_Eight_Pound_INDIGITAL_ms: {
                    t: "string",
                    required: true,
                    documentation:
                        "Delay in milliseconds to wait after the *8# before dialing a 9-1-1 INdigital CAMA transfer.",
                    pattern: NUMBER_PATTERN,
                    min: 0,
                },
                Delay_Between_DTMF_INDIGITAL_ms: {
                    t: "string",
                    required: true,
                    documentation: (
                        <>
                            Delay between dialing{" "}
                            <abbr title="dual-tone multi-frequency">DTMF</abbr>{" "}
                            digits during a 9-1-1 INdigital CAMA transfer.
                        </>
                    ),
                    pattern: NUMBER_PATTERN,
                    min: 0,
                },
            },
        },
        Freeswitch_CLI_IP_Address: {
            t: "string",
            required: false,
            documentation: (
                <>
                    IP address of the FreeSWITCH CLI (IPv4 address or{" "}
                    <code>localhost</code>).
                </>
            ),
            pattern: `${IPV4_PATTERN}|localhost`,
        },
        USE_CALL_RECORDER: {
            t: "boolean",
            required: false,
            deprecated: true,
        },
        USE_BCF_IP_KEY: {
            t: "boolean",
            required: false,
            documentation:
                "Used in SOCC Ohio for phones that are registered through the Oracle BCF which hides the IP address from the PSAP Controller. Each channel of the phone has been given a key to help determine the position number of the phone. For example, OH010PhoneTwoLineFour.",
        },
        AUDIOCODES_911_ANI_REVERSED: {
            t: "boolean",
            required: false,
        },
        Gateway_Transfer_Order: {
            t: "string",
            required: false,
            documentation: (
                <>
                    Assigns the gateway order for dialing transfers. Values are:
                    <ul>
                        <li>domain</li>
                        <li>cisco</li>
                        <li>switchvox</li>
                        <li>audiocodes-1</li>
                        <li>audiocodes-2</li>
                        <li>mytel</li>
                        <li>ESRP</li>
                        <li>etc</li>
                    </ul>
                    note: only use domain (FreeSWITCH) utilizing the failover of
                    the dialplan.
                </>
            ),
            datalist: [
                "domain",
                "cisco",
                "switchvox",
                "audiocodes-1",
                "audiocodes-2",
                "mytel",
                "ESRP",
            ],
        },
        // FUTURE: To be deprecated
    },
    CAD: {
        // required: true,
        CAD_Exists: {
            t: "boolean",
            required: false,
            documentation: "Enable CAD support.",
        },
        Send_To_CAD_On_Conference: {
            t: "boolean",
            required: false,
            documentation:
                "Whether the ALI record is sent to CAD with the position number conferencing in on a call automatically.",
        },
        Send_To_CAD_On_Take_Control: {
            t: "boolean",
            required: false,
            documentation:
                "Whether the ALI record is sent to CAD with the workstation number taking control of a record/call.",
        },
        Port_Alarm_Delay_Sec: {
            t: "string",
            required: false,
            documentation:
                "Number of seconds elapsed since communication failure to trigger an initial CAD port alarm.",
            pattern: NUMBER_PATTERN,
            min: 0,
        },
        Port_Alarm_Reminder_Sec: {
            t: "string",
            required: false,
            documentation:
                "Number of seconds elapsed since the last alarm to trigger another CAD port alarm.",
            pattern: NUMBER_PATTERN,
            min: 0,
        },
        Ports: {
            t: "numbered-array",
            required: true,
            prefix: "Port",
            schema: {
                _IP_Address: {
                    t: "string",
                    required: true,
                    pattern: IPV4_PORT_PATTERN,
                    documentation: (
                        <>
                            IP Address and port number of CAD system or serial
                            converter box.
                            <br />
                            {IANA_PORT}
                        </>
                    ),
                },
                _Rule_Code: {
                    t: "string",
                    required: false,
                    enum: [
                        { value: "0", name: "No ACKs send no Heartbeats" },
                        { value: "1", name: "No ACKs, send Heartbeat" },
                        { value: "2", name: "ACKs both and send Heartbeat" },
                        {
                            value: "3",
                            name:
                                "ACKs on ALI ALI record only, send no Heartbeats",
                        },
                        {
                            value: "4",
                            name:
                                "ACKs on ALI ALI record only, send Heartbeats",
                        },
                        {
                            value: "5",
                            name: "ACKs on Heartbeat Only, send Heartbeats",
                        },
                    ],
                    documentation: (
                        <>
                            How the system communicates with the CAD system.
                            <ul>
                                <li>0 = No ACKs send no Heartbeats</li>
                                <li>1 = No ACKs, send Heartbeat</li>
                                <li>2 = ACKs both and send Heartbeat</li>
                                <li>
                                    3 = ACKs on ALI ALI record only, send no
                                    Heartbeats
                                </li>
                                <li>
                                    4 = ACKs on ALI ALI record only, send
                                    Heartbeats
                                </li>
                                <li>
                                    5 = ACKs on Heartbeat Only, send Heartbeats
                                </li>
                            </ul>
                        </>
                    ),
                },
                _Erase_Msg: {
                    t: "boolean",
                    required: false,
                    documentation:
                        "CAD system receives erase message when the call is put on hold or is disconnected.",
                },
                _Positions: {
                    t: "string",
                    required: false,
                    pattern: COMMA_SEPARATED_INTEGERS_PATTERN,
                    documentation:
                        "Position numbers used by this port. If all position numbers are used, omit.",
                },
                _Position_Aliases: {
                    t: "string",
                    required: false,
                    pattern: "^([0-9]+\\|[0-9]+,? ?)*$",
                    documentation:
                        "Position alias pairs. This setting will substitute the left side 9-1-1 system position number with the right side CAD system position number. Values are separated by a pipe (|) symbol.",
                },
                _NENA_CAD_Header: {
                    t: "boolean",
                    required: false,
                    documentation:
                        "Use the NENA standard CAD header with ALI transmissins to CAD. Some CAD vendors have implemented using only the ALI header.",
                },
                _Remove_First_ALI_CR: {
                    t: "boolean",
                    required: false,
                    documentation: "Remove the first carriage return from ALI",
                },
                _Connection_Type: {
                    t: "string",
                    required: false,
                    enum: CONNECTION_TYPE,
                    documentation:
                        "TC/IP connection Type UDP or TCP. TCP connection is server not client.",
                },
                _Connection_Note: {
                    t: "string",
                    required: false,
                    documentation:
                        "Notes to be displayed with port alarm messages",
                },
                _CC_Alarm_Email_Address: {
                    t: "string",
                    required: false,
                    pattern: EMAIL_PATTERN,
                    documentation: (
                        <>
                            Email address to carbon copy all CAD alarm emails
                            from specific CAD Ports. Use this entry if each Port
                            has a different email address, otherwise omit. Note:
                            this entry will overwrite a previous assignment by{" "}
                            <code>CC_Alarm_Email_Address</code> for the listed
                            port.
                        </>
                    ),
                },
            },
        },
    },
    WRK: {
        // required: true,
        Number_Of_Workstations: {
            t: "string",
            required: false,
            documentation: "Number of positions in the PSAP.",
            pattern: NUMBER_PATTERN,
            min: 1,
        },
        Workstation_Port_Number: {
            t: "string",
            required: true,
            documentation:
                "Dynamic UDP/TCP port number used to listen for workstation communications.",
            pattern: PORT_PATTERN,
            min: 0,
            max: 65536,
        },
        Connection_Type: {
            t: "string",
            required: false,
            documentation: "Type of connection to Workstation GUI.",
            enum: CONNECTION_TYPE,
        },
        "Workstation Groups": {
            t: "array",
            required: false,
            schema: {
                Workstation_Group_Name: {
                    t: "string",
                    required: false,
                    documentation:
                        "Name of the grouping of workstations. Omit for single sites.",
                },
                Workstation_Group_Trunks: {
                    t: "string",
                    required: true,
                    pattern: COMMA_SEPARATED_INTEGERS_PATTERN,
                    documentation:
                        "Trunks that are associated with the grouping.\nNote: identical trunks cannot be in more than one grouping.",
                },
                Workstation_Group_Positions: {
                    t: "string",
                    required: true,
                    pattern: COMMA_SEPARATED_INTEGERS_PATTERN,
                    documentation:
                        "Workstation position numbers associated with the grouping.",
                },
                Workstation_Group_PSAPURI: {
                    t: "string",
                    required: true,
                    documentation: "URI associated with the workstation group",
                },
            },
        },
        Show_PSAP_Status_Applet: {
            t: "boolean",
            required: false,
            documentation: "Enable the PSAP Status applet window.",
        },
        Show_PSAP_Status_Applet_Admin: {
            t: "boolean",
            required: false,
            documentation:
                "Enable the Admin Call Column within the PSAP status applet.",
        },
        Show_SMS_Applet: {
            t: "boolean",
            required: false,
            deprecated: true,
        },
        Show_Call_Recorder_Button: {
            t: "boolean",
            required: false,
            documentation: "Enable the call recorder window.",
        },
        Show_Text_Button: {
            t: "boolean",
            required: false,
            documentation: "Enable the multipurpose text window.",
        },
        Show_Alert_Button: {
            t: "boolean",
            required: false,
            documentation: "Display GUI alert window button.",
        },
        Show_TDD_Button: {
            t: "boolean",
            required: false,
            documentation:
                "Enable the TDD window. (If text window is on this will be set to false)",
        },
        Show_Color_Palette_User: {
            t: "simple-string-array",
            required: false,
            documentation: "Enable the color editor button for selected users.",
        },
        Port_Alarm_Delay_Sec: {
            t: "string",
            required: false,
            documentation:
                "Number of seconds elapsed since communication failure to trigger an initial ANI port alarm.",
            pattern: NUMBER_PATTERN,
            min: 0,
        },
        Port_Alarm_Reminder_Sec: {
            t: "string",
            required: false,
            documentation:
                "Number of seconds elapsed since the last alarm to trigger another ANI port alarm.",
            pattern: NUMBER_PATTERN,
            min: 0,
        },
        Show_Ringing_Tab: {
            t: "boolean",
            required: false,
            documentation: "Show the ringing tab.",
        },
        GUI_Tab_Behavior_Code: {
            t: "string",
            required: false,
            documentation: (
                <>
                    Automatically determines the focused tab based on start-up,
                    Connect and Disconnect. The behavior is the sum of the
                    following sections:
                    <br />
                    <ul>
                        <li>
                            Tab On Start up Behavior
                            <ul>
                                <li>0 All Tab</li>
                                <li>1 Connect Tab</li>
                                <li>2 Line View Tab</li>
                                <li>3 Position Tab</li>
                                <li>4 Abandoned Tab</li>
                                <li>5 Remembered</li>
                            </ul>
                        </li>
                        <li>
                            Tab On Disconnect Behavior
                            <ul>
                                <li>0 Do Nothing</li>
                                <li>8 All Tab</li>
                                <li>16 Connected Tab</li>
                                <li>24 Line View Tab</li>
                                <li>32 Position Tab</li>
                                <li>40 Abandoned Tab</li>
                            </ul>
                        </li>
                        <li>
                            Tab On Connect Behavior
                            <ul>
                                <li>0 Do Nothing</li>
                                <li>64 Connected Tab</li>
                                <li>128 Line View Tab</li>
                                <li>192 Position Tab</li>
                                <li>256 Previous if abandoned Tab</li>
                            </ul>
                        </li>
                        <li>
                            Tab On Ring Behavior
                            <ul>
                                <li>0 Do Nothing</li>
                                <li>512 All Tab</li>
                                <li>1024 Line View Tab</li>
                                <li>1536 Position Tab</li>
                            </ul>
                        </li>
                        <li>
                            Tab On Take Control Behavior
                            <ul>
                                <li>0 Do Nothing</li>
                                <li>4096 All Tab</li>
                                <li>8192 Position Tab</li>
                                <li>12288 Previous or ALL</li>
                                <li>16384 Previous or Position</li>
                            </ul>
                        </li>
                    </ul>
                </>
            ),
            pattern: NUMBER_PATTERN,
            min: 0,
            max: 18221,
        },
        GUI_View_Position_Default: {
            t: "string",
            required: false,
            documentation: (
                <>
                    Sets the GUI view position drop down box to either{" "}
                    <code>All</code> or <code>ThisPosition</code>.
                </>
            ),
            enum: ["All", "ThisPosition"],
        },
        TDD_Default_Language: {
            t: "string",
            required: false,
            documentation: "TDD Default Language.",
            datalist: ["English"],
        },
        TDD_Send_Delay: {
            t: "string",
            required: false,
            documentation:
                "Number of seconds to wait after text received before allowing sending.",
            pattern: NUMBER_PATTERN,
        },
        TDD_Send_Text_Color: {
            t: "string",
            required: false,
            documentation: "Color of text for operator sent characters.",
            enum: COLORS,
        },
        TDD_Received_Text_Color: {
            t: "string",
            required: false,
            documentation: "Color of text for received characters.",
            enum: COLORS,
        },
        TDD_CPS_Threshold: {
            t: "string",
            required: false,
            documentation:
                "Number of characters entered per second to trigger the TDD applet activation.",
            pattern: NUMBER_PATTERN,
            min: 0,
        },
        TDD_Messages: {
            t: "array",
            required: true,
            schema: {
                TDD_Message_Text: {
                    t: "string",
                    required: true,
                    documentation: "Message text.",
                },
                TDD_Message_Language: {
                    t: "string",
                    required: false,
                    datalist: ["English"],
                    documentation: "Message language.",
                },
            },
        },
        MSRP_Default_Language: {
            t: "string",
            required: false,
            documentation:
                "Default language group for TDD messages list display.",
            datalist: ["English"],
        },
        MSRP_Send_Text_Color: {
            t: "string",
            required: false,
            documentation: "Color of text for operator sent characters.",
            enum: COLORS,
        },
        MSRP_Received_Text_Color: {
            t: "string",
            required: false,
            documentation: "Color of text for received characters.",
            enum: COLORS,
        },
        MSRP_Messages: {
            t: "array",
            required: true,
            schema: {
                MSRP_Message_Text: {
                    t: "string",
                    required: true,
                    documentation: "Message text.",
                },
                MSRP_Message_Language: {
                    t: "string",
                    required: false,
                    datalist: ["English"],
                    documentation: "Message language.",
                },
            },
        },
        Transfer_Default_Group: {
            t: "string",
            required: false,
            documentation:
                "Default transfer group for transfer list display.\nWith the exception of non-ASCII characters, text must be in transfer group.",
            datalist: ["ALL", "PSAP", "Fire", "Police"],
            deprecated: true,
        },
        Transfer_Name: {
            t: "string",
            required: false,
            documentation: "Name of agency for transfers.",
            deprecated: true,
        },
        Transfer_Group: {
            t: "string",
            required: false,
            documentation: "Group that the agency is assigned to.",
            datalist: ["PSAP", "Fire", "Police"],
            deprecated: true,
        },
        Transfer_Number: {
            t: "string",
            required: false,
            documentation:
                "Agency's speed dial (*100) or number (*3034553901).\nLocal SIP extension (10XX) or WWW SIP (sip:123456@sip.place.com).",
            deprecated: true,
        },
        Transfer_Code: {
            t: "string",
            required: false,
            documentation: (
                <>
                    Determines the functionality of the five transfer/call
                    buttons for the listed number. The code is a sum of the
                    following:
                    <br />
                    <ul>
                        <li>0 All</li>
                        <li>2 Blind Transfer</li>
                        <li>4 Attended Transfer</li>
                        <li>8 Conference Transfer</li>
                        <li>16 Tandem Transfer</li>
                        <li>32 Dial Out</li>
                    </ul>
                </>
            ),
            min: 0,
            max: 62,
            deprecated: true,
        },

        GUI_SIP_Phone_Exists: {
            t: "boolean",
            required: false,
            documentation:
                "Determine whether to activate the SIP phone feature of the GUI.",
        },
        "911_Transfers_Tandem_Only": {
            t: "boolean",
            required: false,
            documentation: 'Restrict transfers of 911 calls to "tandem only"',
        },
        Mapping_Regex: {
            t: "string",
            required: false,
            datalist: [
                "(lat|lon|x|y)? *[=:]? *([\\\\+-]?[0-9]+\\.[0-9]+) *(lat|lon|x|y)? *[=:]? *([\\\\+-]?[0-9]+\\.[0-9]+)",
            ],
            documentation: (
                <>
                    Optional Regex for the GUI to use to determine latitude or
                    longitude
                    <br />
                    <ul>
                        <li>It is case insensitive</li>
                        <li>
                            It uses 4 groups designated by parentheses (...)
                        </li>
                        <li>
                            An optional name (LAT,LON,X,Y) for the first
                            numerical value
                        </li>
                        <li>
                            A double precision numerical value with an optional
                            &lsquo;+&rsquo; or &lsquo;-&rsquo;
                        </li>
                        <li>
                            An optional name (LAT,LON,X,Y) for the second
                            numerical value
                        </li>
                        <li>
                            A double precision numerical value with an optional
                            &lsquo;+&rsquo; or &lsquo;-&rsquo;
                        </li>
                        <li>
                            After the optional names there can be an optional
                            &lsquo;=&rsquo; or &lsquo;:&rsquo; surrounded by
                            optional spaces
                        </li>
                        <li>
                            To figure out which value is latitude and which is
                            longitude
                            <br />
                            The name is used only if it is &lsquo;LAT&rsquo; or
                            &lsquo;LON&rsquo; since &lsquo;X&rsquo; and
                            &lsquo;Y&rsquo; has been known to mean either
                        </li>
                        <li>
                            If the first value is less than -40.0 or greater
                            than 80.0 it is assumed to be longitude. Otherwise
                            the first value is assumed to be latitude
                        </li>
                    </ul>
                </>
            ),
        },
    },
    TELEPHONE: {
        // required: true,
        Phone_Alarm_Delay_Sec: {
            t: "string",
            required: false,
            documentation:
                "Number of seconds elapsed since communication failure to trigger an initial hardphone alarm.",
            pattern: NUMBER_PATTERN,
            min: 0,
        },
        Phone_Alarm_Reminder_Sec: {
            t: "string",
            required: false,
            documentation:
                "Number of seconds elapsed since the last alarm to trigger another hardphone alarm.",
            pattern: NUMBER_PATTERN,
            min: 0,
        },
        "PBX Disable on Failure Detection": {
            t: "optional-subsection",
            name: "PBX Disable on Failure Detection",
            documentation: (
                <>
                    If the ANI FreeSWITCH is not set to <code>localhost</code>,
                    disable this subsection.
                    <br />
                    If PBX is on the same IP as the Controller, this subsection
                    will not work.
                </>
            ),
            schema: {
                DISABLE_PBX_ON_PHONE_FAILURE_LIST: {
                    t: "string",
                    required: true,
                    documentation:
                        "Disables the PBX when the list of phones fail to respond to pings. This feature busies out the 9-1-1 trunks.",
                    pattern: COMMA_SEPARATED_INTEGERS_PATTERN,
                },
                TELEPHONE_PING_INTERVAL_SEC: {
                    t: "string",
                    required: true,
                    documentation:
                        "Interval in seconds that the system will ping the telephones.",
                    pattern: NUMBER_PATTERN,
                    min: 0,
                },
                DISABLE_PBX_CONSECUTIVE_FAILURES: {
                    t: "string",
                    required: true,
                    documentation:
                        "The number of consecutive ping failures of ALL phones that initiates the shutdown of the PBX. Any phone ping success prevents the process.",
                    pattern: NUMBER_PATTERN,
                    min: 0,
                    deprecated: false,
                },
                DISABLE_PBX_CONSECTUTIVE_FAILURES: {
                    t: "string",
                    required: false,
                    documentation: (
                        <>
                            Use <code>DISABLE_PBX_CONSECUTIVE_FAILURES</code>{" "}
                            instead.
                        </>
                    ),
                    pattern: NUMBER_PATTERN,
                    min: 0,
                    deprecated: true,
                },
                ENABLE_PBX_CONSECUTIVE_PING: {
                    t: "string",
                    required: true,
                    documentation:
                        "Following a PBX shutdown, any single phone must ping consecutive this number of pings to restart the PBX.",
                    pattern: NUMBER_PATTERN,
                    min: 0,
                    deprecated: false,
                },
                ENABLE_PBX_CONSECTUTIVE_PING: {
                    t: "string",
                    required: false,
                    documentation: (
                        <>
                            Use <code>ENABLE_PBX_CONSECUTIVE_PING</code>{" "}
                            instead.
                        </>
                    ),
                    pattern: NUMBER_PATTERN,
                    min: 0,
                    deprecated: true,
                },
            },
        },
        CTI: {
            t: "optional-subsection",
            name: "CTI",
            ini_key: "Enable_Polycom_CTI",
            schema: {
                Enable_Polycom_CTI: {
                    t: "boolean",
                    required: true,
                    documentation: "Enables polycom IP phone CTI interface.",
                },
                CTI_HTTP_Port: {
                    t: "string",
                    required: true,
                    documentation: (
                        <>
                            HTTP port for the Polycom CTI.
                            <br />
                            {IANA_PORT}
                        </>
                    ),
                    pattern: PORT_PATTERN,
                    min: 0,
                    max: 65536,
                },
                CTI_TCP_Server_Port: {
                    t: "string",
                    required: true,
                    documentation: (
                        <>
                            Local URL GET port for the Polycom CTI.
                            <br />
                            {IANA_PORT}
                        </>
                    ),
                    pattern: PORT_PATTERN,
                    min: 0,
                    max: 65536,
                },
                CTI_END_CALL_BUTTON: {
                    t: "string",
                    required: true,
                    documentation:
                        'Determines which softkey has the "end call" option on the Polycom phone. If on the second page add 4 to the button number. For example, page two button two the value is 6.',
                    pattern: NUMBER_PATTERN,
                    min: 0,
                },
                CTI_USERNAME: {
                    t: "string",
                    required: true,
                    documentation:
                        // TODO: improve
                        "Username used in the web form Polycom → Applications → Push and Polycom → Applications → Phone State Polling.",
                },
                CTI_PASSWORD: {
                    t: "string",
                    required: true,
                    documentation:
                        // TODO: improve
                        "Password used in the web form Polycom → Applications → Push and Polycom → Applications → Phone State Polling.",
                },
            },
        },
        Show_Answer_Button: {
            t: "boolean",
            required: false,
            documentation: "Display CTI answer button on GUI.",
        },
        Show_Dial_Pad_Button: {
            t: "boolean",
            required: false,
            documentation: "Display CTI dial pad tab on GUI.",
        },
        Show_Speed_Dial_Button: {
            t: "boolean",
            required: false,
            documentation: "Display dial button on GUI.",
        },
        Show_Callback_Button: {
            t: "boolean",
            required: false,
            documentation: "Display CTI callback button on GUI.",
        },
        Show_Tandem_Transfer_Button: {
            t: "boolean",
            required: false,
            documentation: "Display 9-1-1 transfer button on GUI.",
        },
        Show_Drop_Last_Leg_Button: {
            t: "boolean",
            required: false,
            documentation: (
                <>
                    Display 9-1-1 Drop Last Leg button on GUI.
                    <br />
                    note: if <code>Show_Tandem_Transfer_Button</code> is false
                    this button will not display.
                </>
            ),
        },
        Show_Attended_Transfer_Button: {
            t: "boolean",
            required: false,
            documentation: "Display attended transfer button on GUI.",
        },
        Show_Blind_Transfer_Button: {
            t: "boolean",
            required: false,
            documentation: "Display blind transfer button on GUI.",
        },
        Show_Conference_Transfer_Button: {
            t: "boolean",
            required: false,
            documentation: "Display conference transfer button on GUI.",
        },
        Show_Flash_Transfer_Button: {
            t: "boolean",
            required: false,
            documentation: "Display conference transfer button on GUI.",
        },
        Show_Flash_Hook_Button: {
            t: "boolean",
            required: false,
            documentation: "Display Flash Hook button on GUI.",
        },
        Show_Volume_Button: {
            t: "boolean",
            required: false,
            documentation: "Display CTI volume button on GUI.",
        },
        Show_Hangup_Button: {
            t: "boolean",
            required: false,
            documentation: "Display CTI hangup button on GUI.",
        },
        Show_Hold_Button: {
            t: "boolean",
            required: false,
            documentation: "Display CTI hold button on GUI.",
        },
        Show_Barge_Button: {
            t: "boolean",
            required: false,
            documentation: "Display CTI barge button on GUI.",
        },
        Show_Mute_Button: {
            t: "boolean",
            required: false,
            documentation: "Display CTI mute button on GUI.",
        },
        Show_Alert_Button: {
            t: "boolean",
            required: false,
            documentation: "Display GUI alert window button.",
        },
        Add_Nine_Outbound_Calls: {
            t: "optional-subsection",
            name: "Split Registered Phones",
            schema: {
                Add_Nine_Outbound_Calls: {
                    t: "boolean",
                    required: true,
                    documentation:
                        "Prepend a 9 for outbound administrative calls.",
                },
                LocalAreaCode: {
                    t: "string",
                    required: true,
                    documentation:
                        "Defines the area codes that are local calls for the administrative lines (NOT 9-1-1). Add a new entry for each area code.",
                    pattern: "^[0-9]{3}$",
                },
                LocalNXXPrefix: {
                    t: "simple-string-array",
                    required: true,
                    documentation: "Local NXX Prefix.",
                    pattern: "^[0-9]{3}$",
                },
            },
            documentation:
                "Used for split registered phoens.\nPrepend a 9 for outbound administrative calls.",
        },
        Routing_Prefix: {
            t: "simple-string-array",
            required: false,
            documentation:
                "Prefix used to route calls to different AudioCodes trunk groups. Omit if not used.",
        },
        NG911_Transfer_Header: {
            t: "string",
            required: false,
            documentation: "Set the header in Phonebook for NG9-1-1 transfers.",
            deprecated: true,
        },
        NG911_Transfer_Icon_File: {
            t: "string",
            required: false,
            deprecated: true,
        },
        Caller_ID_Name: {
            t: "string",
            required: true,
            documentation: "Default CallerID name for outbound calls.",
        },
        Caller_ID_Number: {
            t: "string",
            required: true,
            documentation: "Default CallerID number for outbound calls.",
        },
        Parking_Lot: {
            t: "simple-string-array",
            required: false,
            documentation: (
                <>
                    Parking Lot extensions. The first Lot is designated as the
                    valet lot. If the first lot is not a valet lot, use a dummy
                    extension such as <code>0000</code>.
                </>
            ),
            pattern: NUMBER_PATTERN,
        },
        Phones: {
            t: "complex-array-in-array",
            required: true,
            outer_schema: {
                Phone_Device_Name: {
                    t: "string",
                    required: true,
                    documentation: "Name of the phone.",
                },
                Phone_Device_Workstation: {
                    t: "string",
                    required: true,
                    pattern: NUMBER_PATTERN,
                    datalist: ["1", "2", "3", "4", "5"],
                    min: 1,
                    documentation:
                        "Workstation number associated with the phone.",
                },
                Phone_Device_MAC_Addr: {
                    t: "string",
                    required: true,
                    pattern:
                        "^[0-9a-fA-F]{1,2}([\\.:-])(?:[0-9a-fA-F]{1,2}\\1){4}[0-9a-fA-F]{1,2}$",
                    documentation: "The phone's MAC address.",
                },
                Phone_Device_PSAP_URI: {
                    t: "string",
                    required: false,
                    documentation: "URI of the PSAP that the phone belongs to.",
                },
                // MT-020 for example
                Phone_Device_Caller_Name: {
                    t: "string",
                    required: false,
                    documentation: (
                        <>
                            Overwrites <code>Caller_ID_Name</code>
                        </>
                    ),
                },
                Phone_Device_Caller_Number: {
                    t: "string",
                    required: false,
                    pattern: NUMBER_PATTERN,
                    documentation: (
                        <>
                            Overwrites <code>Caller_ID_Number</code>
                        </>
                    ),
                },
                Phone_Device_Type: {
                    t: "string",
                    required: true,
                    enum: ["670", "VVX"],
                    documentation: (
                        <>
                            <code>670</code> or <code>VVX</code>
                        </>
                    ),
                },
                Phone_Device_IPaddress: {
                    t: "string",
                    required: true,
                    pattern: IPV4_PATTERN,
                    documentation: "The phone's IPV4 address.",
                },
                Phone_Device_Status: {
                    t: "string",
                    required: true,
                    enum: ["Monitor", "UnMonitor"],
                    documentation:
                        "Monitor the status of the phone. Used in sites where phones are removed from service and replaced.",
                },
            },
            inner_prefix: "Line_",
            inner_array_name: "Lines",
            inner_length_name: "Phone_Device_Num_Lines",
            inner_schema: {
                _Reg_Name: {
                    t: "string",
                    required: true,
                    documentation: "SIP registartion name of the line.",
                },
                _Dial_Out: {
                    t: "boolean",
                    required: true,
                    documentation:
                        "Determines if CTI can dial outbout on this line.",
                },
                _Shared: {
                    t: "boolean",
                    required: true,
                    documentation: "Marks if the line is a shared line.",
                },
                _Index: {
                    t: "string",
                    required: true,
                    pattern: NUMBER_PATTERN,
                    datalist: ["1", "2", "3", "4", "5"],
                    min: 1,
                    documentation: "Index of the line.",
                },
            },
        },
        "Line View": {
            t: "numbered-array",
            required: false,
            prefix: "SLA_Line_",
            schema: {
                "": {
                    t: "string",
                    required: true,
                    documentation:
                        "The user name of each line. Each value should be unique.",
                },
                _Text: {
                    t: "string",
                    required: true,
                    documentation: "The label to display for each line.",
                },
            },
        },
        Gateways: {
            t: "array",
            required: true,
            schema: {
                Gateway_Name: {
                    t: "string",
                    required: true,
                    datalist: ["audiocodes-", "domain", "fusionpbx", "ESRP"],
                    documentation: "Name of IP gateway(s)",
                },
                Gateway_IP_Address: {
                    t: "string",
                    required: true,
                    pattern: IPV4_PATTERN,
                    documentation: "IP address of the gateway.",
                },
                Gateway_Dial_Ext: {
                    t: "boolean",
                    required: true,
                    documentation: "Whether the gateway can dial extensions.",
                },
                Gateway_Dial_Out: {
                    t: "boolean",
                    required: true,
                    documentation: "Whether the gateway can dial outbound.",
                },
            },
        },
        Contact_Relays: {
            t: "numbered-array",
            prefix: "Contact_Relay_",
            required: false,
            schema: {
                _IP_Address: {
                    t: "string",
                    required: false,
                    pattern: IPV4_PORT_PATTERN,
                    datalist: ["4998"],
                    documentation:
                        "IP address and port number of contact relay converter box.",
                },
            },
        },
    },
    ADVANCED: {
        // required: false,
        Display_And_Log_Mode: {
            t: "string",
            required: false,
            documentation: (
                <>
                    Selects different display and log settings
                    <br />
                    <ul>
                        <li>
                            Minimal_Display
                            <ul>
                                <li>Normal Log</li>
                                <li>
                                    Display only INFO, WARNING, ALARM to the
                                    screen,
                                </li>
                            </ul>
                        </li>
                        <li>
                            Normal_Display
                            <ul>
                                <li>Normal Log</li>
                                <li>Display all log data to the screen</li>
                            </ul>
                        </li>
                    </ul>
                </>
            ),
            // enum: [
            //     {
            //         value: "Minimal_Display",
            //         name:
            //             "Normal log, display only INFO, WARNING, ALARM to the screen",
            //     },
            //     {
            //         value: "Normal_Display",
            //         name: "Normal log, display all log data to the screen",
            //     },
            // ],
            enum: ["Minimal_Display", "Normal_Display"],
        },
        Polycom_Firmware_404_Plus: {
            t: "boolean",
            required: false,
            documentation:
                "Whether the Polycom telephones have firmware version 4.04 or better",
            deprecated: true,
        },
        FreeSwitch_Version_212_Plus: {
            t: "boolean",
            required: false,
            documentation: "Whether the FreeSWITCH version 2.12 or better",
            deprecated: true,
        },
        Alarm_POPUP_Duration_MS: {
            t: "string",
            required: false,
            documentation:
                "Number of milliseconds alarm PopUps display on the PTK.",
            pattern: NUMBER_PATTERN,
            min: 0,
        },
        TCP_Reconnect_Recursion_Level: {
            t: "string",
            required: false,
            documentation: (
                <>
                    calls to the TCP reconnect algorithm. is delayed by the
                    recursion level times al. After the last recursive call the
                    starts over in a continuous loop until cursions with an
                    interval of 5 yields.
                    <ol>
                        <li>reconnect attempt at +5 seconds from disconnect</li>
                        <li>
                            reconnect attempt at +10 seconds from last
                            unsuccessful connect attempt
                        </li>
                        <li>
                            reconnect attempt at +15 seconds from last
                            unsuccessful connect attempt
                        </li>
                        <li>
                            reconnect attempt at +20 seconds from last
                            unsuccessful connect attempt
                        </li>
                        <li>
                            reconnect attempt at +25 seconds from last
                            unsuccessful connect attempt
                        </li>
                        <li>
                            reconnect attempt at +30 seconds from last
                            unsuccessful connect attempt
                        </li>
                        <li>
                            reconnect attempt at +35 seconds from last
                            unsuccessful connect attempt
                        </li>
                        <li>Start over at 1.</li>
                    </ol>
                </>
            ),
            datalist: ["1", "2", "3", "4", "5", "6", "7", "8"],
            pattern: NUMBER_PATTERN,
            min: 0,
        },
        TCP_Reconnect_Time_Interval_sec: {
            t: "string",
            required: false,
            documentation:
                "Number of seconds used when calculating the delay between each reconnection attempt in the TCP reconnect algorithm.",
            pattern: NUMBER_PATTERN,
            min: 0,
        },
        Debug_Reminder_Sec: {
            t: "string",
            required: false,
            documentation:
                "Number of seconds between reminders when Debug Mode is activated.",
            pattern: NUMBER_PATTERN,
            min: 0,
        },
        Email_Service_On: {
            t: "boolean",
            required: false,
            documentation: "Turn the ability to email on.",
        },
        Email_Relay_Server: {
            t: "string",
            required: false,
            documentation:
                "Host address of SMTP Server. Format: IP or HTTP address",
        },
        Email_Relay_UserName: {
            t: "string",
            required: false,
            documentation: "Username for SMTP server if required.",
        },
        Email_Relay_Password: {
            t: "string",
            required: false,
            documentation: "Password for SMTP server if required.",
        },
    },
};
