import React from "react";

export interface SchemaString {
    readonly t: "string";
    readonly required: boolean;
    readonly documentation?: React.ReactNode;
    readonly enum?: (
        | string
        | { readonly value: string; readonly name: string }
    )[];
    readonly datalist?: string[];
    /** JavaScript-style regex that it must match, like the pattern attribute of the input element.
     *  Default pattern is ASCII printable characters.
     **/
    readonly pattern?: string;
    readonly min?: number;
    readonly max?: number;
    readonly deprecated?: boolean;
}

export interface SchemaBoolean {
    readonly t: "boolean";
    readonly required: boolean;
    readonly documentation?: React.ReactNode;
    readonly deprecated?: boolean;
}

export interface SchemaSimpleStringArray {
    readonly t: "simple-string-array";
    readonly required: boolean;
    readonly documentation?: React.ReactNode;
    readonly datalist?: string[];
    readonly pattern?: string;
    readonly min?: number;
    readonly max?: number;
    readonly deprecated?: boolean;
}

export interface SchemaSimpleNumberedArray {
    readonly t: "simple-numbered-array";
    readonly required: boolean;
    readonly documentation?: React.ReactNode;
    readonly pattern?: string;
    readonly datalist?: string[];
}

export interface SchemaNumberedArray {
    readonly t: "numbered-array";
    readonly prefix: string;
    readonly required: boolean;
    readonly schema: {
        readonly [index: string]:
            | SchemaString
            | SchemaBoolean
            | SchemaSimpleStringArray
            | SchemaSimpleNumberedArray
            | SchemaOptionalSubsection;
    };
    readonly documentation?: React.ReactNode;
}

export interface SchemaComplexArray {
    readonly t: "complex-array-in-array";
    readonly required: boolean;
    readonly outer_schema: {
        readonly [index: string]: SchemaString;
    };
    readonly inner_length_name: string;
    readonly inner_prefix: string;
    readonly inner_schema: {
        readonly [index: string]: SchemaString | SchemaBoolean;
    };
    readonly inner_array_name: string;
    readonly documentation?: React.ReactNode;
}

export interface SchemaOptionalSubsection {
    readonly t: "optional-subsection";
    readonly name: string;
    readonly schema: {
        readonly [index: string]:
            | SchemaString
            | SchemaBoolean
            | SchemaSimpleStringArray;
    };
    readonly documentation?: React.ReactNode;
    readonly ini_key?: string;
}

export interface SchemaArray {
    readonly t: "array";
    readonly required: boolean;
    readonly schema: {
        [index: string]: SchemaString | SchemaBoolean;
    };
    readonly documentation?: React.ReactNode;
}

export type LSchema =
    | SchemaBoolean
    | SchemaString
    | SchemaSimpleStringArray
    | SchemaNumberedArray
    | SchemaArray
    | SchemaComplexArray
    | SchemaOptionalSubsection;
export interface LittleSchema {
    readonly [index: string]: { readonly [index: string]: LSchema };
}
