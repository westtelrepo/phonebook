/** @jsx jsx */
import { css, jsx } from "@emotion/core";
import assert from "assert";
import { ReactNode, useMemo, useState } from "react";
import ReactDiffViewer from "react-diff-viewer";
import useSWR, { mutate } from "swr";
import createPersistedState from "use-persisted-state";

import * as ini from "./ini";
import { gen_ini_file, NG911File, NGTopLevelValue } from "./ng911ini";
import { parse_ini_file } from "./ng911ini";
import { little_schema } from "./schema/schema";
import { Section } from "./Section";
import { useBeforeUnload } from "./useBeforeUnload";
import { CollapseProvider } from "./useCollapse";

const useTextFile = createPersistedState("ng911ini");

function parse(
    ini_contents: string,
    trim = false,
): [NG911File | null, readonly string[]] {
    try {
        return parse_ini_file(ini.parseINI(ini_contents, trim));
    } catch (e) {
        return [null, [e.message]];
    }
}

const style_the_buttons = css`
    display: flex;
    flex-direction: column;
    button {
        font-size: 18px;
        margin: 10px;
    }

    .current_section {
        color: blue;
        font-weight: bold;
    }
`;

const style_the_all = css`
    display: flex;
    flex-direction: row;
    .menu {
        flex-basis: 150px;
    }
    .edit {
        flex-basis: 1200px;
    }
    * {
        box-sizing: border-box;
    }
    h1 {
        text-align: center;
    }
    .keyvalue {
        input[type="text"],
        select {
            width: 390px;
        }
        margin-top: 10px;
        margin-bottom: 10px;
    }
    .text_editor {
        width: 100%;
        min-height: 540px;
        max-height: 100vh;
        resize: none;
        font-family: Consolas, monospace;
    }

    input:invalid,
    select:invalid {
        border: red solid 3px;
        background-color: #ffdddd;
    }

    input:required:invalid,
    select:required:invalid {
        border: #800000 solid 3px;
    }

    .required {
        color: #800000;
    }

    .deprecated {
        color: #808080;
    }

    code {
        font-family: Consolas, monospace;
    }
`;

export function Editor() {
    const [_input, set_input] = useTextFile("");
    const input = _input === "" ? undefined : _input;
    const original = useSWR(
        "/api/ng911cfg",
        async url => {
            const d = await fetch(url);
            if (!d.ok) {
                throw new Error(
                    d.status + ":" + d.statusText + "\n" + (await d.text()),
                );
            }
            return d.text();
        },
        { refreshInterval: 15000 },
    );
    const [page, set_page_var] = useState("Text Editor");

    function set_page(page: string) {
        if (typeof input === "string") {
            const parsed = parse(input, true)[0];
            assert(parsed, "parse failed in set_page");
            set_input(gen_ini_file(parsed));
        }
        set_page_var(page);
    }

    const [state, errors] = useMemo(() => parse(input ?? original.data ?? ""), [
        input,
        original.data,
    ]);

    useBeforeUnload(() => input !== undefined);

    async function save() {
        if (page !== "Diff") {
            alert("Please review Diff page before saving.");
            return;
        }
        if (!confirm("Save local changes?")) {
            return;
        }

        try {
            const body = input ?? original.data;
            assert(typeof body === "string");
            const req = await fetch("/api/ng911cfg", {
                method: "POST",
                body,
                credentials: "same-origin",
                cache: "no-store",
                headers: {
                    "Content-Type": "text/plain",
                    "Cache-Control": "no-cache",
                    Pragma: "no-cache",
                },
            });

            if (!req.ok) {
                throw new Error(await req.text());
            }

            void mutate("/api/ng911cfg", body);
            set_input("");

            alert("Saved!");
        } catch (e) {
            alert(
                "ERROR: " + (e instanceof Error ? e.message : "unknown error"),
            );
        }
    }

    function reset() {
        if (page !== "Diff" && input !== original.data) {
            alert("Please review Diff page before throwing away local data.");
            return;
        }

        if (confirm("Throw away unsaved changes?")) {
            set_input("");
        }
    }

    function onChange(section: string, key: string, newValue: NGTopLevelValue) {
        assert(state, "state was null in onChange");
        // TODO: this is subtly broken
        const new_obj = {
            ...state,
            [section]: { ...state[section], [key]: newValue },
        };

        try {
            const gen = gen_ini_file(new_obj);
            set_input(gen);
        } catch (e) {
            console.error(e);
        }
    }

    function add(section: string) {
        const new_obj = { ...state, [section]: {} };

        try {
            const gen = gen_ini_file(new_obj);
            set_input(gen);
        } catch (e) {
            console.error(e);
        }
    }

    function remove(section: string) {
        assert(state, "state was null in remove");
        const new_obj = Object.fromEntries(
            Object.entries(state).filter(([x]) => x !== section),
        );

        try {
            const gen = gen_ini_file(new_obj);
            set_input(gen);
        } catch (e) {
            console.error(e);
        }
    }

    let page_content: ReactNode;
    switch (page) {
        case "Text Editor":
            page_content = (
                <div style={{ marginTop: 75 }}>
                    <textarea
                        className="text_editor"
                        style={{ marginBottom: 10, height: 500 }}
                        value={input ?? original.data ?? ""}
                        spellCheck={false}
                        onChange={e => {
                            set_input(e.target.value);
                        }}
                    />
                    <br />
                    {errors.length > 0 ? (
                        <div
                            style={{
                                backgroundColor: "#fdf2f5",
                                padding: 10,
                                border: "3px solid #f9d9df",
                                marginBottom: 10,
                            }}
                        >
                            <code
                                style={{
                                    color: "#b84053",
                                    whiteSpace: "pre-wrap",
                                }}
                            >
                                {errors.join("\n")}
                            </code>
                        </div>
                    ) : null}
                </div>
            );
            break;
        case "Original":
            page_content = (
                <code style={{ whiteSpace: "pre", marginTop: 75 }}>
                    {original.data}
                </code>
            );
            break;
        case "Diff":
            page_content = (
                <div style={{ marginTop: 75 }}>
                    <ReactDiffViewer
                        oldValue={original.data}
                        newValue={input ?? original.data}
                    />
                </div>
            );
            break;
        default:
            assert(state, "tried to display page when state was null");
            page_content = (
                <Section
                    key={page}
                    schema={little_schema[page]}
                    state={state[page]}
                    onChange={onChange.bind(null, page)}
                    add={add.bind(null, page)}
                    remove={remove.bind(null, page)}
                />
            );
            break;
    }

    if (original.error) {
        return (
            <div css={{ color: "red" }}>
                Error:{" "}
                {original.error instanceof Error
                    ? original.error.message
                    : JSON.stringify(original.error)}
            </div>
        );
    }

    return (
        <CollapseProvider>
            <button onClick={save} disabled={input === undefined}>
                Save
            </button>
            <button onClick={reset} disabled={input === undefined}>
                Throw Away Local Changes
            </button>
            <br />
            {input === undefined ? null : (
                <div>
                    There are local changes saved in your browser. These will
                    not be removed until ng911.cfg is either saved or local
                    changes are cleared.
                </div>
            )}
            <div css={style_the_all}>
                <div css={[style_the_buttons, { flexBasis: 150 }]}>
                    <button
                        onClick={() => set_page("Original")}
                        className={
                            page === "Original" ? "current_section" : undefined
                        }
                        disabled={page === "Original"}
                    >
                        Original
                    </button>
                    <button
                        onClick={() => set_page("Diff")}
                        className={
                            page === "Diff" ? "current_section" : undefined
                        }
                        disabled={page === "Diff"}
                    >
                        Diff
                    </button>
                    <button
                        onClick={() => set_page("Text Editor")}
                        className={
                            page === "Text Editor"
                                ? "current_section"
                                : undefined
                        }
                        disabled={page === "Text Editor"}
                    >
                        Text Editor
                    </button>
                    {Object.keys(little_schema).map(e => (
                        <button
                            key={e}
                            onClick={() => set_page(e)}
                            className={
                                page === e ? "current_section" : undefined
                            }
                            style={{ fontFamily: "Consolas, monospace" }}
                            disabled={!state || page === e}
                        >
                            [{e}]
                        </button>
                    ))}
                </div>
                <div className="edit">
                    <div
                        style={{
                            overflowY: "auto",
                            maxHeight: 800,
                        }}
                    >
                        <h1>{page}</h1>
                        {page_content}
                    </div>
                </div>
            </div>
        </CollapseProvider>
    );
}
