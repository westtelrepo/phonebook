import { useState } from "react";

let ids = 0;

export function useUniqueID(): string {
    return useState(() => "_uid_" + ++ids)[0];
}
