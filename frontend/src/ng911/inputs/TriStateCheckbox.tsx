import assert from "assert";
import React, { useLayoutEffect, useRef } from "react";

export function TriStateCheckbox(props: {
    value: boolean | undefined;
    onChange: (v: boolean | undefined) => void;
    required: boolean;
    name: string;
}) {
    const { value, onChange, required, name } = props;
    const ref = useRef<HTMLInputElement>(null);

    useLayoutEffect(() => {
        assert(
            ref.current,
            "programmer error in TriStateCheckbox component: ref.current is null",
        );
        switch (value) {
            case true:
            case false:
                ref.current.indeterminate = false;
                ref.current.setCustomValidity("");
                break;
            case undefined:
                if (required) {
                    ref.current.setCustomValidity("Please fill out this field");
                } else {
                    ref.current.setCustomValidity("");
                }
                ref.current.indeterminate = true;
                break;
        }
    }, [value, required, name]);

    return (
        <input
            ref={ref}
            type="checkbox"
            checked={value ?? false}
            onChange={() => {
                switch (value) {
                    case true:
                        onChange(false);
                        break;
                    case false:
                        if (props.required) {
                            onChange(true);
                        } else {
                            onChange(undefined);
                        }
                        break;
                    case undefined:
                        onChange(true);
                        break;
                }
            }}
        />
    );
}
