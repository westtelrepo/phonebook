import assert from "assert";
import React, { useLayoutEffect, useRef } from "react";

export function FancySelect(props: {
    value: string;
    onChange: (newValue: string) => void;
    enum: (string | { value: string; name: string })[];
    required: boolean;
    customValidity?: string;
}) {
    const { value, onChange, enum: enum_, required, customValidity } = props;
    const ref = useRef<HTMLSelectElement>(null);

    useLayoutEffect(() => {
        assert(
            ref.current,
            "Programmer error in FancySelect component: ref.current is null",
        );
        if (typeof customValidity === "string") {
            ref.current.setCustomValidity(customValidity);
        } else {
            ref.current.setCustomValidity("");
        }
    }, [customValidity]);

    return (
        <select
            ref={ref}
            value={value}
            onChange={e => onChange(e.target.value)}
            style={{ padding: 2 }}
            required={required}
        >
            <option value="">No option is selected</option>
            {value !== "" &&
            value !== undefined &&
            !enum_.some(x =>
                typeof x === "string" ? x === value : x.value === value,
            ) ? (
                <option value={value}>Unknown option: {value}</option>
            ) : null}
            {enum_.map(e => {
                const name = typeof e === "string" ? e : e.name;
                const val = typeof e === "string" ? e : e.value;
                return (
                    <option value={val} key={val}>
                        {typeof e === "string" ? name : `${val} : ${name}`}
                    </option>
                );
            })}
        </select>
    );
}
