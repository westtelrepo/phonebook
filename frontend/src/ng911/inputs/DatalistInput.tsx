import assert from "assert";
import React, { useLayoutEffect, useRef } from "react";

import {
    SchemaSimpleStringArray,
    SchemaString,
} from "../schema/InterfaceSchemas";
import { useUniqueID } from "../useUniqueID";

export function DatalistInput(
    props: Omit<
        React.DetailedHTMLProps<
            React.InputHTMLAttributes<HTMLInputElement>,
            HTMLInputElement
        >,
        "list"
    > & {
        schema: SchemaString | SchemaSimpleStringArray;
        value: string;
        customValidity?: string;
    },
) {
    const id = useUniqueID();
    const ref = useRef<HTMLInputElement>(null);

    useLayoutEffect(() => {
        assert(
            ref.current,
            "Programmer error in DatalistInput component: ref.current is null",
        );
        if (typeof props.customValidity === "string") {
            ref.current.setCustomValidity(props.customValidity);
        } else {
            ref.current.setCustomValidity("");
        }
    }, [props.customValidity]);

    return (
        <>
            <input
                ref={ref}
                list={props.schema.datalist ? id : undefined}
                {...Object.fromEntries(
                    Object.entries(props).filter(
                        ([x]) =>
                            x !== "datalist" &&
                            x !== "schema" &&
                            x !== "customValidity",
                    ),
                )}
                required={props.schema.required}
            />
            {props.schema.datalist ? (
                <datalist id={id}>
                    {props.schema.datalist.map(e => (
                        <option key={e} value={e} />
                    ))}
                </datalist>
            ) : null}
        </>
    );
}
