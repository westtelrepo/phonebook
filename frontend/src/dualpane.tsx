/** @jsx jsx */
import { css, jsx } from "@emotion/core";
import React from "react";

const style = css`
    width: 600px;
    height: 600px;
    overflow-y: auto;
    border: 2px solid;
`;

export function DualPane(props: {
    children: [React.ReactNode, React.ReactNode];
}) {
    const first = props.children[0];
    const second = props.children[1];

    return (
        <table>
            <tbody>
                <tr>
                    <td>
                        <div css={[style, { width: 400 }]}>{first}</div>
                    </td>
                    <td>
                        <div css={style}>{second}</div>
                    </td>
                </tr>
            </tbody>
        </table>
    );
}
