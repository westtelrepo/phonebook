/** @jsx jsx */
import { jsx } from "@emotion/core";
import { ANY, IPDPUser, pdp_allowed, pdp_ask } from "@w/shared/dist/pdp";
import * as Phonebook from "@w/shared/dist/phonebook";
import {
    IPatchContact,
    IPatchDirectory,
    IPostContact,
    IPostDirectory,
} from "@w/shared/dist/phonebook_interfaces";
import {
    formatPhoneNumber,
    is_sip_uri,
    normalize_phone_number,
} from "@w/shared/dist/validation";
import fuzzysort from "fuzzysort";
import Immutable from "immutable";
import React, { useContext, useEffect, useReducer } from "react";
import { useMemoOne } from "use-memo-one";

import {
    AddUndo,
    DEBOUNCE_TIME,
    divStyle,
    editorStyle,
    inline_icon,
    inputStyle,
} from "./constants";
import { DualPane } from "./dualpane";
import { json_delete, post, useDebounce, usePatch } from "./hooks";
import { IconField } from "./iconfield";
import { Tree, TreeEntry, TreeGroup } from "./tree_component";

interface IContactEditorState {
    readonly open_groups: Immutable.Set<string>; // which groups are currently open
    readonly selected_group: string | null; // which UUID is selected
    readonly selected_number: string | null; // which UUID is selected
}

type ContactEditorAction =
    | {
          op: "open_group";
          id: string;
      }
    | {
          op: "close_group";
          id: string;
      }
    | {
          op: "select_group";
          id: string;
      }
    | {
          op: "unselect_group";
      }
    | {
          op: "select_number";
          id: string;
      }
    | {
          op: "unselect_number";
      };

export function ContactsEditor(props: {
    readonly phonebook: Phonebook.Phonebook;
    readonly icon_list: string[];
    readonly search_text: string;
    readonly whoami: IPDPUser;
}) {
    function reducer(
        reducer_state: IContactEditorState,
        action: ContactEditorAction,
    ): IContactEditorState {
        switch (action.op) {
            case "open_group":
                return {
                    ...reducer_state,
                    open_groups: reducer_state.open_groups.add(action.id),
                };
            case "close_group":
                return {
                    ...reducer_state,
                    open_groups: reducer_state.open_groups.delete(action.id),
                };
            case "select_group": {
                return {
                    ...reducer_state,
                    selected_group: action.id,
                    selected_number: null,
                };
            }
            case "unselect_group":
                return {
                    ...reducer_state,
                    selected_group: null,
                    selected_number: null,
                };
            case "select_number": {
                return {
                    ...reducer_state,
                    selected_group: null,
                    selected_number: action.id,
                };
            }
            case "unselect_number":
                return { ...reducer_state, selected_number: null };
        }
    }

    const add_undo = useContext(AddUndo);

    const [state, dispatch] = useReducer(reducer, {
        open_groups: Immutable.Set(),
        selected_group: null,
        selected_number: null,
    });

    const { phonebook, search_text } = props;

    function groupOpen(id: string) {
        dispatch({ op: "open_group", id });
    }

    function groupSelect(id: string) {
        dispatch({ op: "select_group", id });
    }

    function numberSelect(id: string) {
        dispatch({ op: "select_number", id });
    }

    async function groupSort(gid: string) {
        try {
            const grp = props.phonebook.dirs.get(gid);
            if (grp && window.confirm(`Sort the contents of ${grp.name}?`)) {
                const ret = await post(
                    "/api/phonebook/" + phonebook.id + "/dirs/" + gid + "/sort",
                );
                if (
                    typeof ret === "object" &&
                    ret !== null &&
                    typeof ret.change_id === "number"
                ) {
                    add_undo(ret.change_id);
                }
            }
        } catch (e) {
            if (e instanceof Error) {
                window.alert("Error deleting directory: " + e.message);
            } else {
                throw e;
            }
        }
    }

    async function onGroupAdd() {
        try {
            let name = "New Group";
            if (props.phonebook.dirs.find(e => e.name === name)) {
                for (let i = 1; i < 100; i++) {
                    name = "New Group " + i;
                    if (!props.phonebook.dirs.find(e => e.name === name)) {
                        break;
                    }
                }
            }
            const post_directory: IPostDirectory = {
                name,
            };
            const ret: {
                id: string;
                change_id?: number;
            } = await post(
                "/api/phonebook/" + phonebook.id + "/dirs",
                post_directory,
            );
            if (typeof ret.change_id === "number") {
                add_undo(ret.change_id);
            }
            groupSelect(ret.id);
            groupOpen(ret.id);
        } catch (e) {
            if (e instanceof Error) {
                window.alert("Error adding directory: " + e.message);
            } else {
                throw e;
            }
        }
    }

    async function numberAdd(gid: string) {
        try {
            let name = "New Contact";

            const filter = props.phonebook.contacts.filter(
                x => x.parent === gid,
            );

            if (filter.find(e => e.name === name)) {
                for (let i = 1; i < 100; i++) {
                    name = "New Contact " + i;
                    if (!filter.find(e => e.name === name)) {
                        break;
                    }
                }
            }

            const post_contact: IPostContact = {
                name,
                parent: gid,
            };
            const id = await post(
                "/api/phonebook/" + phonebook.id + "/contacts",
                post_contact,
            );
            if (typeof id.change_id === "number") {
                add_undo(id.change_id);
            }
            numberSelect(id.id);
        } catch (e) {
            if (e instanceof Error) {
                window.alert("Error adding contact: " + e.message);
            } else {
                throw e;
            }
        }
    }

    async function onGroupDelete(id: string) {
        try {
            if (!phonebook.children_of(id).isEmpty()) {
                window.alert(
                    "Can not delete directory that still contains entries!",
                );
                return;
            }
            const ret = await json_delete(
                "/api/phonebook/" + phonebook.id + "/dirs/" + id,
            );
            if (
                typeof ret === "object" &&
                ret !== null &&
                typeof ret.change_id === "number"
            ) {
                add_undo(ret.change_id);
            }
        } catch (e) {
            if (e instanceof Error) {
                window.alert("Error deleting directory: " + e.message);
            } else {
                throw e;
            }
        }
    }

    async function numberDelete(id: string) {
        try {
            const n = props.phonebook.contacts.get(id);
            if (phonebook.speed_dials.find(x => x.contact_id === id)) {
                window.alert(
                    "Can not delete contact that is referenced by a speed dial!",
                );
                return;
            }
            if (n && window.confirm(`Delete ${n.name}?`)) {
                const ret = await json_delete(
                    "/api/phonebook/" + phonebook.id + "/contacts/" + id,
                );
                if (
                    typeof ret === "object" &&
                    ret !== null &&
                    typeof ret.change_id === "number"
                ) {
                    add_undo(ret.change_id);
                }
            }
        } catch (e) {
            if (e instanceof Error) {
                window.alert("Error deleting contact: " + e.message);
            } else {
                throw e;
            }
        }
    }

    async function groupUp(id: string) {
        try {
            const ret = await post(
                "/api/phonebook/" + phonebook.id + "/dirs/" + id + "/moveup",
            );
            if (
                typeof ret === "object" &&
                ret !== null &&
                typeof ret.change_id === "number"
            ) {
                add_undo(ret.change_id);
            }
        } catch (e) {
            if (e instanceof Error) {
                window.alert("Error moving directory: " + e.message);
            } else {
                throw e;
            }
        }
    }

    async function groupDown(id: string) {
        try {
            const ret = await post(
                "/api/phonebook/" + phonebook.id + "/dirs/" + id + "/movedown",
            );
            if (
                typeof ret === "object" &&
                ret !== null &&
                typeof ret.change_id === "number"
            ) {
                add_undo(ret.change_id);
            }
        } catch (e) {
            if (e instanceof Error) {
                window.alert("Error moving directory: " + e.message);
            } else {
                throw e;
            }
        }
    }

    async function numberUp(id: string) {
        try {
            const ret = await post(
                "/api/phonebook/" +
                    phonebook.id +
                    "/contacts/" +
                    id +
                    "/moveup",
            );
            if (
                typeof ret === "object" &&
                ret !== null &&
                typeof ret.change_id === "number"
            ) {
                add_undo(ret.change_id);
            }
        } catch (e) {
            if (e instanceof Error) {
                window.alert("Error moving contact: " + e.message);
            } else {
                throw e;
            }
        }
    }

    async function numberDown(id: string) {
        try {
            const ret = await post(
                "/api/phonebook/" +
                    phonebook.id +
                    "/contacts/" +
                    id +
                    "/movedown",
            );
            if (
                typeof ret === "object" &&
                ret !== null &&
                typeof ret.change_id === "number"
            ) {
                add_undo(ret.change_id);
            }
        } catch (e) {
            if (e instanceof Error) {
                window.alert("Error moving contact: " + e.message);
            } else {
                throw e;
            }
        }
    }

    interface IToSearch {
        name: string;
        id: string;
    }

    let showable: Map<string, Fuzzysort.KeysResult<IToSearch> | []> | undefined;
    if (search_text !== "") {
        showable = new Map();
        const to_search: IToSearch[] = [];
        for (const group of phonebook.dirs.filter(
            x => x.parent === undefined,
        )) {
            to_search.push({ name: group[1].name, id: group[0] });
            for (const contact of phonebook.contacts.filter(
                x => x.parent === group[0],
            )) {
                to_search.push({ name: contact[1].name, id: contact[0] });
            }
        }

        const search = fuzzysort.go(search_text, to_search, {
            keys: ["name"],
            threshold: -1000,
        });

        for (const result of search) {
            showable.set(result.obj.id, result);
        }

        for (const contact of props.phonebook.contacts) {
            if (contact[1].contact.includes(search_text)) {
                showable.set(contact[0], showable.get(contact[0]) ?? []);
            }
        }
    }

    const match = ([id, obj]: [
        string,
        Phonebook.Directory | Phonebook.Contact,
    ]): boolean => {
        if (!showable) {
            return true;
        } else if (showable.has(id)) {
            return true;
        } else if (obj instanceof Phonebook.Directory) {
            // do any entries show?
            return props.phonebook.children_of_only_contacts(id).some(match);
        } else {
            return false;
        }
    };

    const display = (id: string, name: string): React.ReactNode => {
        if (showable) {
            const result = showable.get(id);
            if (result) {
                // we only look at the first result because the first result is for the key 'name'
                // eslint-disable-next-line @typescript-eslint/strict-boolean-expressions
                if (result[0]) {
                    const indexes = result[0].indexes;
                    let i = 0;
                    const ret: JSX.Element[] = [];
                    for (const c of name) {
                        if (indexes.find(value => value === i) !== undefined) {
                            ret.push(
                                <b style={{ color: "#C62828" }} key={i}>
                                    {c}
                                </b>,
                            );
                        } else {
                            ret.push(<span key={i}>{c}</span>);
                        }
                        i++;
                    }
                    return ret;
                } else {
                    return name;
                }
            } else {
                return name;
            }
        } else {
            return name;
        }
    };

    const groups = phonebook
        .children_of_only_dirs(undefined)
        .filter(match)
        .map(
            ([id, g]): TreeGroup => ({
                select: groupSelect.bind(null, id),
                unselect: () => dispatch({ op: "unselect_group" }),
                open: groupOpen.bind(null, id),
                close: () => dispatch({ op: "close_group", id }),
                trash: onGroupDelete.bind(null, id),
                up: groupUp.bind(null, id),
                down: groupDown.bind(null, id),
                sort: groupSort.bind(null, id),
                is_selected: id === state.selected_group,
                key: id,
                text: (
                    <span>
                        {inline_icon(phonebook.id, g.icon)}{" "}
                        {display(id, g.name)}
                    </span>
                ),
                is_open: state.open_groups.has(id),
                items: props.phonebook
                    .children_of_only_contacts(id)
                    .filter(match)
                    .map(
                        ([nid, n]): TreeEntry => ({
                            key: nid,
                            select: numberSelect.bind(null, nid),
                            unselect: () => dispatch({ op: "unselect_number" }),
                            up: numberUp.bind(null, nid),
                            down: numberDown.bind(null, nid),
                            trash: numberDelete.bind(null, nid),
                            is_selected: state.selected_number === nid,
                            text: (
                                <span>
                                    {inline_icon(phonebook.id, n.icon)}{" "}
                                    {display(nid, n.name)}
                                </span>
                            ),
                        }),
                    ),
                add_item: search_text ? undefined : numberAdd.bind(null, id),
            }),
        );

    let editor: JSX.Element | undefined;
    if (state.selected_number !== null) {
        editor = (
            <NumberEditor
                icon_list={props.icon_list}
                cid={state.selected_number}
                phonebook={phonebook}
                whoami={props.whoami}
            />
        );
    } else if (state.selected_group !== null) {
        editor = (
            <GroupEditor
                phonebook={phonebook}
                icon_list={props.icon_list}
                gid={state.selected_group}
            />
        );
    }

    return (
        <DualPane>
            <Tree
                groups={groups}
                onGroupAdd={search_text ? undefined : onGroupAdd}
                new_item={
                    <span>
                        {inline_icon(phonebook.id, "person.ico")}{" "}
                        <i>New Contact</i>
                    </span>
                }
                new_group={
                    <span>
                        {inline_icon(phonebook.id, "folder.ico")}{" "}
                        <i>New Group</i>
                    </span>
                }
            />
            {editor}
        </DualPane>
    );
}

interface INumberEditorState {
    pid: string;
    cid: string;
    to_send: IPatchContact | null;
}

type INumberEditorAction =
    | {
          op: "set_cid";
          pid: string;
          cid: string;
      }
    | {
          op: "update_name";
          name: string;
      }
    | {
          op: "update_icon";
          icon: string | null;
      }
    | {
          op: "update_contact";
          contact: string;
      }
    | {
          op: "update_parent";
          parent: string | null;
      }
    | {
          op: "update_ng911";
          ng911: boolean;
      }
    | {
          op: "update_blind";
          blind: boolean;
      }
    | {
          op: "update_attended";
          attended: boolean;
      }
    | {
          op: "update_conference";
          conference: boolean;
      }
    | {
          op: "update_outbound";
          outbound: boolean;
      }
    | {
          op: "update_ng911_transfer_type";
          ng911_transfer_type: Phonebook.ng911_transfer_type;
      };

function number_reducer(
    state: INumberEditorState,
    action: INumberEditorAction,
): INumberEditorState {
    switch (action.op) {
        case "set_cid":
            return { pid: action.pid, cid: action.cid, to_send: null };
        case "update_name":
            return {
                ...state,
                to_send: { ...state.to_send, name: action.name },
            };
        case "update_icon":
            return {
                ...state,
                to_send: { ...state.to_send, icon: action.icon },
            };
        case "update_contact":
            return {
                ...state,
                to_send: { ...state.to_send, contact: action.contact },
            };
        case "update_parent":
            return {
                ...state,
                to_send: { ...state.to_send, parent: action.parent },
            };
        case "update_ng911":
            return {
                ...state,
                to_send: { ...state.to_send, xfer_ng911: action.ng911 },
            };
        case "update_blind":
            return {
                ...state,
                to_send: { ...state.to_send, xfer_blind: action.blind },
            };
        case "update_attended":
            return {
                ...state,
                to_send: { ...state.to_send, xfer_attended: action.attended },
            };
        case "update_conference":
            return {
                ...state,
                to_send: {
                    ...state.to_send,
                    xfer_conference: action.conference,
                },
            };
        case "update_outbound":
            return {
                ...state,
                to_send: { ...state.to_send, xfer_outbound: action.outbound },
            };
        case "update_ng911_transfer_type":
            return {
                ...state,
                to_send: {
                    ...state.to_send,
                    ng911_transfer_type: action.ng911_transfer_type,
                },
            };
    }
}

export function NumberEditor(props: {
    icon_list: string[];
    cid: string;
    phonebook: Phonebook.Phonebook;
    readonly whoami: IPDPUser;
}) {
    const { icon_list, cid, phonebook } = props;

    const [state, dispatch] = useReducer(number_reducer, {
        pid: phonebook.id,
        cid,
        to_send: null,
    });

    const debounced = useDebounce(state, DEBOUNCE_TIME);

    useEffect(() => {
        dispatch({ op: "set_cid", cid: props.cid, pid: phonebook.id });
    }, [phonebook.id, props.cid]);

    const to_send = useMemoOne<IPatchContact | null>(() => {
        if (debounced.to_send) {
            const normalized =
                typeof debounced.to_send.contact === "string"
                    ? normalize_phone_number(debounced.to_send.contact)
                    : undefined;
            // if it doesn't normalize, don't even send it (it is invalid)
            if (normalized === false) {
                return null;
            }

            return {
                ...debounced.to_send,
                contact: normalized,
                name:
                    debounced.to_send.name !== undefined
                        ? debounced.to_send.name.trim()
                        : undefined,
            };
        } else {
            return null;
        }
    }, [debounced.to_send]);

    usePatch(
        "/api/phonebook/" + debounced.pid + "/contacts/" + debounced.cid,
        to_send,
    );

    const current = phonebook.contacts.get(cid);

    if (!current) {
        return null;
    }

    const name = [(state.to_send ?? {}).name, current.name].find(
        x => typeof x === "string",
    );
    const icon =
        [(state.to_send ?? {}).icon, current.icon].find(
            x => typeof x === "string" || x === null,
        ) ?? null;
    const contact = [
        (state.to_send ?? {}).contact,
        formatPhoneNumber(current.contact),
    ].find(x => typeof x === "string");

    const ng911 = [(state.to_send ?? {}).xfer_ng911, current.xfer_ng911].find(
        x => typeof x === "boolean",
    );
    const blind = [(state.to_send ?? {}).xfer_blind, current.xfer_blind].find(
        x => typeof x === "boolean",
    );
    const attended = [
        (state.to_send ?? {}).xfer_attended,
        current.xfer_attended,
    ].find(x => typeof x === "boolean");
    const conference = [
        (state.to_send ?? {}).xfer_conference,
        current.xfer_conference,
    ].find(x => typeof x === "boolean");
    const outbound = [
        (state.to_send ?? {}).xfer_outbound,
        current.xfer_outbound,
    ].find(x => typeof x === "boolean");
    const parent =
        [(state.to_send ?? {}).parent, current.parent].find(
            x => x !== undefined,
        ) ?? "unknown"; // TODO: undefined is root directory
    const ng911_transfer_type =
        (state.to_send ?? {}).ng911_transfer_type ??
        current.ng911_transfer_type ??
        "internal";

    const group_list = props.phonebook
        .children_of_only_dirs(undefined)
        .map(x => {
            const disabled_option =
                x[0] !== parent &&
                phonebook.children_of(x[0]).some(y => y[1].name === name);
            return {
                id: x[0],
                name: x[1].name,
                disabled: disabled_option,
                title: disabled
                    ? "This group already contains a contact by that name"
                    : undefined,
            };
        });

    const disabled = !pdp_allowed({
        user: props.whoami,
        target: { type: "CONTACT", is_sip: is_sip_uri(current.contact) },
        action: { type: "EDIT", attr_name: ANY },
    });

    const phone_number_error = (() => {
        const e = pdp_ask({
            user: props.whoami,
            target: {
                type: "CONTACT",
                is_sip:
                    typeof contact === "string" ? is_sip_uri(contact) : false,
            },
            action: { type: "EDIT", attr_name: ANY },
        });

        if (
            state.to_send?.contact !== undefined &&
            normalize_phone_number(state.to_send.contact) === false
        ) {
            return <span style={{ color: "red" }}>Invalid Phone Number</span>;
        } else if (!disabled && e !== false) {
            return <span style={{ color: "red" }}>{e.message}</span>;
        } else {
            return null;
        }
    })();

    const name_error =
        state.to_send?.name !== undefined &&
        phonebook
            .children_of(parent)
            // eslint-disable-next-line @typescript-eslint/no-non-null-assertion
            .some(x => x[0] !== cid && x[1].name === state.to_send!.name) ? (
            <span style={{ color: "red" }}>Duplicate Name</span>
        ) : null;

    return (
        <div css={editorStyle}>
            <div css={divStyle}>
                <label>
                    Contact Name {name_error}
                    <br />
                    <input
                        css={inputStyle}
                        type="text"
                        value={name}
                        onChange={e =>
                            dispatch({
                                op: "update_name",
                                name: e.target.value,
                            })
                        }
                        disabled={disabled}
                    />
                </label>
            </div>

            <div css={divStyle}>
                <label>
                    Phone Number/NG Transfer URI {phone_number_error}
                    <br />
                    <input
                        css={inputStyle}
                        type="text"
                        value={contact}
                        onChange={e =>
                            dispatch({
                                op: "update_contact",
                                contact: e.target.value,
                            })
                        }
                        disabled={disabled}
                    />
                </label>
            </div>

            <div css={divStyle}>
                <label>
                    Group
                    <br />
                    <select
                        css={inputStyle}
                        value={parent}
                        onChange={e =>
                            dispatch({
                                op: "update_parent",
                                parent: e.target.value,
                            })
                        }
                        disabled={disabled}
                    >
                        {group_list.map(g => (
                            <option
                                key={g.id}
                                value={g.id}
                                disabled={g.disabled}
                                title={g.title}
                            >
                                {g.name}
                            </option>
                        ))}
                    </select>
                </label>
            </div>

            {typeof contact === "string" && is_sip_uri(contact) ? (
                <div css={divStyle}>
                    <label>
                        SIP Type
                        <br />
                        <select
                            css={inputStyle}
                            value={ng911_transfer_type}
                            onChange={e =>
                                dispatch({
                                    op: "update_ng911_transfer_type",
                                    ng911_transfer_type: e.target.value as any,
                                })
                            }
                            disabled={disabled}
                        >
                            <option value="internal">internal</option>
                            <option value="external">external</option>
                            <option value="external.experient">
                                external.experient
                            </option>
                        </select>
                    </label>
                </div>
            ) : (
                <div
                    style={{
                        marginTop: "10px",
                        marginLeft: "10px",
                        marginBottom: "10px",
                    }}
                >
                    <label>
                        <input
                            type="checkbox"
                            checked={ng911}
                            onChange={e =>
                                dispatch({
                                    op: "update_ng911",
                                    ng911: e.target.checked,
                                })
                            }
                            disabled={disabled}
                        />{" "}
                        9-1-1 Transfer
                    </label>
                    <br />
                    <label>
                        <input
                            type="checkbox"
                            checked={blind}
                            onChange={e =>
                                dispatch({
                                    op: "update_blind",
                                    blind: e.target.checked,
                                })
                            }
                            disabled={disabled}
                        />{" "}
                        Blind Transfer
                    </label>
                    <br />
                    <label>
                        <input
                            type="checkbox"
                            checked={attended}
                            onChange={e =>
                                dispatch({
                                    op: "update_attended",
                                    attended: e.target.checked,
                                })
                            }
                            disabled={disabled}
                        />{" "}
                        Attended Transfer
                    </label>
                    <br />
                    <label>
                        <input
                            type="checkbox"
                            checked={conference}
                            onChange={e =>
                                dispatch({
                                    op: "update_conference",
                                    conference: e.target.checked,
                                })
                            }
                            disabled={disabled}
                        />{" "}
                        Conference Transfer
                    </label>
                    <br />
                    <label>
                        <input
                            type="checkbox"
                            checked={outbound}
                            onChange={e =>
                                dispatch({
                                    op: "update_outbound",
                                    outbound: e.target.checked,
                                })
                            }
                            disabled={disabled}
                        />{" "}
                        Outbound
                    </label>
                    <br />
                </div>
            )}

            <IconField
                icon_list={icon_list}
                pid={phonebook.id}
                onChange={e => dispatch({ op: "update_icon", icon: e })}
                value={icon}
                disabled={disabled}
            />
        </div>
    );
}

interface IGroupEditorState {
    pid: string;
    gid: string;
    to_send: IPatchDirectory | null;
}

type IGroupEditorAction =
    | {
          op: "set_gid";
          pid: string;
          gid: string;
      }
    | {
          op: "update_name";
          name: string;
      }
    | {
          op: "update_icon";
          icon: string | null;
      };

function group_reducer(
    state: IGroupEditorState,
    action: IGroupEditorAction,
): IGroupEditorState {
    switch (action.op) {
        case "set_gid":
            return { pid: action.pid, gid: action.gid, to_send: null };
        case "update_name":
            return {
                ...state,
                to_send: { ...state.to_send, name: action.name },
            };
        case "update_icon":
            return {
                ...state,
                to_send: { ...state.to_send, icon: action.icon },
            };
    }
}

export function GroupEditor(props: {
    icon_list: string[];
    gid: string;
    phonebook: Phonebook.Phonebook;
}) {
    const { icon_list, gid, phonebook } = props;

    const [state, dispatch] = useReducer(group_reducer, {
        pid: props.phonebook.id,
        gid,
        to_send: null,
    });

    useEffect(() => {
        dispatch({ op: "set_gid", gid: props.gid, pid: props.phonebook.id });
    }, [props.phonebook.id, props.gid]);

    const debounced = useDebounce(state, DEBOUNCE_TIME);

    const to_send = useMemoOne(() => {
        if (debounced.to_send === null) {
            return null;
        }

        return {
            ...debounced.to_send,
            name:
                typeof debounced.to_send.name === "string"
                    ? debounced.to_send.name.trim()
                    : undefined,
        };
    }, [debounced.to_send]);

    usePatch(
        "/api/phonebook/" + debounced.pid + "/dirs/" + debounced.gid,
        to_send,
    );

    const current = props.phonebook.dirs.get(gid);

    if (!current) {
        return null;
    }

    const name = [(state.to_send ?? {}).name, current.name].find(
        x => typeof x === "string",
    );
    const icon =
        [(state.to_send ?? {}).icon, current.icon].find(
            x => typeof x === "string" || x === null,
        ) ?? null;

    const name_error =
        state.to_send?.name !== undefined &&
        phonebook
            .children_of(undefined)
            // eslint-disable-next-line @typescript-eslint/no-non-null-assertion
            .some(x => x[0] !== gid && x[1].name === state.to_send!.name) ? (
            <span style={{ color: "red" }}>Duplicate Name</span>
        ) : null;

    return (
        <div css={editorStyle}>
            <div css={divStyle}>
                <label>
                    Name {name_error}
                    <br />
                    <input
                        css={inputStyle}
                        type="text"
                        value={name}
                        onChange={e =>
                            dispatch({
                                op: "update_name",
                                name: e.target.value,
                            })
                        }
                    />
                </label>
            </div>

            <IconField
                onChange={new_icon =>
                    dispatch({ op: "update_icon", icon: new_icon })
                }
                value={icon}
                icon_list={icon_list}
                pid={phonebook.id}
            />
        </div>
    );
}
