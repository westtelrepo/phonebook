/** @jsx jsx */
import { css, jsx } from "@emotion/core";
import { IUser } from "@w/shared/dist/db_users";
import { ANY, pdp_allowed } from "@w/shared/dist/pdp";
import { useState } from "react";

import {
    divStyle,
    editorStyle,
    HOVER_ITEM_COLOR,
    inputStyle,
    SELECTED_COLOR,
    UNSELECTED_ITEM_COLOR,
} from "./constants";
import { DualPane } from "./dualpane";
import { json_delete, post } from "./hooks";
import { useServerState } from "./useServerState";

export function UsersEditor(props: {
    whoami: { permissions: any; is_superadmin: boolean; uid: string };
}) {
    const user_list =
        useServerState<(IUser & { permissions: any })[]>(
            "/api/user/list",
            o => o as any,
        ) ?? [];
    const [current, set_current] = useState<any>(null);

    const unselected_style = css({
        "&:hover": {
            backgroundColor: HOVER_ITEM_COLOR,
        },
    });
    const selected_style = css({
        backgroundColor: SELECTED_COLOR,
    });
    const common_style = css({
        transition: "background-color 0.25s ease-out",
        padding: 2,
    });

    return (
        <DualPane>
            <div
                style={{
                    backgroundColor: UNSELECTED_ITEM_COLOR,
                    height: "100%",
                }}
            >
                {user_list.map(e => (
                    <div
                        key={e.uid}
                        onClick={
                            current && current.uid === e.uid
                                ? () => set_current(null)
                                : () => set_current(e)
                        }
                        css={[
                            current && current.uid === e.uid
                                ? selected_style
                                : unselected_style,
                            common_style,
                        ]}
                    >
                        {e.username}
                    </div>
                ))}
                <div
                    key="new"
                    onClick={
                        current && !current.uid
                            ? _ => set_current(null)
                            : addUser
                    }
                    css={[
                        current && !current.uid
                            ? selected_style
                            : unselected_style,
                        common_style,
                    ]}
                >
                    New User
                </div>
            </div>
            {current ? (
                <UserEditor
                    current={current}
                    onChange={onUserChange}
                    onSave={onUserSave}
                    onUserDelete={onUserDelete}
                    whoami={props.whoami}
                    username_exists={u =>
                        user_list.some(
                            x => x.uid !== current.uid && x.username === u,
                        )
                    }
                />
            ) : (
                <div />
            )}
        </DualPane>
    );

    function addUser() {
        set_current({
            username: "",
            is_superadmin: false,
            permissions: {},
        });
    }

    function onUserChange(e: any) {
        set_current(e);
    }

    async function onUserSave() {
        await post("/api/user/upsert", current);
        set_current(null);
    }

    async function onUserDelete() {
        await json_delete("/api/user/" + current.uid);
        set_current(null);
    }
}

function UserEditor(props: {
    current: any;
    whoami: { permissions: any; is_superadmin: boolean; uid: string };
    onChange: (current: any) => void;
    onSave: () => void;
    onUserDelete: (current: any) => void;
    username_exists: (username: string) => boolean;
}) {
    const { whoami, current } = props;

    const buttonStyle = css({
        fontSize: "inherit",
        marginLeft: 1,
        marginRight: 1,
    });

    const name_error = props.username_exists(current.username) ? (
        <span style={{ color: "red" }}>That username already exists</span>
    ) : null;

    return (
        <div css={editorStyle}>
            <div css={divStyle}>
                <label>
                    Username {name_error}
                    <br />
                    <input
                        disabled={
                            !pdp_allowed({
                                user: whoami,
                                target: { type: "USER", user: current },
                                action: { type: "EDIT", attr_name: "username" },
                            })
                        }
                        css={inputStyle}
                        type="text"
                        value={current.username}
                        onChange={onTextChange.bind(null, "username")}
                    />
                </label>
            </div>

            <div css={divStyle}>
                <label>
                    Set New Password
                    <br />
                    <input
                        disabled={
                            !pdp_allowed({
                                user: whoami,
                                target: { type: "USER", user: current },
                                action: { type: "EDIT", attr_name: "password" },
                            })
                        }
                        css={inputStyle}
                        type="password"
                        value={current.password ? current.password : ""}
                        onChange={onTextChange.bind(null, "password")}
                    />
                </label>
            </div>

            <div
                css={css`
                    margin-top: 10px;
                    margin-left: 10px;
                    margin-bottom: 10px;
                `}
            >
                {/* the next two permissions are to prevent the user from editing themselves
            so they don't lock themselves out */}
                <label>
                    <input
                        disabled={
                            current.uid === whoami.uid ||
                            !pdp_allowed({
                                user: whoami,
                                target: { type: "USER", user: current },
                                action: {
                                    type: "EDIT",
                                    attr_name: "is_superadmin",
                                },
                            })
                        }
                        type="checkbox"
                        checked={current.is_superadmin}
                        onChange={onCheckboxChange.bind(null, "is_superadmin")}
                    />{" "}
                    Superadmin
                </label>
                <br />
                <label>
                    <input
                        disabled={
                            current.uid === whoami.uid ||
                            !pdp_allowed({
                                user: whoami,
                                target: { type: "USER", user: current },
                                action: {
                                    type: "EDIT",
                                    attr_name: "permissions",
                                },
                            })
                        }
                        type="checkbox"
                        checked={current.permissions.USER_MANAGEMENT || false}
                        onChange={onPermissionChange.bind(
                            null,
                            "USER_MANAGEMENT",
                        )}
                    />{" "}
                    User Management
                </label>
                <br />
                <label>
                    <input
                        disabled={
                            !pdp_allowed({
                                user: whoami,
                                target: { type: "USER", user: current },
                                action: {
                                    type: "EDIT",
                                    attr_name: "permissions",
                                },
                            })
                        }
                        type="checkbox"
                        checked={current.permissions.UPDATE_PHONEBOOK || false}
                        onChange={onPermissionChange.bind(
                            null,
                            "UPDATE_PHONEBOOK",
                        )}
                    />{" "}
                    Can Update Phonebook (Click Update Button)
                </label>
                <br />
                <label>
                    <input
                        disabled={
                            !pdp_allowed({
                                user: whoami,
                                target: { type: "USER", user: current },
                                action: {
                                    type: "EDIT",
                                    attr_name: "permissions",
                                },
                            })
                        }
                        type="checkbox"
                        checked={
                            current.permissions.PHONEBOOK_IMPORT_EXPORT || false
                        }
                        onChange={onPermissionChange.bind(
                            null,
                            "PHONEBOOK_IMPORT_EXPORT",
                        )}
                    />{" "}
                    Can use Phonebook importer/exporter
                </label>
                <br />
                <label>
                    <input
                        disabled={
                            !pdp_allowed({
                                user: whoami,
                                target: { type: "USER", user: current },
                                action: {
                                    type: "EDIT",
                                    attr_name: "permissions",
                                },
                            })
                        }
                        type="checkbox"
                        checked={current.permissions.PHONEBOOK_HISTORY || false}
                        onChange={onPermissionChange.bind(
                            null,
                            "PHONEBOOK_HISTORY",
                        )}
                    />{" "}
                    Can open History tab
                </label>
                <br />
                <label>
                    <input
                        disabled={
                            !pdp_allowed({
                                user: whoami,
                                target: { type: "USER", user: current },
                                action: {
                                    type: "EDIT",
                                    attr_name: "permissions",
                                },
                            })
                        }
                        type="checkbox"
                        checked={current.permissions.CAN_SIP_EDIT || false}
                        onChange={onPermissionChange.bind(null, "CAN_SIP_EDIT")}
                    />{" "}
                    Can edit SIP contact records
                </label>
                <br />
            </div>

            <div>
                <button
                    disabled={
                        !pdp_allowed({
                            user: whoami,
                            target: { type: "USER", user: current },
                            action: { type: "EDIT", attr_name: ANY },
                        })
                    }
                    css={buttonStyle}
                    onClick={() => props.onSave()}
                >
                    {current.uid ? "Save User" : "Add User"}
                </button>
                {current.uid ? (
                    <button
                        disabled={
                            !pdp_allowed({
                                user: whoami,
                                target: { type: "USER", user: current },
                                action: { type: "DELETE" },
                            })
                        }
                        css={buttonStyle}
                        onClick={onDelete}
                    >
                        Delete
                    </button>
                ) : null}
            </div>
        </div>
    );

    function onTextChange(name: string, e: any) {
        props.onChange({ ...current, [name]: e.target.value });
    }

    function onCheckboxChange(name: string, e: any) {
        props.onChange({ ...current, [name]: e.target.checked });
    }

    function onPermissionChange(name: string, e: any) {
        props.onChange({
            ...current,
            permissions: { ...current.permissions, [name]: e.target.checked },
        });
    }

    function onDelete() {
        if (
            !window.confirm(
                `Are you sure you wish to delete the user ${current.username}?`,
            )
        ) {
            return;
        }
        props.onUserDelete({ uid: current.uid });
    }
}
