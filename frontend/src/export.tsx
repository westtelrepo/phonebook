/* eslint-disable @typescript-eslint/no-non-null-assertion */
/** @jsx jsx */
import { css, jsx } from "@emotion/core";
import { Contact, Phonebook } from "@w/shared/dist/phonebook";
import { formatPhoneNumber } from "@w/shared/dist/validation";
import * as d3 from "d3-dsv";
import Immutable from "immutable";
import { useState } from "react";

import { editorStyle, inputStyle } from "./constants";

const table_style = css`
    border-collapse: collapse;
    width: 1000px;

    th,
    td {
        border: 1px solid black;
        font-size: 14px;
    }
    thead,
    tbody {
        display: block;
        width: 100%;
        table-layout: fixed;
    }
    tbody {
        display: block;
        height: 420px;
        overflow-y: scroll;
    }

    th:nth-child(1),
    td:nth-child(1) {
        min-width: 220px;
        max-width: 220px;
    }

    th:nth-child(2),
    td:nth-child(2) {
        min-width: 220px;
        max-width: 220px;
    }

    th:nth-child(3),
    td:nth-child(3) {
        min-width: 100px;
        max-width: 100px;
    }

    th:nth-child(4),
    td:nth-child(4) {
        min-width: 60px;
        max-width: 60px;
    }

    th:nth-child(5),
    td:nth-child(5) {
        min-width: 60px;
        max-width: 60px;
    }

    th:nth-child(6),
    td:nth-child(6) {
        min-width: 60px;
        max-width: 60px;
    }

    th:nth-child(7),
    td:nth-child(7) {
        min-width: 80px;
        max-width: 80px;
    }

    th:nth-child(8),
    td:nth-child(8) {
        min-width: 65px;
        max-width: 65px;
    }

    th:nth-child(9),
    td:nth-child(9) {
        width: 150px;
    }
`;

const style = css`
    border: 2px solid;
    height: 600px;
    width: 1000px;
`;

export function ExportEditor(props: { phonebook: Phonebook }) {
    const [group, set_group] = useState("all");

    function cmpContact(a: Contact, b: Contact) {
        // order by parent ord, then by contact ord
        const grp_ord = props.phonebook.dirs
            .get(a.parent!)!
            .ord.cmp(props.phonebook.dirs.get(b.parent!)!.ord);
        if (grp_ord === 0) {
            return a.ord.cmp(b.ord);
        } else {
            return grp_ord;
        }
    }

    const csv = d3.csvFormat(
        Immutable.List(props.phonebook.contacts)
            .filter(e => group === "all" || group === e[1].parent)
            .sortBy(v => v[1], cmpContact)
            .map(e => {
                return {
                    "Contact Name": e[1].name,
                    // TODO: parent can be undefined (in theory)
                    Group: props.phonebook.dirs.get(e[1].parent!)!.name,
                    "Phone Number": formatPhoneNumber(e[1].contact),
                    "9-1-1 Transfer": e[1].xfer_ng911 ? "Yes" : "No",
                    "Blind Transfer": e[1].xfer_blind ? "Yes" : "No",
                    "Attended Transfer": e[1].xfer_attended ? "Yes" : "No",
                    "Conference Transfer": e[1].xfer_conference ? "Yes" : "No",
                    Outbound: e[1].xfer_outbound ? "Yes" : "No",
                    ID: e[0],
                };
            })
            .toJSON(),
        [
            "Contact Name",
            "Group",
            "Phone Number",
            "9-1-1 Transfer",
            "Blind Transfer",
            "Attended Transfer",
            "Conference Transfer",
            "Outbound",
            "ID",
        ],
    );

    return (
        <div css={[style, editorStyle]}>
            <div>
                Group to Export:&nbsp;
                <select
                    css={inputStyle}
                    value={group}
                    onChange={e => set_group(e.target.value)}
                >
                    <option value="all">All Groups</option>
                    {Immutable.List(props.phonebook.dirs)
                        .sortBy(
                            x => x[1].ord,
                            (a, b) => a.cmp(b),
                        )
                        .map(e => {
                            return (
                                <option key={e[0]} value={e[0]}>
                                    {e[1].name}
                                </option>
                            );
                        })}
                </select>
            </div>
            <div
                css={css`
                    margin-top: 20px;
                    margin-bottom: 20px;
                `}
            >
                <a
                    download={"phonebook.csv"}
                    href={"data:text/csv," + encodeURIComponent(csv)}
                >
                    Download CSV
                </a>
            </div>
            <table css={table_style}>
                <thead>
                    <tr>
                        <th>Contact Name</th>
                        <th>Group</th>
                        <th>Phone Number</th>
                        <th>9-1-1 Transfer</th>
                        <th>Blind Transfer</th>
                        <th>Attended Transfer</th>
                        <th>Conference Transfer</th>
                        <th>Outbound</th>
                        <th>ID</th>
                    </tr>
                </thead>
                <tbody>
                    {Immutable.List(props.phonebook.contacts)
                        .filter(e => group === "all" || e[1].parent === group)
                        .sortBy(v => v[1], cmpContact)
                        .map(e => {
                            return (
                                <tr key={e[0]}>
                                    <td>{e[1].name}</td>
                                    <td>
                                        {
                                            props.phonebook.dirs.get(
                                                e[1].parent!,
                                            )!.name
                                        }
                                    </td>
                                    <td>{formatPhoneNumber(e[1].contact)}</td>
                                    <td>{e[1].xfer_ng911 ? "Y" : "N"}</td>
                                    <td>{e[1].xfer_blind ? "Y" : "N"}</td>
                                    <td>{e[1].xfer_attended ? "Y" : "N"}</td>
                                    <td>{e[1].xfer_conference ? "Y" : "N"}</td>
                                    <td>{e[1].xfer_outbound ? "Y" : "N"}</td>
                                    {/* TODO: don't substring ourselves, have browser do it somehow */}
                                    <td
                                        title={e[0]}
                                        style={{ fontSize: "80%" }}
                                    >
                                        <code>
                                            {e[0].substring(0, 10) + "..."}
                                        </code>
                                    </td>
                                </tr>
                            );
                        })}
                </tbody>
            </table>
        </div>
    );
}
