export interface IUser {
    uid: string;
    username: string;
    is_superadmin: boolean;
}
