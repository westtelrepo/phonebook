/* eslint-disable @typescript-eslint/no-non-null-assertion, @typescript-eslint/no-unsafe-call */

import assert from "assert";
import Big from "big.js";
import Immutable from "immutable";
import { encodePointer } from "json-ptr";
import * as rfc6902 from "rfc6902";
import { v4 as gen_uuid } from "uuid";

import { is_sip_uri, normalize_phone_number } from "./validation";

function check_uuid(uuid: string | undefined): boolean {
    return (
        typeof uuid === "string" &&
        /^[a-f0-9]{8}(-[a-f0-9]{4}){3}-([a-f0-9]{12})$/.test(uuid)
    );
}

export class Phonebook extends Immutable.Record(
    {
        id: "" as string,
        change_id: 0 as number,

        contacts: Immutable.Map<string, Contact>(),
        speed_dials: Immutable.Map<string, SpeedDial>(),

        dirs: Immutable.Map<string, Directory>(),
        sd_dirs: Immutable.Map<string, Directory>(),

        name: undefined as string | undefined,

        filename: undefined as string | undefined,
        current_change_id: undefined as number | undefined,
    },
    "Phonebook",
) {
    public static from_old(phonebook: any) {
        const new_phonebook = {
            id: phonebook.id,
            change_id: 0, // no other value really makes sense

            contacts: new Map(),
            speed_dials: new Map(),
            dirs: Immutable.Map<string, any>(
                phonebook.groups.map((e: any, i: number) => [
                    e.id,
                    { icon: e.icon, name: e.name, ord: i },
                ]),
            ),
            sd_dirs: new Map(),
        };

        const ret = new Phonebook(new_phonebook);
        ret.validate();
        return ret;
    }

    constructor(
        obj?:
            | {
                  id: string;
                  change_id?: number;

                  contacts?:
                      | Map<string, any>
                      | Immutable.Map<string, any>
                      | { [index: string]: any };
                  speed_dials?:
                      | Map<string, any>
                      | Immutable.Map<string, any>
                      | { [index: string]: any };

                  dirs?:
                      | Map<string, any>
                      | Immutable.Map<string, any>
                      | { [index: string]: any };
                  sd_dirs?:
                      | Map<string, any>
                      | Immutable.Map<string, any>
                      | { [index: string]: any };

                  name?: string;

                  filename?: string;
                  current_change_id?: number;
              }
            | Phonebook,
    ) {
        if (obj instanceof Phonebook) {
            super(obj);
        } else if (obj) {
            super({
                ...obj,
                contacts: obj.contacts
                    ? Immutable.Map(obj.contacts).map(x => new Contact(x))
                    : undefined,
                speed_dials: obj.speed_dials
                    ? Immutable.Map(obj.speed_dials).map(x => new SpeedDial(x))
                    : undefined,
                dirs: obj.dirs
                    ? Immutable.Map(obj.dirs).map(x => new Directory(x))
                    : undefined,
                sd_dirs: obj.sd_dirs
                    ? Immutable.Map(obj.sd_dirs).map(x => new Directory(x))
                    : undefined,
            });
        } else {
            super({
                id: gen_uuid(),
            });
        }
    }

    public validate() {
        assert(check_uuid(this.id), "Phonebook.id must be UUID");
        assert(typeof this.change_id === "number");

        assert(this.contacts instanceof Immutable.Map);
        assert(this.speed_dials instanceof Immutable.Map);

        assert(this.dirs instanceof Immutable.Map);
        assert(this.sd_dirs instanceof Immutable.Map);

        assert(this.name === undefined || typeof this.name === "string");
        assert(
            this.filename === undefined || typeof this.filename === "string",
        );

        for (const contact of this.contacts) {
            assert(check_uuid(contact[0]));
            contact[1].validate();
            assert(
                typeof contact[1].parent === "undefined" ||
                    this.dirs.has(contact[1].parent),
            );
        }

        for (const speeddial of this.speed_dials) {
            assert(check_uuid(speeddial[0]));
            speeddial[1].validate();
            assert(
                typeof speeddial[1].parent === "undefined" ||
                    this.sd_dirs.has(speeddial[1].parent),
            );
            assert(
                this.contacts.has(speeddial[1].contact_id),
                "speeddial contact_id must be valid",
            );
        }

        // this is to prevent nodes from using themselves as a parent,
        // and to also prevent cycles of indefinite length that do
        // not tie back to the "undefined" root directory
        function goes_to_null(dir_id: any, dirs: any) {
            const visited = new Set();

            while (true) {
                if (dir_id === undefined) {
                    return true;
                }

                assert(dirs.has(dir_id));

                if (visited.has(dir_id)) {
                    return false;
                }

                visited.add(dir_id);
                dir_id = dirs.get(dir_id).parent;
            }
        }

        for (const dir of this.dirs) {
            assert(check_uuid(dir[0]), "dir id must be UUID");
            dir[1].validate();
            assert(
                dir[1].parent === undefined || this.dirs.has(dir[1].parent),
                "dir parent must exist",
            );
        }

        for (const dir of this.dirs) {
            assert(goes_to_null(dir[0], this.dirs), "cycles are not allowed");
        }

        for (const dir of this.sd_dirs) {
            assert(check_uuid(dir[0]));
            dir[1].validate();
            assert(
                dir[1].parent === undefined || this.sd_dirs.has(dir[1].parent),
                "sd_dir parent must exist",
            );
        }

        for (const dir of this.sd_dirs) {
            assert(
                goes_to_null(dir[0], this.sd_dirs),
                "cycles are not allowed",
            );
        }

        function check_name_unique(
            array: Immutable.Map<string, { parent?: string; name: string }>,
        ) {
            const parent_set = new Map(); // UUID of parent -> Set<String> name of children

            for (const elem of array) {
                if (!parent_set.has(elem[1].parent)) {
                    parent_set.set(elem[1].parent, new Set());
                }

                const set = parent_set.get(elem[1].parent);
                assert(
                    !set.has(elem[1].name),
                    "directory entry is doubled: " +
                        JSON.stringify(elem[1].name),
                );

                set.add(elem[1].name);
            }
        }

        check_name_unique(this.dirs.concat(this.contacts));
        check_name_unique(this.sd_dirs);
    }

    // does not change change_id
    public _apply(op: Operation) {
        // TODO: stupidly inefficient
        const new_object: any = this.toJS();
        const result = rfc6902.applyPatch(
            new_object,
            JSON.parse(JSON.stringify(op.patch)),
        );
        for (const res of result) {
            if (res !== null) {
                throw res;
            }
        }
        return new Phonebook(new_object);
    }

    // returns new Phonebook, change_id increments
    public apply(op: Operation) {
        return this._apply(op).set("change_id", this.change_id + 1);
    }

    // returns new Phonebook, change_id decrements, applies inverse of op
    public reverse(op: Operation) {
        return this._apply(op.inverse()).set("change_id", this.change_id - 1);
    }

    // for any phonebook, op: phonebook.apply(op).reverse(op) === phonebook
    // phonebook.apply(op).apply(op.inverse()) has a slightly different result: the change_id increments

    // for any op: op.inverse().inverse() === op

    // create Operations on current phonebook

    public dir_add(dir: Directory, { id } = { id: gen_uuid() }) {
        assert(dir instanceof Directory, "dir to dir_add must be Directory");
        dir.validate();

        const path = encodePointer(["dirs", id]);

        return new Operation({
            patch: [{ op: "add", path, value: dir.toJS() }],
            inv_patch: [{ op: "remove", path }],
        });
    }

    public dir_del(id: string) {
        assert(check_uuid(id));

        assert(
            this.dirs.has(id),
            "id to delete must exist TODO make it not needed",
        );

        const path = encodePointer(["dirs", id]);

        return new Operation({
            patch: [{ op: "remove", path }],
            inv_patch: [{ op: "add", path, value: this.dirs.get(id)!.toJS() }],
        });
    }

    public dir_move(id: string, id_new_parent: string) {
        assert(check_uuid(id));
        assert(id_new_parent === undefined || check_uuid(id_new_parent));

        assert(this.dirs.has(id));
        assert(id_new_parent === undefined || this.dirs.has(id_new_parent));

        return Operation.change_val(
            ["dirs", id, "parent"],
            this.dirs.get(id)!.parent,
            id_new_parent,
        );
    }

    public dir_edit(id: string, option: "name" | "icon" | "ord", value: any) {
        assert(check_uuid(id));
        assert(this.dirs.has(id));

        assert(option === "name" || option === "icon" || option === "ord");

        return Operation.change_val(
            ["dirs", id, option],
            this.getIn(["dirs", id, option]),
            value,
        );
    }

    public dir_sort(_id: string) {
        throw new Error("TODO");
        /*assert(id === undefined || check_uuid(id));
        assert(id === undefined || this.dirs.has(id));

        const list = [];*/
        // take this.contacts and this.dirs, filter out everything except
        // those whose parent is `id`.
        // sort this new array by name.
        // then, create a patch set that sets ord to 10 * index
        // inv_patch sets old ord
    }

    public sd_dir_add(dir: Directory, { id } = { id: gen_uuid() }) {
        assert(dir instanceof Directory, "dir to dir_add must be Directory");
        dir.validate();

        const path = encodePointer(["sd_dirs", id]);

        return new Operation({
            patch: [{ op: "add", path, value: dir.toJS() }],
            inv_patch: [{ op: "remove", path }],
        });
    }

    public sd_dir_del(id: string) {
        assert(check_uuid(id));

        assert(
            this.sd_dirs.has(id),
            "id to delete must exist TODO make it not needed",
        );

        const path = encodePointer(["sd_dirs", id]);

        return new Operation({
            patch: [{ op: "remove", path }],
            inv_patch: [
                { op: "add", path, value: this.sd_dirs.get(id)!.toJS() },
            ],
        });
    }

    public sd_dir_move(id: string, id_new_parent: string | undefined) {
        assert(check_uuid(id));
        assert(id_new_parent === undefined || check_uuid(id_new_parent));

        assert(this.sd_dirs.has(id));
        assert(id_new_parent === undefined || this.dirs.has(id_new_parent));

        return Operation.change_val(
            ["sd_dirs", id, "parent"],
            this.dirs.get(id)!.parent,
            id_new_parent,
        );
    }

    public sd_dir_edit(
        id: string,
        option: "name" | "icon" | "ord",
        value: any,
    ) {
        assert(check_uuid(id));
        assert(this.sd_dirs.has(id));

        assert(option === "name" || option === "icon" || option === "ord");

        return Operation.change_val(
            ["sd_dirs", id, option],
            this.getIn(["sd_dirs", id, option]),
            value,
        );
    }

    public contact_add(contact: Contact, { id } = { id: gen_uuid() }) {
        assert(contact instanceof Contact, "contact_add must be contact");
        contact.validate();

        const path = encodePointer(["contacts", id]);

        return new Operation({
            patch: [{ op: "add", path, value: contact.toJS() }],
            inv_patch: [{ op: "remove", path }],
        });
    }

    public contact_del(id: string) {
        assert(check_uuid(id));
        assert(this.contacts.has(id));

        const path = encodePointer(["contacts", id]);
        return new Operation({
            patch: [{ op: "remove", path }],
            inv_patch: [
                { op: "add", path, value: this.contacts.get(id)!.toJS() },
            ],
        });
    }

    public contact_edit(
        id: string,
        option:
            | "name"
            | "contact"
            | "xfer_ng911"
            | "xfer_blind"
            | "xfer_attended"
            | "xfer_conference"
            | "xfer_outbound"
            | "ord",
        value: string | boolean,
    ) {
        assert(check_uuid(id));
        assert(this.contacts.has(id));
        assert(
            option === "name" ||
                option === "contact" ||
                option === "xfer_ng911" ||
                option === "xfer_blind" ||
                option === "xfer_attended" ||
                option === "xfer_conference" ||
                option === "xfer_outbound" ||
                option === "ord",
        );

        return Operation.change_val(
            ["contacts", id, option],
            this.getIn(["contacts", id, option]),
            value,
        );
    }

    public sd_add(contact: SpeedDial, { id } = { id: gen_uuid() }) {
        assert(contact instanceof SpeedDial);

        const path = encodePointer(["speed_dials", id]);

        return new Operation({
            patch: [{ op: "add", path, value: contact.toJS() }],
            inv_patch: [{ op: "remove", path }],
        });
    }

    public sd_del(id: string) {
        assert(check_uuid(id));
        assert(this.speed_dials.has(id));

        const path = encodePointer(["speed_dials", id]);
        return new Operation({
            patch: [{ op: "remove", path }],
            inv_patch: [
                { op: "add", path, value: this.speed_dials.get(id)!.toJS() },
            ],
        });
    }

    public sd_edit(
        id: string,
        option: "contact_id" | "action" | "icon" | "ord",
        value: any,
    ) {
        assert(check_uuid(id));
        assert(this.contacts.has(id));
        assert(
            option === "contact_id" ||
                option === "action" ||
                option === "icon" ||
                option === "ord",
        );

        return Operation.change_val(
            ["speed_dials", id, option],
            this.getIn(["speed_dials", id, option]),
            value,
        );
    }

    // convert to the old phonebook structure
    // IS LOSSY! will lose new things
    public to_old_lossy() {
        const ret: any = {};
        ret.id = this.id;
        ret.filename = this.filename;
        // only keep root directories (aka, parent is undefined)
        ret.groups = this.dirs
            .filter(value => value.parent === undefined)
            .sortBy(value => value.ord, (Big as any).cmp)
            .map((value, key) => ({ id: key, name: value.name }))
            .valueSeq()
            .toArray();

        return ret;
    }

    public children_of(
        gid: string | undefined,
    ): Immutable.List<[string, Directory | Contact]> {
        return Immutable.List(
            this.contacts
                .filter(x => x.parent === gid)
                .concat(this.dirs.filter(x => x.parent === gid)),
        ).sort((a, b) => {
            // sort by ord, and if that fails, sort by ID
            const ord = a[1].ord.cmp(b[1].ord);
            if (ord === 0) {
                if (a[0] < b[0]) {
                    return -1;
                } else if (a[0] === b[0]) {
                    return 0;
                } else {
                    return 1;
                }
            } else {
                return ord;
            }
        });
    }

    public children_of_only_dirs(
        gid: string | undefined,
    ): Immutable.List<[string, Directory]> {
        return this.children_of(gid).filter(
            x => x[1] instanceof Directory,
        ) as Immutable.List<[string, Directory]>;
    }

    public children_of_only_contacts(
        gid: string | undefined,
    ): Immutable.List<[string, Contact]> {
        return this.children_of(gid).filter(
            x => x[1] instanceof Contact,
        ) as Immutable.List<[string, Contact]>;
    }

    public sd_children_of(
        gid: string | undefined,
    ): Immutable.List<[string, Directory | SpeedDial]> {
        return Immutable.List(
            this.speed_dials
                .filter(x => x.parent === gid)
                .concat(this.sd_dirs.filter(x => x.parent === gid)),
        ).sort((a, b) => {
            // sort by ord, and if that fails, sort by ID
            const ord = a[1].ord.cmp(b[1].ord);
            if (ord === 0) {
                if (a[0] < b[0]) {
                    return -1;
                } else if (a[0] === b[0]) {
                    return 0;
                } else {
                    return 1;
                }
            } else {
                return ord;
            }
        });
    }

    public sd_children_of_only_dirs(
        gid: string | undefined,
    ): Immutable.List<[string, Directory]> {
        return this.sd_children_of(gid).filter(
            x => x[1] instanceof Directory,
        ) as Immutable.List<[string, Directory]>;
    }

    public sd_children_of_only_sds(
        gid: string | undefined,
    ): Immutable.List<[string, SpeedDial]> {
        return this.sd_children_of(gid).filter(
            x => x[1] instanceof SpeedDial,
        ) as Immutable.List<[string, SpeedDial]>;
    }
}

export class Directory extends Immutable.Record(
    {
        icon: undefined as string | undefined,
        name: "",

        parent: undefined as string | undefined, // UUID (or undefined for root directory)
        ord: Big(0), // Big
    },
    "Phonebook Directory",
) {
    constructor(
        obj:
            | {
                  icon?: string;
                  name: string;

                  parent?: string;
                  ord: Big | string | number;
              }
            | Directory,
    ) {
        if (obj instanceof Directory) {
            super(obj);
        } else {
            super({
                ...obj,
                ord: Big(obj.ord),
            });
        }
    }

    public validate() {
        assert(this.icon === undefined || typeof this.icon === "string");
        assert(this.parent === undefined || check_uuid(this.parent));
        assert(this.ord instanceof Big, "Directory.ord must be of type Big");
    }
}

export type ng911_transfer_type =
    | undefined
    | "internal"
    | "external"
    | "external.experient";

export class Contact extends Immutable.Record({
    name: "" as string,
    contact: "" as string,
    xfer_ng911: false,
    xfer_blind: false,
    xfer_attended: false,
    xfer_conference: false,
    xfer_outbound: false,
    icon: undefined as string | undefined,
    ng911_transfer_type: undefined as ng911_transfer_type,

    parent: undefined as string | undefined,
    ord: Big(0), // Big
}) {
    constructor(
        obj:
            | {
                  name: string;
                  contact: string;
                  xfer_ng911?: boolean;
                  xfer_blind?: boolean;
                  xfer_attended?: boolean;
                  xfer_conference?: boolean;
                  xfer_outbound?: boolean;
                  icon?: string;

                  parent?: string;
                  ord: Big | number | string;
              }
            | Contact,
    ) {
        if (obj instanceof Contact) {
            super(obj);
        } else {
            super({
                ...obj,
                ord: Big(obj.ord),
            });
        }
    }

    public validate() {
        assert(typeof this.name === "string");
        assert(typeof this.contact === "string");
        // TODO: validate contact to be phone number
        assert(typeof this.xfer_ng911 === "boolean");
        assert(typeof this.xfer_blind === "boolean");
        assert(typeof this.xfer_attended === "boolean");
        assert(typeof this.xfer_conference === "boolean");
        assert(typeof this.xfer_outbound === "boolean");
        assert(check_uuid(this.parent), "parent must be UUID");
        assert(this.ord instanceof Big);
        assert.strictEqual(
            normalize_phone_number(this.contact),
            this.contact,
            "contact must be normalized",
        );
        assert(
            this.ng911_transfer_type === undefined ||
                this.ng911_transfer_type === "internal" ||
                this.ng911_transfer_type === "external" ||
                this.ng911_transfer_type === "external.experient",
            "ng911_transfer_type must be correct",
        );

        if (is_sip_uri(this.contact)) {
            assert(
                this.ng911_transfer_type !== undefined,
                "sip uri must have type",
            );
            assert.strictEqual(
                this.xfer_ng911,
                true,
                "ng911 must be true for sip",
            );
            assert.strictEqual(
                this.xfer_attended,
                false,
                "attended must be false for sip",
            );
            assert.strictEqual(
                this.xfer_blind,
                false,
                "blind must be false for sip",
            );
            assert.strictEqual(
                this.xfer_conference,
                false,
                "conference must be false for sip",
            );
            assert.strictEqual(
                this.xfer_outbound,
                false,
                "outbound must be false for sip",
            );
        } else {
            assert(
                this.ng911_transfer_type === undefined,
                "non sip uri must not have type",
            );
        }
    }
}

export type sd_action =
    | "dial_out"
    | "blind_transfer"
    | "attended_transfer"
    | "conference_transfer"
    | "tandem_transfer";

export function is_sd_action(str: any): str is sd_action {
    if (typeof str === "string") {
        return (
            str === "dial_out" ||
            str === "blind_transfer" ||
            str === "attended_transfer" ||
            str === "conference_transfer" ||
            str === "tandem_transfer"
        );
    } else {
        return false;
    }
}

export class SpeedDial extends Immutable.Record({
    contact_id: "", // refers
    action: "dial_out" as sd_action,
    icon: undefined as string | undefined,

    parent: undefined as string | undefined, // UUID
    ord: Big(0), // Big
}) {
    constructor(
        obj:
            | {
                  contact_id: string;
                  action: sd_action;
                  icon?: string;

                  parent?: string;
                  ord: Big | string | number;
              }
            | SpeedDial,
    ) {
        if (obj instanceof SpeedDial) {
            super(obj);
        } else {
            super({
                ...obj,
                ord: Big(obj.ord),
            });
        }
    }

    public validate() {
        assert(check_uuid(this.contact_id), "contact_id must be UUID");
        assert(typeof this.action === "string");
        assert(
            this.action === "dial_out" ||
                this.action === "blind_transfer" ||
                this.action === "attended_transfer" ||
                this.action === "conference_transfer" ||
                this.action === "tandem_transfer",
        );
        assert(this.icon === undefined || typeof this.icon === "string");
        assert(check_uuid(this.parent), "parent must be UUID");
        assert(this.ord instanceof Big);
    }
}

interface IAToB {
    a: Phonebook;
    b: Phonebook;
}

interface IOperation {
    patch: rfc6902.Patch;
    inv_patch: rfc6902.Patch;
}

function is_a_to_b(obj: IAToB | IOperation): obj is IAToB {
    return (obj as any).a instanceof Phonebook;
}

export class Operation extends Immutable.Record({
    patch: [] as rfc6902.Operation[],
    inv_patch: [] as rfc6902.Operation[],
}) {
    public static change_val(
        path: string[] | string,
        old_value: any,
        new_value: any,
    ) {
        if (Array.isArray(path)) {
            path = encodePointer(path);
        }

        assert(typeof path === "string");

        if (old_value === new_value) {
            // if both values are the same this is a null operation
            // (including both undefined)
            return new Operation();
        } else if (old_value === undefined && new_value !== undefined) {
            return new Operation({
                patch: [{ op: "add", path, value: new_value }],
                inv_patch: [{ op: "remove", path }],
            });
        } else if (old_value !== undefined && new_value === undefined) {
            return new Operation({
                patch: [{ op: "remove", path }],
                inv_patch: [{ op: "add", path, value: old_value }],
            });
        } else {
            // both values are not undefined
            return new Operation({
                patch: [{ op: "replace", path, value: new_value }],
                inv_patch: [{ op: "replace", path, value: old_value }],
            });
        }
    }

    constructor(
        obj?:
            | {
                  a: Phonebook;
                  b: Phonebook;
              }
            | {
                  patch: rfc6902.Patch;
                  inv_patch: rfc6902.Patch;
              },
    ) {
        if (obj) {
            if (is_a_to_b(obj)) {
                assert(obj.a instanceof Phonebook);
                assert(obj.b instanceof Phonebook);
                const a = obj.a.toJS();
                const b = obj.b.toJS();

                super({
                    patch: rfc6902.createPatch(a, b),
                    inv_patch: rfc6902.createPatch(b, a),
                });
            } else {
                super(obj);
            }
        } else {
            super();
        }
    }

    public inverse() {
        return new Operation({ patch: this.inv_patch, inv_patch: this.patch });
    }

    public concat(op: Operation) {
        assert(op instanceof Operation);

        return new Operation({
            patch: this.patch.concat(op.patch),
            inv_patch: this.inv_patch.concat(op.inv_patch),
        });
    }
}

export default {
    Contact,
    Directory,
    Operation,
    Phonebook,
    SpeedDial,
};
