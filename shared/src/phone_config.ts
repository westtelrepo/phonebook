import assert from "assert";
import Immutable from "immutable";

export class PolycomLineKey extends Immutable.Record({
    category: null as null | "SpeedDial" | "Line" | "BLF", // SpeedDial or Line or BLF
    // SpeedDial
    lb: undefined as undefined | string,
    ct: undefined as undefined | string,
    // BLF (no attributes)
    // Line
    explicit_index: undefined as undefined | number,
}) {
    constructor(obj: any) {
        if (obj instanceof PolycomLineKey) {
            super(obj);
        } else {
            if (obj.category === "SpeedDial") {
                assert(typeof obj.lb === "string", "lb is not a string");
                assert(typeof obj.ct === "string", "ct is not a string");
                super({ category: "SpeedDial", lb: obj.lb, ct: obj.ct });
            } else if (obj.category === "BLF") {
                super({ category: "BLF" });
            } else if (obj.category === "Line") {
                //assert(typeof obj.explicit_index === 'number', 'explicit_index is not a number');
                super({
                    category: "Line",
                    explicit_index: Number(obj.explicit_index),
                });
            } else {
                assert(false, "category is unknown");
            }
        }
    }
}

export class PolycomTemplate extends Immutable.Record(
    {
        id: null as null | string, // uuid
        name: null as null | string, // string
        lineKeys: Immutable.List(),
        tftp_directory: null,
    },
    "PolycomTemplate",
) {
    constructor(obj?: any) {
        if (obj instanceof PolycomTemplate || !obj) {
            super(obj);
        } else {
            const lineKeys = Immutable.List(
                // eslint-disable-next-line @typescript-eslint/no-unsafe-call
                (obj.lineKeys || []).map((e: any) =>
                    e ? new PolycomLineKey(e) : null,
                ),
            );
            super(Object.assign({}, obj, { lineKeys }));
        }
    }

    public directory() {
        const ret: any[] = [];
        let index = 1;
        // because polycom can't have two entries with the same phone number, check here
        const cnt_map = new Set();

        for (const key of this.lineKeys) {
            if (key && key.category === "SpeedDial") {
                if (cnt_map.has(key.ct)) {
                    // ideally this never happens because we prevent this in the UI/editor layer
                    const e = new Error(
                        "multiple speed dials can't have the same phone number",
                    );
                    (e as any).expose = true;
                    throw e;
                }
                cnt_map.add(key.ct);
                ret.push(
                    Object.freeze({
                        sd: index++,
                        ct: key.ct,
                        lb: key.lb,
                    }),
                );
            }
        }

        return Immutable.List(ret);
    }

    public setLineKeySideCar(location: any, lineKey: any) {
        return this.setIn(
            [
                "lineKeys",
                16 + 28 * location.page + 14 * location.column + location.entry,
            ],
            lineKey ? new PolycomLineKey(lineKey) : null,
        );
    }

    public getLineKeySideCar(location: any) {
        // eslint-disable-next-line @typescript-eslint/no-unsafe-call
        return (this as any).getIn(
            [
                "lineKeys",
                16 + 28 * location.page + 14 * location.column + location.entry,
            ],
            null,
        );
    }
}

/*const config = {
    id: "uuid",
    name: "PSAP A",
    lineKeys: [
        [
            [
                null,
                {
                    category: "SpeedDial",
                    contact_id: "uuid",
                    lb: "shorter", // optional
                },
                {
                    category: "SpeedDial",
                    lb: "Some Place",
                    ct: "333",
                },
                {
                    category: "Line",
                    explicit_index: 2,
                },
                {
                    category: "BLF",
                },
            ],
        ],
    ],
};*/
