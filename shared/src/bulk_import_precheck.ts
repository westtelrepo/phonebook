/* eslint-disable @typescript-eslint/no-non-null-assertion, @typescript-eslint/no-unsafe-call */

import * as d3 from "d3-dsv";
import Immutable from "immutable";

import { IUpsertContact } from "./db_phonebook";
import { Phonebook } from "./phonebook";
import { normalize_phone_number } from "./validation";

function parse_boolean(string: string | null | undefined) {
    if (string === null || string === undefined) {
        // TODO: what is our default?
        return true;
    }
    const str = string.trim().toLowerCase();
    switch (str) {
        case "y":
        case "yes":
        case "true":
        case "t":
            return true;
        case "n":
        case "no":
        case "false":
        case "f":
            return false;
        default:
            return false; // TODO: not this
    }
}

export interface RowStatus {
    contact: IUpsertContact;

    contact_state: "change" | "error" | "add";
    contact_tooltip: string;

    number_state: string;
    number_tooltip?: string;

    group_state: "change" | "add";
    group_tooltip: string;

    index: number;
}

export class BulkImportReturn extends Immutable.Record(
    {
        row_status: Immutable.List() as Immutable.List<RowStatus>,
    },
    "BulkImportReturn",
) {
    public get allow_import() {
        for (const entry of this.row_status) {
            if (entry.contact_state === "error") {
                return false;
            }
            if (entry.number_state === "error") {
                return false;
            }
        }

        return true;
    }
}

export function bulk_import_precheck(
    phonebook: Phonebook,
    csv: string,
): BulkImportReturn {
    const text = d3.csvParse(csv).map((e, index: number) => ({
        ContactName: e["Contact Name"]!.trim(),
        Group: e.Group!.trim(),
        PhoneNumber: e["Phone Number"],
        NG911: parse_boolean(e["9-1-1 Transfer"]),
        Blind: parse_boolean(e["Blind Transfer"]),
        Attended: parse_boolean(e["Attended Transfer"]),
        Conference: parse_boolean(e["Conference Transfer"]),
        Outbound: parse_boolean(e.Outbound),
        ID: e.ID,
        Index: index + 1,
    }));

    // check for new entries with names that conflict with each other
    const name_check = new Map();
    text.forEach(e => {
        if (!name_check.has(e.Group)) {
            name_check.set(e.Group, new Map());
        }
        const grp = name_check.get(e.Group);

        if (!grp.has(e.ContactName)) {
            grp.set(e.ContactName, []);
        }

        grp.set(e.ContactName, grp.get(e.ContactName).concat([e.Index]));
    });

    const entries: RowStatus[] = text.map(
        (e): RowStatus => {
            const group = phonebook.dirs.findKey(x => x.name === e.Group);
            const group_exists = group !== undefined;

            const contact_exists_in_phonebook =
                group_exists &&
                Boolean(
                    phonebook.contacts
                        .filter(x => x.parent === group)
                        .find(x => x.name === e.ContactName),
                );
            const conflicting_names = name_check
                .get(e.Group)
                .get(e.ContactName);

            let contact_state: "change" | "error" | "add";
            let contact_tooltip: string;
            if (
                typeof e.ID === "string" &&
                e.ID.trim() !== "" &&
                conflicting_names.length === 1
            ) {
                const old = phonebook.contacts.get(e.ID);
                if (old) {
                    contact_state = "change";
                    contact_tooltip = `Contact ${
                        phonebook.dirs.get(old.parent!)!.name
                    } → ${old.name} will be overwritten`;
                } else {
                    contact_state = "error";
                    contact_tooltip = `The ID (${e.ID}) for this contact no longer exists, so it can not be edited`;
                }
            } else if (contact_exists_in_phonebook) {
                contact_state = "error";
                contact_tooltip = `${e.Group} → ${e.ContactName} already exists in the phonebook`;
            } else if (conflicting_names.length > 1) {
                contact_state = "error";
                // TODO: better entry list
                contact_tooltip = `The name of this entry conflicts with ${JSON.stringify(
                    conflicting_names,
                )}`;
            } else {
                contact_state = "add";
                contact_tooltip = `${e.Group} → ${e.ContactName} will be created`;
            }

            const normalized = normalize_phone_number(e.PhoneNumber!);

            return {
                contact: {
                    name: e.ContactName,
                    gname: e.Group,
                    contact: normalized !== false ? normalized : e.PhoneNumber!,
                    xfer_ng911: e.NG911,
                    xfer_blind: e.Blind,
                    xfer_attended: e.Attended,
                    xfer_conference: e.Conference,
                    xfer_outbound: e.Outbound,
                    id: e.ID,
                },
                contact_state,
                contact_tooltip,
                group_state: group_exists ? "change" : "add",
                group_tooltip: group_exists
                    ? `Group ${e.Group} already exists and will be added to`
                    : `Group ${e.Group} will be created in the phonebook`,
                number_state: normalized !== false ? "good" : "error",
                number_tooltip:
                    normalized !== false
                        ? undefined
                        : `${e.PhoneNumber!} is not a recognized phone number format`,
                index: e.Index,
            };
        },
    );

    return new BulkImportReturn({ row_status: Immutable.List(entries) });
}
