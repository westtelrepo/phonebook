import assert from "assert";

export const ANY = Symbol("ANY");

/*function implies(
    a: boolean | (() => boolean),
    b: boolean | (() => boolean),
): boolean {
    let pre: boolean;
    if (typeof a === "boolean") {
        pre = a;
    } else if (typeof a === "function") {
        pre = a();
    } else {
        throw new Error("Unknown type for implies a");
    }

    if (!pre) {
        return true;
    }

    if (typeof b === "boolean") {
        return b;
    } else if (typeof b === "function") {
        return b();
    } else {
        throw new Error("Unknown type for implies b");
    }
}*/

export interface IPDPUser {
    is_superadmin: boolean;
    permissions: any;
}

export type PDPTarget =
    | {
          type: "PHONEBOOK";
      }
    | {
          type: "USER";
          user: {
              is_superadmin: boolean;
          };
      }
    | {
          type: "USER_LIST";
      }
    | {
          type: "CONTACT";
          is_sip: boolean;
      };

type PDPAction =
    | {
          type: "UPDATE";
      }
    | {
          type: "EDIT";
          attr_name: string | typeof ANY;
      }
    | {
          type: "DELETE";
      }
    | {
          type: "READ";
      }
    | {
          type: "IMPORT_EXPORT";
      }
    | {
          type: "READ_HISTORY";
      };

function user_error(msg: string): Error {
    const ret: any = new Error(msg);
    ret.expose = true;
    ret.status = 403; // 403 Forbidden
    return ret;
}

/** returns false when allowed, truthy on error */
export function pdp_ask({
    user,
    target,
    action,
}: {
    user: IPDPUser;
    target: PDPTarget;
    action: PDPAction;
}): false | Error {
    assert.strictEqual(
        typeof target.type,
        "string",
        "target.type must be a string",
    );
    assert.strictEqual(
        typeof action.type,
        "string",
        "action.type must be a string",
    );
    assert.strictEqual(
        typeof user.is_superadmin,
        "boolean",
        "user.is_superadmin must be boolean",
    );
    assert.strictEqual(
        typeof user.permissions,
        "object",
        "user.permissions must be an object",
    );

    if (user.is_superadmin) {
        return false;
    }

    if (action.type === "UPDATE" && target.type === "PHONEBOOK") {
        if (user.permissions.UPDATE_PHONEBOOK) {
            return false;
        } else {
            return user_error(
                "Permission denied: you must have the UPDATE_PHONEBOOK permission",
            );
        }
    }

    if (action.type === "EDIT" && target.type === "USER") {
        if (target.user.is_superadmin) {
            return user_error(
                "Permission denied: users may not edit superadmins",
            );
        }

        if (action.attr_name === "is_superadmin") {
            return user_error(
                "Permission denied: users may not edit superadmin permission",
            );
        }

        if (!user.permissions.USER_MANAGEMENT) {
            return user_error(
                "Permission denied: you must have the USER_MANAGEMENT permission to edit users",
            );
        }

        return false;
    }

    if (action.type === "DELETE" && target.type === "USER") {
        if (target.user.is_superadmin) {
            return user_error(
                "Permission denied: users may not delete superadmins",
            );
        }

        if (!user.permissions.USER_MANAGEMENT) {
            return user_error(
                "Permission denied: you must have the USER_MANAGEMENT permission to delete users",
            );
        }

        return false;
    }

    if (action.type === "READ" && target.type === "USER_LIST") {
        if (user.permissions.USER_MANAGEMENT) {
            return false;
        }

        return user_error(
            "Permission denied: you must have the USER_MANAGEMENT permission",
        );
    }

    if (action.type === "IMPORT_EXPORT" && target.type === "PHONEBOOK") {
        if (user.permissions.PHONEBOOK_IMPORT_EXPORT) {
            return false;
        }

        return user_error(
            "Permission denied: you must have the PHONEBOOK_IMPORT_EXPORT permission for this action",
        );
    }

    if (action.type === "READ_HISTORY" && target.type === "PHONEBOOK") {
        if (user.permissions.PHONEBOOK_HISTORY) {
            return false;
        }

        return user_error(
            "Permission denied: you must have the PHONEBOOK_HISTORY permission for this action",
        );
    }

    if (action.type === "EDIT" && target.type === "CONTACT") {
        if (!target.is_sip) {
            return false;
        }

        if (user.permissions.CAN_SIP_EDIT) {
            return false;
        }

        return user_error(
            "Permission denied: you do not have permission to create/edit/delete SIP contact records",
        );
    }

    return user_error("There is no policy for that action");
}

export function pdp_assert(param: {
    user: IPDPUser;
    target: PDPTarget;
    action: PDPAction;
}): void {
    const ret = pdp_ask(param);
    if (ret !== false) {
        throw ret;
    }
}

/** returns true if allowed, false if not */
export function pdp_allowed(param: {
    user: IPDPUser;
    target: PDPTarget;
    action: PDPAction;
}): boolean {
    const ret = pdp_ask(param);
    return ret === false;
}
