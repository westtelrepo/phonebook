export function is_sip_uri(num: string): boolean {
    return /^([sS][iI][pP][sS]?):([^@]+)(@.+)?$/.test(num);
}

export function formatPhoneNumber(num: string): string {
    let regex: RegExpExecArray | null;
    if ((regex = /^(1|9|91)(\d{3})(\d{3})(\d{4})$/.exec(num))) {
        return regex[1] + " (" + regex[2] + ") " + regex[3] + "-" + regex[4];
    } else if ((regex = /^(\d{3})(\d{3})(\d{4})$/.exec(num))) {
        return "(" + regex[1] + ") " + regex[2] + "-" + regex[3];
    } else if ((regex = /^9(\d{3})(\d{4})$/.exec(num))) {
        return "9." + regex[1] + "-" + regex[2];
    } else if ((regex = /^(\d{3})(\d{4})$/.exec(num))) {
        return regex[1] + "-" + regex[2];
    } else {
        return num;
    }
}

// returns false if not a valid format
// returns truthy value otherwise (i.e. not '')
export function normalize_phone_number(num: string): string | false {
    num = num.trim().replace(/ /g, "");
    const regx = [
        /^\(?\d{3}\)?[- ]?\d{3}[- ]?\d{4}$/, // (DDD)-DDD-DDDD
        /^(?:9|1|91)[-.( ]?\d{3}[-.) ]?\d{3}[-. ]?\d{4}$/, // 91-999-999-9999
        /^\d{11}$/, // haiti TODO: is this always +509? conflicts with +1 & +9
        /^\*\d{2,3}$/, // *23 *234
        /^\d{3}[-. ]?\d{4}$/, // 555-5555
        /^9[-. ]?\d{3}[-. ]?\d{4}$/, // 9.555-5555
        /^\d{3,4}$/, // 234 2345
        /^\*8#\d{4}#$/, // *8#XXXX#
        /^\*\d{10,11}$/,
    ];
    if (regx.some(r => r.test(num))) {
        let ret = "";
        for (let i = 0; i < num.length; i++) {
            if (/^[#\d]$/.test(num[i])) {
                ret += num[i];
            } else if (i === 0 && num[i] === "*") {
                ret += num[i];
            }
        }
        return ret;
    } else if (is_sip_uri(num)) {
        return num;
    } else {
        return false;
    }
}
