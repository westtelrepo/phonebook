import Immutable from "immutable";
import { decodePointer } from "json-ptr";
import { Operation } from "rfc6902";

import { IHistoryEvent } from "./db_phonebook";
import { Phonebook } from "./phonebook";

type ParsedOperation = Operation & {
    parsed_path: string[];
};

function parse(ops: Operation[]): Immutable.List<ParsedOperation> {
    return Immutable.List(
        ops.map(e => ({
            ...e,
            parsed_path: [...decodePointer(e.path).map(e => String(e))],
        })),
    );
}

export function desc(event: IHistoryEvent, current: Phonebook): string {
    const patch = parse(event.patch);
    const inv_patch = parse(event.inv_patch);

    let ret: string = "";

    if (event.change_id === 0) {
        return "Initial phonebook commit\n";
    }

    for (const e of patch) {
        if (e.path === "/current_change_id") {
            if (e.op === "add") {
                ret += `Update published phonebook state to ${e.value}\n`;
            } else if (e.op === "replace") {
                const right = inv_patch.find(other => other.path === e.path);
                if (!right || right.op !== "replace") {
                    ret += `Update published phonebook state to ${e.value}\n`;
                } else {
                    ret += `Update published phonebook state to ${e.value} (was ${right.value})`;
                }
            }
        } else if (
            e.parsed_path.length === 3 &&
            e.parsed_path[0] === "contacts"
        ) {
            if (e.op === "replace") {
                const right = inv_patch.find(other => other.path === e.path);
                const name = current.contacts.get(e.parsed_path[1]) ?? {
                    name: "(deleted)",
                };
                switch (e.parsed_path[2]) {
                    case "name":
                        if (!right || right.op !== "replace") {
                            ret += `Set name of contact ${name.name} to ${e.value}\n`;
                        } else {
                            ret += `Set name of contact ${name.name} to ${e.value} (was ${right.value})\n`;
                        }
                        break;
                    default:
                        if (!right || right.op !== "replace") {
                            ret += `Set attribute ${e.parsed_path[2]} of contact ${name.name} to ${e.value}\n`;
                        } else {
                            ret += `Set attribute ${e.parsed_path[2]} of contact ${name.name} to ${e.value} (was ${right.value})\n`;
                        }
                        break;
                }
            } else if (e.op === "remove" && e.parsed_path[2] === "icon") {
                const name = current.contacts.get(e.parsed_path[1]) ?? {
                    name: "(deleted)",
                };
                ret += `Remove icon from contact ${name.name}\n`;
            } else if (e.op === "add" && e.parsed_path[2] === "icon") {
                const name = current.contacts.get(e.parsed_path[1]) ?? {
                    name: "(deleted)",
                };
                ret += `Set icon to ${e.value} for contact ${name.name}\n`;
            }
        } else if (e.parsed_path.length === 3 && e.parsed_path[0] === "dirs") {
            if (e.op === "replace") {
                const right = inv_patch.find(other => other.path === e.path);
                const name = current.dirs.get(e.parsed_path[1]) ?? {
                    name: "(deleted)",
                };
                if (!right || right.op !== "replace") {
                    ret += `Set attribute ${e.parsed_path[2]} of directory ${name.name} to ${e.value}\n`;
                } else {
                    ret += `Set attribute ${e.parsed_path[2]} of directory ${name.name} to ${e.value} (was ${right.value})\n`;
                }
            } else if (e.op === "remove" && e.parsed_path[2] === "icon") {
                const name = current.dirs.get(e.parsed_path[1]) ?? {
                    name: "(deleted)",
                };
                ret += `Remove icon from directory ${name.name}\n`;
            } else if (e.op === "add" && e.parsed_path[2] === "icon") {
                const name = current.dirs.get(e.parsed_path[1]) ?? {
                    name: "(deleted)",
                };
                ret += `Set icon to ${e.value} for directory ${name.name}\n`;
            }
        } else if (
            e.parsed_path.length === 3 &&
            e.parsed_path[0] === "speed_dials"
        ) {
            if (e.op === "replace") {
                const right = inv_patch.find(other => other.path === e.path);
                if (!right || right.op !== "replace") {
                    if (e.parsed_path[2] === "contact_id") {
                        const contact = current.contacts.get(e.value);
                        if (contact) {
                            ret += `Repoint speed dial to ${contact.name}\n`;
                        } else {
                            ret += `Repoint speed dial to deleted contact\n`;
                        }
                    } else {
                        ret += `Set attribute ${e.parsed_path[2]} of speed dial ${e.parsed_path[1]} to ${e.value}\n`;
                    }
                } else {
                    if (e.parsed_path[2] === "contact_id") {
                        const contact = current.contacts.get(e.value);
                        const old_contact = current.contacts.get(right.value);
                        ret +=
                            `Repoint speed dial to ${
                                contact ? contact.name : "deleted contact"
                            } ` +
                            `(was ${
                                old_contact
                                    ? old_contact.name
                                    : "deleted contact"
                            })\n`;
                    } else {
                        ret +=
                            `Set attribute ${e.parsed_path[2]} of speed dial ` +
                            `${e.parsed_path[1]} to ${e.value} (was ${right.value})\n`;
                    }
                }
            } else if (e.op === "remove" && e.parsed_path[2] === "icon") {
                ret += `Remove icon from speed dial ${e.parsed_path[1]}\n`;
            } else if (e.op === "add" && e.parsed_path[2] === "icon") {
                ret += `Set icon to ${e.value} for directory ${e.parsed_path[1]}\n`;
            }
        } else if (
            e.parsed_path.length === 3 &&
            e.parsed_path[0] === "sd_dirs"
        ) {
            if (e.op === "replace") {
                const right = inv_patch.find(other => other.path === e.path);
                const name = current.sd_dirs.get(e.parsed_path[1]) ?? {
                    name: "(deleted)",
                };
                if (!right || right.op !== "replace") {
                    ret += `Set attribute ${e.parsed_path[2]} of speed dial directory ${name.name} to ${e.value}\n`;
                } else {
                    ret += `Set attribute ${e.parsed_path[2]} of speed dial directory ${name.name} to ${e.value} (was ${right.value})\n`;
                }
            } else if (e.op === "remove" && e.parsed_path[2] === "icon") {
                const name = current.dirs.get(e.parsed_path[1]) ?? {
                    name: "(deleted)",
                };
                ret += `Remove icon from speed dial directory ${name.name}\n`;
            } else if (e.op === "add" && e.parsed_path[2] === "icon") {
                const name = current.dirs.get(e.parsed_path[1]) ?? {
                    name: "(deleted)",
                };
                ret += `Set icon to ${e.value} for speed dial directory ${name.name}\n`;
            }
        } else if (
            e.parsed_path.length === 2 &&
            e.parsed_path[0] === "contacts" &&
            e.op === "remove"
        ) {
            const add = inv_patch.find(other => other.path === e.path);
            if (!add || add.op !== "add") {
                ret += "Error: Expected to find add operation";
            } else {
                ret += `Deleted contact ${add.value.name}\n`;
            }
        } else if (
            e.parsed_path.length === 2 &&
            e.parsed_path[0] === "dirs" &&
            e.op === "remove"
        ) {
            const add = inv_patch.find(other => other.path === e.path);
            if (!add || add.op !== "add") {
                ret += "Error: Expected to find add operation";
            } else {
                ret += `Deleted directory ${add.value.name}\n`;
            }
        } else if (
            e.parsed_path.length === 2 &&
            e.parsed_path[0] === "sd_dirs" &&
            e.op === "remove"
        ) {
            const add = inv_patch.find(other => other.path === e.path);
            if (!add || add.op !== "add") {
                ret += "Error: Expected to find add operation";
            } else {
                ret += `Deleted speed dial directory ${add.value.name}\n`;
            }
        } else if (
            e.parsed_path.length === 2 &&
            e.parsed_path[0] === "contacts" &&
            e.op === "add"
        ) {
            ret += `Added contact ${e.value.name}\n`;
        } else if (
            e.parsed_path.length === 2 &&
            e.parsed_path[0] === "sd_dirs" &&
            e.op === "add"
        ) {
            ret += `Added speed dial directory ${e.value.name}\n`;
        } else if (
            e.parsed_path.length === 2 &&
            e.parsed_path[0] === "dirs" &&
            e.op === "add"
        ) {
            ret += `Added directory ${e.value.name}\n`;
        } else if (
            e.parsed_path.length === 2 &&
            e.parsed_path[0] === "speed_dials" &&
            e.op === "add"
        ) {
            const contact = current.contacts.get(e.value.contact_id);
            if (contact) {
                ret += `Added speed dial that refers to ${contact.name}\n`;
            } else {
                ret += `Added speed dial that refers to deleted contact\n`;
            }
        } else if (
            e.parsed_path.length === 2 &&
            e.parsed_path[0] === "speed_dials" &&
            e.op === "remove"
        ) {
            const add = inv_patch.find(other => other.path === e.path);
            if (!add || add.op !== "add") {
                ret += `Removed speed dial ${e.parsed_path[1]}\n`;
            } else {
                const contact = current.contacts.get(add.value.contact_id);
                if (contact) {
                    ret += `Removed speed dial that refered to contact ${contact.name}\n`;
                } else {
                    ret += `Removed speed dial that refers to deleted contact\n`;
                }
            }
        } else {
            ret += `Unknown operation\n`;
        }
    }

    return ret === "" ? "Unknown" : ret;
}
