import { ng911_transfer_type, sd_action } from "./phonebook";

export interface IPatchDirectory {
    icon?: string | null;
    name?: string;
}

export interface IPostDirectory {
    name: string;
    icon?: string;
}

export interface IPatchContact {
    name?: string;
    contact?: string;
    xfer_ng911?: boolean;
    xfer_blind?: boolean;
    xfer_attended?: boolean;
    xfer_conference?: boolean;
    xfer_outbound?: boolean;
    icon?: string | null;
    parent?: string | null;
    ng911_transfer_type?: ng911_transfer_type | null;
}

export interface IPostContact {
    name: string;
    parent?: string;
}

export interface IPostSpeedDial {
    parent?: string;
    contact_id: string;
}

export interface IPatchSpeedDial {
    parent?: string | null;
    contact_id?: string;
    icon?: string | null;
    action?: sd_action;
}
