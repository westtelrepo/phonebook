import rfc6902 from "rfc6902";

import { IUser } from "./db_users";

export interface IUpsertContact {
    id?: string;
    name: string;
    gname: string;
    contact: string;
    xfer_ng911: boolean;
    xfer_blind: boolean;
    xfer_attended: boolean;
    xfer_conference: boolean;
    xfer_outbound: boolean;
}

export interface IMeta {
    user: IUser | null;
    /** UUID of the user session */
    user_session?: string;
    ip?: string;
    undo_of?: number;
    restore_to?: number;
    /** is this change an import. undefined is false. */
    is_import?: true;
}

export interface IHistoryEvent {
    /** seconds since UNIX epoch */
    utc: number;
    patch: rfc6902.Patch;
    change_id: number;
    inv_patch: rfc6902.Patch;
    meta: IMeta | null;
}
