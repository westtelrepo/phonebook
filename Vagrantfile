# -*- mode: ruby -*-
# vi: set ft=ruby :

def common_config(config)
  # setup dev environment
  config.vm.provision "ansible_local" do |ansible|
    ansible.playbook = "phonebook_dev.yml"
  end

  config.vm.provision "shell", run: "always", inline: <<-SHELL
    mkdir -p /vagrant/node_modules
    mkdir -p /home/vagrant/node_modules
    mount --bind /home/vagrant/node_modules /vagrant/node_modules
    chown vagrant:vagrant /home/vagrant/node_modules

    mkdir -p /vagrant/backend/node_modules
    mkdir -p /home/vagrant/backend_node_modules
    mount --bind /home/vagrant/backend_node_modules /vagrant/backend/node_modules
    chown vagrant:vagrant /home/vagrant/backend_node_modules

    mkdir -p /vagrant/shared/node_modules
    mkdir -p /home/vagrant/shared_node_modules
    mount --bind /home/vagrant/shared_node_modules /vagrant/shared/node_modules
    chown vagrant:vagrant /home/vagrant/shared_node_modules

    mkdir -p /vagrant/frontend/node_modules
    mkdir -p /home/vagrant/frontend_node_modules
    mount --bind /home/vagrant/frontend_node_modules /vagrant/frontend/node_modules
    chown vagrant:vagrant /home/vagrant/frontend_node_modules
  SHELL
end

Vagrant.configure("2") do |config|
  config.vm.box = "debian/contrib-stretch64"

  # install ansible
  config.vm.provision "shell", inline: <<-SHELL
    if ! which ansible > /dev/null ; then
      export DEBIAN_FRONTEND=noninteractive
      apt-get update
      apt-get -y install dirmngr
      echo deb http://ppa.launchpad.net/ansible/ansible/ubuntu trusty main >> /etc/apt/sources.list
      apt-key adv --keyserver keyserver.ubuntu.com --recv-keys 93C4A3FD7BB9C367
      apt-get update
      apt-get -y install ansible
   fi
  SHELL

  config.vm.define "phonebook", primary: true do |phonebook|
    common_config phonebook
    phonebook.vm.network "forwarded_port", guest: 22, host: 4442, id: "ssh"
    phonebook.vm.network "forwarded_port", guest: 3001, host: 3001

    phonebook.vm.network "private_network", ip: "10.2.0.4", virtualbox__intnet: 'phonebook_db_network'

    phonebook.vm.provider "virtualbox" do |vb|
      vb.memory = 8192
    end

    # so that we can bridge this for DHCP/TFTP
    phonebook.vm.network "private_network", ip: "10.0.0.1", auto_config: false, virtualbox__intnet: 'phonebook_dummy_interface'

    phonebook.vm.provision "ansible_local" do |ansible|
      ansible.playbook = "tftp.yml"
    end
  end

  # TODO: this VM is currently broken
  # need to use unofficial build of node instead of repo (which has nothing)
  config.vm.define "phonebook32", autostart: false do |phonebook32|
    common_config phonebook32

    phonebook32.vm.network "private_network", ip: "10.2.0.5", virtualbox__intnet: 'phonebook_db_network'

    phonebook32.vm.box = "bento/debian-9.12-i386"
    phonebook32.vm.network "forwarded_port", guest: 3001, host: 3002
  end

  config.vm.define "jenkins", autostart: false do |jenkins|
    jenkins.vm.box = "debian/contrib-stretch64"
    jenkins.vm.hostname = "jenkins"
    jenkins.vm.network "forwarded_port", guest: 8080, host: 3003, host_ip: "127.0.0.1"

    # don't allow jenkins to write to /vagrant
    jenkins.vm.synced_folder ".", "/vagrant", owner: "vagrant", group: "vagrant", mount_options: ["dmode=775,fmode=664"]

    jenkins.vm.provider "virtualbox" do |vb|
      vb.memory = 4096
    end

    jenkins.vm.provision "ansible_local" do |ansible|
      ansible.playbook = "Jenkins/jenkins.yml"
    end
  end

  config.vm.define "deploy", autostart: false do |deploy|
    deploy.vm.box = "debian/contrib-stretch64"
    deploy.vm.hostname = "deploy"
    deploy.vm.network "forwarded_port", guest: 3001, host: 3004, host_ip: "127.0.0.1"

    deploy.vm.provision "ansible_local" do |ansible|
      ansible.playbook = "deploy.yml"
    end
  end
end
