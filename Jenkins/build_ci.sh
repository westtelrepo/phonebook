#!/bin/bash

set -e

# so that pg_tmp can find a username (Dockerfile will have set SETUID bit on it, which is very insecure in general)
/usr/sbin/useradd -u `id -u` --non-unique inside_jenkins

VERSION="2.7.14+$(git rev-parse --verify --short HEAD)"

yarn install --frozen-lockfile

echo "export const version: string = \"$VERSION\";" > backend/src/version.ts

yarn tsc --build

# lint everything, fail build if lint fails
# we have to lint after build, reference: https://github.com/typescript-eslint/typescript-eslint/issues/2094
yarn lint

pushd backend
yarn run ava --tap > ../report.tap
popd

pushd frontend
NODE_ENV=production yarn build
popd

mkdir /tmp/phonebook
cp -r . /tmp/phonebook/phonebook/

pushd /tmp/phonebook/phonebook/

rm -rf node_modules
rm -rf backend/node_modules
rm -rf shared/node_modules
rm -rf frontend/node_modules

# only have two workspaces so we don't install frontend dependencies
echo '{"private":true,"workspaces":["backend","shared"]}' > package.json
yarn install --production --frozen-lockfile

# TODO: only keep what is needed
rm -rf .git .gitignore Jenkins Jenkinsfile Vagrantfile babel.config.js \
    browser_src icons npm_build.sh package.json production_build.sh static/open-iconic-master \
    src dist/test test version.sh backend/xunit-ci.xml yarn.lock phonebook_dev.yml tftp.yml tsconfig.json \
    tslint.json backend/{src,babel.config.js,npm_build.sh,package.json,tsconfig.json,tslint.json} \
    deploy.yml backend/dist/tsconfig.tsbuildinfo backend/dist/test frontend/{package.json,rollup.config.js,tsconfig.json,src} \
    shared/{tsconfig.json,src} frontend/dist
find backend/dist -name '*.d.ts' -delete
find shared/dist -name '*.d.ts' -delete
find backend/dist -name '*.js.map' -delete
find shared/dist -name '*.js.map' -delete

# include minimal package.json so `node .` works
echo '{"main":"backend/dist/index.js"}' > package.json

# allow compressed delivery
gzip -k frontend/lib/tree.js

mkdir /tmp/phonebook/node/
tar -xf /usr/local/src/node.tar.xz -C /tmp/phonebook/node/ --strip-components=1

popd

mv /tmp/phonebook /tmp/phonebook-$VERSION

tar -cjf phonebook-$(dpkg --print-architecture)-$VERSION.tar.bz -C /tmp --sort=name --mtime="2019-01-15 22:51:13Z" --owner=0 --group=0 --numeric-owner phonebook-$VERSION
