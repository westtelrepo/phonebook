/* eslint-disable @typescript-eslint/strict-boolean-expressions */
import { IUser } from "@w/shared/dist/db_users";
import { pdp_assert } from "@w/shared/dist/pdp";
import { strict as assert } from "assert";
import bcrypt from "bcrypt";
import { IDatabase } from "pg-promise";
import uuid from "uuid-1345";

import * as migrations from "./migrations";
import { InitialMigration, ISchemaTask, transactor } from "./schema";

// since these database configs have no concepts of users, we want to have
// at least a fake "westtel" user so that we can migrate to one that does
const fake_user_info: Set<string> = new Set();
fake_user_info.add("00000000-0000-0000-0000-000000000000"); // empty database
fake_user_info.add(InitialMigration.state_uuid);
fake_user_info.add(migrations.mig.phonebook.state_uuid);

export async function user_authenticate(
    db: IDatabase<unknown>,
    user: { username?: string; uid?: string },
    password: string,
): Promise<false | { uid: string }> {
    const info = await transactor(
        db,
        async (t: ISchemaTask<unknown>) => {
            if (fake_user_info.has(t.ctx.context.schema_id)) {
                if (
                    user.username === "westtel" ||
                    user.uid === migrations.westtel_uid
                ) {
                    return {
                        bcrypt: migrations.westtel_bcrypt,
                        uid: migrations.westtel_uid,
                    };
                } else {
                    return null;
                }
            } else {
                if (user.uid) {
                    return t.oneOrNone<{ uid: string; bcrypt: string | null }>(
                        "SELECT uid, bcrypt FROM users WHERE uid = $(uid)",
                        user,
                    );
                } else if (user.username) {
                    return t.oneOrNone<{ uid: string; bcrypt: string | null }>(
                        "SELECT uid, bcrypt FROM users WHERE username = $(username)",
                        user,
                    );
                } else {
                    throw new Error("invalid user object to user_authenticate");
                }
            }
        },
        "readonly",
    );
    // if user doesn't have a hash don't allow login
    if (info?.bcrypt) {
        if (await bcrypt.compare(password, info.bcrypt)) {
            return { uid: info.uid };
        } else {
            return false;
        }
    } else {
        return false;
    }
}

export async function get_user_info(
    db: IDatabase<unknown>,
    user: { username?: string; uid?: string },
): Promise<(IUser & { permissions: any }) | null> {
    return transactor(
        db,
        async (t: ISchemaTask<unknown>) => {
            if (fake_user_info.has(t.ctx.context.schema_id)) {
                if (
                    user.username === "westtel" ||
                    user.uid === migrations.westtel_uid
                ) {
                    return {
                        is_superadmin: true,
                        uid: migrations.westtel_uid,
                        username: "westtel",
                        permissions: {},
                    };
                } else {
                    return null;
                }
            } else {
                if (typeof user.uid === "string") {
                    return t.oneOrNone<{
                        uid: string;
                        username: string;
                        is_superadmin: boolean;
                        permissions: any;
                    }>(
                        "SELECT uid, username, is_superadmin, permissions FROM users WHERE uid = $(uid)",
                        user,
                    );
                } else if (typeof user.username === "string") {
                    return t.oneOrNone<{
                        uid: string;
                        username: string;
                        is_superadmin: boolean;
                        permissions: any;
                    }>(
                        "SELECT uid, username, is_superadmin, permissions FROM users WHERE username = $(username)",
                        user,
                    );
                } else {
                    return null;
                }
            }
        },
        "readonly",
    );
}

export async function get_user_list(
    db: IDatabase<unknown>,
): Promise<(IUser & { permissions: any })[]> {
    return transactor(
        db,
        async t => {
            return t.any<{
                uid: string;
                username: string;
                is_superadmin: boolean;
                permissions: any;
            }>(
                "SELECT uid, username, is_superadmin, permissions FROM users ORDER BY username",
            );
        },
        "readonly",
    );
}

export async function user_upsert(
    db: IDatabase<unknown>,
    user: {
        username: string;
        uid?: string;
        is_superadmin?: boolean;
        password?: string;
        permissions?: any;
    },
    { user_to_check }: { user_to_check: any },
): Promise<string> {
    return transactor(db, async t => {
        assert(typeof user.username === "string");
        assert(user.username.length >= 1);

        let uid: string;
        let prev: { is_superadmin: boolean; permissions: any };
        if (user.uid && user.username) {
            assert(uuid.check(user.uid), "uid must be UUID");
            prev = await t.one<{
                uid: string;
                username: string;
                is_superadmin: boolean;
                permissions: any;
            }>(
                "SELECT uid, username, is_superadmin, permissions FROM users WHERE uid = $(uid)",
                user,
            );
            await t.none(
                "UPDATE users SET username = $(username) WHERE uid = $(uid)",
                user,
            );
            uid = user.uid;
        } else {
            uid = uuid.v4();
            user = { ...user, uid };
            await t.none(
                "INSERT INTO users(uid, username) VALUES($(uid), $(username))",
                user,
            );
            prev = {
                is_superadmin: false,
                permissions: {},
            };
        }

        const attrs_to_edit = new Set<string>();
        if (user.is_superadmin !== prev.is_superadmin) {
            attrs_to_edit.add("is_superadmin");
        }

        if (user.password) {
            attrs_to_edit.add("password");
        }

        for (const attr_name of attrs_to_edit) {
            pdp_assert({
                user: user_to_check,
                target: { type: "USER", user: prev },
                action: { type: "EDIT", attr_name },
            });
        }

        if (typeof user.is_superadmin === "boolean") {
            await t.none(
                "UPDATE users SET is_superadmin = $(is_superadmin) WHERE uid = $(uid)",
                user,
            );
        }

        if (user.password) {
            const hash = await bcrypt.hash(user.password, 10);
            await t.none(
                "UPDATE users SET bcrypt = $(hash) WHERE uid = $(uid)",
                { uid, hash },
            );
        }

        if (user.permissions) {
            const permissions = { ...prev.permissions, ...user.permissions };
            await t.none(
                "UPDATE users SET permissions = $(permissions) WHERE uid = $(uid)",
                { uid, permissions },
            );
        }

        return uid;
    });
}

export async function user_delete(
    db: IDatabase<unknown>,
    user: { uid: string },
    {
        user_to_check,
    }: { user_to_check: { is_superadmin: boolean; permissions: any } },
): Promise<void> {
    assert(uuid.check(user.uid), "uid must be a UUID");
    return transactor(db, async t => {
        const prev = await t.one<{
            uid: string;
            username: string;
            is_superadmin: boolean;
        }>(
            "SELECT uid, username, is_superadmin FROM users WHERE uid = $(uid)",
            user,
        );

        pdp_assert({
            user: user_to_check,
            target: { type: "USER", user: prev },
            action: { type: "DELETE" },
        });

        await t.none("DELETE FROM users WHERE uid = $(uid)", user);
    });
}
