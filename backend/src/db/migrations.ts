import { Phonebook } from "@w/shared/dist/phonebook";
import { strict as assert } from "assert";
import uuid from "uuid-1345";

import * as schema from "./schema";

// we give the westtel user a specific uid so that once the initial migration happens,
// westtel's sessions will not break as sessions are done by user id.
export const westtel_uid: string = "1273be2b-8b6e-493b-a86f-9b0ee7170af4";
export const westtel_bcrypt: string =
    "$2b$10$y4sR1VGfwnLzTtiDV.tpIu0tV/yQnQID9UyZtgutcZdkKVEU7Rr3W";

export const mig: { [name: string]: schema.Migration } = {};

mig.phonebook = new schema.Migration({
    name: "Initial Phonebook",
    description: "Adds phonebook storage capability",
    parent: schema.InitialMigration.state_uuid,
    after_func: async t => {
        await t.none(
            "INSERT INTO phonebook(pid, filename) VALUES($(pid), $(filename))",
            {
                pid: uuid.v4(),
                filename: "/datadisk1/ng911/config/ng911_phonebook.xml",
            },
        );
    },
    // NOTE: DO NOT EDIT!
    sql: `CREATE TABLE phonebook (
    pid uuid NOT NULL,
    filename text NOT NULL,
    last_saved jsonb
);

ALTER TABLE ONLY phonebook
    ADD CONSTRAINT phonebook_pkey PRIMARY KEY (pid);

CREATE TABLE phonebook_groups (
    gid uuid NOT NULL,
    pid uuid NOT NULL,
    gname text NOT NULL,
    icon text,
    ord integer NOT NULL,
    CONSTRAINT phonebook_groups_icon_check CHECK ((icon <> ''::text)),
    CONSTRAINT phonebook_groups_gname_check CHECK ((gname <> ''::text))
);

ALTER TABLE ONLY phonebook_groups
    ADD CONSTRAINT phonebook_groups_pid_ord_key UNIQUE (pid, ord);

ALTER TABLE ONLY phonebook_groups
    ADD CONSTRAINT phonebook_groups_pkey PRIMARY KEY (gid);

ALTER TABLE ONLY phonebook_groups
    ADD CONSTRAINT phonebook_groups_unique_pid_gname UNIQUE (pid, gname);

CREATE TABLE phonebook_numbers (
    nid uuid NOT NULL,
    gid uuid NOT NULL,
    nname text NOT NULL,
    number text NOT NULL,
    ng911 boolean NOT NULL,
    blind boolean NOT NULL,
    attended boolean NOT NULL,
    conference boolean NOT NULL,
    outbound boolean NOT NULL,
    icon text,
    ord integer NOT NULL,
    CONSTRAINT phonebook_numbers_icon_check CHECK ((icon <> ''::text)),
    CONSTRAINT phonebook_numbers_nname_check CHECK ((nname <> ''::text))
);

ALTER TABLE ONLY phonebook_numbers
    ADD CONSTRAINT phonebook_groups_unique_gid_ename UNIQUE (gid, nname);

ALTER TABLE ONLY phonebook_numbers
    ADD CONSTRAINT phonebook_numbers_gid_ord_key UNIQUE (gid, ord);

ALTER TABLE ONLY phonebook_numbers
    ADD CONSTRAINT phonebook_numbers_pkey PRIMARY KEY (nid);

CREATE TABLE phonebook_sd (
    sdid uuid NOT NULL,
    sdgid uuid NOT NULL,
    nid uuid NOT NULL,
    action text NOT NULL,
    icon text,
    ord integer NOT NULL
);

ALTER TABLE ONLY phonebook_sd
    ADD CONSTRAINT phonebook_sd_pkey PRIMARY KEY (sdid);

ALTER TABLE ONLY phonebook_sd
    ADD CONSTRAINT phonebook_sd_sdgid_ord_key UNIQUE (sdgid, ord);

CREATE TABLE phonebook_sd_groups (
    sdgid uuid NOT NULL,
    pid uuid NOT NULL,
    gname text NOT NULL,
    ord integer NOT NULL,
    CONSTRAINT phonebook_sd_groups_gname_check CHECK((gname <> ''::text))
);

ALTER TABLE ONLY phonebook_sd_groups
    ADD CONSTRAINT phonebook_sd_groups_gname_pid_key UNIQUE (gname, pid);

ALTER TABLE ONLY phonebook_sd_groups
    ADD CONSTRAINT phonebook_sd_groups_pid_ord_key UNIQUE (pid, ord);

ALTER TABLE ONLY phonebook_sd_groups
    ADD CONSTRAINT phonebook_sd_groups_pkey PRIMARY KEY (sdgid);

-- foreign keys

ALTER TABLE ONLY phonebook_numbers
    ADD CONSTRAINT phonebook_numbers_gid_fkey FOREIGN KEY (gid) REFERENCES phonebook_groups(gid);

ALTER TABLE ONLY phonebook_groups
    ADD CONSTRAINT phonebook_groups_pid_fkey FOREIGN KEY (pid) REFERENCES phonebook(pid);

ALTER TABLE ONLY phonebook_sd
    ADD CONSTRAINT phonebook_sd_nid_fkey FOREIGN KEY (nid) REFERENCES phonebook_numbers(nid);

ALTER TABLE ONLY phonebook_sd
    ADD CONSTRAINT phonebook_sd_sdgid_fkey FOREIGN KEY (sdgid) REFERENCES phonebook_sd_groups(sdgid);

ALTER TABLE ONLY phonebook_sd_groups
    ADD CONSTRAINT phonebook_sd_groups_pid_fkey FOREIGN KEY (pid) REFERENCES phonebook(pid);
`,
    // NOTE: whitespace was edited above and so we need to set the state explicitly
    state_uuid: "6a544ec1-f593-5d5a-a168-898d9e26511a",
});

assert(mig.phonebook.state_uuid === "6a544ec1-f593-5d5a-a168-898d9e26511a");

mig.users = new schema.Migration({
    name: "Users",
    description: "Adds user management into the database",
    parent: exports.mig.phonebook.state_uuid,
    after_func: async t => {
        await t.none(
            `INSERT INTO users(uid, username, bcrypt, is_superadmin)
            VALUES($(uid), $(username), $(bcrypt), $(is_superadmin))`,
            {
                uid: westtel_uid,
                username: "westtel",
                bcrypt: exports.westtel_bcrypt,
                is_superadmin: true,
            },
        );

        await t.none(
            `INSERT INTO users(uid, username, bcrypt, is_superadmin)
            VALUES($(uid), $(username), $(bcrypt), $(is_superadmin))`,
            {
                uid: uuid.v4(),
                username: "admin",
                bcrypt:
                    "$2b$10$GVFTdnZrf3drXUqLJovGyuGePkc05dJBWZJOhxscjP9fmU89vRzHa",
                is_superadmin: false,
            },
        );
    },
    sql: `CREATE TABLE users (
    uid UUID NOT NULL,
    username TEXT NOT NULL,
    bcrypt TEXT,
    is_superadmin BOOLEAN DEFAULT FALSE NOT NULL,
    permissions JSONB DEFAULT '{}' NOT NULL
);

ALTER TABLE ONLY users
    ADD CONSTRAINT users_uid_pkey PRIMARY KEY (uid);

ALTER TABLE ONLY users
    ADD CONSTRAINT users_username_unique_username UNIQUE (username);
`,
});

interface IUndoState {
    phonebook_list: Phonebook[];
}

mig.undo = new schema.Migration({
    name: "Undo",
    description:
        "Adds the ability to track contact editor changes and undo them",
    parent: exports.mig.users.state_uuid,
    before_func: async (t): Promise<IUndoState> => {
        // this require is to break a circular dependency
        // eslint-disable-next-line @typescript-eslint/no-var-requires, @typescript-eslint/no-require-imports
        const { get_phonebook_list_t, get_latest_t } = require("./phonebook");
        // TODO: acquire whole phonebook
        // eslint-disable-next-line @typescript-eslint/no-unsafe-call
        const list = await get_phonebook_list_t(t);
        const phonebook_list: Phonebook[] = [];

        for (const id of list) {
            // eslint-disable-next-line @typescript-eslint/no-unsafe-call
            phonebook_list.push(await get_latest_t(t, id.id));
        }

        return { phonebook_list };
    },
    after_func: async (t, { before_state }: { before_state: IUndoState }) => {
        // this require is to break a circular depencency
        // eslint-disable-next-line @typescript-eslint/no-var-requires, @typescript-eslint/no-require-imports
        const { create_t } = require("./phonebook");
        for (const ph of before_state.phonebook_list) {
            // eslint-disable-next-line @typescript-eslint/no-unsafe-call
            await create_t(t, ph);
        }
    },
    sql: `DROP TABLE phonebook, phonebook_groups, phonebook_numbers, phonebook_sd, phonebook_sd_groups;

CREATE TABLE phonebook_changes(
    id UUID NOT NULL,
    change_id BIGINT NOT NULL,
    utc TIMESTAMPTZ NOT NULL DEFAULT transaction_timestamp(),
    patch JSONB NOT NULL,
    inv_patch JSONB NOT NULL,
    state_hash TEXT NOT NULL,
    state JSONB,
    meta JSONB
);

ALTER TABLE ONLY phonebook_changes
    ADD CONSTRAINT phonebook_changes_pkey PRIMARY KEY (id, change_id);
`,
});

export const migration_list: schema.Migration[] = [];
for (const name in mig) {
    if (Object.prototype.hasOwnProperty.call(mig, name)) {
        migration_list.push(mig[name]);
    }
}

// polycom (TODO)
/*const polycom = `CREATE TABLE polycom_templates (
    id UUID NOT NULL,
    name TEXT NOT NULL,
    config JSONB NOT NULL
);

ALTER TABLE ONLY polycom_templates
    ADD CONSTRAINT polycom_templates_id_name_pkey PRIMARY KEY (id, name);`;*/
