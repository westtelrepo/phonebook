/* eslint-disable @typescript-eslint/strict-boolean-expressions */
import assert from "assert";
import os from "os";
import { Notification } from "pg";
import { IDatabase } from "pg-promise";
import uuid from "uuid-1345";

import { version } from "../version";
import { Listener } from "./listener";

const ping_channel: string = "90948d86-0b7a-4e5a-945c-5a230f5a8a21";

const TIMEOUT = Symbol("Timeout");

export class DBCommunicator {
    public readonly listen_id: string;
    private readonly _db: IDatabase<unknown>;
    private readonly _listener: Listener;
    private readonly _peers: Map<string, any>;
    private readonly _register: Map<string, (arg: any) => Promise<any>>;

    constructor({ db }: { db: IDatabase<unknown> }) {
        this.listen_id = uuid.v4();
        this._db = db;
        this._listener = new Listener({ db });
        this._peers = new Map();
        this._register = new Map();

        this._listener
            .connect()
            .catch(e =>
                console.error("ERROR IN DBCommunicator._listener.connect", e),
            );

        this._listener
            .listen(ping_channel, db_req => {
                if (!db_req.payload) {
                    return;
                }

                const payload = JSON.parse(db_req.payload);

                payload.received_utc = new Date();
                this._peers.set(payload.listen_id, payload);
            })
            .catch(e =>
                console.error(
                    "ERROR IN DBCommunicator._listener.listen(ping_channel)",
                    e,
                ),
            );

        this._listener
            // eslint-disable-next-line @typescript-eslint/no-misused-promises
            .listen(this.listen_id, async db_req => {
                if (!db_req.payload) {
                    return;
                }

                const payload = JSON.parse(db_req.payload);

                const func = this._register.get(payload.func);

                let result;
                if (!func) {
                    result = { error: "No Such Function" };
                } else {
                    try {
                        result = { ret: await func(payload.args) };
                    } catch (e) {
                        result = { error: e.message || "Unknown error" };
                    }
                }

                await this._db.one("SELECT pg_notify($(channel), $(payload))", {
                    channel: payload.return_path,
                    payload: result,
                });
            })
            .catch(e =>
                console.error(
                    "ERROR IN DBCommunicator._listener.listen(this.listen_id)",
                    e,
                ),
            );

        // eslint-disable-next-line @typescript-eslint/no-misused-promises
        setInterval(async () => {
            // constantly yell that we exist
            await this._db.one("SELECT pg_notify($(channel), $(payload))", {
                channel: ping_channel,
                payload: {
                    hostname: os.hostname(),
                    listen_id: this.listen_id,
                    version,
                },
            });

            // if we don't get anything for a minute, assume they are gone forever
            for (const peer of this._peers) {
                if (peer[1].received_utc < Number(new Date()) - 60 * 1000) {
                    this._peers.delete(peer[0]);
                }
            }
        }, 1000).unref(); // don't keep Node alive

        Object.seal(this);
    }

    public peers() {
        return this._peers.values();
    }

    public async call(peer_id: string, func: string, args: any) {
        assert(typeof peer_id === "string");
        assert(typeof func === "string");
        assert(typeof args === "object");
        const return_path = uuid.v4();

        let resolve: (value?: unknown) => void;
        let reject: (reason?: any) => void;
        const wait = new Promise((res, rej) => {
            resolve = res;
            reject = rej;
        });
        let not_first = false;
        const listener = (db_req: Notification) => {
            // sometimes this gets called twice and the second time db_req is undefined
            if (not_first) {
                return;
            }

            not_first = true;

            if (!db_req.payload) {
                throw new Error("no payload in response");
            }

            const payload = JSON.parse(db_req.payload);
            if (payload.error) {
                reject(new Error(payload.error));
            } else {
                resolve(payload.ret);
            }
        };

        await this._listener.listen(return_path, listener);
        await this._db.one("SELECT pg_notify($(channel), $(req))", {
            channel: peer_id,
            req: {
                func,
                args,
                return_path,
            },
        });

        try {
            const result = await Promise.race([
                wait,
                new Promise(res => setTimeout(() => res(TIMEOUT), 1500)),
            ]);

            if (result !== TIMEOUT) {
                return result;
            } else {
                throw new Error(`Timeout in db_communicator.call for ${func}`);
            }
        } finally {
            await this._listener.unlisten(return_path, listener);
        }
    }

    // function names can not be unregistered
    public register(name: string, func: (arg: any) => Promise<any>) {
        this._register.set(name, func);
    }

    public get listener() {
        return this._listener;
    }
}
