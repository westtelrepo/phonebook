import { Notification } from "pg";
import { IConnected, IDatabase, ILostContext } from "pg-promise";
import { IClient } from "pg-promise/typescript/pg-subset";

// this class automatically reconnects to the pg database on connection failure.
// HOWEVER, this will NOT receive notifications while it is not connected.
export class Listener {
    private readonly _db: IDatabase<unknown>;
    private readonly _listeners: Map<
        string,
        ((notification: Notification) => void)[]
    >;
    private _client: IConnected<unknown, IClient> | null;
    private _connecting: boolean;
    private readonly _listener_func: (notification: Notification) => void;
    private _done: boolean;

    constructor({ db }: { db: IDatabase<unknown> }) {
        this._db = db;
        this._listeners = new Map();
        this._client = null;
        this._connecting = false;
        this._listener_func = this._listener.bind(this);

        this._done = false;

        Object.seal(this);
    }

    public async connect() {
        if (this._client || this._connecting) {
            return;
        }

        this._connecting = true;
        let c: IConnected<unknown, IClient>;

        let failing: boolean;
        do {
            failing = false;
            try {
                c = await this._db.connect({
                    direct: true,
                    onLost: this._onLost.bind(this),
                });

                c.client.on("notification", this._listener_func);

                this._client = c;
                for (const channel of this._listeners.keys()) {
                    await c.none("LISTEN $1~", channel);
                }
            } catch (e) {
                // TODO: don't spam console
                console.log("error connecting Listener", e);
                await new Promise(resolve => setTimeout(resolve, 1000));
                failing = true;
            }
        } while (failing);

        this._connecting = false;
    }

    public done() {
        this._done = true;
        if (this._client) {
            this._client.done();
            this._client = null;
        }
    }

    // returns once "LISTEN channel" has been executed once
    public async listen(
        channel: string,
        func: (notification: Notification) => void,
    ) {
        const get = this._listeners.get(channel) ?? [];
        get.push(func);
        this._listeners.set(channel, get);
        if (this._client) {
            await this._client.none("LISTEN $1~", channel);
        }
    }

    // returns once "UNLISTEN channel" has been executed
    public async unlisten(
        channel: string,
        func: (notification: Notification) => void,
    ) {
        let get = this._listeners.get(channel) ?? [];
        get = get.filter(x => !Object.is(x, func));
        this._listeners.set(channel, get);
        if (get.length === 0 && this._client) {
            await this._client.none("UNLISTEN $1~", channel);
        }
    }

    private _onLost(err?: any, e?: ILostContext) {
        if (this._done || this._connecting) {
            return;
        }
        console.log("lost connection", err, e);
        if (e) {
            e.client.removeListener("notification", this._listener_func);
        }
        this._client = null;
        this.connect().catch(e =>
            console.error("ERROR IN Listener._onLost, this.connect()", e),
        );
    }

    private _listener(notification: Notification) {
        for (const f of this._listeners.get(notification.channel) ?? []) {
            f(notification);
        }
    }
}
