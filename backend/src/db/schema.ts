/* eslint-disable @typescript-eslint/strict-boolean-expressions */
import { strict as assert } from "assert";
import { IDatabase, ITask, ITaskContext, TransactionMode } from "pg-promise";
import uuid from "uuid-1345";

// this weird dance is so that TypeScript can enforce the type of t.ctx.context
export interface ISchemaContext {
    readonly schema_id: string;
}

const ignore = Symbol("ignore this");
export interface ISchemaTaskContext extends ITaskContext {
    readonly context: ISchemaContext;
    // HACK: this is to prevent ITask<T> from being assignable to ISchemaTask<T>
    readonly [ignore]: never;
}

export interface ISchemaTask<T> extends ITask<T> {
    ctx: ISchemaTaskContext;
}

export async function transactor<T>(
    db: IDatabase<unknown>,
    f: (t: ISchemaTask<unknown>) => Promise<T>,
    txMode?: TransactionMode | "readonly",
): Promise<T> {
    const mode = (() => {
        if (!txMode) {
            return new db.$config.pgp.txMode.TransactionMode({
                tiLevel: db.$config.pgp.txMode.isolationLevel.serializable,
                readOnly: false,
            });
        } else if (txMode === "readonly") {
            return new db.$config.pgp.txMode.TransactionMode({
                tiLevel: db.$config.pgp.txMode.isolationLevel.serializable,
                readOnly: true,
            });
        } else {
            return txMode;
        }
    })();

    let error: any;
    for (let i = 0; i < 3; i++) {
        try {
            return await db.task(async t => {
                try {
                    await lock_shared_schema_t(t);

                    // get the schema version once, and let everything in the transaction access it
                    // since the schema is locked above, it will not change
                    const schema_id = await get_schema_version_t(t);
                    const context: ISchemaContext = {
                        schema_id,
                    };

                    return await (t.tx.call(
                        context,
                        { mode },
                        f as (t: ITask<unknown>) => Promise<T>,
                    ) as Promise<T>);
                } finally {
                    try {
                        await t.one("SELECT pg_advisory_unlock_all()");
                    } catch (e) {
                        console.log("could not unlock advisory locks: ", e);
                    }
                }
            });
        } catch (e) {
            error = e;
            // retry on 40001 SERIALIZATION FAILURE
            if (e instanceof Error && (e as any).code === "40001") {
                // do nothing, aka retry
            } else {
                break;
            }
        }
    }
    throw error;
}

// Note: does not currently support rollbacks/etc
export const migrate = (
    migrations: Migration[],
    from: string,
    to: string,
): Migration[] | false => {
    const migs: Map<string, Migration> = new Map();

    for (const m of migrations) {
        assert(!migs.has(m.parent));
        migs.set(m.parent, m);
    }

    const ret: Migration[] = [];

    if (from === to) {
        return ret;
    }

    while (true) {
        const next = migs.get(from);
        if (next) {
            ret.push(next);
            from = next.state_uuid;
            if (from === to) {
                return ret;
            }
        } else {
            return false;
        }
    }
};

export class Migration {
    public readonly name: string;
    public readonly description: string;
    public readonly state_uuid: string;

    private readonly _sql: string;
    private readonly _parent: string;
    private readonly _before_func?: (t: ISchemaTask<unknown>) => Promise<any>;
    private readonly _after_func?: (
        t: ISchemaTask<unknown>,
        obj: { before_state: any },
    ) => Promise<void>;

    constructor({
        sql,
        parent,
        state_uuid,
        before_func,
        after_func,
        name,
        description,
    }: {
        sql: string;
        parent: string;
        state_uuid?: string;
        before_func?: (t: ISchemaTask<unknown>) => Promise<any>;
        after_func?: (
            t: ISchemaTask<unknown>,
            obj: { before_state: any },
        ) => Promise<void>;
        name: string;
        description: string;
    }) {
        assert(uuid.check(parent));
        assert(typeof sql === "string");

        this._sql = sql;
        this._parent = parent;
        this._before_func = before_func;
        this._after_func = after_func;

        if (state_uuid) {
            this.state_uuid = state_uuid;
        } else {
            this.state_uuid = uuid.v5({
                namespace: "f80b1d58-2959-4411-babb-5761e344bf50",
                name: parent + "\n" + sql,
            });
        }

        this.name = name;
        this.description = description;
    }

    public get sql() {
        return this._sql;
    }
    public get parent() {
        return this._parent;
    }
    public get before_func() {
        return this._before_func;
    }
    public get after_func() {
        return this._after_func;
    }
}

export const InitialMigration = new Migration({
    name: "Initial Migration",
    description:
        "The first migration, adds the ability to track migrations in the database",
    parent: "00000000-0000-0000-0000-000000000000",
    sql: `CREATE TABLE schema_version (
    dummy BOOLEAN PRIMARY KEY DEFAULT(FALSE),
    current UUID NOT NULL,
    CHECK(NOT dummy)
);
INSERT INTO schema_version(current) VALUES('00000000-0000-0000-0000-000000000000');
`,
    // we insert a UUID into the schema_version just to have one, it actually doesn't matter
    // what it is so that we can just use UPDATE further on
});
assert.equal(
    InitialMigration.state_uuid,
    "5a4d0f26-fcd2-5f68-a27f-4035dfd78f9b",
);

export const has_schema_version_t = async (
    t: ITask<unknown>,
): Promise<boolean> => {
    return (
        Number(
            (
                await t.one(`SELECT COUNT(*) AS count
            FROM information_schema.tables
            WHERE table_schema = 'public' AND table_name = 'schema_version'`)
            ).count,
        ) === 1
    );
};

export const get_schema_version_t = async (
    t: ITask<unknown>,
): Promise<string> => {
    try {
        return (await t.one("SELECT current FROM schema_version")).current;
    } catch (e) {
        // if we get a table undefined error then we use different logic,
        // but only if we're not in a transaction (because then we failed)
        if (!t.ctx.isTX && e.code === "42P01") {
            if (await is_database_empty_t(t)) {
                return "00000000-0000-0000-0000-000000000000";
            } else {
                // doesn't matter what this is, as long as it matches nothing
                return "cc39f305-578d-40e5-83aa-259b02242ce3";
            }
        } else {
            throw e;
        }
    }
};

export const is_database_empty_t = async (
    t: ITask<unknown>,
): Promise<boolean> => {
    // count the number of tables in the public schema
    // if they exist then it is very likely that something is already there
    const ret = await t.one(
        `SELECT COUNT(*) AS count FROM information_schema.tables WHERE table_schema = 'public'`,
    );
    return Number(ret.count) === 0;
};

// this needs to be a string. JavaScript can't represent this as a number
const lock_id = "-620431419248978490";

// if you need database schema to stay consistent (you probably do), lock this
export const lock_shared_schema_t = async (
    t: ITask<unknown>,
): Promise<void> => {
    assert(!t.ctx.isTX);
    await t.one("SELECT pg_advisory_lock_shared($(lock_id))", { lock_id });
};

// if you need to change the schema, lock this
export const lock_schema_t = async (t: ITask<unknown>): Promise<void> => {
    assert(!t.ctx.isTX);
    await t.one("SELECT pg_advisory_lock($(lock_id))", { lock_id });
};

export const assert_schema_version_t = async (
    t: ITask<unknown>,
    version: string,
): Promise<void> => {
    assert(uuid.check(version));
    const ret = await get_schema_version_t(t);
    if (ret !== version) {
        throw new Error(
            `database has unexpected schema version ${ret}, do you need to migrate?`,
        );
    }
};

const SCHEMA_TIMEOUT = process.env.PHONEBOOK_SCHEMA_TIMEOUT ?? "1s";

export const migrate_db = async ({
    db,
    migrations,
    to,
    txMode,
    backup,
}: {
    db: IDatabase<unknown>;
    migrations: Migration[];
    to: string;
    txMode?: TransactionMode;
    backup?: (t: ISchemaTask<unknown>) => Promise<void>;
}) => {
    assert(uuid.check(to));

    await db.task(async t => {
        let timeout: any;
        try {
            timeout = (await t.one("SHOW statement_timeout")).statement_timeout;
            // set statement_timeout so we don't cause lots of downtime
            // if we can't acquire the lock for some reason
            await t.none("SET statement_timeout = $(timeout)", {
                timeout: SCHEMA_TIMEOUT,
            });

            await lock_shared_schema_t(t);
            const from = await get_schema_version_t(t);

            const migs = migrate([...migrations, InitialMigration], from, to);
            if (!migs) {
                throw new Error("Could not generate migration");
            } else if (migs.length === 0) {
                // if the array is zero that means that we don't need to migrate
                return;
            }

            await lock_schema_t(t);

            await t.tx.call({ schema_id: from }, { mode: txMode }, (async (
                inner_t: ISchemaTask<unknown>,
            ) => {
                (inner_t.ctx.context as any).schema_id = from;
                if (backup) {
                    await backup(inner_t);
                }

                for (const m of migs) {
                    let before_state: any;
                    if (m.before_func) {
                        before_state = await m.before_func(inner_t);
                    }
                    await inner_t.none(m.sql);
                    await inner_t.none(
                        `UPDATE schema_version SET current = $(current)`,
                        { current: m.state_uuid },
                    );
                    (inner_t.ctx.context as any).schema_id = m.state_uuid;
                    if (m.after_func) {
                        await m.after_func(inner_t, { before_state });
                    }
                }
            }) as (t: ITask<unknown>) => Promise<void>);
        } finally {
            try {
                await t.one("SELECT pg_advisory_unlock_all()");
                if (timeout) {
                    await t.none("SET statement_timeout = $(timeout)", {
                        timeout,
                    });
                }
            } catch (e) {
                console.log("Could not unlock locks: ", e);
            }
        }
    });
};
