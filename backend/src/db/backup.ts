import assert from "assert";
import canonify from "canonical-json";
import crypto from "crypto";
import { IDatabase } from "pg-promise";

import { mig } from "./migrations";
import { get_state_t } from "./phonebook";
import { ISchemaTask, transactor } from "./schema";

export interface IDump {
    schema_id: string;
    hash: string;
    data: unknown;
}

async function dump_users_t(
    t: ISchemaTask<unknown>,
): Promise<
    {
        uid: string;
        username: string;
        bcrypt: string | null;
        is_superadmin: boolean;
        permissions: unknown;
    }[]
> {
    const migs = new Set([mig.undo.state_uuid, mig.users.state_uuid]);
    assert(migs.has(t.ctx.context.schema_id), "must have users to dump users");

    return t.any<{
        uid: string;
        username: string;
        bcrypt: string | null;
        is_superadmin: boolean;
        permissions: any;
    }>("SELECT uid, username, bcrypt, is_superadmin, permissions FROM users");
}

async function dump_phonebook_changes_t(t: ISchemaTask<unknown>): Promise<any> {
    if (t.ctx.context.schema_id === mig.undo.state_uuid) {
        const phonebook_changes = await t.any<{
            id: string;
            change_id: string;
            utc: number;
            patch: any;
            inv_patch: any;
            state_hash: string;
            meta: any;
            state?: any; // not actually returned, set below
        }>(
            `SELECT id, change_id, EXTRACT(EPOCH FROM utc)::text AS utc, patch, inv_patch, state_hash, meta
            FROM phonebook_changes ORDER BY id, change_id`,
        );

        for (let i = 0; i < phonebook_changes.length; i++) {
            const next = i + 1;

            // we want to output the latest state of each id, change_id run
            // that is, if change_id has a gap, we should output the latest state
            // before the gap
            let output_state = false;
            // if there is no next, output state
            if (next >= phonebook_changes.length) {
                output_state = true;
                // if next is either a gap or a different phonebook, output state
            } else if (
                phonebook_changes[i].id !== phonebook_changes[next].id ||
                Number(phonebook_changes[i].change_id) + 1 !==
                    Number(phonebook_changes[next].change_id)
            ) {
                output_state = true;
            }

            if (output_state) {
                phonebook_changes[i].state = (
                    await get_state_t(
                        t,
                        phonebook_changes[i].id,
                        Number(phonebook_changes[i].change_id),
                    )
                ).state;
            }
        }

        return phonebook_changes;
    } else {
        throw new Error("unexpected schema_id version!");
    }
}

async function dump_old_phonebook_t(t: ISchemaTask<unknown>): Promise<unknown> {
    const migs = new Set([mig.phonebook.state_uuid, mig.users.state_uuid]);
    assert(
        migs.has(t.ctx.context.schema_id),
        "must have old phonebook to dump old phonebook",
    );

    return {
        phonebook: await t.any("SELECT * FROM phonebook"),
        phonebook_groups: await t.any("SELECT * FROM phonebook_groups"),
        phonebook_numbers: await t.any("SELECT * FROM phonebook_numbers"),
        phonebook_sd: await t.any("SELECT * FROM phonebook_sd"),
        phonebook_sd_groups: await t.any("SELECT * FROM phonebook_sd_groups"),
    };
}

export async function dump_t(t: ISchemaTask<unknown>): Promise<IDump> {
    let data: unknown = {};

    if (t.ctx.context.schema_id === mig.undo.state_uuid) {
        data = {
            phonebook_changes: await dump_phonebook_changes_t(t),
            users: await dump_users_t(t),
        };
    } else if (t.ctx.context.schema_id === mig.users.state_uuid) {
        data = {
            phonebook: await dump_old_phonebook_t(t),
            users: await dump_users_t(t),
        };
    } else if (t.ctx.context.schema_id === mig.phonebook.state_uuid) {
        data = {
            phonebook: await dump_old_phonebook_t(t),
        };
    } else if (
        t.ctx.context.schema_id === "00000000-0000-0000-0000-000000000000"
    ) {
        // an empty database has no data
        data = {};
    } else {
        throw new Error("unexpected schema_id version!");
    }

    const hasher = crypto.createHash("sha256");
    hasher.update(canonify(data));
    const hash = hasher.digest("hex");

    return {
        schema_id: t.ctx.context.schema_id,
        hash,
        data,
    };
}

export async function dump(db: IDatabase<unknown>): Promise<IDump> {
    return transactor(
        db,
        async t => {
            return dump_t(t);
        },
        "readonly",
    );
}

async function restore_old_phonebook_t(
    t: ISchemaTask<unknown>,
    data: any,
): Promise<void> {
    await t.none(
        "TRUNCATE phonebook, phonebook_groups, phonebook_numbers, phonebook_sd, phonebook_sd_groups",
    );
    for (const e of data.phonebook) {
        await t.none(
            "INSERT INTO phonebook(pid, filename, last_saved) VALUES($(pid), $(filename), $(last_saved))",
            e,
        );
    }
    for (const e of data.phonebook_groups) {
        await t.none(
            `INSERT INTO phonebook_groups(gid, pid, gname, icon, ord)
            VALUES($(gid), $(pid), $(gname), $(icon), $(ord))`,
            e,
        );
    }
    for (const e of data.phonebook_numbers) {
        await t.none(
            `INSERT INTO phonebook_numbers(
                nid, gid, nname, number, ng911, blind, attended, conference, outbound, icon, ord
            ) VALUES (
                $(nid), $(gid), $(nname), $(number), $(ng911), $(blind),
                $(attended), $(conference), $(outbound), $(icon), $(ord)
            )`,
            e,
        );
    }
    for (const e of data.phonebook_sd_groups) {
        await t.none(
            `INSERT INTO phonebook_sd_groups(sdgid, pid, gname, ord)
            VALUES($(sdgid), $(pid), $(gname), $(ord))`,
            e,
        );
    }
    for (const e of data.phonebook_sd) {
        await t.none(
            `INSERT INTO phonebook_sd(sdid, sdgid, nid, action, icon, ord)
            VALUES($(sdid), $(sdgid), $(nid), $(action), $(icon), $(ord))`,
            e,
        );
    }
}

async function restore_users_t(
    t: ISchemaTask<unknown>,
    users: any[],
): Promise<void> {
    await t.none("TRUNCATE users");
    for (const u of users) {
        await t.none(
            `INSERT INTO users(uid, username, bcrypt, is_superadmin, permissions)
            VALUES($(uid), $(username), $(bcrypt), $(is_superadmin), $(permissions))`,
            u,
        );
    }
}

async function restore_phonebook_changes_t(
    t: ISchemaTask<unknown>,
    changes: any[],
): Promise<void> {
    await t.none("TRUNCATE phonebook_changes");
    for (const c of changes) {
        await t.none(
            `INSERT INTO phonebook_changes(id, change_id, utc, patch, inv_patch, state_hash, meta, state)
            VALUES(
                $(id), $(change_id), to_timestamp($(utc)), $(patch), $(inv_patch), $(state_hash), $(meta), $(state)
            )`,
            {
                ...c,
                patch: JSON.stringify(c.patch),
                inv_patch: JSON.stringify(c.inv_patch),
                meta: JSON.stringify(c.meta),
                state: Object.prototype.hasOwnProperty.call(c, "state")
                    ? JSON.stringify(c.state)
                    : null,
            },
        );
    }
}

function has_property<V extends string>(
    // eslint-disable-next-line @typescript-eslint/ban-types
    obj: object,
    name: V,
): obj is { [_ in V]: unknown } {
    return Object.prototype.hasOwnProperty.call(obj, name);
}

export async function restore_t(
    t: ISchemaTask<unknown>,
    dmp: IDump,
): Promise<void> {
    const hasher = crypto.createHash("sha256");
    hasher.update(canonify(dmp.data));
    const hash = hasher.digest("hex");

    if (hash !== dmp.hash) {
        const e: any = new Error(
            "The checksum in the database dump is invalid; the database dump is corrupted",
        );
        e.expose = true;
        throw e;
    }

    if (t.ctx.context.schema_id !== dmp.schema_id) {
        const e: any = new Error(
            "The database dump is for a different version of the database schema, refusing to import",
        );
        e.expose = true;
        throw e;
    }

    switch (t.ctx.context.schema_id) {
        case mig.undo.state_uuid: {
            if (
                typeof dmp.data !== "object" ||
                dmp.data === null ||
                !has_property(dmp.data, "users") ||
                !has_property(dmp.data, "phonebook_changes") ||
                !Array.isArray(dmp.data.users) ||
                !Array.isArray(dmp.data.phonebook_changes)
            ) {
                throw new Error("invalid/corrupt database dump");
            }
            await restore_users_t(t, dmp.data.users);
            await restore_phonebook_changes_t(t, dmp.data.phonebook_changes);
            break;
        }
        case mig.users.state_uuid: {
            await restore_users_t(t, (dmp.data as any).users);
            await restore_old_phonebook_t(t, (dmp.data as any).phonebook);
            break;
        }
        case mig.phonebook.state_uuid: {
            await restore_old_phonebook_t(t, (dmp.data as any).phonebook);
            break;
        }
        default: {
            const e: any = new Error("unrecognized database schema version");
            e.expose = true;
            throw e;
        }
    }
}

export async function restore(
    db: IDatabase<unknown>,
    dmp: IDump,
    get_dump: true,
): Promise<IDump>;
export async function restore(
    db: IDatabase<unknown>,
    dmp: IDump,
    get_dump: false,
): Promise<null>;
export async function restore(
    db: IDatabase<unknown>,
    dmp: IDump,
    get_dump: boolean,
): Promise<IDump | null> {
    return transactor(db, async t => {
        const ret = get_dump ? await dump_t(t) : null;
        await restore_t(t, dmp);
        return ret;
    });
}
