/* eslint-disable @typescript-eslint/no-non-null-assertion, @typescript-eslint/strict-boolean-expressions */
import {
    IHistoryEvent,
    IMeta,
    IUpsertContact,
} from "@w/shared/dist/db_phonebook";
import { ANY, IPDPUser, pdp_assert } from "@w/shared/dist/pdp";
import * as Phonebook from "@w/shared/dist/phonebook";
import {
    IPatchContact,
    IPatchDirectory,
    IPatchSpeedDial,
    IPostContact,
    IPostDirectory,
    IPostSpeedDial,
} from "@w/shared/dist/phonebook_interfaces";
import { is_sip_uri } from "@w/shared/dist/validation";
import { strict as assert } from "assert";
import Big from "big.js";
import canonify from "canonical-json";
import crypto from "crypto";
import fs from "fs";
import Immutable from "immutable";
import path from "path";
import { IDatabase } from "pg-promise";
import * as rfc6902 from "rfc6902";
import uuid from "uuid-1345";

import { to_xml } from "../phonebook/xml";
import * as migrations from "./migrations";
import { ISchemaTask, transactor } from "./schema";

export function empty_meta(): IMeta {
    return { user: null };
}

const undo_migs = new Set([migrations.mig.undo.state_uuid]);
const old_migs = new Set([
    migrations.mig.phonebook.state_uuid,
    migrations.mig.users.state_uuid,
]);

function user_error(msg: string) {
    const e: any = new Error(msg);
    e.expose = true;
    return e;
}

function assert_undo(t: ISchemaTask<unknown>) {
    if (!undo_migs.has(t.ctx.context.schema_id)) {
        const e: any = new Error("Database has inappriopriate version");
        e.expose = true;
        throw e;
    }
}

export async function create_t(
    t: ISchemaTask<unknown>,
    initial_state: Phonebook.Phonebook,
) {
    assert_undo(t);
    assert(initial_state instanceof Phonebook.Phonebook);
    assert.equal(initial_state.change_id, 0);
    initial_state.validate();

    const hash = crypto.createHash("sha256");
    hash.update(canonify(initial_state));
    const state_hash = hash.digest("hex");

    const to_insert = {
        id: initial_state.id,
        change_id: initial_state.change_id,
        patch: "[]",
        inv_patch: "[]",
        state: initial_state,
        state_hash,
    };

    // assert that that phonebook ID does not already exist
    await t.none(
        "SELECT change_id FROM phonebook_changes WHERE id = $(id) LIMIT 1",
        { id: initial_state.id },
    );

    await t.none(
        "INSERT INTO phonebook_changes(id, change_id, patch, inv_patch, state_hash, state)" +
            " VALUES($(id), $(change_id), $(patch), $(inv_patch), $(state_hash), $(state))",
        to_insert,
    );
}

export async function get_state_t(
    t: ISchemaTask<unknown>,
    id: string,
    change_id: "latest" | number,
): Promise<{
    change_id: number;
    id: string;
    inv_patch: any;
    patch: any;
    state: any;
    state_hash: string;
}> {
    assert_undo(t);
    if (change_id === "latest") {
        change_id = Number(
            (
                await t.one<{ change_id: string }>(
                    "SELECT max(change_id) AS change_id FROM phonebook_changes WHERE id = $(id)",
                    { id },
                )
            ).change_id,
        );
    }

    const state = await t.one<{
        patch: any;
        inv_patch: any;
        state_hash: string;
        state: any;
    }>(
        "SELECT patch, inv_patch, state_hash, state FROM phonebook_changes" +
            " WHERE id = $(id) AND change_id = $(change_id)",
        { id, change_id },
    );

    // calculate the state if we don't have it
    // for now we only try going back from a future state (last state will always be set)
    if (!state.state) {
        const future_change_id = await t.one<{ change_id: string; state: any }>(
            `SELECT change_id, state FROM phonebook_changes
            WHERE id = $(id) AND state IS NOT NULL AND change_id > $(change_id)
            ORDER BY change_id DESC LIMIT 1`,
            {
                change_id,
                id,
            },
        );

        const inv_patch_q = await t.many<{ inv_patch: any }>(
            `SELECT inv_patch FROM phonebook_changes
            WHERE change_id > $(change_id) AND change_id <= $(future_change_id)
            ORDER BY change_id DESC`,
            {
                id,
                change_id,
                future_change_id: future_change_id.change_id,
            },
        );

        let inv_patch: rfc6902.Patch = [];
        for (const p of inv_patch_q) {
            inv_patch = inv_patch.concat(p.inv_patch);
        }

        state.state = future_change_id.state;
        state.state.change_id = change_id;

        const errors = rfc6902.applyPatch(state.state, inv_patch);
        for (const e of errors) {
            if (e) {
                throw e;
            }
        }
    }

    // we always check the state hash
    const hash = crypto.createHash("sha256");
    hash.update(canonify(state.state));
    assert.equal(
        state.state_hash,
        hash.digest("hex"),
        "state hash is not equal!",
    );

    return { ...state, change_id, id };
}

export async function get_state(
    db: IDatabase<unknown>,
    id: string,
    change_id: "latest" | number,
) {
    const get = async (t: ISchemaTask<unknown>) => {
        return get_state_t(t, id, change_id);
    };

    return transactor(db, get, "readonly");
}

export async function get_history_t(
    t: ISchemaTask<unknown>,
    id: string,
): Promise<IHistoryEvent[]> {
    assert_undo(t);
    return (
        await t.any<{
            utc: number;
            patch: any;
            change_id: string;
            inv_patch: any;
            meta: any;
        }>(
            `SELECT EXTRACT(EPOCH FROM utc) AS utc, patch, change_id, inv_patch, meta
            FROM phonebook_changes WHERE id = $(id) ORDER BY change_id DESC`,
            { id },
        )
    ).map(e => ({
        utc: e.utc,
        patch: e.patch,
        inv_patch: e.inv_patch,
        change_id: Number(e.change_id),
        meta: e.meta,
    }));
}

export async function get_history(db: IDatabase<unknown>, id: string) {
    return transactor(db, async t => {
        return get_history_t(t, id);
    });
}

export async function get_history_one_t(
    t: ISchemaTask<unknown>,
    id: string,
    change_id: number,
): Promise<IHistoryEvent | null> {
    assert_undo(t);
    const ret = await t.oneOrNone<{
        utc: number;
        patch: any;
        change_id: string;
        inv_patch: any;
        meta: any;
    }>(
        `SELECT EXTRACT(EPOCH FROM utc) AS utc, patch, change_id, inv_patch, meta
        FROM phonebook_changes WHERE id = $(id) AND change_id = $(change_id) ORDER BY change_id DESC`,
        { id, change_id },
    );

    if (ret === null) {
        return ret;
    } else {
        return { ...ret, change_id: Number(ret.change_id) };
    }
}

export async function get_history_one(
    db: IDatabase<unknown>,
    id: string,
    change_id: number,
) {
    return transactor(db, async t => {
        return get_history_one_t(t, id, change_id);
    });
}

export async function update_phonebook_t(
    t: ISchemaTask<unknown>,
    id: string,
    operation: Phonebook.Operation,
    meta: IMeta,
): Promise<{ change_id: number }> {
    if (operation.patch.length === 0) {
        const e: any = new Error("Can not apply null operation");
        e.expose = true;
        e.code = 500;
        throw e;
    }
    assert_undo(t);
    const current = await get_state_t(t, id, "latest");

    operation = new Phonebook.Operation({
        patch: JSON.parse(JSON.stringify(operation.patch)),
        inv_patch: JSON.parse(JSON.stringify(operation.inv_patch)),
    });

    const prev = new Phonebook.Phonebook(current.state);

    // poor man's clone
    const next = JSON.parse(JSON.stringify(current.state));
    const result = rfc6902.applyPatch(next, operation.patch);
    next.change_id += 1;
    for (const e of result) {
        if (e) {
            throw e;
        }
    }

    assert.equal(
        current.change_id + 1,
        next.change_id,
        "change_id must increment by one",
    );
    assert.equal(id, next.id, "id must not change");

    assert.equal(
        canonify(next),
        canonify(new Phonebook.Phonebook(next)),
        "new phonebook must still be valid",
    );

    // validate the new state
    new Phonebook.Phonebook(next).validate();

    {
        // assert that going backwards still works perfectly
        const reverse = JSON.parse(JSON.stringify(next));
        for (const e of rfc6902.applyPatch(reverse, operation.inv_patch)) {
            if (e) {
                throw e;
            }
        }
        reverse.change_id--;

        assert.equal(canonify(prev), canonify(reverse), "rev_patch must work");
    }

    const hash = crypto.createHash("sha256");
    hash.update(canonify(next));
    const state_hash = hash.digest("hex");

    const to_meta: IMeta = {
        user: meta.user
            ? {
                  username: meta.user.username,
                  uid: meta.user.uid,
                  is_superadmin: meta.user.is_superadmin,
              }
            : null,
        ip: meta.ip,
        undo_of: meta.undo_of,
        restore_to: meta.restore_to,
        user_session: meta.user_session,
        is_import: meta.is_import,
    };

    const to_insert = {
        id,
        patch: JSON.stringify(operation.patch),
        inv_patch: JSON.stringify(operation.inv_patch),
        change_id: current.change_id + 1,
        state: JSON.stringify(next),
        state_hash,
        meta: to_meta,
    };

    await t.none(
        "INSERT INTO phonebook_changes(id, change_id, patch, inv_patch, state_hash, state, meta)" +
            " VALUES($(id), $(change_id), $(patch), $(inv_patch), $(state_hash), $(state), $(meta))",
        to_insert,
    );

    // delete the state of the one before to save space in the long run
    await t.none(
        "UPDATE phonebook_changes SET state = NULL WHERE id = $(id) AND change_id = $(change_id)",
        {
            id,
            change_id: current.change_id,
        },
    );

    // notify that we are changing this phonebook
    await t.one("SELECT pg_notify($(id), '')", { id });

    // return the new change_id
    return { change_id: current.change_id + 1 };
}

export async function update_phonebook(
    db: IDatabase<unknown>,
    id: string,
    operation: Phonebook.Operation,
    meta: IMeta,
) {
    const set = async (t: ISchemaTask<unknown>) => {
        await update_phonebook_t(t, id, operation, meta);
    };

    await transactor(db, set);
    // TODO: WRITE FILE IF COMMIT STATUS CHANGES
}

export async function undo_changeid_t(
    t: ISchemaTask<unknown>,
    id: string,
    change_id: number,
    meta: IMeta,
): Promise<{ change_id: number }> {
    assert_undo(t);
    const old_state = await get_state_t(t, id, change_id);
    const ph = await get_latest_t(t, id);

    const new_ph = ph._apply(
        new Phonebook.Operation({ patch: old_state.inv_patch, inv_patch: [] }),
    );

    // create the new inv_patch instead of the old one
    // this allows the patch to be applied in cases where the old inv_patch wouldn't work,
    // for example:
    //    rename "A" to "B"
    //    rename "B" to "C"
    //    undo rename "A" to "B" (name becomes "A")
    const inv_patch = rfc6902.createPatch(
        JSON.parse(JSON.stringify(new_ph)),
        JSON.parse(JSON.stringify(ph)),
    );

    // invert the operation
    // TODO: change inv_patch as needed
    const op = new Phonebook.Operation({
        patch: old_state.inv_patch,
        inv_patch,
    });

    return update_phonebook_t(t, id, op, { ...meta, undo_of: change_id });
}

export async function undo_changeid(
    db: IDatabase<unknown>,
    id: string,
    change_id: number,
    meta: IMeta,
): Promise<{ change_id: number }> {
    return transactor(db, async t => {
        return undo_changeid_t(t, id, change_id, meta);
    });
}

/**
 * (mostly) restore to a change_id. DOES NOT restore /current_change_id.
 * @param t database task
 * @param id phonebook id
 * @param change_id change identifier
 * @param meta meta information (who, why, etc)
 */
export async function restore_changeid_t(
    t: ISchemaTask<unknown>,
    id: string,
    change_id: number,
    meta: IMeta,
) {
    assert_undo(t);
    const ph = await get_latest_t(t, id);
    const ph_old = await get_state_t(t, id, change_id);

    const op = new Phonebook.Operation({
        a: ph,
        b: new Phonebook.Phonebook(ph_old.state)
            // ignore changes to current_change_id; we don't want to update
            .set("current_change_id", ph.current_change_id)
            // ignore changes to change_id (update_phonebook_t deals with this)
            .set("change_id", ph.change_id),
    });

    await update_phonebook_t(t, id, op, { ...meta, restore_to: change_id });
}

export async function restore_changeid(
    db: IDatabase<unknown>,
    id: string,
    change_id: number,
    meta: IMeta,
) {
    return transactor(db, async t => {
        return restore_changeid_t(t, id, change_id, meta);
    });
}

// returns the UUID of the new phonebook
// works under old and new structure
// filename has a default under the old structure, has no default under the new structure
export async function create_phonebook_t(
    t: ISchemaTask<unknown>,
    id: string = uuid.v4(),
    filename?: string,
): Promise<string> {
    if (undo_migs.has(t.ctx.context.schema_id)) {
        const phonebook = new Phonebook.Phonebook({ id, filename });
        await create_t(t, phonebook);
        return id;
    } else if (old_migs.has(t.ctx.context.schema_id)) {
        filename = filename ?? "/datadisk1/ng911/config/ng911_phonebook.xml";
        await t.none(
            "INSERT INTO phonebook(pid, filename) VALUES($(id), $(filename))",
            { id, filename },
        );
        return id;
    } else {
        throw new Error(
            "inappropriate database for create_phonebook_t operation",
        );
    }
}

// id must exist (TODO)
export async function get_latest_t(
    t: ISchemaTask<unknown>,
    id: string,
): Promise<Phonebook.Phonebook> {
    if (undo_migs.has(t.ctx.context.schema_id)) {
        return new Phonebook.Phonebook(
            (await get_state_t(t, id, "latest")).state,
        );
    } else if (old_migs.has(t.ctx.context.schema_id)) {
        const props: { filename: string } = await t.one(
            `SELECT filename
    FROM phonebook WHERE pid = $1`,
            id,
        );

        const groups = await t.any<{
            gid: string;
            gname: string;
            icon: string | null;
        }>(
            "SELECT gid, gname, icon FROM phonebook_groups WHERE pid = $1 ORDER BY ord",
            id,
        );

        const numbers = await t.any<{
            nid: string;
            gid: string;
            nname: string;
            number: string;
            icon: string | null;
            ng911: boolean;
            blind: boolean;
            attended: boolean;
            conference: boolean;
            outbound: boolean;
        }>(
            `SELECT nid, gid, nname, number, n.icon, ng911, blind, attended, conference, outbound
    FROM phonebook_groups AS g JOIN phonebook_numbers AS n USING (gid) WHERE g.pid = $1 ORDER BY g.ord, n.ord`,
            id,
        );

        const sdgroups = await t.any<{ sdgid: string; gname: string }>(
            "SELECT sdgid, gname FROM phonebook_sd_groups WHERE pid = $1 ORDER BY ord",
            id,
        );

        const sds = await t.any<{
            sdid: string;
            sdgid: string;
            action: string;
            icon: string | null;
            nid: string;
        }>(
            `SELECT sdid, sdgid, action, icon, nid
    FROM phonebook_sd_groups JOIN phonebook_sd USING(sdgid)
    WHERE pid = $1 ORDER BY phonebook_sd_groups.ord, phonebook_sd.ord`,
            id,
        );

        const contacts: Map<string, Phonebook.Contact> = new Map();
        const speed_dials: Map<string, Phonebook.SpeedDial> = new Map();
        const dirs: Map<string, Phonebook.Directory> = new Map();
        const sd_dirs: Map<string, Phonebook.Directory> = new Map();

        groups.forEach((e, i) => {
            dirs.set(
                e.gid,
                new Phonebook.Directory({
                    name: e.gname,
                    ord: i,
                    icon: e.icon === null ? undefined : e.icon,
                }),
            );
        });

        sdgroups.forEach((e, i) => {
            sd_dirs.set(
                e.sdgid,
                new Phonebook.Directory({ name: e.gname, ord: i }),
            );
        });

        const number_ord: Map<string, number> = new Map(); // Group UUID -> Ord

        numbers.forEach(e => {
            const ord = number_ord.get(e.gid) ?? 0;

            contacts.set(
                e.nid,
                new Phonebook.Contact({
                    parent: e.gid,
                    ord,
                    name: e.nname,
                    contact: e.number,
                    icon: e.icon === null ? undefined : e.icon,
                    xfer_ng911: e.ng911,
                    xfer_blind: e.blind,
                    xfer_attended: e.attended,
                    xfer_conference: e.conference,
                    xfer_outbound: e.outbound,
                }),
            );

            number_ord.set(e.gid, ord + 1);
        });

        const sd_ord: Map<string, number> = new Map(); // SD Group UUID -> Ord

        sds.forEach(e => {
            const ord = sd_ord.get(e.sdgid) ?? 0;

            if (!Phonebook.is_sd_action(e.action)) {
                throw new Error(
                    "unknown sd_action from old pre-undo phonebook",
                );
            }
            speed_dials.set(
                e.sdid,
                new Phonebook.SpeedDial({
                    action: e.action,
                    contact_id: e.nid,
                    parent: e.sdgid,
                    icon: e.icon === null ? undefined : e.icon,
                    ord,
                }),
            );

            sd_ord.set(e.sdgid, ord + 1);
        });

        const ret = new Phonebook.Phonebook({
            id,
            filename: props.filename,
            contacts,
            speed_dials,
            sd_dirs,
            dirs,
        });

        ret.validate();
        return ret;
    } else {
        throw new Error("inappropriate database version");
    }
}

export async function get_latest(
    db: IDatabase<unknown>,
    id: string,
): Promise<Phonebook.Phonebook> {
    return transactor(db, async t => {
        return get_latest_t(t, id);
    });
}

export async function update_current_change_id(
    db: IDatabase<unknown>,
    pid: string,
    meta: IMeta,
): Promise<{ change_id: number }> {
    return transactor(db, async t => {
        const latest = await get_latest_t(t, pid);
        const op = Phonebook.Operation.change_val(
            ["current_change_id"],
            latest.current_change_id,
            latest.change_id,
        );
        return update_phonebook_t(t, pid, op, meta);
    });
}

const BASE_URL = process.env.BASE_URL ?? "http://192.168.62.11:3001/";

export async function write_phonebook(
    db: IDatabase<unknown>,
    pid: string,
): Promise<void> {
    const to_url = (x: string) =>
        BASE_URL +
        "icon?pid=" +
        encodeURIComponent(pid) +
        "&icon=" +
        encodeURIComponent(x);

    const ph = await get_latest(db, pid);
    if (ph.filename && ph.current_change_id) {
        const to_write = await get_state(db, pid, ph.current_change_id);
        const new_ph = new Phonebook.Phonebook(to_write.state);

        fs.writeFileSync(ph.filename, to_xml(new_ph, to_url));
    }
}

// TODO: how to handle deleted phonebooks (undo-style)
export async function get_phonebook_list_t(
    t: ISchemaTask<unknown>,
): Promise<{ id: string }[]> {
    if (undo_migs.has(t.ctx.context.schema_id)) {
        return t.any<{ id: string }>(
            "SELECT DISTINCT ON (id) id FROM phonebook_changes GROUP BY id",
        );
    } else if (old_migs.has(t.ctx.context.schema_id)) {
        return t.any<{ id: string }>("SELECT pid AS id FROM phonebook");
    } else if (
        t.ctx.context.schema_id === "00000000-0000-0000-0000-000000000000"
    ) {
        return []; // empty database has no phonebooks
    } else {
        throw new Error("inappropriate database version");
    }
}

export async function get_phonebook_list(
    db: IDatabase<unknown>,
): Promise<{ id: string }[]> {
    return transactor(db, async t => {
        return get_phonebook_list_t(t);
    });
}

// TODO: make it so which node you run this from does not matter
/** @param id Phonebook to get item list for */
export async function get_icon_list(
    db: IDatabase<unknown>,
    id: string,
): Promise<string[]> {
    const ph = await get_latest(db, id);

    if (!ph.filename) {
        return [];
    }

    const ret: string[] = [];

    const ph_dir = path.dirname(ph.filename);
    const add_files = async (dir: string) => {
        let list: string[] = [];
        try {
            list = fs.readdirSync(dir);
        } catch (e) {
            // ignore the error if the directory doesn't exist
            if (e.code === "ENOENT") {
                list = [];
            } else {
                throw e;
            }
        }
        for (const file of list) {
            const stat = fs.statSync(path.join(dir, file));
            if (stat.isFile() && /\.(png|ico)$/.test(file)) {
                ret.push(
                    path.relative(
                        path.join(ph_dir, "icons"),
                        path.join(dir, file),
                    ),
                );
            } else if (stat.isDirectory()) {
                await add_files(path.join(dir, file));
            }
        }
    };
    await add_files(path.join(ph_dir, "icons"));
    // eslint-disable-next-line @typescript-eslint/require-array-sort-compare
    ret.sort();
    return ret;
}

export async function patch_directory_t(
    t: ISchemaTask<unknown>,
    pid: string,
    gid: string,
    patch: IPatchDirectory,
    meta: IMeta,
): Promise<{ change_id: number } | null> {
    if (undo_migs.has(t.ctx.context.schema_id)) {
        const ph = await get_latest_t(t, pid);

        let op = new Phonebook.Operation();

        const old_dir = ph.dirs.get(gid);
        if (old_dir === undefined) {
            throw user_error("Directory must exist to be edited");
        }

        if (typeof patch.name === "string") {
            op = op.concat(
                Phonebook.Operation.change_val(
                    ["dirs", gid, "name"],
                    old_dir.name,
                    patch.name,
                ),
            );
        }

        if (patch.icon !== undefined) {
            op = op.concat(
                Phonebook.Operation.change_val(
                    ["dirs", gid, "icon"],
                    old_dir.icon,
                    patch.icon === null ? undefined : patch.icon,
                ),
            );
        }

        return update_phonebook_t(t, pid, op, meta);
    } else if (old_migs.has(t.ctx.context.schema_id)) {
        assert.strictEqual(
            pid,
            (
                await t.one(
                    "SELECT pid FROM phonebook_groups WHERE gid = $(gid)",
                    { gid },
                )
            ).pid,
        );

        let name: string | undefined;
        if (typeof patch.name === "string") {
            name = patch.name.trim();
            if (name === "") {
                throw user_error("Group name must not be empty.");
            }
        }

        try {
            if (name !== undefined) {
                await t.none(
                    "UPDATE phonebook_groups SET gname = $(name) WHERE gid = $(gid)",
                    { name, gid },
                );
            }
            if (patch.icon !== undefined) {
                await t.none(
                    "UPDATE phonebook_groups SET icon = $(icon) WHERE gid = $(gid)",
                    { icon: patch.icon, gid },
                );
            }
        } catch (e) {
            if (
                e.code === "23505" &&
                e.table === "phonebook_groups" &&
                e.constraint === "phonebook_groups_unique_pid_gname"
            ) {
                throw user_error("Group name already exists");
            } else if (
                e.code === "23514" &&
                e.table === "phonebook_groups" &&
                e.constraint === "phonebook_groups_gname_check"
            ) {
                throw user_error("Group name can not be empty");
            }
            throw e;
        }
        return null;
    } else {
        throw new Error("inappropriate database version");
    }
}

export async function patch_directory(
    db: IDatabase<unknown>,
    pid: string,
    gid: string,
    patch: IPatchDirectory,
    meta: IMeta,
): Promise<{ change_id: number } | null> {
    return transactor(db, async t => {
        return patch_directory_t(t, pid, gid, patch, meta);
    });
}

export async function post_directory_t(
    t: ISchemaTask<unknown>,
    pid: string,
    obj: IPostDirectory,
    meta: IMeta,
): Promise<{ id: string; change_id?: number }> {
    assert_undo(t);

    const ph = await get_latest_t(t, pid);

    const id = uuid.v4();

    const biggest_dir = ph.dirs.maxBy(
        v => v.ord,
        (x, y) => x.cmp(y),
    );

    const big = biggest_dir ? biggest_dir.ord.plus(1) : Big(0);
    const op = ph.dir_add(
        new Phonebook.Directory({ ...obj, ord: big, icon: "folder.ico" }),
        { id },
    );

    const c = await update_phonebook_t(t, pid, op, meta);

    return { id, change_id: c ? c.change_id : undefined };
}

export async function post_directory(
    db: IDatabase<unknown>,
    pid: string,
    obj: IPostDirectory,
    meta: IMeta,
): Promise<{ id: string; change_id?: number }> {
    return transactor(db, async t => {
        return post_directory_t(t, pid, obj, meta);
    });
}

export async function delete_directory_t(
    t: ISchemaTask<unknown>,
    pid: string,
    gid: string,
    meta: IMeta,
): Promise<{ change_id: number }> {
    assert_undo(t);

    const ph = await get_latest_t(t, pid);

    const op = ph.dir_del(gid);

    return update_phonebook_t(t, pid, op, meta);
}

export async function delete_directory(
    db: IDatabase<unknown>,
    pid: string,
    gid: string,
    meta: IMeta,
): Promise<{ change_id: number }> {
    return transactor(db, async t => {
        return delete_directory_t(t, pid, gid, meta);
    });
}

function almost_avg_big(x: Big, y: Big): Big {
    assert(!x.eq(y), "almost_avg_big x can not equal y");

    // put the smallest number first
    if (x.gt(y)) {
        return almost_avg_big(y, x);
    }

    const avg = x.times("0.5").plus(y.times("0.5"));

    assert(x.lt(avg));
    assert(avg.lt(y));

    for (let i = 1; true; i++) {
        const rounded = avg.round(i);

        if (rounded.gt(x) && rounded.lt(y)) {
            return rounded;
        }
    }
}

/**
 * Moves an element (by ord) in a list. May return multiple ord changes if necessary.
 * @param list must be sorted appropriately
 * @param id id of element to be moved
 * @param direction direction to move element
 */
function move(
    list: Immutable.List<[string, { ord: Big }]>,
    id: string,
    direction: "up" | "down",
): { id: string; new_ord: Big; old_ord: Big }[] {
    let index = list.findIndex(v => v[0] === id);
    if (index === -1) {
        return [];
    }
    const elem = list.get(index)!;

    // swap the elements so the array looks like what we want it to look like
    switch (direction) {
        case "down": {
            const next = list.get(index + 1);
            if (!next) {
                return [];
            }

            list = list.set(index + 1, elem).set(index, next);
            index = index + 1;
            break;
        }
        case "up": {
            const prev = index - 1 >= 0 ? list.get(index - 1) : undefined;
            if (!prev) {
                return [];
            }

            list = list.set(index - 1, elem).set(index, prev);
            index = index - 1;
            break;
        }
        default:
            throw new Error("unknown direction");
    }

    const initial_midpoint = (() => {
        const prev = index - 1 >= 0 ? list.get(index - 1) : undefined;
        const next = list.get(index + 1);
        if (next && prev) {
            if (next[1].ord.eq(prev[1].ord)) {
                // initial midpoint is not sufficient in this case
                return next[1].ord;
            } else {
                return almost_avg_big(prev[1].ord, next[1].ord);
            }
        } else if (next) {
            return next[1].ord.minus("1");
        } else if (prev) {
            return prev[1].ord.plus("1");
        } else {
            return Big(0);
        }
    })();

    let next_ord: Big = initial_midpoint.plus("1");
    let prev_ord: Big = initial_midpoint.minus("1");

    const three_quarters = new Set<number>();
    const one_quarter = new Set<number>();

    // if any later elements are equal to the initial midpoint than they must be moved
    for (let i = index + 1; i < list.size; i++) {
        const e = list.get(i)!;
        if (e[1].ord.gt(initial_midpoint)) {
            next_ord = e[1].ord;
            break;
        } else {
            three_quarters.add(i);
        }
    }

    // if any previous elements are equal to the initial midpoint than they must be moved
    for (let i = index - 1; i >= 0; i--) {
        const e = list.get(i)!;
        if (e[1].ord.lt(initial_midpoint)) {
            prev_ord = e[1].ord;
            break;
        } else {
            one_quarter.add(i);
        }
    }

    const ret: { id: string; old_ord: Big; new_ord: Big }[] = [];
    const midpoint = almost_avg_big(prev_ord, next_ord);
    ret.push({ id: elem[0], old_ord: elem[1].ord, new_ord: midpoint });

    const one_quarter_point = almost_avg_big(prev_ord, midpoint);
    for (const i of one_quarter) {
        const e = list.get(i)!;
        ret.push({ id: e[0], old_ord: e[1].ord, new_ord: one_quarter_point });
    }

    const three_quarter_point = almost_avg_big(midpoint, next_ord);
    for (const i of three_quarters) {
        const e = list.get(i)!;
        ret.push({ id: e[0], old_ord: e[1].ord, new_ord: three_quarter_point });
    }

    return ret;
}

export async function move_directory_t(
    t: ISchemaTask<unknown>,
    pid: string,
    gid: string,
    direction: "up" | "down",
    meta: IMeta,
): Promise<{ change_id?: number }> {
    const ph = await get_latest_t(t, pid);

    const group = ph.dirs.get(gid);

    if (!group) {
        return {};
    }

    const ops = move(ph.children_of(group.parent), gid, direction);
    let op = new Phonebook.Operation();
    for (const o of ops) {
        op = op.concat(
            Phonebook.Operation.change_val(
                ["dirs", o.id, "ord"],
                o.old_ord,
                o.new_ord,
            ),
        );
    }

    return update_phonebook_t(t, pid, op, meta);
}

export async function move_directory(
    db: IDatabase<unknown>,
    pid: string,
    gid: string,
    direction: "up" | "down",
    meta: IMeta,
): Promise<{ change_id?: number }> {
    return transactor(db, async t => {
        return move_directory_t(t, pid, gid, direction, meta);
    });
}

export async function patch_sd_directory_t(
    t: ISchemaTask<unknown>,
    pid: string,
    gid: string,
    patch: IPatchDirectory,
    meta: IMeta,
): Promise<{ change_id: number }> {
    if (undo_migs.has(t.ctx.context.schema_id)) {
        const ph = await get_latest_t(t, pid);

        let op = new Phonebook.Operation();

        const old_dir = ph.sd_dirs.get(gid);
        if (old_dir === undefined) {
            throw user_error("Directory must exist to be edited");
        }

        if (typeof patch.name === "string") {
            op = op.concat(
                Phonebook.Operation.change_val(
                    ["sd_dirs", gid, "name"],
                    old_dir.name,
                    patch.name,
                ),
            );
        }

        if (patch.icon !== undefined) {
            op = op.concat(
                Phonebook.Operation.change_val(
                    ["sd_dirs", gid, "icon"],
                    old_dir.icon,
                    patch.icon === null ? undefined : patch.icon,
                ),
            );
        }

        return update_phonebook_t(t, pid, op, meta);
    } else if (old_migs.has(t.ctx.context.schema_id)) {
        assert(false);
        throw new Error("dead code");
    } else {
        throw new Error("inappropriate database version");
    }
}

export async function patch_sd_directory(
    db: IDatabase<unknown>,
    pid: string,
    gid: string,
    patch: IPatchDirectory,
    meta: IMeta,
): Promise<{ change_id: number }> {
    return transactor(db, async t => {
        return patch_sd_directory_t(t, pid, gid, patch, meta);
    });
}

export async function post_sd_directory_t(
    t: ISchemaTask<unknown>,
    pid: string,
    obj: IPostDirectory,
    meta: IMeta,
): Promise<{ id: string; change_id?: number }> {
    assert_undo(t);

    const ph = await get_latest_t(t, pid);

    const id = uuid.v4();

    const biggest_dir = ph.sd_dirs.maxBy(
        v => v.ord,
        (x, y) => x.cmp(y),
    );

    const big = biggest_dir ? biggest_dir.ord.plus(1) : Big(0);
    const op = ph.sd_dir_add(new Phonebook.Directory({ ...obj, ord: big }), {
        id,
    });

    const c = await update_phonebook_t(t, pid, op, meta);

    return { id, change_id: c.change_id };
}

export async function post_sd_directory(
    db: IDatabase<unknown>,
    pid: string,
    obj: IPostDirectory,
    meta: IMeta,
): Promise<{ id: string; change_id?: number }> {
    return transactor(db, async t => {
        return post_sd_directory_t(t, pid, obj, meta);
    });
}

export async function delete_sd_directory_t(
    t: ISchemaTask<unknown>,
    pid: string,
    gid: string,
    meta: IMeta,
): Promise<{ change_id: number }> {
    assert_undo(t);

    const ph = await get_latest_t(t, pid);

    const op = ph.sd_dir_del(gid);

    return update_phonebook_t(t, pid, op, meta);
}

export async function delete_sd_directory(
    db: IDatabase<unknown>,
    pid: string,
    gid: string,
    meta: IMeta,
): Promise<{ change_id: number }> {
    return transactor(db, async t => {
        return delete_sd_directory_t(t, pid, gid, meta);
    });
}

export async function move_sd_directory_t(
    t: ISchemaTask<unknown>,
    pid: string,
    gid: string,
    direction: "up" | "down",
    meta: IMeta,
): Promise<{ change_id?: number }> {
    const ph = await get_latest_t(t, pid);

    const group = ph.sd_dirs.get(gid);

    if (!group) {
        return {};
    }

    const ops = move(ph.sd_children_of(group.parent), gid, direction);
    let op = new Phonebook.Operation();
    for (const o of ops) {
        op = op.concat(
            Phonebook.Operation.change_val(
                ["sd_dirs", o.id, "ord"],
                o.old_ord,
                o.new_ord,
            ),
        );
    }

    return update_phonebook_t(t, pid, op, meta);
}

export async function move_sd_directory(
    db: IDatabase<unknown>,
    pid: string,
    gid: string,
    direction: "up" | "down",
    meta: IMeta,
): Promise<{ change_id?: number }> {
    return transactor(db, async t => {
        return move_sd_directory_t(t, pid, gid, direction, meta);
    });
}

export async function patch_contact_t(
    t: ISchemaTask<unknown>,
    pid: string,
    cid: string,
    patch: IPatchContact,
    meta: IMeta,
    user_to_check: IPDPUser,
): Promise<{ change_id: number }> {
    assert_undo(t);

    const ph = await get_latest_t(t, pid);

    let op = new Phonebook.Operation();

    const old_dir = ph.contacts.get(cid);
    if (old_dir === undefined) {
        throw user_error("Directory must exist to be edited");
    }

    pdp_assert({
        user: user_to_check,
        target: { type: "CONTACT", is_sip: is_sip_uri(old_dir.contact) },
        action: { type: "EDIT", attr_name: ANY },
    });

    if (patch.contact && is_sip_uri(patch.contact)) {
        pdp_assert({
            user: user_to_check,
            target: { type: "CONTACT", is_sip: true },
            action: { type: "EDIT", attr_name: ANY },
        });
    }

    if (typeof patch.name === "string") {
        op = op.concat(
            Phonebook.Operation.change_val(
                ["contacts", cid, "name"],
                old_dir.name,
                patch.name,
            ),
        );
    }

    if (typeof patch.contact === "string") {
        op = op.concat(
            Phonebook.Operation.change_val(
                ["contacts", cid, "contact"],
                old_dir.contact,
                patch.contact,
            ),
        );
    }

    if (patch.icon !== undefined) {
        op = op.concat(
            Phonebook.Operation.change_val(
                ["contacts", cid, "icon"],
                old_dir.icon,
                patch.icon === null ? undefined : patch.icon,
            ),
        );
    }

    if (patch.ng911_transfer_type !== undefined) {
        op = op.concat(
            Phonebook.Operation.change_val(
                ["contacts", cid, "ng911_transfer_type"],
                old_dir.ng911_transfer_type,
                patch.ng911_transfer_type === null
                    ? undefined
                    : patch.ng911_transfer_type,
            ),
        );
    }

    if (typeof patch.contact === "string" && is_sip_uri(patch.contact)) {
        const do_one = (name: keyof Phonebook.Contact) => {
            op = op.concat(
                Phonebook.Operation.change_val(
                    ["contacts", cid, name],
                    old_dir[name],
                    false,
                ),
            );
        };
        do_one("xfer_blind");
        do_one("xfer_attended");
        do_one("xfer_conference");
        do_one("xfer_outbound");

        op = op.concat(
            Phonebook.Operation.change_val(
                ["contacts", cid, "xfer_ng911"],
                old_dir.xfer_ng911,
                true,
            ),
        );

        if (old_dir.ng911_transfer_type === undefined) {
            op = op.concat(
                Phonebook.Operation.change_val(
                    ["contacts", cid, "ng911_transfer_type"],
                    old_dir.ng911_transfer_type,
                    "internal",
                ),
            );
        }
    } else if (
        typeof patch.contact === "string" &&
        !is_sip_uri(patch.contact)
    ) {
        op = op.concat(
            Phonebook.Operation.change_val(
                ["contacts", cid, "ng911_transfer_type"],
                old_dir.ng911_transfer_type,
                undefined,
            ),
        );
    }

    const do_bool = (name: keyof Phonebook.Contact & keyof IPatchContact) => {
        if (typeof patch[name] === "boolean") {
            op = op.concat(
                Phonebook.Operation.change_val(
                    ["contacts", cid, name],
                    old_dir[name],
                    patch[name],
                ),
            );
        }
    };

    do_bool("xfer_ng911");
    do_bool("xfer_blind");
    do_bool("xfer_attended");
    do_bool("xfer_conference");
    do_bool("xfer_outbound");

    if (patch.parent !== undefined) {
        const new_parent = patch.parent === null ? undefined : patch.parent;

        const max_ord = ph.contacts
            .filter(x => x.parent === new_parent)
            .maxBy(
                x => x.ord,
                (a, b) => a.cmp(b),
            );
        const new_ord = max_ord ? max_ord.ord.plus("1") : Big(0);

        op = op.concat(
            Phonebook.Operation.change_val(
                ["contacts", cid, "parent"],
                old_dir.parent,
                patch.parent === null ? undefined : patch.parent,
            ),
        );

        // in addition to changing the parent, we need to
        // change the ord as well
        op = op.concat(
            Phonebook.Operation.change_val(
                ["contacts", cid, "ord"],
                old_dir.ord,
                new_ord,
            ),
        );
    }

    return update_phonebook_t(t, pid, op, meta);
}

export async function patch_contact(
    db: IDatabase<unknown>,
    pid: string,
    cid: string,
    patch: IPatchContact,
    meta: IMeta,
    user_to_check: IPDPUser,
): Promise<{ change_id: number }> {
    return transactor(db, async t => {
        return patch_contact_t(t, pid, cid, patch, meta, user_to_check);
    });
}

export async function move_contact_t(
    t: ISchemaTask<unknown>,
    pid: string,
    cid: string,
    direction: "up" | "down",
    meta: IMeta,
): Promise<{ change_id?: number }> {
    const ph = await get_latest_t(t, pid);

    const contact = ph.contacts.get(cid);

    if (!contact) {
        return {};
    }

    const ops = move(ph.children_of(contact.parent), cid, direction);
    let op = new Phonebook.Operation();
    for (const o of ops) {
        op = op.concat(
            Phonebook.Operation.change_val(
                ["contacts", o.id, "ord"],
                o.old_ord,
                o.new_ord,
            ),
        );
    }

    return update_phonebook_t(t, pid, op, meta);
}

export async function move_contact(
    db: IDatabase<unknown>,
    pid: string,
    cid: string,
    direction: "up" | "down",
    meta: IMeta,
): Promise<{ change_id?: number }> {
    return transactor(db, async t => {
        return move_contact_t(t, pid, cid, direction, meta);
    });
}

function string_compare(a: string, b: string): number {
    return a.localeCompare(b, "en-US", { usage: "sort", numeric: true });
}

export async function sort_directory_t(
    t: ISchemaTask<unknown>,
    pid: string,
    gid: string,
    meta: IMeta,
): Promise<{ change_id: number }> {
    assert_undo(t);
    const ph = await get_latest_t(t, pid);

    const list = ph.contacts
        .filter(x => x.parent === gid)
        .sortBy(x => x.name, string_compare);

    let op = new Phonebook.Operation();

    let index = Big(0);
    for (const elem of list) {
        op = op.concat(
            Phonebook.Operation.change_val(
                ["contacts", elem[0], "ord"],
                elem[1].ord,
                index,
            ),
        );

        index = index.plus(1);
    }

    return update_phonebook_t(t, pid, op, meta);
}

export async function sort_directory(
    db: IDatabase<unknown>,
    pid: string,
    gid: string,
    meta: IMeta,
): Promise<{ change_id: number }> {
    return transactor(db, async t => {
        return sort_directory_t(t, pid, gid, meta);
    });
}

export async function post_contact_t(
    t: ISchemaTask<unknown>,
    pid: string,
    obj: IPostContact,
    meta: IMeta,
): Promise<{ id: string; change_id?: number }> {
    assert_undo(t);

    const ph = await get_latest_t(t, pid);

    const id = uuid.v4();

    const biggest_contact = ph.contacts
        .filter(x => x.parent === obj.parent)
        .maxBy(
            v => v.ord,
            (x, y) => x.cmp(y),
        );

    const big = biggest_contact ? biggest_contact.ord.plus(1) : Big(0);
    const op = ph.contact_add(
        new Phonebook.Contact({
            ...obj,
            ord: big,
            contact: "404",
            xfer_ng911: true,
            xfer_blind: true,
            xfer_attended: true,
            xfer_conference: true,
            xfer_outbound: true,
            icon: "person.ico",
        }),
        { id },
    );

    const c = await update_phonebook_t(t, pid, op, meta);

    return { id, change_id: c.change_id };
}

export async function post_contact(
    db: IDatabase<unknown>,
    pid: string,
    obj: IPostContact,
    meta: IMeta,
): Promise<{ id: string; change_id?: number }> {
    return transactor(db, async t => {
        return post_contact_t(t, pid, obj, meta);
    });
}

export async function delete_contact_t(
    t: ISchemaTask<unknown>,
    pid: string,
    cid: string,
    meta: IMeta,
    user_to_check: IPDPUser,
): Promise<{ change_id: number }> {
    assert_undo(t);

    const ph = await get_latest_t(t, pid);

    const contact = ph.contacts.get(cid)!;

    pdp_assert({
        user: user_to_check,
        target: { type: "CONTACT", is_sip: is_sip_uri(contact.contact) },
        action: { type: "EDIT", attr_name: ANY },
    });

    const op = ph.contact_del(cid);

    return update_phonebook_t(t, pid, op, meta);
}

export async function delete_contact(
    db: IDatabase<unknown>,
    pid: string,
    cid: string,
    meta: IMeta,
    user_to_check: IPDPUser,
): Promise<{ change_id: number }> {
    return transactor(db, async t => {
        return delete_contact_t(t, pid, cid, meta, user_to_check);
    });
}

export async function bulk_import_t(
    t: ISchemaTask<unknown>,
    pid: string,
    bulk: IUpsertContact[],
    meta: IMeta,
    user_to_check: IPDPUser,
): Promise<{ change_id: number }> {
    let ph = await get_latest_t(t, pid);

    let op = new Phonebook.Operation();

    for (const elem of bulk) {
        let group = ph.dirs.findEntry(x => x.name === elem.gname);

        if (!group) {
            const ord = ph.dirs
                .filter(x => x.parent === undefined)
                .maxBy(
                    x => x.ord,
                    (a, b) => a.cmp(b),
                ) ?? { ord: Big(-1) };
            const o = ph.dir_add(
                new Phonebook.Directory({
                    name: elem.gname,
                    ord: ord.ord.plus(1),
                }),
            );
            op = op.concat(o);
            ph = ph.apply(o);
            group = ph.dirs.findEntry(x => x.name === elem.gname)!;
        }

        pdp_assert({
            user: user_to_check,
            target: { type: "CONTACT", is_sip: is_sip_uri(elem.contact) },
            action: { type: "EDIT", attr_name: ANY },
        });

        if (elem.id) {
            const old = ph.contacts.get(elem.id)!;
            pdp_assert({
                user: user_to_check,
                target: { type: "CONTACT", is_sip: is_sip_uri(old.contact) },
                action: { type: "EDIT", attr_name: ANY },
            });
            op = op.concat(ph.contact_edit(elem.id, "name", elem.name));
            op = op.concat(ph.contact_edit(elem.id, "contact", elem.contact));
            op = op.concat(
                ph.contact_edit(elem.id, "xfer_ng911", elem.xfer_ng911),
            );
            op = op.concat(
                ph.contact_edit(elem.id, "xfer_blind", elem.xfer_blind),
            );
            op = op.concat(
                ph.contact_edit(elem.id, "xfer_attended", elem.xfer_attended),
            );
            op = op.concat(
                ph.contact_edit(
                    elem.id,
                    "xfer_conference",
                    elem.xfer_conference,
                ),
            );
            op = op.concat(
                ph.contact_edit(elem.id, "xfer_outbound", elem.xfer_outbound),
            );
        } else {
            const ord = ph.contacts
                .filter(x => x.parent === group![0])
                .maxBy(
                    x => x.ord,
                    (a, b) => a.cmp(b),
                ) ?? { ord: Big(-1) };
            const o = ph.contact_add(
                new Phonebook.Contact({
                    name: elem.name,
                    contact: elem.contact,
                    xfer_ng911: elem.xfer_ng911,
                    xfer_blind: elem.xfer_blind,
                    xfer_attended: elem.xfer_attended,
                    xfer_conference: elem.xfer_conference,
                    xfer_outbound: elem.xfer_outbound,
                    ord: ord.ord.plus(1),
                    parent: group[0],
                }),
            );
            op = op.concat(o);
            ph = ph.apply(o);
        }
    }

    return update_phonebook_t(t, pid, op, { ...meta, is_import: true });
}

export async function bulk_import(
    db: IDatabase<unknown>,
    pid: string,
    bulk: IUpsertContact[],
    meta: IMeta,
    user_to_check: IPDPUser,
): Promise<{ change_id: number }> {
    return transactor(db, async t => {
        return bulk_import_t(t, pid, bulk, meta, user_to_check);
    });
}

export async function replace_t(
    t: ISchemaTask<unknown>,
    pid: string,
    new_phonebook: Phonebook.Phonebook,
    meta: IMeta,
): Promise<void> {
    if (undo_migs.has(t.ctx.context.schema_id)) {
        const current = await get_latest_t(t, pid);

        const to_go = new_phonebook
            // we don't want to update the file on disk
            .set("current_change_id", current.current_change_id)
            // we don't care about the change_id
            .set("change_id", current.change_id)
            // id must not change
            .set("id", current.id)
            // filename stays the same
            .set("filename", current.filename);

        const op = new Phonebook.Operation({
            a: current,
            b: to_go,
        });

        await update_phonebook_t(t, pid, op, meta);
    } else if (old_migs.has(t.ctx.context.schema_id)) {
        await t.none(
            `DELETE FROM phonebook_sd AS n USING phonebook_sd_groups AS g
		WHERE n.sdgid = g.sdgid AND g.pid = $(pid)`,
            { pid },
        );
        await t.none(`DELETE FROM phonebook_sd_groups WHERE pid = $(pid)`, {
            pid,
        });
        await t.none(
            `DELETE FROM phonebook_numbers AS n USING phonebook_groups AS g
            WHERE n.gid = g.gid AND g.pid = $(pid)`,
            { pid },
        );
        await t.none("DELETE FROM phonebook_groups WHERE pid = $(pid)", {
            pid,
        });

        const dirs = new_phonebook.children_of_only_dirs(undefined);

        for (let i = 0; i < dirs.size; i++) {
            const dir = dirs.get(i)!;
            await t.none(
                `INSERT INTO phonebook_groups(gid, pid, gname, icon, ord)
                VALUES($(id), $(pid), $(name), $(icon), $(ord))`,
                {
                    icon: null,
                    ...(dir[1].toJS() as any),
                    pid,
                    ord: i + 1,
                    id: dir[0],
                },
            );

            const contacts = new_phonebook.children_of_only_contacts(dir[0]);
            for (let j = 0; j < contacts.size; j++) {
                const num = contacts.get(j)!;
                await t.none(
                    `INSERT INTO phonebook_numbers(
                        nid, gid, nname, number, ng911, blind, attended, conference, outbound, icon, ord
                    ) VALUES(
                        $(id), $(gid), $(name), $(number), $(ng911), $(blind),
                        $(attended), $(conference), $(outbound), $(icon), $(ord)
                    )`,
                    {
                        icon: null,
                        ...(num[1].toJS() as any),
                        number: num[1].contact,
                        ng911: num[1].xfer_ng911,
                        blind: num[1].xfer_blind,
                        attended: num[1].xfer_attended,
                        conference: num[1].xfer_conference,
                        outbound: num[1].xfer_outbound,
                        ord: j + 1,
                        gid: dir[0],
                        id: num[0],
                    },
                );
            }
        }

        const sd_dirs = new_phonebook.sd_children_of_only_dirs(undefined);
        for (let i = 0; i < sd_dirs.size; i++) {
            const dir = sd_dirs.get(i)!;
            await t.none(
                `INSERT INTO phonebook_sd_groups(sdgid, pid, gname, ord)
            VALUES($(sdgid), $(pid), $(gname), $(ord))`,
                {
                    sdgid: dir[0],
                    pid,
                    gname: dir[1].name,
                    ord: i + 1,
                },
            );
            const sds = new_phonebook.sd_children_of_only_sds(dir[0]);
            for (let j = 0; j < sds.size; j++) {
                const sd = sds.get(j)!;
                await t.none(
                    `INSERT INTO phonebook_sd(sdid, sdgid, nid, action, icon, ord)
                VALUES($(sdid), $(sdgid), $(nid), $(action), $(icon), $(ord))`,
                    {
                        sdid: sd[0],
                        sdgid: dir[0],
                        nid: sd[1].contact_id,
                        action: sd[1].action,
                        icon: sd[1].icon,
                        ord: j + 1,
                    },
                );
            }
        }
    } else {
        throw new Error("inappriopriate database schema version");
    }
}

export async function replace(
    db: IDatabase<unknown>,
    pid: string,
    new_phonebook: Phonebook.Phonebook,
    meta: IMeta,
): Promise<void> {
    return transactor(db, async t => {
        return replace_t(t, pid, new_phonebook, meta);
    });
}

export async function post_speed_dial_t(
    t: ISchemaTask<unknown>,
    pid: string,
    obj: IPostSpeedDial,
    meta: IMeta,
): Promise<{ id: string; change_id: number }> {
    assert_undo(t);

    const ph = await get_latest_t(t, pid);

    const id = uuid.v4();

    const biggest_sd = ph.speed_dials
        .filter(x => x.parent === obj.parent)
        .maxBy(
            v => v.ord,
            (x, y) => x.cmp(y),
        );

    const big = biggest_sd ? biggest_sd.ord.plus(1) : Big(0);
    const op = ph.sd_add(
        new Phonebook.SpeedDial({
            ...obj,
            ord: big,
            action: "dial_out",
        }),
        { id },
    );

    const c = await update_phonebook_t(t, pid, op, meta);

    return { id, change_id: c.change_id };
}

export async function post_speed_dial(
    db: IDatabase<unknown>,
    pid: string,
    obj: IPostSpeedDial,
    meta: IMeta,
): Promise<{ id: string; change_id: number }> {
    return transactor(db, async t => {
        return post_speed_dial_t(t, pid, obj, meta);
    });
}

export async function delete_speed_dial_t(
    t: ISchemaTask<unknown>,
    pid: string,
    cid: string,
    meta: IMeta,
): Promise<{ change_id: number }> {
    assert_undo(t);

    const ph = await get_latest_t(t, pid);

    const op = ph.sd_del(cid);

    return update_phonebook_t(t, pid, op, meta);
}

export async function delete_speed_dial(
    db: IDatabase<unknown>,
    pid: string,
    cid: string,
    meta: IMeta,
): Promise<{ change_id: number }> {
    return transactor(db, async t => {
        return delete_speed_dial_t(t, pid, cid, meta);
    });
}

export async function patch_speed_dial_t(
    t: ISchemaTask<unknown>,
    pid: string,
    cid: string,
    patch: IPatchSpeedDial,
    meta: IMeta,
): Promise<{ change_id: number }> {
    assert_undo(t);

    const ph = await get_latest_t(t, pid);

    let op = new Phonebook.Operation();

    const old_sd = ph.speed_dials.get(cid);
    if (old_sd === undefined) {
        throw user_error("Speed Dial must exist to be edited");
    }

    if (patch.icon !== undefined) {
        op = op.concat(
            Phonebook.Operation.change_val(
                ["speed_dials", cid, "icon"],
                old_sd.icon,
                patch.icon === null ? undefined : patch.icon,
            ),
        );
    }

    if (patch.contact_id !== undefined) {
        op = op.concat(
            Phonebook.Operation.change_val(
                ["speed_dials", cid, "contact_id"],
                old_sd.contact_id,
                patch.contact_id,
            ),
        );
    }

    if (patch.action !== undefined) {
        op = op.concat(
            Phonebook.Operation.change_val(
                ["speed_dials", cid, "action"],
                old_sd.action,
                patch.action,
            ),
        );
    }

    if (patch.parent !== undefined) {
        const new_parent = patch.parent === null ? undefined : patch.parent;

        const max_ord = ph.speed_dials
            .filter(x => x.parent === new_parent)
            .maxBy(
                x => x.ord,
                (a, b) => a.cmp(b),
            );
        const new_ord = max_ord ? max_ord.ord.plus("1") : Big(0);

        op = op.concat(
            Phonebook.Operation.change_val(
                ["speed_dials", cid, "parent"],
                old_sd.parent,
                patch.parent === null ? undefined : patch.parent,
            ),
        );

        // in addition to changing the parent, we need to
        // change the ord as well
        op = op.concat(
            Phonebook.Operation.change_val(
                ["speed_dials", cid, "ord"],
                old_sd.ord,
                new_ord,
            ),
        );
    }

    return update_phonebook_t(t, pid, op, meta);
}

export async function patch_speed_dial(
    db: IDatabase<unknown>,
    pid: string,
    cid: string,
    patch: IPatchSpeedDial,
    meta: IMeta,
): Promise<{ change_id: number }> {
    return transactor(db, async t => {
        return patch_speed_dial_t(t, pid, cid, patch, meta);
    });
}

export async function move_sd_t(
    t: ISchemaTask<unknown>,
    pid: string,
    sdid: string,
    direction: "up" | "down",
    meta: IMeta,
): Promise<{ change_id?: number }> {
    const ph = await get_latest_t(t, pid);

    const speed_dial = ph.speed_dials.get(sdid);

    if (!speed_dial) {
        return {};
    }

    const ops = move(ph.sd_children_of(speed_dial.parent), sdid, direction);
    let op = new Phonebook.Operation();
    for (const o of ops) {
        op = op.concat(
            Phonebook.Operation.change_val(
                ["speed_dials", o.id, "ord"],
                o.old_ord,
                o.new_ord,
            ),
        );
    }

    return update_phonebook_t(t, pid, op, meta);
}

export async function move_sd(
    db: IDatabase<unknown>,
    pid: string,
    sdid: string,
    direction: "up" | "down",
    meta: IMeta,
): Promise<{ change_id?: number }> {
    return transactor(db, async t => {
        return move_sd_t(t, pid, sdid, direction, meta);
    });
}
