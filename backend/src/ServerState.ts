import canonify from "canonical-json";
import crypto from "crypto";
import { Middleware, ParameterizedContext } from "koa";
import LRU from "lru-cache";
import { createPatch } from "rfc6902";

import { Listener } from "./db/listener";

function diff_from_present(past: [number, number]): number {
    const diff = process.hrtime(past);

    return diff[0] + 1e-9 * diff[1];
}

export function server_state_middleware<S, T>(
    get_state: (ctx: ParameterizedContext<S, T>) => Promise<unknown>,
    listen_channels_func?: (
        ctx: ParameterizedContext<S, T>,
    ) => Promise<{ listener: Listener; channels: string[] }>,
    short_poll?: number,
): Middleware<S, T> {
    const lru_cache = new LRU<string, string>({
        max: 500,
        maxAge: 1000 * 60 * 60,
    });

    function set(text: string) {
        const hasher = crypto.createHash("sha256");
        hasher.update(text);
        lru_cache.set(hasher.digest("hex"), text);
    }

    function get(hash: string): unknown {
        const ret = lru_cache.get(hash);
        if (typeof ret === "string") {
            return JSON.parse(ret);
        } else {
            return undefined;
        }
    }

    return async function (ctx: ParameterizedContext<S, T>) {
        ctx.set({
            "Cache-Control": "private, no-cache, no-store, max-age=0",
        });

        let ret = canonify(await get_state(ctx));

        const h = ctx.URL.searchParams.get("h") ?? "";

        if (typeof short_poll === "number") {
            ctx.set("X-Short-Poll", String(short_poll));
        }

        if (
            ctx.accepts("application/json-patch+json") ===
                "application/json-patch+json" &&
            ctx.accepts("application/json") === "application/json" &&
            h !== ""
        ) {
            if (typeof short_poll !== "number") {
                const begin = process.hrtime();
                while (true) {
                    const hasher = crypto.createHash("sha256");
                    hasher.update(ret);
                    if (hasher.digest("hex") !== h) {
                        break;
                    }
                    if (listen_channels_func) {
                        const listen = await listen_channels_func(ctx);
                        let resolve: () => void = () => undefined;

                        try {
                            const promise = new Promise(res => {
                                resolve = res;
                            });

                            for (const e of listen.channels) {
                                await listen.listener.listen(e, resolve);
                            }

                            await Promise.race([
                                promise,
                                new Promise(resolve =>
                                    setTimeout(resolve, 30000),
                                ),
                            ]);
                        } finally {
                            for (const e of listen.channels) {
                                await listen.listener.unlisten(e, resolve);
                            }
                        }
                    } else {
                        await new Promise(resolve => setTimeout(resolve, 100));
                    }
                    ret = canonify(await get_state(ctx));
                    if (diff_from_present(begin) > 30) {
                        break;
                    }
                }
            }

            const before = get(h);
            set(ret);
            if (before !== undefined) {
                const patch = createPatch(before, JSON.parse(ret));
                ctx.type = "application/json-patch+json";
                ctx.body = JSON.stringify(patch);
            } else {
                ctx.type = "application/json";
                ctx.body = ret;
            }
        } else if (ctx.accepts("application/json") === "application/json") {
            set(ret);
            ctx.type = "application/json";
            ctx.body = ret;
        } else {
            ctx.throw(406);
        }
    };
}
