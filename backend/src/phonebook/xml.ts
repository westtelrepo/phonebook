/* eslint-disable @typescript-eslint/no-non-null-assertion, @typescript-eslint/no-unsafe-call, @typescript-eslint/strict-boolean-expressions */
import * as Phonebook from "@w/shared/dist/phonebook";
import assert from "assert";
import Immutable from "immutable";
import libxmljs from "libxmljs";
import url from "url";
import uuid from "uuid-1345";

export function to_xml(
    phonebook: Phonebook.Phonebook,
    to_url: (x: string) => string,
): string {
    const doc = new libxmljs.Document();
    const root = doc.node("TransferList");

    // we only want to output the <ng911_transfer_type> element if it is used at least once
    // and then output it for every single contact
    const has_ng911_transfer_type = phonebook.contacts.some(
        x => x.ng911_transfer_type !== undefined,
    );

    phonebook.children_of_only_dirs(undefined).forEach(([gid, grp]) => {
        phonebook.children_of_only_contacts(gid).forEach(([nid, num]) => {
            root.node("Transfer_Name", num.name).attr({ id: nid });
            root.node("Transfer_Group", grp.name);
            root.node("Transfer_Number", num.contact);

            let code = 0;
            if (num.xfer_ng911) {
                code |= 16;
            }
            if (num.xfer_blind) {
                code |= 2;
            }
            if (num.xfer_attended) {
                code |= 4;
            }
            if (num.xfer_conference) {
                code |= 8;
            }
            if (num.xfer_outbound) {
                code |= 32;
            }
            root.node("Transfer_Code", String(code));
            root.node("Speed_Dial_Action", "none");
            if (num.icon) {
                root.node("icon_file", to_url(num.icon)).attr({
                    icon: num.icon || "",
                });
            } else {
                root.node("icon_file", "none");
            }

            if (has_ng911_transfer_type) {
                if (num.ng911_transfer_type) {
                    root.node("ng911_transfer_type", num.ng911_transfer_type);
                } else {
                    root.node("ng911_transfer_type", "legacy");
                }
            }
        });
    });

    phonebook.sd_children_of_only_dirs(undefined).forEach(([gid, grp]) => {
        phonebook.sd_children_of_only_sds(gid).forEach(([sid, sd]) => {
            const num = phonebook.contacts.get(sd.contact_id)!;
            root.node("Transfer_Name", num.name).attr({
                id: sid,
                nid: sd.contact_id,
            });

            if (sd.icon) {
                root.node("icon_file", to_url(sd.icon)).attr({
                    icon: sd.icon,
                });
            } else if (num.icon) {
                // yes the attribute "icon" is sd.icon on purpose
                root.node("icon_file", to_url(num.icon)).attr({
                    icon: sd.icon ?? "",
                });
            } else {
                root.node("icon_file", "none");
            }

            root.node("Transfer_Group", "SpeedDial " + grp.name);
            root.node("Transfer_Number", num.contact);
            root.node("Transfer_Code", "0");
            root.node("Speed_Dial_Action", sd.action);

            if (has_ng911_transfer_type) {
                if (num.ng911_transfer_type) {
                    root.node("ng911_transfer_type", num.ng911_transfer_type);
                } else {
                    root.node("ng911_transfer_type", "legacy");
                }
            }
        });
    });

    const order = root.node("Display_Order");
    let i = 0;
    phonebook.children_of_only_dirs(undefined).forEach(([, grp]) => {
        order.node("Transfer_Group", grp.name);
        order.node("List_Priority", String(++i));
        if (grp.icon) {
            order
                .node("icon_file", to_url(grp.icon))
                .attr({ icon: grp.icon || "" });
        } else {
            order.node("icon_file", "none");
        }
    });
    phonebook.sd_children_of_only_dirs(undefined).forEach(([, grp]) => {
        order.node("Transfer_Group", "SpeedDial " + grp.name);
        order.node("List_Priority", String(++i));
        order.node("icon_file", "none");
    });
    return doc.toString();
}

function parse_icon_url(u: string): string | null {
    if (u === "none") {
        return null;
    }
    const icon_url = url.parse(u, true);
    const ret = icon_url.query.icon!;
    if (typeof ret === "string") {
        return ret;
    } else {
        if (ret.length === 0) {
            return null;
        } else if (ret.length === 1) {
            return ret[0];
        } else {
            throw new Error("could not parse URL");
        }
    }
}

export function from_xml(xml_string: string): Phonebook.Phonebook {
    interface IGroup {
        name: string;
        priority?: number;
        icon?: string | null;
        id?: string;
    }

    interface ITransfer {
        name: string;
        id: string;
        nid?: string;
        group?: string;
        number?: string;
        code?: number;
        icon?: string | null;
        speed_dial_action?: string;
        ng911_transfer_type?: Phonebook.ng911_transfer_type;
    }

    const doc = libxmljs.parseXml(xml_string);
    assert.strictEqual(doc.root()!.name()!, "TransferList");
    const transfers: ITransfer[] = [];
    const groups: IGroup[] = [];
    const sdgroups: IGroup[] = [];
    const sds: ITransfer[] = [];

    function parseDisplayOrder(root: any) {
        let currentGroup: IGroup | undefined;

        const push_group = (x: IGroup) => {
            if (x.name.startsWith("SpeedDial ")) {
                x.name = x.name.replace(/^SpeedDial /, "");
                sdgroups.push(x);
            } else {
                groups.push(x);
            }
        };

        root.childNodes().forEach((node: any) => {
            if (node.type() === "element") {
                if (node.name() === "Transfer_Group") {
                    if (currentGroup) {
                        push_group(currentGroup);
                    }
                    currentGroup = { name: node.text().trim() };
                } else if (!currentGroup) {
                    throw new Error("unexpected XML");
                } else if (node.name() === "List_Priority") {
                    currentGroup.priority = Number(node.text());
                } else if (node.name() === "icon_file") {
                    currentGroup.icon = node.attr("icon")
                        ? node.attr("icon").value() || null
                        : parse_icon_url(node.text());
                }
            }
        });
        if (currentGroup) {
            push_group(currentGroup);
        }
    }

    let currentTransfer: ITransfer | undefined;

    const push = (x: ITransfer) => {
        if (!x.group) {
            const e: any = new Error(
                `entry "${x.name}" must be a member of a group.`,
            );
            e.status = 400;
            e.expose = true;
            throw e;
        }
        if (x.group.startsWith("SpeedDial ")) {
            x.group = x.group.replace(/^SpeedDial /, "");
            sds.push(x);
        } else {
            transfers.push(x);
        }
    };

    doc.childNodes().forEach(node => {
        if (node.type() === "element") {
            if (node.name() === "Transfer_Name") {
                if (currentTransfer) {
                    push(currentTransfer);
                }
                currentTransfer = {
                    name: node.text().trim(),
                    id: node.attr("id") ? node.attr("id")!.value() : uuid.v4(),
                    nid: node.attr("nid")
                        ? node.attr("nid")!.value()
                        : undefined,
                };
            } else if (!currentTransfer) {
                throw new Error("unexpected XML");
            } else if (node.name() === "Transfer_Group") {
                currentTransfer.group = node.text().trim();
            } else if (node.name() === "Transfer_Number") {
                currentTransfer.number = node.text();
            } else if (node.name() === "Transfer_Code") {
                currentTransfer.code = Number(node.text());
            } else if (node.name() === "icon_file") {
                currentTransfer.icon = node.attr("icon")
                    ? node.attr("icon")!.value() || null
                    : parse_icon_url(node.text());
            } else if (node.name() === "Speed_Dial_Action") {
                currentTransfer.speed_dial_action = node.text().trim();
            } else if (node.name() === "ng911_transfer_type") {
                const type: string = node.text().trim();
                if (type === "legacy") {
                    currentTransfer.ng911_transfer_type = undefined;
                } else if (
                    type === "internal" ||
                    type === "external" ||
                    type === "external.experient"
                ) {
                    currentTransfer.ng911_transfer_type = type;
                } else {
                    throw new Error("Unsupported ng911_transfer_type");
                }
            } else if (node.name() === "Display_Order") {
                parseDisplayOrder(node);
            }
        }
    });
    if (currentTransfer) {
        push(currentTransfer);
    }

    const priority_sort = (a: IGroup, b: IGroup) => {
        if (typeof a.priority !== "number" || typeof b.priority !== "number") {
            throw new Error("expected priority in XML");
        }
        if (a.priority < b.priority) {
            return -1;
        } else if (a.priority > b.priority) {
            return 1;
        } else {
            return 0;
        }
    };

    groups.sort(priority_sort);
    sdgroups.sort(priority_sort);

    /** name -> group */
    const group_map: Map<string, IGroup> = new Map();
    groups.forEach(group => {
        group.id = uuid.v4();
        group_map.set(group.name, group);
    });

    /** name -> group */
    const sdgroup_map: Map<string, IGroup> = new Map();
    sdgroups.forEach(group => {
        group.id = uuid.v4();
        sdgroup_map.set(group.name, group);
    });

    const ret = new Phonebook.Phonebook({
        id: uuid.v4(),
        dirs: Immutable.Map(
            groups.map((e, i) => {
                assert(typeof e.id === "string" && uuid.check(e.id));
                return [
                    e.id,
                    new Phonebook.Directory({
                        name: e.name,
                        icon: e.icon === null ? undefined : e.icon,
                        ord: i,
                    }),
                ];
            }),
        ),
        sd_dirs: Immutable.Map(
            sdgroups.map((e, i) => {
                assert(typeof e.id === "string" && uuid.check(e.id));
                return [
                    e.id,
                    new Phonebook.Directory({
                        name: e.name,
                        ord: i,
                    }),
                ];
            }),
        ),
        contacts: Immutable.Map(
            transfers.map((e, i) => {
                if (e.group === undefined) {
                    const error: any = new Error(
                        `Entry ${e.name} has no group membership.`,
                    );
                    error.expose = true;
                    error.status = 400;
                    throw error;
                }

                const group = group_map.get(e.group);
                if (!group) {
                    const error: any = new Error(
                        `Entry "${e.name}" is a member of nonexistent group "${e.group}".`,
                    );
                    error.expose = true;
                    error.status = 400;
                    throw error;
                }
                return [
                    e.id,
                    new Phonebook.Contact({
                        name: e.name,
                        contact: e.number!,
                        icon: e.icon === null ? undefined : e.icon,
                        xfer_ng911: Boolean(e.code! & 16),
                        xfer_blind: Boolean(e.code! & 2),
                        xfer_attended: Boolean(e.code! & 4),
                        xfer_conference: Boolean(e.code! & 8),
                        xfer_outbound: Boolean(e.code! & 32),
                        ord: i,
                        ng911_transfer_type: e.ng911_transfer_type,
                        parent: group.id,
                    }),
                ];
            }),
        ),
        speed_dials: Immutable.Map(
            sds.map((e, i) => {
                if (e.group === undefined) {
                    const error: any = new Error(
                        `Speed Dial Entry ${e.name} has no group membership.`,
                    );
                    error.expose = true;
                    error.status = 400;
                    throw error;
                }

                const group = sdgroup_map.get(e.group);
                if (!group) {
                    const error: any = new Error(
                        `Speed dial entry "${e.name}" is a member of nonexistent group "${e.group}".`,
                    );
                    error.expose = true;
                    error.status = 400;
                    throw error;
                }
                return [
                    e.id,
                    new Phonebook.SpeedDial({
                        icon: e.icon === null ? undefined : e.icon,
                        contact_id: e.nid
                            ? e.nid
                            : transfers.find(
                                  t =>
                                      t.name === e.name &&
                                      t.number === e.number,
                              )!.id,
                        action: e.speed_dial_action as Phonebook.sd_action,
                        parent: group.id,
                        ord: i,
                    }),
                ];
            }),
        ),
    });

    ret.validate();

    return ret;
}
