/* eslint-disable @typescript-eslint/strict-boolean-expressions */
import koa_router from "@koa/router";
import { IUser } from "@w/shared/dist/db_users";
import { strict as assert } from "assert";
import canonify from "canonical-json";
import crypto from "crypto";
import debug_orig from "debug";
import fs from "fs";
import fs_extra from "fs-extra";
import handlebars from "handlebars";
import jsonwebtoken from "jsonwebtoken";
import koa, { ParameterizedContext } from "koa";
import koa_body from "koa-body";
import koa_bodyparser from "koa-bodyparser";
import helmet from "koa-helmet";
import koa_mount from "koa-mount";
import koa_static from "koa-static";
import os from "os";
import path from "path";
import pg_promise, { IDatabase } from "pg-promise";
import uuid from "uuid-1345";
import zlib from "zlib";

import phonebook_router from "./api/phonebook";
import users_router from "./api/users";
import { dump, IDump, restore } from "./db/backup";
import { DBCommunicator } from "./db/communication";
import { Listener } from "./db/listener";
import { migration_list } from "./db/migrations";
import { get_latest } from "./db/phonebook";
import * as schema from "./db/schema";
import * as users_db from "./db/users";
import * as phdb from "./phonebook_db";
import { phone_config_to_xml } from "./polycom/phone_config_xml";
import { server_state_middleware } from "./ServerState";
import { version } from "./version";

const debug = debug_orig("westtel-phonebook:main");

/** the audience for this application. Overkill but potentially useful */
const JWT_AUDIENCE: string = "b6826f78-a848-416b-a761-9c0f5101020b";
/** the name of the cookie that contains the session JWT */
const JWT_NAME: string = "jwt_phonebook";

/**
 * the fields of the JWT cookie we directly use.
 * iat, exp, nbf, and aud are set by jwt_sign and jwt_verify.
 */
interface ICookie {
    /** UUID of user */
    sub: string;
    /** UUID of user's session (set at login) */
    userSession: string;
}

/** the JWT key, used for HS256 */
const jwt_key =
    process.env.NODE_ENV === "production"
        ? crypto.randomBytes(32)
        : // insecure key used for development only
          // <https://jwt.io/#debugger> is useful for debugging JWTs (and this key has been posted there a lot)
          Buffer.from("JDIkOhFEH2OiubnvJ59fpnwxE91VaKiPurL5m2G+XPg=", "base64");

/**
 * Signs and creates a JWT.
 * @param cookie the parameters to be signed
 */
function jwt_sign(cookie: ICookie): string {
    return jsonwebtoken.sign(cookie, jwt_key, {
        algorithm: "HS256",
        expiresIn: "20m", // don't change this... EventSourceServer expects 20 minutes
        audience: JWT_AUDIENCE,
        notBefore: "-1m",
    });
}

/**
 * Sets a cookie with our base settings.
 * @param ctx a koa context
 * @param name the cookies name
 * @param value the value of the cookie
 */
function set_cookie(
    ctx: ParameterizedContext<unknown>,
    name: string,
    value: string,
) {
    ctx.cookies.set(name, value, {
        httpOnly: true,
        sameSite: "lax",
    });
}

/**
 * Verifies a JWT token and returns ICookie if it passes (null if not)
 * @param jwt a token string to be verified
 */
function jwt_verify(jwt: string | undefined): ICookie | null {
    if (jwt === undefined) {
        return null;
    }
    try {
        const cookie = jsonwebtoken.verify(jwt, jwt_key, {
            audience: JWT_AUDIENCE,
            algorithms: ["HS256"],
        });

        if (typeof cookie !== "object") {
            throw new Error("unable to parse jwt");
        }

        const ret: any = cookie;
        assert.strictEqual(typeof ret.sub, "string");
        assert.strictEqual(typeof ret.userSession, "string");
        assert(uuid.check(ret.sub));
        assert(uuid.check(ret.userSession));
        return {
            sub: ret.sub,
            userSession: ret.userSession,
        };
    } catch (e) {
        if (e instanceof jsonwebtoken.JsonWebTokenError) {
            console.log(e);
            return null;
        } else {
            throw e;
        }
    }
}

const pgp = pg_promise();

const login_template = handlebars.compile(
    fs.readFileSync("./views/login.html", "utf8"),
);

// config handled through environment
const phonebook_db = pgp<unknown>({});

const db_communicator = new DBCommunicator({ db: phonebook_db });

db_communicator.register("has_dir", async args => {
    try {
        assert.equal(
            typeof args.dir,
            "string",
            "args.dir must be string for has_dir",
        );
        const stat = fs.statSync(args.dir);
        return stat.isDirectory();
    } catch (e) {
        if (e.code === "ENOENT") {
            return false;
        } else {
            throw e;
        }
    }
});

db_communicator.register("write_polycom_template", async args => {
    const phone_config: any = await phdb.get_polycom(phonebook_db, args.id);

    const files = phone_config_to_xml(phone_config);

    fs.writeFileSync(
        path.join(phone_config.tftp_directory, "phonebook.cfg"),
        files.cfg,
    );
    fs.writeFileSync(
        path.join(phone_config.tftp_directory, "000000000000-directory.xml"),
        files.dir,
    );
});

interface IKoaState {
    requestUUID?: string;
    logged_in?: boolean;
    user: IUser & { permissions: any };
    user_session: string;
    is_superadmin?: boolean;
    db: IDatabase<unknown>;
    listener: Listener;
    nonce?: string;
}

const app: koa<IKoaState> = new koa();
const router: koa_router<IKoaState> = new koa_router();

// doesn't use database connection or anything
const cheap_router: koa_router<unknown> = new koa_router();

cheap_router.get("/ping", async ctx => {
    ctx.body = "Hello, world!";
});

cheap_router.get("/version", async ctx => {
    ctx.body = "Phonebook " + version;
});

router.get("/icon", async ctx => {
    const icon_name = ctx.query.icon;
    assert(typeof icon_name === "string");
    const ph = await get_latest(phonebook_db, ctx.query.pid);

    if (ph.filename === undefined) {
        throw new Error("no filename set for phonebook");
    }
    const dir = path.dirname(ph.filename);

    // we assert that this is a very simple name to disallow .. and such
    // FIXME: do this a proper way
    assert(
        /^([-_a-zA-Z0-9]+\/)*[-_a-zA-Z0-9]+\.(ico|png)$/.test(icon_name),
        "invalid icon name",
    );

    const icon = fs.readFileSync(path.join(dir, "icons", icon_name));
    const icon_hash = crypto.createHash("sha256").update(icon).digest("base64");

    if (icon_name.endsWith(".ico")) {
        ctx.type = "image/vnd.microsoft.icon";
    } else if (icon_name.endsWith(".png")) {
        ctx.type = "image/png";
    } else {
        ctx.assert("unsupported image type");
    }

    ctx.set(
        "Cache-Control",
        "private, max-age=1200, stale-while-revalidate=3600",
    );
    ctx.status = 200;
    ctx.etag = icon_hash;

    if (ctx.fresh) {
        ctx.status = 304;
        ctx.body = null;
    } else {
        ctx.body = icon;
    }
});

router.get("/login", async ctx => {
    const current = await phonebook_db.task(async t =>
        schema.get_schema_version_t(t),
    );
    const desired = phdb.schema_version;

    let admin_message: string | undefined;
    if (current === "00000000-0000-0000-0000-000000000000") {
        admin_message =
            'The database is currently uninitialized, please login as westtel and then go to the "DB Management" page to update.';
    } else if (current !== desired) {
        admin_message =
            "The database schema version is not up to date and needs to be updated by an administrator.";
    }
    ctx.body = login_template({ admin_message });
});

router.post("/login", koa_body(), async ctx => {
    // do other things
    const body = ctx.request.body;

    const user_info = await users_db.user_authenticate(
        phonebook_db,
        { username: body.username },
        body.password,
    );
    if (user_info) {
        set_cookie(
            ctx,
            JWT_NAME,
            jwt_sign({
                sub: user_info.uid,
                userSession: uuid.v4(),
            }),
        );
        ctx.redirect("/");
    } else {
        ctx.body = login_template({
            error: "Username or password is incorrect",
        });
    }
});

router.post("/api", koa_body({ formLimit: "10mb" }), async ctx => {
    assert(ctx.accepts("json"));

    if (!ctx.state.logged_in) {
        ctx.body = { error: "Logged out", code: "UNAUTHORIZED" };
        return;
    }

    const command = JSON.parse(ctx.request.body.command);
    const ret: any = {};
    ret.ignore = uuid.v4();

    try {
        switch (command.code) {
            case "polycom_commit": {
                throw new Error("TODO");
                /*await pdp.assert({
                user: ctx.state.user,
                action: {type: "UPDATE"},
                target: {type: "polycom_template_config"},
            });

            const phone_config = await phdb.get_polycom(phonebook_db, command.id);

            if (!phone_config.tftp_directory) {
                const e: any = new Error("Polycom template must have directory to be updated");
                e.expose = true;
                throw e;
            }

            let peer_with_dir: any = null;
            for (const peer of db_communicator.peers()) {
                if (await db_communicator.call(peer.listen_id, "has_dir", {dir: phone_config.tftp_directory})) {
                    if (peer_with_dir) {
                        const e: any = new Error("Cannot write template when two machines have the same directory");
                        e.expose = true;
                        throw e;
                    } else {
                        peer_with_dir = peer;
                    }
                }
            }

            if (peer_with_dir) {
                await db_communicator.call(peer_with_dir.listen_id, "write_polycom_template", {id: command.id});
            } else {
                const e: any = new Error("Could not find machine to write template to");
                e.expose = true;
                throw e;
            }

            break;*/
            }
            case "upsert_linekey": {
                // TODO
                throw new Error("TODO");
                /*await phdb.upsert_linekey(
                    phonebook_db,
                    command.location,
                    command.linekey,
                    command.id,
                    { user_to_check: ctx.state.user },
                );
                break;*/
            }
            case "get_polycom": {
                ret.polycom = await phdb.get_polycom(phonebook_db, command.id);
                break;
            }
            case "update_polycom": {
                throw new Error("TODO");
                /*await phdb.update_polycom(
                    phonebook_db,
                    command.id,
                    command.changes,
                    { user_to_check: ctx.state.user },
                );
                break;*/
            }
            case "get_polycom_list": {
                ret.polycom_list = await phdb.get_polycom_list(phonebook_db);
                break;
            }
            case "add_polycom": {
                throw new Error("TODO");
                /*ret.id = await phdb.add_polycom(phonebook_db, {
                    user_to_check: ctx.state.user,
                });
                break;*/
            }
            case "del_polycom": {
                throw new Error("TODO");
                /*await phdb.del_polycom(phonebook_db, command.id, {
                    user_to_check: ctx.state.user,
                });
                break;*/
            }
            case "pdp_polycom_location": {
                throw new Error("TODO");
                /*const polycom = await phdb.get_polycom(
                    phonebook_db,
                    command.id,
                );
                const line_key = polycom.getLineKeySideCar(command.location);
                const f = async (attr, val) => {
                    throw new Error("TODO");
                }; /*await pdp.ask({
                user: process.env.PHONEBOOK_WHOAMI_SUPERADMIN ? {is_superadmin: true} : ctx.state.user,
                target: {
                    obj: line_key || {category: "Unassigned"},
                    type: "polycom_line_key",
                    location: command.location,
                },
                action: {
                    type: "EDIT",
                    attr_name: attr,
                    attr_val: val,
                },
            });*/
                /*ret.allowed = {
                    category: {
                        BLF: await f("category", "BLF"),
                        SpeedDial: await f("category", "SpeedDial"),
                        Unassigned: await f("category", "Unassigned"),
                        Line: await f("category", "Line"),
                    },
                    lb: await f("lb", ANY),
                    ct: await f("ct", ANY),
                    explicit_index: await f("explicit_index", ANY),
                };
                break;*/
            }
            case "pdp_polycom_list": {
                throw new Error("TODO");
                /*ret.allowed = {
                    create: false /*await pdp.ask({
                    user: process.env.PHONEBOOK_WHOAMI_SUPERADMIN ? {is_superadmin: true} : ctx.state.user,
                    target: {
                        type: "polycom_template_config",
                    },
                    action: {type: "CREATE"},
                }),,
                //};
                //break;*/
            }
            case "pdp_polycom_template": {
                throw new Error("TODO");
                /*const template = await phdb.get_polycom(phonebook_db, command.id);

            ret.allowed = {
                delete: await pdp.ask({
                    user: process.env.PHONEBOOK_WHOAMI_SUPERADMIN ? {is_superadmin: true} : ctx.state.user,
                    target: { type: "polycom_template_config" },
                    action: { type: "DELETE" },
                }),
                update: await pdp.ask({
                    user: process.env.PHONEBOOK_WHOAMI_SUPERADMIN ? {is_superadmin: true} : ctx.state.user,
                    action: {type: "UPDATE"},
                    target: {type: "polycom_template_config"},
                }),
                tftp_directory: await pdp.ask({
                    user: process.env.PHONEBOOK_WHOAMI_SUPERADMIN ? {is_superadmin: true} : ctx.state.user,
                    action: {
                        type: "EDIT",
                        attr_name: "tftp_directory",
                        attr_val: pdp.ANY,
                    },
                    target: {
                        obj: template,
                        type: "polycom_template_config",
                    }
                }),
                name: await pdp.ask({
                    user: process.env.PHONEBOOK_WHOAMI_SUPERADMIN ? {is_superadmin: true} : ctx.state.user,
                    action: {
                        type: "EDIT",
                        attr_name: "name",
                        attr_val: pdp.ANY,
                    },
                    target: {
                        obj: template,
                        type: "polycom_template_config",
                    }
                }),
            };
            break;*/
            }
            case "update_schema": {
                // only allow SUPERADMIN
                if (!ctx.state.is_superadmin) {
                    const e: any = new Error(`Unauthorized`);
                    e.expose = true;
                    e.status = 403;
                    throw e;
                }

                if (
                    !(await users_db.user_authenticate(
                        phonebook_db,
                        { uid: ctx.state.user.uid },
                        command.password,
                    ))
                ) {
                    const e: any = new Error("invalid password");
                    e.expose = true;
                    throw e;
                }

                for (const peer of db_communicator.peers()) {
                    if (peer.version !== version) {
                        const e: any = new Error(
                            "Can not upgrade database, there is an instance with a difference version",
                        );
                        e.expose = true;
                        throw e;
                    }
                }

                await phdb.migrate(phonebook_db, undefined, true);

                break;
            }
            default:
                console.log("unknown command", command.code);
                break;
        }
    } catch (e) {
        if (e.expose || ctx.state.is_superadmin) {
            e.expose = true;
            ctx.app.emit("error", e, ctx);
            ret.error = e.message;
        } else {
            throw e;
        }
    }

    ctx.body = ret;
});

async function get_status() {
    let db: any;
    try {
        const current_schema = await phonebook_db.task(async t =>
            schema.get_schema_version_t(t),
        );
        const desired_schema = phdb.schema_version;
        const migs_b = schema.migrate(
            [...migration_list, schema.InitialMigration],
            current_schema,
            desired_schema,
        );
        const migs =
            migs_b === false
                ? false
                : migs_b.map(e => ({
                      name: e.name,
                      description: e.description,
                      state_uuid: e.state_uuid,
                  }));
        db = {
            current_schema: await phonebook_db.task(async t =>
                schema.get_schema_version_t(t),
            ),
            desired_schema: phdb.schema_version,
            migs,
        };
    } catch (e) {
        db = { error: e.message || "Unknown database error" };
    }

    let datadir_stat: fs.Stats | null;
    try {
        datadir_stat = fs.statSync(process.env.DATADIR ?? "/var/opt/phonebook");
    } catch (e) {
        if (e.code !== "ENOENT") {
            throw e;
        }
        datadir_stat = null;
    }

    return {
        arch: os.arch(),
        db,
        hostname: os.hostname(),
        listen_id: db_communicator.listen_id,
        peers: Array.from(db_communicator.peers()),
        user: {
            uid: process.getuid(),
            gid: process.getgid(),
        },
        datadir_stat,
        datadir: process.env.DATADIR ?? "/var/opt/phonebook",
        version,
    };
}

router.get(
    "/api/db_status",
    async (ctx, next) => {
        // only allow SUPERADMIN
        if (!ctx.state.is_superadmin) {
            const e: any = new Error(`Unauthorized`);
            e.expose = true;
            e.status = 403;
            throw e;
        }

        await next();
    },
    server_state_middleware(get_status, undefined, 1000),
);

router.get(
    "/api/whoami",
    async (ctx, next) => {
        if (!ctx.state.logged_in) {
            ctx.throw(403, "Unauthorized");
        }
        await next();
    },
    server_state_middleware<IKoaState, unknown>(
        async ctx => {
            const user = await users_db.get_user_info(phonebook_db, {
                uid: ctx.state.user.uid,
            });
            if (process.env.PHONEBOOK_WHOAMI_SUPERADMIN) {
                return { ...user, is_superadmin: true };
            } else {
                return user;
            }
        },
        undefined,
        30000,
    ),
);

router.get("/api/dump", async ctx => {
    if (!ctx.state.is_superadmin) {
        ctx.throw(403, "must be superadmin to dump database");
    }

    ctx.body = await dump(ctx.state.db);
});

router.post(
    "/api/restore",
    koa_bodyparser({ enableTypes: ["json"], jsonLimit: "256mb" }),
    async ctx => {
        ctx.assert(
            ctx.state.is_superadmin,
            403,
            "Must be superadmin to restore database",
        );

        ctx.assert(ctx.is("json"), 415);
        ctx.assert(ctx.accepts("json"), 406);
        const dmp: IDump = ctx.request.body;

        const file = await restore(ctx.state.db, dmp, true);

        const compress = await new Promise<Buffer>((resolve, reject) => {
            zlib.gzip(canonify(file), (e, buf) =>
                e ? reject(e) : resolve(buf),
            );
        });

        const BACKUPDIR = path.join(
            process.env.DATADIR ?? "/var/opt/phonebook",
            "backups",
        );

        await fs_extra.mkdirp(BACKUPDIR);

        await fs_extra.writeFile(
            path.join(
                BACKUPDIR,
                "pre-restore." + new Date().toISOString() + ".json.gz",
            ),
            compress,
            {
                mode: 0o440, // only allow user and group to read
                flag: "w",
            },
        );
        ctx.body = null;
    },
);

router.get("/api/ng911cfg", async ctx => {
    ctx.assert(
        ctx.state.is_superadmin,
        403,
        "Must be superadmin to read ng911.cfg",
    );

    const file = fs.readFileSync("/datadisk1/ng911/ng911.cfg");
    const file_hash = crypto.createHash("sha256").update(file).digest("base64");

    ctx.type = "text";
    ctx.status = 200;
    ctx.etag = file_hash;

    if (ctx.fresh) {
        ctx.status = 304;
        ctx.body = null;
    } else {
        ctx.body = file;
    }
});

router.post(
    "/api/ng911cfg",
    koa_bodyparser({ enableTypes: ["text"] }),
    async ctx => {
        ctx.assert(
            ctx.state.is_superadmin,
            403,
            "Must be superadmin to update ng911",
        );
        ctx.assert(ctx.is("text"), 415);
        ctx.assert(typeof ctx.request.body === "string");

        fs.writeFileSync("/datadisk1/ng911/ng911.cfg", ctx.request.body);

        ctx.body = null;
    },
);

router.get("/", async ctx => {
    if (ctx.state.logged_in) {
        const nonce = ctx.state.nonce;
        if (typeof nonce !== "string") {
            ctx.throw("Server Error: nonce not set");
        } else {
            ctx.set("Content-Type", "text/html; charset=utf8");

            ctx.body = `<!DOCTYPE html>
<html>
    <head>
        <title>Contact Manager</title>
    </head>
    <body>
        <div id="root"></div>
        <script src="/lib/tree.js" nonce="${nonce}"></script>
    </body>
</html>`;
        }
    } else {
        ctx.redirect("/login");
    }
});

router.post("/api/logout", async ctx => {
    ctx.cookies.set(JWT_NAME, undefined);
    ctx.set("Content-Type", "application/json");
    ctx.body = "null";
});

router.use(
    "/api/phonebook",
    phonebook_router.routes(),
    phonebook_router.allowedMethods(),
);
router.use("/api/user", users_router.routes(), users_router.allowedMethods());

// add response time and request id headers, keep track of them
app.use(async (ctx, next) => {
    // so that other files can access the db
    ctx.state.db = phonebook_db;
    ctx.state.listener = db_communicator.listener;

    const response_start_time = process.hrtime();
    ctx.state.requestUUID = uuid.v4();
    debug("start request %s", ctx.state.requestUUID);

    // by default cache busting
    ctx.set("Cache-Control", "private, no-cache, max-age=0");
    await next();

    // remove header that koa-static sets
    ctx.remove("Last-Modified");

    const diff = process.hrtime(response_start_time);
    ctx.set("X-Response-Time", 1e3 * diff[0] + 1e-6 * diff[1] + " ms");
    ctx.set("X-Request-ID", ctx.state.requestUUID);
    debug("end request %s", ctx.state.requestUUID);
});

app.use(
    async (
        ctx: koa.ParameterizedContext<{ nonce: string }>,
        next: koa.Next,
    ) => {
        ctx.state.nonce = crypto.randomBytes(16).toString("base64");
        return helmet({
            contentSecurityPolicy: {
                directives: {
                    baseUri: ["'none'"],
                    blockAllMixedContent: true,
                    connectSrc: ["'self'"],
                    defaultSrc: ["'none'"],
                    fontSrc: ["'none'"],
                    formAction: ["'self'"],
                    frameAncestors: ["'none'"],
                    frameSrc: ["'none'"],
                    imgSrc: ["'self'"],
                    manifestSrc: ["'none'"],
                    mediaSrc: ["'none'"],
                    objectSrc: ["'none'"],
                    pluginTypes: ["'none'"],
                    prefetchSrc: ["'none'"],
                    scriptSrc: [
                        "'self'",
                        "'strict-dynamic'",
                        `'nonce-${ctx.state.nonce}'`,
                        '"unsafe-inline"',
                        "'unsafe-eval'",
                    ],
                    styleSrc: ["'unsafe-inline'"],
                    workerSrc: ["'none'"],
                    sandbox: [
                        "allow-modals",
                        "allow-scripts",
                        "allow-same-origin",
                        "allow-forms",
                        "allow-downloads",
                    ],
                },
            },
            dnsPrefetchControl: { allow: false },
            // expectCt: only if secure
            frameguard: { action: "deny" },
            hidePoweredBy: true,
            // hsts: only if secure
            ieNoOpen: true,
            noSniff: true,
            permittedCrossDomainPolicies: { permittedPolicies: "none" },
            referrerPolicy: { policy: "no-referrer" },
            xssFilter: true,
        })(ctx, next);
    },
);

app.use(cheap_router.routes()).use(cheap_router.allowedMethods());

// process the login cookie
app.use(async (ctx, next) => {
    const cookie = jwt_verify(ctx.cookies.get(JWT_NAME));

    if (cookie) {
        const user_info = await users_db.get_user_info(phonebook_db, {
            uid: cookie.sub,
        });
        if (user_info) {
            ctx.state.logged_in = true;
            if (user_info.is_superadmin) {
                ctx.state.is_superadmin = true;
            }

            ctx.state.user = user_info;
            Object.freeze(ctx.state.user);

            ctx.state.user_session = cookie.userSession;

            // refresh the JWT if a non-GET method is used
            if (ctx.method !== "GET") {
                set_cookie(
                    ctx,
                    JWT_NAME,
                    jwt_sign({
                        sub: cookie.sub,
                        userSession: cookie.userSession,
                    }),
                );
            }
        }
    }

    await next();
});

// if the user is a superadmin, expose all errors
app.use(async (ctx, next) => {
    try {
        await next();
    } catch (e) {
        if (ctx.state.is_superadmin) {
            e.expose = true;
        }
        throw e;
    }
});

app.use(koa_mount("/static", koa_static("static")));
app.use(koa_mount("/lib", koa_static("frontend/lib")));
app.use(router.routes()).use(router.allowedMethods());

if (process.env.PHONEBOOK_AUTO_MIGRATE) {
    phdb.migrate(
        phonebook_db,
        uuid.check(process.env.PHONEBOOK_AUTO_MIGRATE)
            ? process.env.PHONEBOOK_AUTO_MIGRATE
            : undefined,
    ).catch(e => console.error("ERROR IN PHONEBOOK_AUTO_MIGRATE", e));
}

app.on("error", err => {
    console.log("Main Koa Error: ", err);
});

app.listen(process.env.PORT ?? 3001);

/** dump the database every hour after startup */
async function dump_database() {
    const BACKUPDIR = path.join(
        process.env.DATADIR ?? "/var/opt/phonebook",
        "backups",
    );
    while (true) {
        try {
            const data = await dump(phonebook_db);

            await fs_extra.mkdirp(BACKUPDIR);

            // we don't compress here so that the backup server can meaningfully diff

            // write and then rename to update atomically
            await fs_extra.writeFile(
                path.join(BACKUPDIR, ".latest.json~"),
                canonify(data),
                {
                    mode: 0o440, // only allow user and group to read
                    flag: "w",
                },
            );
            await fs_extra.rename(
                path.join(BACKUPDIR, ".latest.json~"),
                path.join(BACKUPDIR, "latest.json"),
            );
        } catch (e) {
            console.log("Error occured while dumping database backup: ", e);
        }

        await new Promise(resolve =>
            setTimeout(resolve, 60 * 60 * 1000).unref(),
        );
    }
}

dump_database().catch(e => console.error("FATAL ERROR IN dump_database", e));
