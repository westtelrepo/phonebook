// canonical-json doesn't define types, and there is no @types/canonical-json, so we have to define it ourselves
// thankfully, the type is very simple

declare module "canonical-json" {
    export default function (obj: unknown): string;
}
