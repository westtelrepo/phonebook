/* eslint-disable @typescript-eslint/promise-function-async, @typescript-eslint/no-unsafe-call */
// contains functions that represent the phonebook inside the database.
// Before reading, you'll want to read the PostgreSQL 9.4/9.5 docs on
// serializable transactions.

// In short: "With true serializable transactions, if you can show that your
// transaction will do the right thing if there are no concurrent transactions,
// it will do the right thing in any mix of serializable transactions
// or be rolled back with an error."
// https://wiki.postgresql.org/wiki/SSI

import { PolycomTemplate } from "@w/shared/dist/phone_config";
import * as validation from "@w/shared/dist/validation";
import { strict as assert } from "assert";
import debug_orig from "debug";
import fs from "fs";
import path from "path";
import { IDatabase, ITask } from "pg-promise";
import uuid from "uuid-1345";

import * as migrations from "./db/migrations";
import { ISchemaTask, migrate_db, transactor } from "./db/schema";
import * as phonebook_xml from "./phonebook_xml";

const debug = debug_orig("westtel-phonebook:db");

export const schema_version = migrations.mig.undo.state_uuid;

function string_compare(a: string, b: string): number {
    return a.localeCompare(b, "en-US", { usage: "sort", numeric: true });
}

const getPidOfGid = async (
    t: ISchemaTask<unknown>,
    gid: string,
): Promise<string | null> => {
    await assert_phonebook_schema_t(t);
    const ret = await t.oneOrNone(
        "SELECT pid FROM phonebook_groups WHERE gid = $(gid)",
        { gid },
    );
    return ret ? ret.pid : null;
};

const getPidOfSdgid = async (
    t: ISchemaTask<unknown>,
    sdgid: string,
): Promise<string | null> => {
    await assert_phonebook_schema_t(t);
    const ret = await t.oneOrNone(
        "SELECT pid FROM phonebook_sd_groups WHERE sdgid = $(sdgid)",
        { sdgid },
    );
    return ret ? ret.pid : null;
};

function user_error(msg: string) {
    const e: any = new Error(msg);
    e.expose = true;
    return e;
}

const phonebook_schemas: Set<string> = new Set();
phonebook_schemas.add(migrations.mig.phonebook.state_uuid);
phonebook_schemas.add(migrations.mig.users.state_uuid);

async function assert_phonebook_schema_t(t: ISchemaTask<unknown>) {
    if (!phonebook_schemas.has(t.ctx.context.schema_id)) {
        throw user_error(
            "Database has wrong schema, please have an administrator migrate the database",
        );
    }
}

const replace_with_phonebook_t = async (
    t: ISchemaTask<unknown>,
    pid: string,
    phonebook: any,
) => {
    await assert_phonebook_schema_t(t);
    await t.none(
        `DELETE FROM phonebook_sd AS n USING phonebook_sd_groups AS g
        WHERE n.sdgid = g.sdgid AND g.pid = $(pid)`,
        { pid },
    );
    await t.none(`DELETE FROM phonebook_sd_groups WHERE pid = $(pid)`, { pid });
    await t.none(
        `DELETE FROM phonebook_numbers AS n USING phonebook_groups AS g
        WHERE n.gid = g.gid AND g.pid = $(pid)`,
        { pid },
    );
    await t.none("DELETE FROM phonebook_groups WHERE pid = $(pid)", { pid });
    for (let i = 0; i < phonebook.groups.length; i++) {
        const grp = phonebook.groups[i];
        await t.none(
            `INSERT INTO phonebook_groups(gid, pid, gname, icon, ord)
            VALUES($(id), $(pid), $(name), $(icon), $(ord))`,
            { icon: null, ...grp, pid, ord: i + 1 },
        );
        for (let j = 0; j < grp.numbers.length; j++) {
            const num = grp.numbers[j];
            await t.none(
                `INSERT INTO phonebook_numbers(nid, gid, nname, number, ng911, blind, attended, conference, outbound, icon, ord)
VALUES($(id), $(gid), $(name), $(number), $(ng911), $(blind), $(attended), $(conference), $(outbound), $(icon), $(ord))`,
                {
                    icon: null,
                    ...num,
                    ng911: num.transfer.ng911,
                    blind: num.transfer.blind,
                    attended: num.transfer.attended,
                    conference: num.transfer.conference,
                    outbound: num.transfer.outbound,
                    ord: j + 1,
                    gid: grp.id,
                },
            );
        }
    }

    for (let i = 0; i < phonebook.sdgroups.length; i++) {
        const grp = phonebook.sdgroups[i];
        await t.none(
            `INSERT INTO phonebook_sd_groups(sdgid, pid, gname, ord)
        VALUES($(sdgid), $(pid), $(gname), $(ord))`,
            {
                sdgid: grp.id,
                pid,
                gname: grp.name,
                ord: i + 1,
            },
        );
        for (let j = 0; j < grp.sds.length; j++) {
            const sd = grp.sds[j];
            await t.none(
                `INSERT INTO phonebook_sd(sdid, sdgid, nid, action, icon, ord)
            VALUES($(sdid), $(sdgid), $(nid), $(action), $(icon), $(ord))`,
                {
                    sdid: sd.id,
                    sdgid: grp.id,
                    nid: sd.nid,
                    action: sd.action,
                    icon: sd.icon,
                    ord: j + 1,
                },
            );
        }
    }
};

const get_phonebook_t = async (
    t: ISchemaTask<unknown>,
    pid: string,
    options?: { icons?: boolean },
) => {
    await assert_phonebook_schema_t(t);
    options = options ?? {};
    const phonebook = await t.one(
        `SELECT filename
FROM phonebook WHERE pid = $1`,
        pid,
    );
    const groups = await t.any(
        "SELECT gid, gname, icon FROM phonebook_groups WHERE pid = $1 ORDER BY ord",
        pid,
    );
    const numbers = await t.any(
        `SELECT nid, gid, nname, number, n.icon, ng911, blind, attended, conference, outbound
FROM phonebook_groups AS g JOIN phonebook_numbers AS n USING (gid) WHERE g.pid = $1 ORDER BY g.ord, n.ord`,
        pid,
    );
    const sdgroups = await t.any(
        "SELECT sdgid, gname FROM phonebook_sd_groups WHERE pid = $1 ORDER BY ord",
        pid,
    );
    const sds = await t.any(
        `SELECT sdid, sdgid, action, icon, nid
FROM phonebook_sd_groups JOIN phonebook_sd USING(sdgid) WHERE pid = $1 ORDER BY phonebook_sd_groups.ord, phonebook_sd.ord`,
        pid,
    );

    phonebook.groups = [];
    phonebook.id = pid;
    phonebook.sdgroups = [];

    const grp_by_id = new Map();
    groups.forEach(g => {
        const grp = {
            id: g.gid,
            icon: g.icon,
            name: g.gname,
            numbers: [],
        };
        Object.freeze(grp);
        grp_by_id.set(g.gid, grp.numbers);
        phonebook.groups.push(grp);
    });
    Object.freeze(phonebook.groups);
    numbers.forEach(n => {
        grp_by_id.get(n.gid).push(
            Object.freeze({
                id: n.nid,
                name: n.nname,
                number: n.number,
                icon: n.icon,
                transfer: Object.freeze({
                    ng911: n.ng911,
                    blind: n.blind,
                    attended: n.attended,
                    conference: n.conference,
                    outbound: n.outbound,
                }),
            }),
        );
    });
    phonebook.groups.forEach((g: any) => Object.freeze(g.numbers));

    const sdgrp_by_id = new Map();
    sdgroups.forEach(n => {
        const grp = Object.freeze({
            id: n.sdgid,
            name: n.gname,
            sds: [],
        });
        sdgrp_by_id.set(grp.id, grp.sds);
        phonebook.sdgroups.push(grp);
    });
    sds.forEach(n => {
        sdgrp_by_id.get(n.sdgid).push(
            Object.freeze({
                id: n.sdid,
                action: n.action,
                nid: n.nid,
                icon: n.icon,
            }),
        );
    });

    if (options.icons ?? false) {
        phonebook.icon_list = [];
        const ph_dir = path.dirname(phonebook.filename);
        const add_files = async (dir: string) => {
            let list: string[] = [];
            try {
                list = fs.readdirSync(dir);
            } catch (e) {
                // ignore the error if the directory doesn't exist
                if (e.code === "ENOENT") {
                    list = [];
                } else {
                    throw e;
                }
            }
            for (const file of list) {
                const stat = fs.statSync(path.join(dir, file));
                if (stat.isFile() && /\.(png|ico)$/.test(file)) {
                    phonebook.icon_list.push(
                        path.relative(
                            path.join(ph_dir, "icons"),
                            path.join(dir, file),
                        ),
                    );
                } else if (stat.isDirectory()) {
                    await add_files(path.join(dir, file));
                }
            }
        };
        await add_files(path.join(ph_dir, "icons"));
        phonebook.icon_list.sort();
        Object.freeze(phonebook.icon_list);
    }

    return Object.freeze(phonebook);
};

const add_contact_t = async (
    t: ISchemaTask<unknown>,
    pid: string,
    num: any,
): Promise<string> => {
    await assert_phonebook_schema_t(t);
    num.name = num.name.trim();
    if (num.name === "") {
        throw user_error("Number name must not be empty");
    }
    if (num.number === "") {
        throw user_error("Phone Number must not be empty.");
    }
    num.number = validation.normalize_phone_number(num.number);
    if (!num.number) {
        throw user_error("Invalid format for number.");
    }

    if (num.gname && !num.gid) {
        const result = await t.oneOrNone(
            "SELECT gid FROM phonebook_groups WHERE pid = $(pid) AND gname = $(gname)",
            { pid, gname: num.gname },
        );
        // if we have name for group and it doesn't exist, create it
        if (!result) {
            num.gid = await add_group_t(t, pid, { name: num.gname });
        } else {
            num.gid = result.gid;
        }
    }

    // TODO: group name?
    if (num.gid === "" || num.gid === undefined || num.gid === null) {
        throw user_error("Number must be added to group.");
    }
    if (pid !== (await getPidOfGid(t, num.gid))) {
        throw user_error("Can not add to group that does not exist");
    }

    const ord = await t.one(
        "SELECT max(ord) AS ord FROM phonebook_numbers WHERE gid = $(gid)",
        num,
    );
    const new_ord = ord.ord ? ord.ord + 1 : 1;
    try {
        const nid = uuid.v4();
        return (
            await t.one(
                `INSERT INTO phonebook_numbers(nid, gid, nname, number, ng911, blind, attended, conference, outbound, icon, ord)
VALUES($(nid), $(gid), $(name), $(number), $(ng911), $(blind), $(attended), $(conference), $(outbound), $(icon), $(ord)) RETURNING nid`,
                {
                    icon: null,
                    ...num,
                    nid,
                    ng911: num.transfer.ng911,
                    blind: num.transfer.blind,
                    attended: num.transfer.attended,
                    conference: num.transfer.conference,
                    outbound: num.transfer.outbound,
                    ord: new_ord,
                },
            )
        ).nid;
    } catch (e) {
        if (
            e.code === "23505" &&
            e.table === "phonebook_numbers" &&
            e.constraint === "phonebook_groups_unique_gid_ename"
        ) {
            throw user_error(
                `Number \u201C${num.name}\u201D already exists in group`,
            );
        }
        throw e;
    }
};

const add_group_t = async (
    t: ISchemaTask<unknown>,
    pid: string,
    grp: any,
): Promise<string> => {
    await assert_phonebook_schema_t(t);
    grp.name = grp.name.trim();
    if (grp.name === "") {
        throw user_error("Group name must not be empty");
    }
    try {
        const gid = uuid.v4();
        return (
            await t.one(
                `INSERT INTO phonebook_groups(gid, pid, gname, icon, ord)
            VALUES($(gid), $(pid), $(name), $(icon),
                (SELECT COALESCE(max(ord) + 1, 1) FROM phonebook_groups WHERE pid = $(pid))
            ) RETURNING gid`,
                { icon: null, ...grp, gid, pid },
            )
        ).gid;
    } catch (e) {
        if (
            e.code === "23505" &&
            e.constraint === "phonebook_groups_unique_pid_gname" &&
            e.table === "phonebook_groups"
        ) {
            throw user_error(`Group \u201C${grp.name}\u201D already exists`);
        }
        throw e;
    }
};

const edit_contact_t = async (
    t: ISchemaTask<unknown>,
    pid: string,
    num: any,
) => {
    await assert_phonebook_schema_t(t);
    if (num.name) {
        num.name = num.name.trim();
        if (num.name === "") {
            throw user_error("Contact name must not be empty");
        }
    }
    if (num.number === "") {
        throw user_error("Phone number must not be empty.");
    }
    num.number = validation.normalize_phone_number(num.number);
    if (!num.number) {
        throw user_error("Invalid format for phone number");
    }

    if (num.gname && !num.gid) {
        const result = await t.oneOrNone(
            "SELECT gid FROM phonebook_groups WHERE pid = $(pid) AND gname = $(gname)",
            { pid, gname: num.gname },
        );
        if (!result) {
            num.gid = await add_group_t(t, pid, { name: num.gname });
        } else {
            num.gid = result.gid;
        }
    }

    const old = await t.one(
        "SELECT gid, ord, icon FROM phonebook_numbers WHERE nid = $(id)",
        num,
    );
    // both gids must belong to this pid
    assert(pid === (await getPidOfGid(t, old.gid)));
    assert(pid === (await getPidOfGid(t, num.gid)));

    let new_ord;
    if (old.gid !== num.gid) {
        const max = await t.one(
            "SELECT max(ord) AS ord FROM phonebook_numbers WHERE gid = $(gid)",
            num,
        );
        new_ord = max.ord ? max.ord + 1 : 1;
    } else {
        new_ord = old.ord;
    }

    try {
        await t.none(
            `UPDATE phonebook_numbers SET gid = $(gid), nname = $(name),
number = $(number), ng911 = $(ng911), blind = $(blind),
attended = $(attended), conference = $(conference),
outbound = $(outbound), icon = $(icon), ord = $(ord)
WHERE nid = $(id)`,
            {
                icon: old.icon,
                ...num,
                ng911: num.transfer.ng911,
                blind: num.transfer.blind,
                attended: num.transfer.attended,
                conference: num.transfer.conference,
                outbound: num.transfer.outbound,
                ord: new_ord,
            },
        );
    } catch (e) {
        if (
            e.code === "23505" &&
            e.table === "phonebook_numbers" &&
            e.constraint === "phonebook_groups_unique_gid_ename"
        ) {
            throw user_error(
                `Group already contains a contact with the name of \u201C${num.name}\u201D`,
            );
        } else if (
            e.code === "23514" &&
            e.table === "phonebook_numbers" &&
            e.constraint === "phonebook_numbers_nname_check"
        ) {
            throw user_error(`Contact name can not be empty`);
        }
        throw e;
    }

    if (old.gid !== num.gid) {
        await t.none(
            "UPDATE phonebook_numbers SET ord = -(ord - 1) WHERE ord > $(ord) AND gid = $(gid)",
            old,
        );
        await t.none(
            "UPDATE phonebook_numbers SET ord = -ord WHERE gid = $(gid) AND ord < 0",
            old,
        );
    }
};

const overwrite_file = async (filename: string, data: string) => {
    fs.writeFileSync(filename, data);
    // fs.renameSync(filename + '~', filename);
};

// private(ish) variables
const _pid = Symbol("PhonebookDB._pid");
const _db = Symbol("PhonebookDB._db");

const dirCache = new Map();
setInterval(() => {
    const now = Date.now();
    for (const id of dirCache.keys()) {
        if (dirCache.get(id).expires < now) {
            dirCache.delete(id);
        }
    }
}, 1000 * 60 * 5).unref();

const BASE_URL = process.env.BASE_URL ?? "http://192.168.62.11:3001/";

export class PhonebookDB {
    private [_pid]: string;
    private [_db]: IDatabase<unknown>;

    constructor(options: { pid: string; db: IDatabase<unknown> }) {
        assert(options);
        assert(typeof options.pid === "string");
        assert(options.db);

        this[_pid] = options.pid;
        this[_db] = options.db;
        Object.freeze(this);
    }

    public pid() {
        return this[_pid];
    }

    public save_to_file() {
        const to_url = (x: string) =>
            BASE_URL +
            "icon?pid=" +
            encodeURIComponent(this[_pid]) +
            "&icon=" +
            encodeURIComponent(x);
        const save = async (t: ISchemaTask<unknown>) => {
            await assert_phonebook_schema_t(t);
            const phonebook = await get_phonebook_t(t, this[_pid]);
            debug(`save to old format file: ${phonebook.filename}`);
            await overwrite_file(
                phonebook.filename,
                phonebook_xml.to_xml(phonebook, to_url),
            );
            await t.none(
                "UPDATE phonebook SET last_saved = $(phonebook) WHERE pid = $(pid)",
                { pid: this[_pid], phonebook },
            );
        };
        return transactor(this[_db], save).then(() =>
            debug(`phonebook save successful`),
        );
    }

    public replace_with_phonebook(phonebook: any) {
        const replace = async (t: ISchemaTask<unknown>) =>
            replace_with_phonebook_t(t, this[_pid], phonebook);

        return (async () => {
            if (phonebook.id) {
                assert(phonebook.id === this[_pid], "wrong pid");
            }
            await transactor(this[_db], replace);
        })();
    }

    public get_directory() {
        debug(`get_directory() ${this[_pid]}`);
        const get = async (t: ISchemaTask<unknown>) => {
            await assert_phonebook_schema_t(t);
            const ret = await t.one(
                "SELECT filename FROM phonebook WHERE pid = $(pid)",
                { pid: this[_pid] },
            );
            return ret.filename;
        };
        return transactor(this[_db], get, "readonly").then(f =>
            path.dirname(f),
        );
    }

    public get_directory_cached() {
        return this.get_directory();
        /*return co.wrap(function*() {
            const dir = dirCache.get(this[_pid]);
            if (dir && Date.now() < dir.expires)
                   return dir.dir;
            else {
                debug(`get_directory_cached() miss: ${this[_pid]}`);
                const dir = {expires: Date.now() + 5000};
                dir.dir = yield this.get_directory();
                Object.freeze(dir);
                dirCache.set(this[_pid], dir);
                return dir.dir
            }
        }).bind(this)();*/
    }

    public get_phonebook() {
        debug(`get_phonebook() ${this[_pid]}`);
        const get = async (t: ISchemaTask<unknown>) => {
            return get_phonebook_t(t, this[_pid], { icons: true });
        };
        return transactor(this[_db], get, "readonly");
    }

    public sdgroup_add(grp: any) {
        debug(`sdgroup_add(${JSON.stringify(grp)} ${this[_pid]}`);
        const add = async (t: ISchemaTask<unknown>) => {
            await assert_phonebook_schema_t(t);
            grp.name = grp.name.trim();
            if (grp.name === "") {
                throw user_error("Speed Dial group name must not be empty");
            }
            const sdgid = uuid.v4();
            return (
                await t.one(
                    `INSERT INTO phonebook_sd_groups(sdgid, pid, gname, ord)
                    VALUES($(sdgid), $(pid), $(name),
                        (SELECT COALESCE(max(ord) + 1, 1) FROM phonebook_sd_groups WHERE pid = $(pid))
                    ) RETURNING sdgid`,
                    { sdgid, pid: this[_pid], name: grp.name },
                )
            ).sdgid;
        };
        return transactor(this[_db], add);
    }

    public group_add(grp: any) {
        debug(`group_add(${JSON.stringify(grp)}) ${this[_pid]}`);
        const add = async (t: ISchemaTask<unknown>) => {
            return add_group_t(t, this[_pid], grp);
        };
        return transactor(this[_db], add);
    }

    public group_edit(grp: any) {
        debug(`group_edit(${JSON.stringify(grp)}) ${this[_pid]}`);
        const edit = async (t: ISchemaTask<unknown>) => {
            await assert_phonebook_schema_t(t);
            if (grp.name) {
                grp.name = grp.name.trim();
                if (grp.name === "") {
                    throw user_error("Group name must not be empty.");
                }
            }
            assert.strictEqual(
                this[_pid],
                (
                    await t.one(
                        "SELECT pid FROM phonebook_groups WHERE gid = $(id)",
                        grp,
                    )
                ).pid,
            );
            try {
                await t.none(
                    "UPDATE phonebook_groups SET gname = $(name), icon = $(icon) WHERE gid = $(id)",
                    { icon: null, ...grp },
                );
            } catch (e) {
                if (
                    e.code === "23505" &&
                    e.table === "phonebook_groups" &&
                    e.constraint === "phonebook_groups_unique_pid_gname"
                ) {
                    throw user_error("Group name already exists");
                } else if (
                    e.code === "23514" &&
                    e.table === "phonebook_groups" &&
                    e.constraint === "phonebook_groups_gname_check"
                ) {
                    throw user_error("Group name can not be empty");
                }
                throw e;
            }
        };
        return transactor(this[_db], edit);
    }

    public sdgroup_edit(grp: any) {
        debug(`sdgroup_edit(${JSON.stringify(grp)}) ${this[_pid]}`);
        const edit = async (t: ISchemaTask<unknown>) => {
            await assert_phonebook_schema_t(t);
            if (grp.name) {
                grp.name = grp.name.trim();
                if (grp.name === "") {
                    throw user_error("Group name must not be empty.");
                }
            }
            assert.strictEqual(
                this[_pid],
                (
                    await t.one(
                        "SELECT pid FROM phonebook_sd_groups WHERE sdgid = $(id)",
                        grp,
                    )
                ).pid,
            );
            try {
                await t.none(
                    "UPDATE phonebook_sd_groups SET gname = $(name) WHERE sdgid = $(id)",
                    grp,
                );
            } catch (e) {
                if (
                    e.code === "23505" &&
                    e.table === "phonebook_sd_groups" &&
                    e.constraint === "phonebook_sd_groups_gname_pid_key"
                ) {
                    throw user_error("Speed dial group name already exists!");
                } else if (
                    e.code === "23514" &&
                    e.table === "phonebook_sd_groups" &&
                    e.constraint === "phonebook_sd_groups_gname_check"
                ) {
                    throw user_error("Speed dial group name can not be empty");
                }
                throw e;
            }
        };
        return transactor(this[_db], edit);
    }

    // TODO: if group doesn't exist is a no-op
    public group_del(grp: any) {
        debug(`group_del(${JSON.stringify(grp)}) ${this[_pid]}`);
        const del = async (t: ISchemaTask<unknown>) => {
            await assert_phonebook_schema_t(t);
            assert.strictEqual(
                this[_pid],
                (
                    await t.one(
                        "SELECT pid FROM phonebook_groups WHERE gid = $(id)",
                        grp,
                    )
                ).pid,
                "wrong pid",
            );
            let ord;
            try {
                ord = (
                    await t.one(
                        "DELETE FROM phonebook_groups WHERE gid = $(id) RETURNING ord",
                        grp,
                    )
                ).ord;
            } catch (e) {
                if (
                    e.code === "23503" &&
                    e.table === "phonebook_numbers" &&
                    e.constraint === "phonebook_numbers_gid_fkey"
                ) {
                    throw user_error(`Group still contains entries!`);
                }
                throw e;
            }
            await t.none(
                "UPDATE phonebook_groups SET ord = -(ord - 1) WHERE ord > $(ord) AND pid = $(pid)",
                {
                    pid: this[_pid],
                    ord,
                },
            );
            await t.none(
                "UPDATE phonebook_groups SET ord = -ord WHERE ord < 0 AND pid = $(pid)",
                { pid: this[_pid] },
            );
        };
        return transactor(this[_db], del);
    }

    public sdgroup_del(grp: any) {
        debug(`sdgroup_del(${JSON.stringify(grp)}) ${this[_pid]}`);
        const del = async (t: ISchemaTask<unknown>) => {
            await assert_phonebook_schema_t(t);
            assert.strictEqual(
                this[_pid],
                (
                    await t.one(
                        "SELECT pid FROM phonebook_sd_groups WHERE sdgid = $(id)",
                        grp,
                    )
                ).pid,
                "wrong pid",
            );
            let ord;
            try {
                ord = (
                    await t.one(
                        "DELETE FROM phonebook_sd_groups WHERE sdgid = $(id) RETURNING ord",
                        grp,
                    )
                ).ord;
            } catch (e) {
                if (
                    e.code === "23503" &&
                    e.table === "phonebook_sd" &&
                    e.constraint === "phonebook_sd_sdgid_fkey"
                ) {
                    throw user_error(`Group still contains entries!`);
                }
                throw e;
            }
            await t.none(
                "UPDATE phonebook_sd_groups SET ord = -(ord - 1) WHERE ord > $(ord) AND pid = $(pid)",
                { pid: this[_pid], ord },
            );
            await t.none(
                "UPDATE phonebook_sd_groups SET ord = -ord WHERE ord < 0 AND pid = $(pid)",
                { pid: this[_pid] },
            );
        };
        return transactor(this[_db], del);
    }

    // should have no user-visible errors
    // (too far is a nop)
    public group_moveup(grp: any) {
        const moveup = async (t: ISchemaTask<unknown>) => {
            await assert_phonebook_schema_t(t);
            if (this[_pid] !== (await getPidOfGid(t, grp.id))) {
                return;
            }
            const grp_ord = (
                await t.one(
                    "SELECT ord FROM phonebook_groups WHERE gid = $(id)",
                    grp,
                )
            ).ord;
            const less = await t.oneOrNone(
                `SELECT gid, ord FROM phonebook_groups
                                      WHERE pid = $(pid) AND ord < $(ord)
                                      ORDER BY ord DESC LIMIT 1`,
                { pid: this[_pid], ord: grp_ord },
            );
            if (!less) {
                return;
            }
            await t.none(
                "UPDATE phonebook_groups SET ord = 0 WHERE gid = $(id)",
                grp,
            );
            await t.none(
                "UPDATE phonebook_groups SET ord = $(ord) WHERE gid = $(id)",
                { id: less.gid, ord: grp_ord },
            );
            await t.none(
                "UPDATE phonebook_groups SET ord = $(ord) WHERE gid = $(id)",
                { id: grp.id, ord: less.ord },
            );
        };
        return transactor(this[_db], moveup);
    }

    public sdgroup_moveup(grp: any) {
        debug(`sdgroup_moveup(${JSON.stringify(grp)}) ${this[_pid]}`);
        const moveup = async (t: ISchemaTask<unknown>) => {
            await assert_phonebook_schema_t(t);
            if (this[_pid] !== (await getPidOfSdgid(t, grp.id))) {
                return;
            }
            const grp_ord = (
                await t.one(
                    "SELECT ord FROM phonebook_sd_groups WHERE sdgid = $(id)",
                    grp,
                )
            ).ord;
            const less = await t.oneOrNone(
                `SELECT sdgid, ord FROM phonebook_sd_groups
                WHERE pid = $(pid) AND ord < $(ord)
                ORDER BY ord DESC LIMIT 1`,
                { pid: this[_pid], ord: grp_ord },
            );
            if (!less) {
                return;
            }
            await t.none(
                "UPDATE phonebook_sd_groups SET ord = 0 WHERE sdgid = $(id)",
                grp,
            );
            await t.none(
                "UPDATE phonebook_sd_groups SET ord = $(ord) WHERE sdgid = $(id)",
                { id: less.sdgid, ord: grp_ord },
            );
            await t.none(
                "UPDATE phonebook_sd_groups SET ord = $(ord) WHERE sdgid = $(id)",
                { id: grp.id, ord: less.ord },
            );
        };
        return transactor(this[_db], moveup);
    }

    // should have no user-visible errors
    public group_movedown(grp: any) {
        const movedown = async (t: ISchemaTask<unknown>) => {
            await assert_phonebook_schema_t(t);
            if (this[_pid] !== (await getPidOfGid(t, grp.id))) {
                return;
            }
            const grp_ord = (
                await t.one(
                    "SELECT ord FROM phonebook_groups WHERE gid = $(id)",
                    grp,
                )
            ).ord;
            const greater = await t.oneOrNone(
                `SELECT gid, ord FROM phonebook_groups
                             WHERE pid = $(pid) AND ord > $(ord)
                             ORDER BY ord LIMIT 1`,
                { pid: this[_pid], ord: grp_ord },
            );
            if (!greater) {
                return;
            }
            await t.none(
                "UPDATE phonebook_groups SET ord = 0 WHERE gid = $(id)",
                grp,
            );
            await t.none(
                "UPDATE phonebook_groups SET ord = $(ord) WHERE gid = $(id)",
                { id: greater.gid, ord: grp_ord },
            );
            await t.none(
                "UPDATE phonebook_groups SET ord = $(ord) WHERE gid = $(id)",
                { id: grp.id, ord: greater.ord },
            );
        };
        return transactor(this[_db], movedown);
    }

    public sdgroup_movedown(grp: any) {
        const movedown = async (t: ISchemaTask<unknown>) => {
            await assert_phonebook_schema_t(t);
            if (this[_pid] !== (await getPidOfSdgid(t, grp.id))) {
                return;
            }
            const grp_ord = (
                await t.one(
                    "SELECT ord FROM phonebook_sd_groups WHERE sdgid = $(id)",
                    grp,
                )
            ).ord;
            const greater = await t.oneOrNone(
                `SELECT sdgid, ord FROM phonebook_sd_groups
                WHERE pid = $(pid) AND ord > $(ord)
                ORDER BY ord LIMIT 1`,
                { pid: this[_pid], ord: grp_ord },
            );
            if (!greater) {
                return;
            }
            await t.none(
                "UPDATE phonebook_sd_groups SET ord = 0 WHERE sdgid = $(id)",
                grp,
            );
            await t.none(
                "UPDATE phonebook_sd_groups SET ord = $(ord) WHERE sdgid = $(id)",
                { id: greater.sdgid, ord: grp_ord },
            );
            await t.none(
                "UPDATE phonebook_sd_groups SET ord = $(ord) WHERE sdgid = $(id)",
                { id: grp.id, ord: greater.ord },
            );
        };
        return transactor(this[_db], movedown);
    }

    public sort_groups() {
        const sort = async (t: ISchemaTask<unknown>) => {
            await assert_phonebook_schema_t(t);
            const list = await t.any(
                "SELECT gid, gname FROM phonebook_groups WHERE pid = $(pid)",
                { pid: this[_pid] },
            );
            list.sort((a, b) => string_compare(a.gname, b.gname));
            for (let i = 0; i < list.length; i++) {
                await t.none(
                    "UPDATE phonebook_groups SET ord = $(ord) WHERE gid = $(id)",
                    { ord: -(i + 1), id: list[i].gid },
                );
            }
            await t.none(
                "UPDATE phonebook_groups SET ord = -ord WHERE pid = $(pid)",
                { pid: this[_pid] },
            );
        };
        return transactor(this[_db], sort);
    }

    public number_add(num: any) {
        const add = async (t: ISchemaTask<unknown>) => {
            return add_contact_t(t, this[_pid], num);
        };
        return transactor(this[_db], add);
    }

    public sd_add(sd: any) {
        const add = async (t: ISchemaTask<unknown>) => {
            await assert_phonebook_schema_t(t);
            if (this[_pid] !== (await getPidOfSdgid(t, sd.sdgid))) {
                throw user_error(
                    "Can not add speed dial to group that does not exist",
                );
            }

            const ord = await t.one(
                "SELECT max(ord) AS ord FROM phonebook_sd WHERE sdgid = $(sdgid)",
                sd,
            );
            const new_ord = ord.ord ? ord.ord + 1 : 1;
            const sdid = uuid.v4();
            return (
                await t.one(
                    `INSERT INTO phonebook_sd(sdid, sdgid, nid, action, icon, ord)
VALUES($(sdid), $(sdgid), $(nid), $(action), $(icon), $(ord)) RETURNING sdid`,
                    { icon: null, ...sd, sdid, ord: new_ord },
                )
            ).sdid;
        };
        return transactor(this[_db], add);
    }

    public number_edit(num: any) {
        const edit = async (t: ISchemaTask<unknown>) => {
            return edit_contact_t(t, this[_pid], num);
        };
        return transactor(this[_db], edit);
    }

    public sd_edit(sd: any) {
        debug(`sd_edit(${JSON.stringify(sd)}) ${this[_pid]}`);
        const edit = async (t: ISchemaTask<unknown>) => {
            await assert_phonebook_schema_t(t);
            const old = await t.one(
                "SELECT sdgid, ord FROM phonebook_sd WHERE sdid = $(id)",
                sd,
            );
            assert(this[_pid] === (await getPidOfSdgid(t, old.sdgid)));
            assert(this[_pid] === (await getPidOfSdgid(t, sd.sdgid)));

            let new_ord;
            if (old.sdgid !== sd.sdgid) {
                const max = await t.one(
                    "SELECT max(ord) AS ord FROM phonebook_sd WHERE sdgid = $(sdgid)",
                    sd,
                );
                new_ord = max.ord ? max.ord + 1 : 1;
            } else {
                new_ord = old.ord;
            }

            await t.none(
                `UPDATE phonebook_sd SET sdgid = $(sdgid), action = $(action), nid = $(nid), ord = $(ord), icon = $(icon) WHERE sdid = $(id)`,
                { ...sd, ord: new_ord },
            );

            if (old.sdgid !== sd.sdgid) {
                await t.none(
                    "UPDATE phonebook_sd SET ord = -(ord - 1) WHERE ord > $(ord) AND sdgid = $(sdgid)",
                    old,
                );
                await t.none(
                    "UPDATE phonebook_sd SET ord = -ord WHERE sdgid = $(sdgid) AND ord < 0",
                    old,
                );
            }
        };
        return transactor(this[_db], edit);
    }

    // should do nothing if not exists
    public number_delete(num: any) {
        const del = async (t: ISchemaTask<unknown>) => {
            await assert_phonebook_schema_t(t);
            let old;
            try {
                old = await t.one(
                    "DELETE FROM phonebook_numbers WHERE nid = $(id) RETURNING ord, gid",
                    num,
                );
            } catch (e) {
                if (
                    e.code === "23503" &&
                    e.table === "phonebook_sd" &&
                    e.constraint === "phonebook_sd_nid_fkey"
                ) {
                    throw user_error("Contact has speed dial, can't delete");
                }
                throw e;
            }
            assert(this[_pid] === (await getPidOfGid(t, old.gid)));
            await t.none(
                "UPDATE phonebook_numbers SET ord = -(ord - 1) WHERE ord > $(ord) AND gid = $(gid)",
                old,
            );
            await t.none(
                "UPDATE phonebook_numbers SET ord = -ord WHERE ord < 0",
            );
        };
        return transactor(this[_db], del);
    }

    public sd_delete(sd: any) {
        const del = async (t: ISchemaTask<unknown>) => {
            await assert_phonebook_schema_t(t);
            const old = await t.one(
                "DELETE FROM phonebook_sd WHERE sdid = $(id) RETURNING ord, sdgid",
                sd,
            );
            assert(this[_pid] === (await getPidOfSdgid(t, old.sdgid)));
            await t.none(
                "UPDATE phonebook_sd SET ord = -(ord - 1) WHERE ord > $(ord) AND sdgid = $(sdgid)",
                old,
            );
            await t.none("UPDATE phonebook_sd SET ord = -ord WHERE ord < 0");
        };
        return transactor(this[_db], del);
    }

    public number_moveup(nid: string) {
        const moveup = async (t: ISchemaTask<unknown>) => {
            await assert_phonebook_schema_t(t);
            const num = await t.one(
                "SELECT ord, gid FROM phonebook_numbers WHERE nid = $(id)",
                { id: nid },
            );
            num.id = nid;
            assert(this[_pid] === (await getPidOfGid(t, num.gid)));
            const less = await t.oneOrNone(
                `SELECT nid, ord FROM phonebook_numbers
                WHERE gid = $(gid) AND ord < $(ord)
                ORDER BY ord DESC LIMIT 1`,
                num,
            );
            if (!less) {
                return;
            }
            await t.none(
                "UPDATE phonebook_numbers SET ord = -ord WHERE nid = $(id)",
                num,
            );
            await t.none(
                "UPDATE phonebook_numbers SET ord = $(ord) WHERE nid = $(id)",
                { id: less.nid, ord: num.ord },
            );
            await t.none(
                "UPDATE phonebook_numbers SET ord = $(ord) WHERE nid = $(id)",
                { id: num.id, ord: less.ord },
            );
        };
        return transactor(this[_db], moveup);
    }

    public sd_moveup(sdid: string) {
        const moveup = async (t: ISchemaTask<unknown>) => {
            await assert_phonebook_schema_t(t);
            const sd = await t.one(
                "SELECT ord, sdgid FROM phonebook_sd WHERE sdid = $(id)",
                { id: sdid },
            );
            sd.id = sdid;
            assert(this[_pid] === (await getPidOfSdgid(t, sd.sdgid)));
            const less = await t.oneOrNone(
                `SELECT sdid, ord FROM phonebook_sd
                WHERE sdgid = $(sdgid) AND ord < $(ord)
                ORDER BY ord DESC LIMIT 1`,
                sd,
            );
            if (!less) {
                return;
            }
            await t.none(
                "UPDATE phonebook_sd SET ord = -ord WHERE sdid = $(id)",
                sd,
            );
            await t.none(
                "UPDATE phonebook_sd SET ord = $(ord) WHERE sdid = $(id)",
                { id: less.sdid, ord: sd.ord },
            );
            await t.none(
                "UPDATE phonebook_sd SET ord = $(ord) WHERE sdid = $(id)",
                { id: sd.id, ord: less.ord },
            );
        };
        return transactor(this[_db], moveup);
    }

    public number_movedown(nid: string) {
        const movedown = async (t: ISchemaTask<unknown>) => {
            await assert_phonebook_schema_t(t);
            const num = await t.one(
                "SELECT ord, gid FROM phonebook_numbers WHERE nid = $(id)",
                { id: nid },
            );
            num.id = nid;
            assert(this[_pid] === (await getPidOfGid(t, num.gid)));
            const greater = await t.oneOrNone(
                `SELECT nid, ord FROM phonebook_numbers
                WHERE gid = $(gid) AND ord > $(ord)
                ORDER BY ord LIMIT 1`,
                num,
            );
            if (!greater) {
                return;
            }
            await t.none(
                "UPDATE phonebook_numbers SET ord = -ord WHERE nid = $(id)",
                num,
            );
            await t.none(
                "UPDATE phonebook_numbers SET ord = $(ord) WHERE nid = $(id)",
                { id: greater.nid, ord: num.ord },
            );
            await t.none(
                "UPDATE phonebook_numbers SET ord = $(ord) WHERE nid = $(id)",
                { id: num.id, ord: greater.ord },
            );
        };
        return transactor(this[_db], movedown);
    }

    public sd_movedown(sdid: string) {
        const movedown = async (t: ISchemaTask<unknown>) => {
            await assert_phonebook_schema_t(t);
            const sd = await t.one(
                "SELECT ord, sdgid FROM phonebook_sd WHERE sdid = $(id)",
                { id: sdid },
            );
            sd.id = sdid;
            assert(this[_pid] === (await getPidOfSdgid(t, sd.sdgid)));
            const greater = await t.oneOrNone(
                `SELECT sdid, ord FROM phonebook_sd
                WHERE sdgid = $(sdgid) AND ord > $(ord)
                ORDER BY ord LIMIT 1`,
                sd,
            );
            if (!greater) {
                return;
            }
            await t.none(
                "UPDATE phonebook_sd SET ord = -ord WHERE sdid = $(id)",
                sd,
            );
            await t.none(
                "UPDATE phonebook_sd SET ord = $(ord) WHERE sdid = $(id)",
                { id: greater.sdid, ord: sd.ord },
            );
            await t.none(
                "UPDATE phonebook_sd SET ord = $(ord) WHERE sdid = $(id)",
                { id: sd.id, ord: greater.ord },
            );
        };
        return transactor(this[_db], movedown);
    }

    public group_sort_within(gid: string) {
        const sort = async (t: ISchemaTask<unknown>) => {
            await assert_phonebook_schema_t(t);
            assert(this[_pid] === (await getPidOfGid(t, gid)));
            const list = await t.any(
                "SELECT nid, nname FROM phonebook_numbers WHERE gid = $(gid)",
                { gid },
            );
            list.sort((a, b) => string_compare(a.nname, b.nname));
            for (let i = 0; i < list.length; i++) {
                await t.none(
                    "UPDATE phonebook_numbers SET ord = $(ord) WHERE nid = $(nid)",
                    { ord: -(i + 1), nid: list[i].nid },
                );
            }
            await t.none(
                "UPDATE phonebook_numbers SET ord = -ord WHERE ord < 0 AND gid = $(gid)",
                { gid },
            );
        };
        return transactor(this[_db], sort);
    }

    public bulk_import_contacts(array: any[]) {
        const add = async (t: ISchemaTask<unknown>) => {
            for (const num of array) {
                if (!num.id) {
                    await add_contact_t(t, this[_pid], num);
                } else {
                    await edit_contact_t(t, this[_pid], num);
                }
            }
        };
        return transactor(this[_db], add);
    }
}

export const get_phonebook_list = async (db: IDatabase<unknown>) => {
    const get = async (t: ISchemaTask<unknown>) => {
        await assert_phonebook_schema_t(t);
        return t.any("SELECT pid FROM phonebook");
    };
    return transactor(db, get, "readonly");
};

export const upsert_linekey = async (
    db: IDatabase<unknown>,
    location: any,
    linekey: any,
    id: string,
    { _user_to_check }: { _user_to_check: unknown },
) => {
    assert(uuid.check(id));
    const upsert = async (t: ITask<unknown>) => {
        const prev = new PolycomTemplate(
            (
                await t.one(
                    "SELECT config FROM polycom_templates WHERE id = $(id)",
                    { id },
                )
            ).config,
        );

        const _old_linekey = prev.getLineKeySideCar(location)
            ? prev.getLineKeySideCar(location).toJS()
            : { category: "Unassigned" };

        throw new Error("TODO");
        /*const f = async (attr, val) => await pdp.assert({
            user: user_to_check,
            target: {
                obj: old_linekey,
                type: "polycom_line_key",
                location,
            },
            action: {
                type: "EDIT",
                attr_name: attr,
                attr_val: val,
            },
        });

        switch(linekey ? linekey.category : "Unassigned") {
            case "Unassigned": {
                await f("category", "Unassigned");
                old_linekey = {category: "Unassigned"};
                break;
            }
            case "SpeedDial": {
                await f("category", "SpeedDial");
                old_linekey = {category: "SpeedDial"};
                await f("lb", linekey.lb);
                old_linekey.lb = linekey.lb;
                await f("ct", linekey.ct);
                old_linekey.ct = linekey.ct;
                break;
            }
            case "BLF": {
                await f("category", "BLF");
                old_linekey = {category: "BLF"};
                break;
            }
            case "Line": {
                await f("category", "Line");
                old_linekey = {category: "Line"};
                await f("explicit_index", linekey.explicit_index);
                old_linekey.explicit_index = linekey.explicit_index;
                break;
            }
        }

        const new_config = prev.setLineKeySideCar(location, old_linekey.category === "Unassigned" ? null : old_linekey);
        new_config.directory();
        await t.none("UPDATE polycom_templates SET config = $(config) WHERE id = $(id)", {config: JSON.stringify(new_config), id});*/
    };
    return transactor(db, upsert);
};

export async function get_polycom(
    db: IDatabase<unknown>,
    id: string,
): Promise<any> {
    assert(uuid.check(id));
    const get = async (t: ITask<unknown>) => {
        const ret = await t.one(
            "SELECT id, name, config FROM polycom_templates WHERE id = $(id)",
            { id },
        );
        ret.config.name = ret.name;
        ret.config.id = ret.id;
        return new PolycomTemplate(ret.config);
    };
    return transactor(db, get, "readonly");
}

export const update_polycom = async (
    db: IDatabase<unknown>,
    id: string,
    changes: unknown,
    { _user_to_check }: { _user_to_check: unknown },
) => {
    assert(typeof changes === "object");
    assert(uuid.check(id));
    const update = async (t: ITask<unknown>) => {
        const prev = await t.one(
            "SELECT config, name FROM polycom_templates WHERE id = $(id)",
            { id },
        );
        const _old_config = { ...prev.config, name: prev.name };

        throw new Error("TODO");

        /*const f = async (attr, val) => pdp.assert({
            user: user_to_check,
            target: {
                obj: old_config,
                type: "polycom_template_config",
            },
            action: {
                type: "EDIT",
                attr_name: attr,
                attr_val: val,
            },
        });

        if (changes.name && changes.name !== old_config.name) {
            await f("name", changes.name);
            old_config.name = changes.name;
        }

        if (changes.tftp_directory && changes.tftp_directory !== old_config.tftp_directory) {
            await f("tftp_directory", changes.tftp_directory);
            old_config.tftp_directory = changes.tftp_directory;
        }

        await t.none("UPDATE polycom_templates SET name = $(name), config = $(config) WHERE id = $(id)", {
            id,
            name: old_config.name,
            config: {...old_config, name: undefined},
        });*/
    };
    return transactor(db, update);
};

export const add_polycom = async (
    db: IDatabase<unknown>,
    { _user_to_check }: { _user_to_check: unknown },
) => {
    const add = async (_t: ITask<unknown>) => {
        throw new Error("TODO");
        /*await pdp.assert({
            user: user_to_check,
            target: {
                type: "polycom_template_config",
            },
            action: {type: "CREATE"},
        });

        const id = uuid.v4();
        await t.none("INSERT INTO polycom_templates(id, name, config) VALUES($(id), $(name), $(config))", {id, name: "New Template", config: {}});
        return id;*/
    };
    return transactor(db, add);
};

export const del_polycom = async (
    db: IDatabase<unknown>,
    _id: string,
    { _user_to_check }: { _user_to_check: unknown },
) => {
    const del = async (_t: ITask<unknown>) => {
        throw new Error("TODO");
        /*await pdp.assert({
            user: user_to_check,
            target: {
                type: "polycom_template_config",
            },
            action: {type: "DELETE"},
        });

        await t.none("DELETE FROM polycom_templates WHERE id = $(id)", {id});*/
    };
    return transactor(db, del);
};

export const get_polycom_list = async (db: IDatabase<unknown>) => {
    const get = async (t: ITask<unknown>) => {
        return t.any("SELECT id, name FROM polycom_templates");
    };
    return transactor(db, get, "readonly");
};

import canonify from "canonical-json";
import fs_extra from "fs-extra";
import zlib from "zlib";

import { dump_t } from "./db/backup";

export const migrate = async (
    db: IDatabase<unknown>,
    to_schema: string = schema_version,
    backup: boolean = false,
) => {
    const txMode = new db.$config.pgp.txMode.TransactionMode({
        tiLevel: db.$config.pgp.txMode.isolationLevel.serializable,
        readOnly: false,
    });

    async function backup_func(t: ISchemaTask<unknown>) {
        const BACKUPDIR = path.join(
            process.env.DATADIR ?? "/var/opt/phonebook",
            "backups",
        );
        const data = await dump_t(t);

        const compress = await new Promise<Buffer>((resolve, reject) => {
            zlib.gzip(canonify(data), (e, buf) =>
                e ? reject(e) : resolve(buf),
            );
        });

        await fs_extra.mkdirp(BACKUPDIR);

        const d = new Date();
        fs.writeFileSync(
            path.join(
                BACKUPDIR,
                "pre-migration." + d.toISOString() + ".json.gz",
            ),
            compress,
            {
                mode: 0o440, // only allow user and group to read
                flag: "wx",
            },
        );
    }

    await migrate_db({
        db,
        migrations: migrations.migration_list,
        to: to_schema,
        txMode,
        backup: backup ? backup_func : undefined,
    });
};

Object.freeze(exports);
