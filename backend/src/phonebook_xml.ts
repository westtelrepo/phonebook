/* eslint-disable @typescript-eslint/no-non-null-assertion, @typescript-eslint/no-unsafe-call, curly */
import assert from "assert";
import libxmljs from "libxmljs";
import url from "url";
import uuid from "uuid-1345";

import { Phonebook } from "./phonebook";

export function to_xml(phonebook_arg: any, to_url = (x: string): string => x) {
    const phonebook = new Phonebook(phonebook_arg);
    const doc = new libxmljs.Document();
    const root = doc.node("TransferList");
    phonebook.groups.forEach(grp => {
        grp.numbers.forEach((num: any) => {
            root.node("Transfer_Name", num.name).attr({ id: num.id });
            root.node("Transfer_Group", grp.name);
            root.node("Transfer_Number", num.number);
            let code = 0;
            if (num.transfer.ng911) code |= 16;
            if (num.transfer.blind) code |= 2;
            if (num.transfer.attended) code |= 4;
            if (num.transfer.conference) code |= 8;
            if (num.transfer.outbound) code |= 32;
            root.node("Transfer_Code", String(code));
            root.node("Speed_Dial_Action", "none");
            if (num.icon)
                root.node("icon_file", to_url(num.icon)).attr({
                    icon: num.icon || "",
                });
            else root.node("icon_file", "none");
        });
    });

    phonebook.sdgroups.forEach(grp => {
        grp.sds.forEach((sd: any) => {
            const num = phonebook.get_number(sd.nid);
            root.node("Transfer_Name", num.name)
                .attr({ id: sd.id })
                .attr({ nid: sd.nid });
            if (sd.icon)
                root.node("icon_file", to_url(sd.icon)).attr({
                    icon: sd.icon || "",
                });
            else if (num.icon)
                root.node("icon_file", to_url(num.icon)).attr({
                    icon: sd.icon || "",
                });
            else root.node("icon_file", "none");

            root.node("Transfer_Group", "SpeedDial " + grp.name);
            root.node("Transfer_Number", num.number);
            root.node("Transfer_Code", "0");
            root.node("Speed_Dial_Action", sd.action);
        });
    });

    const order = root.node("Display_Order");
    phonebook.groups.forEach((grp, i) => {
        order.node("Transfer_Group", grp.name);
        order.node("List_Priority", String(i + 1));
        if (grp.icon)
            order
                .node("icon_file", to_url(grp.icon))
                .attr({ icon: grp.icon || "" });
        else order.node("icon_file", "none");
    });
    phonebook.sdgroups.forEach((grp, i) => {
        order.node("Transfer_Group", "SpeedDial " + grp.name);
        order.node("List_Priority", String(i + 1 + phonebook.groups.size));
        order.node("icon_file", "none");
    });
    return doc.toString();
}

function parse_icon_url(u: string) {
    if (u === "none") return null;
    const icon_url = url.parse(u, true);
    return icon_url.query.icon;
}

export function from_xml(xml_string: string) {
    const doc = libxmljs.parseXml(xml_string);
    assert.equal(doc.root()!.name(), "TransferList");
    const transfers: any[] = [];
    const groups: any[] = [];
    const sdgroups: any[] = [];
    const sds: any[] = [];

    function parseDisplayOrder(root: any) {
        let currentGroup: any;

        const push = (x: any) => {
            if (x) {
                if (x.name.startsWith("SpeedDial ")) {
                    x.name = x.name.replace(/^SpeedDial /, "");
                    sdgroups.push(x);
                } else {
                    groups.push(x);
                }
            }
        };

        root.childNodes().forEach((node: any) => {
            if (node.type() === "element") {
                if (node.name() === "Transfer_Group") {
                    push(currentGroup);
                    currentGroup = { name: node.text().trim() };
                } else if (node.name() === "List_Priority") {
                    currentGroup.priority = Number(node.text());
                } else if (node.name() === "icon_file") {
                    if (node.attr("icon")) {
                        currentGroup.icon = node.attr("icon").value() || null;
                    } else currentGroup.icon = parse_icon_url(node.text());
                }
            }
        });
        push(currentGroup);
    }

    let currentTransfer: any;

    const push = (x: any) => {
        if (x) {
            if (x.group.startsWith("SpeedDial ")) {
                x.group = x.group.replace(/^SpeedDial /, "");
                sds.push(x);
            } else {
                transfers.push(x);
            }
        }
    };

    doc.childNodes().forEach(node => {
        if (node.type() === "element") {
            if (node.name() === "Transfer_Name") {
                push(currentTransfer);
                currentTransfer = { name: node.text().trim() };
                if (node.attr("id"))
                    currentTransfer.id = node.attr("id")!.value();
                else currentTransfer.id = uuid.v4();

                // if a speed dial has an "nid" attribute, then use that to find the contact
                if (node.attr("nid"))
                    currentTransfer.nid = node.attr("nid")!.value();
            } else if (node.name() === "Transfer_Group")
                currentTransfer.group = node.text().trim();
            else if (node.name() === "Transfer_Number")
                currentTransfer.number = node.text();
            else if (node.name() === "Transfer_Code")
                currentTransfer.code = Number(node.text());
            else if (node.name() === "icon_file")
                if (node.attr("icon")) {
                    currentTransfer.icon = node.attr("icon")!.value() || null;
                } else currentTransfer.icon = parse_icon_url(node.text());
            else if (node.name() === "Speed_Dial_Action")
                currentTransfer.speed_dial_action = node.text().trim();
            else if (node.name() === "Display_Order") {
                parseDisplayOrder(node);
            }
        }
    });
    push(currentTransfer);

    const priority_sort = (a: any, b: any) => {
        if (a.priority < b.priority) return -1;
        else if (a.priority > b.priority) return 1;
        else return 0;
    };

    groups.sort(priority_sort);
    sdgroups.sort(priority_sort);

    const group_map = new Map();
    groups.forEach(group => {
        group.id = uuid.v4();
        group_map.set(group.name, group);
    });

    const group_arr = new Map();
    transfers.forEach(number => {
        assert(uuid.check(number.id));
        number.gid = group_map.get(number.group).id;
        if (group_arr.get(number.group) === undefined)
            group_arr.set(number.group, []);
        group_arr.get(number.group).push(number);
    });

    const sdgroup_map = new Map();
    sdgroups.forEach(group => {
        group.id = uuid.v4();
        sdgroup_map.set(group.name, group);
    });

    const sdgroup_arr = new Map();
    sds.forEach(sd => {
        assert(uuid.check(sd.id));
        sd.sdgid = sdgroup_map.get(sd.group);
        if (sdgroup_arr.get(sd.group) === undefined)
            sdgroup_arr.set(sd.group, []);
        sdgroup_arr.get(sd.group).push(sd);
    });

    const phonebook: Record<string, any> = {};

    phonebook.name = "a phonebook";
    phonebook.id = uuid.v4();
    phonebook.groups = [];
    phonebook.sdgroups = [];
    groups.forEach(group => {
        const pgrp: Record<string, any> = {};
        pgrp.name = group.name;
        pgrp.id = group.id;
        pgrp.icon = group.icon || null;
        pgrp.numbers = [];
        phonebook.groups.push(pgrp);

        const elem = group_arr.get(group.name);
        if (elem !== undefined) {
            elem.forEach((number: any) => {
                pgrp.numbers.push({
                    id: number.id,
                    name: number.name,
                    number: number.number,
                    icon: number.icon || null,
                    transfer: {
                        ng911: Boolean(number.code & 16),
                        blind: Boolean(number.code & 2),
                        attended: Boolean(number.code & 4),
                        conference: Boolean(number.code & 8),
                        outbound: Boolean(number.code & 32),
                    },
                });
            });
        }
    });

    sdgroups.forEach(group => {
        const pgrp = {
            name: group.name,
            id: group.id,
            sds: [] as any[],
        };
        phonebook.sdgroups.push(pgrp);

        const elem = sdgroup_arr.get(group.name);
        if (elem !== undefined) {
            elem.forEach((sd: any) => {
                pgrp.sds.push({
                    id: sd.id,
                    icon: sd.icon,
                    nid: sd.nid
                        ? sd.nid
                        : transfers.find(
                              e => e.name === sd.name && e.number === sd.number,
                          ).id,
                    action: sd.speed_dial_action,
                });
            });
        }
    });
    return new Phonebook(phonebook);
}
