/* eslint-disable @typescript-eslint/no-unsafe-call */
import libxmljs_orig from "libxmljs";

const libxmljs: any = libxmljs_orig;

export function phone_config_to_xml(phone_config: any) {
    const directory = phone_config.directory();

    const xml_cfg = new libxmljs.Document();
    const xml_dir = new libxmljs.Document();

    xml_dir.root(
        new libxmljs.Comment(
            xml_dir,
            " WARNING: DO NOT EDIT. Changes will be overwritten ",
        ),
    );
    xml_cfg.root(
        new libxmljs.Comment(
            xml_cfg,
            " WARNING: DO NOT EDIT. Changes will be overwritten ",
        ),
    );

    {
        const item_list = xml_dir.node("directory").node("item_list");
        directory.forEach((e: any) => {
            item_list
                .node("item")
                .node("ct", e.ct)
                .parent()
                .node("sd", String(e.sd))
                .parent()
                .node("lb", e.lb);
        });
    }

    {
        const polycomConfig = xml_cfg
            .node("polycomConfig")
            .attr("xmlns:xsi", "http://www.w3.org/2001/XMLSchema-instance")
            .attr("xsi:noNamespaceSchemaLocation", "polycomConfig.xsd");

        // insert other config into here
        polycomConfig.node("video").attr("video.enable", "0");
        polycomConfig.node("powerSaving").attr("powerSaving.enable", "0");
        polycomConfig.node("httpd").attr("httpd.cfg.secureTunnelRequired", "0");
        const device = polycomConfig.node("device").attr("device.set", "1");
        device
            .node("device.auth")
            .attr("device.auth.localAdminPassword", "911")
            .node("device.auth.localAdminPassword")
            .attr("device.auth.localAdminPassword.set", "1");
        device
            .node("device.sntp")
            .attr("device.sntp.gmtOffset", "-25200")
            .attr("device.sntp.gmtOffsetcityID", "6")
            .node("device.sntp.gmtOffsetcityID")
            .attr("device.sntp.gmtOffsetcityID.set", "1")
            .parent()
            .node("device.sntp.gmtOffset")
            .attr("device.sntp.gmtOffset.set", "1");
        polycomConfig
            .node("feature")
            // we need this to set line keys
            .node("feature.enhancedFeatureKeys")
            .attr("feature.enhancedFeatureKeys.enabled", "1")
            .parent()
            .node("feature.bluetooth")
            .attr("feature.bluetooth.enabled", "0");

        const lineKey = polycomConfig.node("lineKey");
        // we also need this to set line keys
        lineKey
            .node("lineKey.reassignment")
            .attr("lineKey.reassignment.enabled", "1");

        let sd_index = 0; // directory() above sets sd indexes in the same order that we do, so just count
        phone_config.lineKeys.forEach((v: any, i: number) => {
            if (!v) {
                return;
            }

            if (v.category === "SpeedDial") {
                lineKey
                    .attr(`lineKey.${i + 1}.category`, "SpeedDial")
                    .attr(`lineKey.${i + 1}.index`, String(++sd_index));
            } else if (v.category === "BLF") {
                lineKey.attr(`lineKey.${i + 1}.category`, "BLF");
            } else if (v.category === "Line") {
                lineKey
                    .attr(`lineKey.${i + 1}.category`, "Line")
                    .attr(`lineKey.${i + 1}.index`, String(v.explicit_index));
            }
        });
    }

    return { cfg: xml_cfg.toString(), dir: xml_dir.toString() };
}
