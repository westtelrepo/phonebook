/* eslint-disable @typescript-eslint/no-non-null-assertion */
import { bulk_import_precheck } from "@w/shared/dist/bulk_import_precheck";
import * as Phonebook from "@w/shared/dist/phonebook";
import test from "ava";
import Immutable from "immutable";
import uuid from "uuid-1345";

test("should accept empty phonebook and empty csv", t => {
    const ret = bulk_import_precheck(new Phonebook.Phonebook(), "");
    t.assert(ret.allow_import);
    t.assert(Immutable.List.isList(ret.row_status));
    t.assert(ret.row_status.size === 0);
});

test("should accept empty phonebook and csv", t => {
    const csv = `Contact Name,Group,Phone Number,9-1-1 Transfer,Blind Transfer,Attended Transfer,Conference Transfer,Outbound
Alice,Group Name,111-1111,Y,Yes,yES,T,true
Bob,Group Name,222-2222,no,nO,f,FaLse,N
Eve,Eves,666-6666,Y,N,Y,N,Y
`;
    const ret = bulk_import_precheck(new Phonebook.Phonebook(), csv);

    t.assert(ret.allow_import);
    t.assert(Immutable.List.isList(ret.row_status));
    t.assert(ret.row_status.size === 3);

    t.assert(typeof ret.row_status.get(0)!.contact === "object");
    t.assert(typeof ret.row_status.get(1)!.contact === "object");
    t.assert(typeof ret.row_status.get(2)!.contact === "object");

    const alice = ret.row_status.get(0)!;
    const bob = ret.row_status.get(1)!;
    const eve = ret.row_status.get(2)!;

    t.assert(alice.contact.name === "Alice");
    t.assert(alice.contact.gname === "Group Name");
    t.assert(alice.contact.contact === "1111111");
    t.assert(alice.contact.xfer_ng911);
    t.assert(alice.contact.xfer_blind);
    t.assert(alice.contact.xfer_attended);
    t.assert(alice.contact.xfer_conference);
    t.assert(alice.contact.xfer_outbound);
    t.assert(alice.contact_state === "add");
    t.assert(alice.group_state === "add");

    t.assert(bob.contact.name === "Bob");
    t.assert(bob.contact.gname === "Group Name");
    t.assert(bob.contact.contact === "2222222");
    t.assert(!bob.contact.xfer_ng911);
    t.assert(!bob.contact.xfer_blind);
    t.assert(!bob.contact.xfer_attended);
    t.assert(!bob.contact.xfer_conference);
    t.assert(!bob.contact.xfer_outbound);
    t.assert(bob.contact_state === "add");
    t.assert(bob.group_state === "add");

    t.assert(eve.contact.name === "Eve");
    t.assert(eve.contact.gname === "Eves");
    t.assert(eve.contact.contact === "6666666");
    t.assert(eve.contact.xfer_ng911);
    t.assert(!eve.contact.xfer_blind);
    t.assert(eve.contact.xfer_attended);
    t.assert(!eve.contact.xfer_conference);
    t.assert(eve.contact.xfer_outbound);
    t.assert(eve.contact_state === "add");
    t.assert(eve.group_state === "add");
});

test("should accept phonebook and empty csv", t => {
    let phonebook = new Phonebook.Phonebook();

    const gid = uuid.v4();

    phonebook = phonebook.apply(
        phonebook.dir_add(
            new Phonebook.Directory({ name: "Group A", ord: 0 }),
            { id: gid },
        ),
    );

    phonebook = phonebook.apply(
        phonebook.contact_add(
            new Phonebook.Contact({
                name: "Alice",
                parent: gid,
                contact: "404",
                ord: 0,
            }),
        ),
    );

    phonebook = phonebook.apply(
        phonebook.contact_add(
            new Phonebook.Contact({
                name: "Bob",
                parent: gid,
                contact: "404",
                ord: 1,
            }),
        ),
    );

    const ret = bulk_import_precheck(phonebook, "");
    t.assert(ret.row_status.size === 0);
    t.assert(ret.allow_import);
});

test("existing group should set group_state", t => {
    let phonebook = new Phonebook.Phonebook();

    phonebook = phonebook.apply(
        phonebook.dir_add(new Phonebook.Directory({ name: "Group", ord: 0 })),
    );

    const ret = bulk_import_precheck(
        phonebook,
        `Contact Name,Group,Phone Number,9-1-1 Transfer,Blind Transfer,Attended Transfer,Conference Transfer,Outbound
Alice,Group,111-1111,Y,Y,Y,Y,Y`,
    );

    t.assert(ret.allow_import);
    t.assert(ret.row_status.size === 1);

    t.assert(ret.row_status.get(0)!.group_state === "change");
});

test("existing entry should set error contact_state", t => {
    let phonebook = new Phonebook.Phonebook();

    const gid = uuid.v4();

    phonebook = phonebook.apply(
        phonebook.dir_add(new Phonebook.Directory({ name: "Group", ord: 0 }), {
            id: gid,
        }),
    );

    phonebook = phonebook.apply(
        phonebook.contact_add(
            new Phonebook.Contact({
                name: "Alice",
                ord: 0,
                parent: gid,
                contact: "404",
            }),
        ),
    );

    const ret = bulk_import_precheck(
        phonebook,
        `Contact Name,Group,Phone Number,9-1-1 Transfer,Blind Transfer,Attended Transfer,Conference Transfer,Outbound
Alice,Group,111-1111,Y,Y,Y,Y,Y`,
    );

    t.assert(!ret.allow_import);
    t.assert(ret.row_status.get(0)!.contact_state === "error");
});

test("conflicting row_status should set error contact_state", t => {
    const ret = bulk_import_precheck(
        new Phonebook.Phonebook(),
        `Contact Name,Group,Phone Number,9-1-1 Transfer,Blind Transfer,Attended Transfer,Conference Transfer,Outbound
Alice,Group,111-1111,Y,Y,Y,Y,Y
Alice,Group,222-2222,N,Y,N,Y,Y`,
    );

    t.assert(ret.row_status.size === 2);
    t.assert(ret.row_status.get(0)!.contact_state === "error");
    t.assert(ret.row_status.get(1)!.contact_state === "error");
    t.assert(!ret.allow_import);
});

test("row_status should still conflict even with differing whitespace pre and post", t => {
    const ret = bulk_import_precheck(
        new Phonebook.Phonebook(),
        `Contact Name,Group,Phone Number,9-1-1 Transfer,Blind Transfer,Attended Transfer,Conference Transfer,Outbound
Alice , Group  ,111-1111,Y,Y,Y,Y,Y
 Alice,  Group ,222-2222,N,Y,N,Y,Y`,
    );

    t.assert(ret.row_status.size === 2);
    t.assert(ret.row_status.get(0)!.contact_state === "error");
    t.assert(ret.row_status.get(1)!.contact_state === "error");
    t.assert(!ret.allow_import);
});

test("phone numbers should be normalized", t => {
    const ret = bulk_import_precheck(
        new Phonebook.Phonebook(),
        `Contact Name,Group,Phone Number,9-1-1 Transfer,Blind Transfer,Attended Transfer,Conference Transfer,Outbound
Alice,Group,111-1111,Y,Y,Y,Y,Y
Bob,Group,(555) 555-5555,Y,Y,Y,Y,Y`,
    );

    t.assert(ret.row_status.size === 2);
    t.assert(ret.row_status.get(0)!.contact.contact === "1111111");
    t.assert(ret.row_status.get(1)!.contact.contact === "5555555555");
});

test("phone number with ID should override", t => {
    let phonebook = new Phonebook.Phonebook();

    const gid = uuid.v4();

    phonebook = phonebook.apply(
        phonebook.dir_add(new Phonebook.Directory({ name: "Group", ord: 0 }), {
            id: gid,
        }),
    );

    phonebook = phonebook.apply(
        phonebook.contact_add(
            new Phonebook.Contact({
                name: "Alice",
                contact: "404",
                ord: 0,
                parent: gid,
            }),
            { id: "0a13d7e7-23be-4b22-83ba-8764f5bde04f" },
        ),
    );

    const ret = bulk_import_precheck(
        phonebook,
        `Contact Name,Group,Phone Number,9-1-1 Transfer,Blind Transfer,Attended Transfer,Conference Transfer,Outbound,ID
Alice,Group,111-1111,Y,Y,Y,Y,Y,0a13d7e7-23be-4b22-83ba-8764f5bde04f`,
    );

    t.assert(ret.row_status.get(0)!.contact_state === "change");
    t.assert(
        ret.row_status.get(0)!.contact.id ===
            "0a13d7e7-23be-4b22-83ba-8764f5bde04f",
    );
    t.assert(ret.allow_import);
});

test("phone number with ID that doesnt exist should error", t => {
    let phonebook = new Phonebook.Phonebook();

    const gid = uuid.v4();

    phonebook = phonebook.apply(
        phonebook.dir_add(new Phonebook.Directory({ name: "Group", ord: 0 }), {
            id: gid,
        }),
    );

    phonebook = phonebook.apply(
        phonebook.contact_add(
            new Phonebook.Contact({
                name: "Alice",
                contact: "404",
                ord: 0,
                parent: gid,
            }),
            { id: "0a13d7e7-23be-4b22-83ba-8764f5bde04e" },
        ),
    );

    const ret = bulk_import_precheck(
        phonebook,
        `Contact Name,Group,Phone Number,9-1-1 Transfer,Blind Transfer,Attended Transfer,Conference Transfer,Outbound,ID
Alice,Group,111-1111,Y,Y,Y,Y,Y,0a13d7e7-23be-4b22-83ba-8764f5bde04f`,
    );

    t.assert(ret.row_status.get(0)!.contact_state === "error");
    t.assert(
        ret.row_status.get(0)!.contact.id ===
            "0a13d7e7-23be-4b22-83ba-8764f5bde04f",
    );
    t.assert(!ret.allow_import);
});

test("bad phone numbers should not allow import", t => {
    const ret = bulk_import_precheck(
        new Phonebook.Phonebook(),
        `Contact Name,Group,Phone Number,9-1-1 Transfer,Blind Transfer,Attended Transfer,Conference Transfer,Outbound
Alice,Group,notaPhone Number,Y,Y,Y,Y,Y
Bob,Group,(555) 555-5555,Y,Y,Y,Y,Y`,
    );

    t.assert(ret.row_status.size === 2);
    t.assert(ret.row_status.get(0)!.contact.contact === "notaPhone Number");
    t.assert(ret.row_status.get(0)!.number_state === "error");
    t.assert(ret.row_status.get(1)!.contact.contact === "5555555555");
    t.assert(!ret.allow_import);
});

test("if it exists in a different group that is fine", t => {
    let phonebook = new Phonebook.Phonebook();

    const gid = uuid.v4();

    phonebook = phonebook.apply(
        phonebook.dir_add(new Phonebook.Directory({ name: "Group", ord: 0 }), {
            id: gid,
        }),
    );

    phonebook = phonebook.apply(
        phonebook.dir_add(
            new Phonebook.Directory({ name: "Different", ord: 1 }),
        ),
    );

    phonebook = phonebook.apply(
        phonebook.contact_add(
            new Phonebook.Contact({
                name: "Eve",
                contact: "404",
                ord: 0,
                parent: gid,
            }),
        ),
    );

    const ret = bulk_import_precheck(
        phonebook,
        `Contact Name,Group,Phone Number,9-1-1 Transfer,Blind Transfer,Attended Transfer,Conference Transfer,Outbound
Eve,Different,333,Y,Y,Y,Y,Y`,
    );

    t.assert(ret.allow_import, "import should be allowed");
    t.assert(ret.row_status.get(0)!.contact_state === "add");
});
