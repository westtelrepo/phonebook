/* eslint-disable @typescript-eslint/strict-boolean-expressions */
import test from "ava";
import pg_promise, { IDatabase, IMain } from "pg-promise";

import * as schema from "../../db/schema";
import * as pg_tmp from "./_pg_tmp";

test("Migration constructor", t => {
    const mig = new schema.Migration({
        parent: "014cee37-fa1a-4581-9c2f-c65709ea2e57",
        sql: "CREATE TABLE something(name TEXT PRIMARY KEY);",
        name: "something",
        description: "something",
    });

    t.assert(mig.parent === "014cee37-fa1a-4581-9c2f-c65709ea2e57");
    t.assert(mig.sql === "CREATE TABLE something(name TEXT PRIMARY KEY);");
    t.assert(mig.state_uuid === "c4feb36b-f646-5688-9110-f2063d54923a");
});

test("Migration constructor with custom state_uuid", t => {
    const mig = new schema.Migration({
        parent: "014cee37-fa1a-4581-9c2f-c65709ea2e57",
        sql: "CREATE TABLE something(name TEXT PRIMARY KEY);",
        state_uuid: "dc35ff75-fa31-4fc8-a50d-920d95fb953f",
        name: "something",
        description: "something",
    });

    t.assert(mig.parent === "014cee37-fa1a-4581-9c2f-c65709ea2e57");
    t.assert(mig.sql === "CREATE TABLE something(name TEXT PRIMARY KEY);");
    t.assert(mig.state_uuid === "dc35ff75-fa31-4fc8-a50d-920d95fb953f");
});

test("single migration", t => {
    const m = new schema.Migration({
        parent: "903523de-5e0f-4d4c-b927-8bafdf0bcfef",
        sql: "not really sql",
        name: "name",
        description: "description",
    });

    const ms = schema.migrate([m], m.parent, m.state_uuid);

    if (!ms) {
        throw new Error("returned false");
    }

    t.assert(ms.length === 1);
    t.assert(ms[0].sql === "not really sql");
});

test("multiple migrations", t => {
    const m1 = new schema.Migration({
        parent: "903523de-5e0f-4d4c-b927-8bafdf0bcfef",
        sql: "m1",
        name: "m1",
        description: "m1",
    });

    const m2 = new schema.Migration({
        parent: m1.state_uuid,
        sql: "m2",
        name: "m2",
        description: "m2",
    });

    const m3 = new schema.Migration({
        parent: m2.state_uuid,
        sql: "m3",
        name: "m3",
        description: "m3",
    });

    const ms = schema.migrate([m3, m2, m1], m1.parent, m3.state_uuid);

    if (!ms) {
        throw new Error("returned false");
    }

    t.assert(ms.length === 3);
    t.assert(ms[0].sql === "m1");
    t.assert(ms[1].sql === "m2");
    t.assert(ms[2].sql === "m3");
});

test("migrate middle subset", t => {
    const m1 = new schema.Migration({
        parent: "903523de-5e0f-4d4c-b927-8bafdf0bcfef",
        sql: "m1",
        name: "m1",
        description: "m1",
    });

    const m2 = new schema.Migration({
        parent: m1.state_uuid,
        sql: "m2",
        name: "m2",
        description: "m2",
    });

    const m3 = new schema.Migration({
        parent: m2.state_uuid,
        sql: "m3",
        name: "m3",
        description: "m3",
    });

    const ms = schema.migrate([m3, m2, m1], m2.parent, m2.state_uuid);

    if (!ms) {
        throw new Error("returned false");
    }

    t.assert(ms.length === 1);
    t.assert(ms[0].sql === "m2");
});

test("migrate initial subset", t => {
    const m1 = new schema.Migration({
        parent: "903523de-5e0f-4d4c-b927-8bafdf0bcfef",
        sql: "m1",
        name: "m1",
        description: "m1",
    });

    const m2 = new schema.Migration({
        parent: m1.state_uuid,
        sql: "m2",
        name: "m2",
        description: "m2",
    });

    const m3 = new schema.Migration({
        parent: m2.state_uuid,
        sql: "m3",
        name: "m3",
        description: "m3",
    });

    const ms = schema.migrate([m3, m2, m1], m1.parent, m1.state_uuid);

    if (!ms) {
        throw new Error("returned false");
    }

    t.assert(ms.length === 1);
    t.assert(ms[0].sql === "m1");
});

test("migrate last subset", t => {
    const m1 = new schema.Migration({
        parent: "903523de-5e0f-4d4c-b927-8bafdf0bcfef",
        sql: "m1",
        name: "m1",
        description: "m1",
    });

    const m2 = new schema.Migration({
        parent: m1.state_uuid,
        sql: "m2",
        name: "m2",
        description: "m2",
    });

    const m3 = new schema.Migration({
        parent: m2.state_uuid,
        sql: "m3",
        name: "m3",
        description: "m3",
    });

    const ms = schema.migrate([m3, m2, m1], m3.parent, m3.state_uuid);

    if (!ms) {
        throw new Error("returned false");
    }

    t.assert(ms.length === 1);
    t.assert(ms[0].sql === "m3");
});

test("null migration", t => {
    const m1 = new schema.Migration({
        parent: "903523de-5e0f-4d4c-b927-8bafdf0bcfef",
        sql: "m1",
        name: "m1",
        description: "m1",
    });

    const m2 = new schema.Migration({
        parent: m1.state_uuid,
        sql: "m2",
        name: "m2",
        description: "m2",
    });

    const m3 = new schema.Migration({
        parent: m2.state_uuid,
        sql: "m3",
        name: "m3",
        description: "m3",
    });

    const ms = schema.migrate([m3, m2, m1], m3.parent, m3.parent);

    if (!ms) {
        throw new Error("returned false");
    }

    t.assert(ms.length === 0);
});

test("migrate unknown from", t => {
    const m1 = new schema.Migration({
        parent: "903523de-5e0f-4d4c-b927-8bafdf0bcfef",
        sql: "m1",
        name: "m1",
        description: "m1",
    });

    const m2 = new schema.Migration({
        parent: m1.state_uuid,
        sql: "m2",
        name: "m2",
        description: "m2",
    });

    const m3 = new schema.Migration({
        parent: m2.state_uuid,
        sql: "m3",
        name: "m3",
        description: "m3",
    });

    const ms = schema.migrate(
        [m3, m2, m1],
        "00bc3974-c513-4ef2-b4ea-ff3c123d422d",
        m3.state_uuid,
    );

    t.assert(!ms);
});

test("migrate unknown to", t => {
    const m1 = new schema.Migration({
        parent: "903523de-5e0f-4d4c-b927-8bafdf0bcfef",
        sql: "m1",
        name: "m1",
        description: "m1",
    });

    const m2 = new schema.Migration({
        parent: m1.state_uuid,
        sql: "m2",
        name: "m2",
        description: "m2",
    });

    const m3 = new schema.Migration({
        parent: m2.state_uuid,
        sql: "m3",
        name: "m3",
        description: "m3",
    });

    const ms = schema.migrate(
        [m3, m2, m1],
        m1.parent,
        "6280f572-51e6-47b0-9adf-0fa782d0ba11",
    );

    t.assert(!ms);
});

{
    let db: IDatabase<unknown>;
    let _pgp: IMain;

    // eslint-disable-next-line ava/hooks-order
    test.before(() => {
        _pgp = pg_promise();
        const uri = pg_tmp.create_temporary_database();
        db = _pgp(uri);
    });

    // eslint-disable-next-line ava/hooks-order
    test.after(() => {
        _pgp.end();
    });

    test("transactor default is serializable", async t => {
        await schema.transactor(db, async tx => {
            t.assert(
                (await tx.one("SHOW transaction_isolation"))
                    .transaction_isolation === "serializable",
            );

            t.assert(
                (await tx.one("SHOW transaction_read_only"))
                    .transaction_read_only === "off",
            );
        });
    });

    test("transactor readonly is serializable, readonly", async t => {
        await schema.transactor(
            db,
            async tx => {
                t.assert(
                    (await tx.one("SHOW transaction_isolation"))
                        .transaction_isolation === "serializable",
                );

                t.assert(
                    (await tx.one("SHOW transaction_read_only"))
                        .transaction_read_only === "on",
                );
            },
            "readonly",
        );
    });

    test("transactor ctx[SCHEMA_ID] should equal zeros on empty database", async t => {
        await schema.transactor(db, async tx => {
            t.assert(
                tx.ctx.context.schema_id ===
                    "00000000-0000-0000-0000-000000000000",
            );
        });
    });

    test("transactor can return a value", async t => {
        const value: string = await schema.transactor(db, async tx => {
            const result = (await tx.one("SELECT 'PostgreSQL' AS version"))
                .version;
            if (typeof result === "string") {
                return result;
            } else {
                throw new Error("got unexpected type");
            }
        });

        t.assert(value === "PostgreSQL");
    });

    // TODO: test retry logic on serialization failure
    // test/implement retry logic on database disconnection
}
