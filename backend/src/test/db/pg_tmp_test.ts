import test from "ava";
import pg_promise from "pg-promise";
import uuid from "uuid-1345";

import { create_temporary_database } from "./_pg_tmp";

test("create_temporary_database should work", async t => {
    const pgp = pg_promise();
    try {
        const uri = create_temporary_database();
        const database = pgp(uri);

        const s1 = uuid.v4();
        const s2 = uuid.v4();
        // make the database do an operation to ensure it is working
        const value = await database.one("SELECT $1 || $2 AS value", [s1, s2]);
        t.assert(s1 + s2 === value.value);
    } finally {
        pgp.end();
    }
});
