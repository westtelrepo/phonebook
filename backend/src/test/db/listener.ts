import test from "ava";
import pg_promise from "pg-promise";

import { Listener } from "../../db/listener";
import * as pg_tmp from "./_pg_tmp";

let db: pg_promise.IDatabase<unknown>;
let _pgp: pg_promise.IMain;

test.before(() => {
    _pgp = pg_promise();
    const uri = pg_tmp.create_temporary_database();
    db = _pgp(uri);
});

test.after(() => {
    _pgp.end();
});

test.serial("Listener should construct", t => {
    const L = new Listener({ db });
    L.done();
    t.pass();
});

test.serial("Listener should listen", async t => {
    const L = new Listener({ db });
    const promise = new Promise(resolve => {
        // eslint-disable-next-line @typescript-eslint/no-floating-promises
        L.listen("event", () => {
            resolve();
        });
    });
    await L.connect();
    await db.none("NOTIFY event");
    await promise;
    L.done();
    t.pass();
});

test.serial("Listener should listen to two channels", async t => {
    const L = new Listener({ db });
    const promise1 = new Promise(resolve => {
        // eslint-disable-next-line @typescript-eslint/no-floating-promises
        L.listen("event", () => {
            resolve();
        });
    });
    const promise2 = new Promise(resolve => {
        // eslint-disable-next-line @typescript-eslint/no-floating-promises
        L.listen("event2", () => {
            resolve();
        });
    });
    await L.connect();
    await db.none("NOTIFY event");
    await promise1;
    await db.none("NOTIFY event2");
    await promise2;
    L.done();
    t.pass();
});

test.serial("Listener unlisten should unlisten", async t => {
    const L = new Listener({ db });
    let i = 0;
    const listener = () => i++;
    await L.listen("add", listener);
    await L.connect();
    await db.none("NOTIFY add");
    await new Promise(resolve => setTimeout(resolve, 100));
    await L.unlisten("add", listener);
    await db.none("NOTIFY add");
    await new Promise(resolve => setTimeout(resolve, 100));
    t.assert(i === 1);
    L.done();
});

test.serial(
    "Listener unlisten should unlisten but only when all unlisten",
    async t => {
        const L = new Listener({ db });
        let i = 0;
        let j = 0;
        const listener = () => i++;
        const listener2 = () => j++;
        await L.listen("add", listener);
        await L.listen("add", listener2);
        await L.connect();
        await db.none("NOTIFY add");
        await new Promise(resolve => setTimeout(resolve, 100));
        await L.unlisten("add", listener);
        await db.none("NOTIFY add");
        await new Promise(resolve => setTimeout(resolve, 100));
        t.assert(i === 1);
        t.assert(j === 2);
        L.done();
    },
);
