import child_process from "child_process";

// TODO: list installed PostgreSQL versions
// TODO: set version of PostgreSQL to use

// a helper function for a test environment
export function create_temporary_database(): string {
    // pg-promise does not support unix sockets, so use a TCP socket with -t
    return child_process.execFileSync("pg_tmp", ["-t"], {
        env: {
            PATH: "/usr/lib/postgresql/9.6/bin/:" + process.env.PATH,
        },
        encoding: "utf8",
    });
}
