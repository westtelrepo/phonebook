/* eslint-disable @typescript-eslint/no-non-null-assertion */
import * as Phonebook from "@w/shared/dist/phonebook";
import test from "ava";
import canonify from "canonical-json";
import Immutable from "immutable";
import pg_promise, { IDatabase, IMain, ITask } from "pg-promise";
import uuid from "uuid-1345";

import * as migrations from "../../db/migrations";
import * as phonebook_db from "../../db/phonebook";
import { ISchemaTask, transactor } from "../../db/schema";
import * as db_main from "../../phonebook_db";
import * as pg_tmp from "./_pg_tmp";

{
    let db: IDatabase<unknown>;
    let _pgp: IMain;

    // these transactions always rollback
    const rollback_tx = async (
        f: (t: ISchemaTask<unknown>) => Promise<void>,
    ) => {
        const ROLLBACK = Symbol("rollback");
        try {
            await transactor(db, async t => {
                await f(t);
                // eslint-disable-next-line @typescript-eslint/no-throw-literal
                throw ROLLBACK;
            });
        } catch (e) {
            if (e !== ROLLBACK) {
                throw e;
            }
        }
    };

    test.before(async () => {
        _pgp = pg_promise({ noWarnings: true });
        const uri = pg_tmp.create_temporary_database();
        db = _pgp(uri);
        await db_main.migrate(db, migrations.mig.undo.state_uuid);

        await db.none("TRUNCATE phonebook_changes");
    });

    test.after(() => {
        _pgp.end();
    });

    test("simple get_state_t", async t => {
        await rollback_tx(async tx => {
            await tx.none(
                "INSERT INTO phonebook_changes(id, change_id, patch, inv_patch, state_hash, state) VALUES($(id), $(change_id), $(patch), $(inv_patch), $(state_hash), $(state))",
                {
                    id: "2ad78599-66a8-4eae-bcb6-34f4325fe5a8",
                    change_id: 0,
                    patch: [],
                    inv_patch: [],
                    state_hash:
                        "65afd1693eb3fa81478b42d1a509c4252b3f50d2459eca20c48943cfe56bfe9e",
                    state:
                        '{"change_id":0,"contacts":{},"dirs":{},"id":"2ad78599-66a8-4eae-bcb6-34f4325fe5a8","sd_dirs":{},"speed_dials":{}}',
                },
            );

            const state = await phonebook_db.get_state_t(
                tx,
                "2ad78599-66a8-4eae-bcb6-34f4325fe5a8",
                0,
            );

            t.assert(
                canonify(state.state) ===
                    '{"change_id":0,"contacts":{},"dirs":{},"id":"2ad78599-66a8-4eae-bcb6-34f4325fe5a8","sd_dirs":{},"speed_dials":{}}',
            );
        });
    });

    test("simple update_phonebook_t", async t => {
        await rollback_tx(async tx => {
            await tx.none(
                "INSERT INTO phonebook_changes(id, change_id, patch, inv_patch, state_hash, state) VALUES($(id), $(change_id), $(patch), $(inv_patch), $(state_hash), $(state))",
                {
                    id: "2ad78599-66a8-4eae-bcb6-34f4325fe5a8",
                    change_id: 0,
                    patch: [],
                    inv_patch: [],
                    state_hash:
                        "65afd1693eb3fa81478b42d1a509c4252b3f50d2459eca20c48943cfe56bfe9e",
                    state:
                        '{"change_id":0,"contacts":{},"dirs":{},"id":"2ad78599-66a8-4eae-bcb6-34f4325fe5a8","sd_dirs":{},"speed_dials":{}}',
                },
            );

            const op = new Phonebook.Operation({
                patch: [{ op: "add", path: "/name", value: "A Name" }],
                inv_patch: [{ op: "remove", path: "/name" }],
            });

            await phonebook_db.update_phonebook_t(
                tx,
                "2ad78599-66a8-4eae-bcb6-34f4325fe5a8",
                op,
                phonebook_db.empty_meta(),
            );

            const latest = await phonebook_db.get_state_t(
                tx,
                "2ad78599-66a8-4eae-bcb6-34f4325fe5a8",
                "latest",
            );
            t.assert(latest.change_id === 1);
            t.assert(
                canonify(latest.state) ===
                    '{"change_id":1,"contacts":{},"dirs":{},"id":"2ad78599-66a8-4eae-bcb6-34f4325fe5a8","name":"A Name","sd_dirs":{},"speed_dials":{}}',
            );
            t.assert(
                latest.state_hash ===
                    "c2d4acea346e27a49ac376c6b927d59788e6abc4aaa2f3370b6e6cc7d1a14a49",
            );
            t.assert(latest.id === "2ad78599-66a8-4eae-bcb6-34f4325fe5a8");
        });
    });

    test("update_phonebook_t where inv_patch is wrong", async t => {
        await rollback_tx(async tx => {
            await tx.none(
                "INSERT INTO phonebook_changes(id, change_id, patch, inv_patch, state_hash, state) VALUES($(id), $(change_id), $(patch), $(inv_patch), $(state_hash), $(state))",
                {
                    id: "2ad78599-66a8-4eae-bcb6-34f4325fe5a8",
                    change_id: 0,
                    patch: [],
                    inv_patch: [],
                    state_hash:
                        "65afd1693eb3fa81478b42d1a509c4252b3f50d2459eca20c48943cfe56bfe9e",
                    state:
                        '{"change_id":0,"contacts":{},"dirs":{},"id":"2ad78599-66a8-4eae-bcb6-34f4325fe5a8","sd_dirs":{},"speed_dials":{}}',
                },
            );

            const op = new Phonebook.Operation({
                patch: [{ op: "add", path: "/name", value: "A Name" }],
                inv_patch: [{ op: "add", path: "/name", value: "nooo" }],
            });

            await t.throwsAsync(async () => {
                await phonebook_db.update_phonebook_t(
                    tx,
                    "2ad78599-66a8-4eae-bcb6-34f4325fe5a8",
                    op,
                    phonebook_db.empty_meta(),
                );
            });
        });
    });

    test("update_phonebook_t where inv_patch does not apply", async t => {
        await rollback_tx(async tx => {
            await tx.none(
                "INSERT INTO phonebook_changes(id, change_id, patch, inv_patch, state_hash, state) VALUES($(id), $(change_id), $(patch), $(inv_patch), $(state_hash), $(state))",
                {
                    id: "2ad78599-66a8-4eae-bcb6-34f4325fe5a8",
                    change_id: 0,
                    patch: [],
                    inv_patch: [],
                    state_hash:
                        "65afd1693eb3fa81478b42d1a509c4252b3f50d2459eca20c48943cfe56bfe9e",
                    state:
                        '{"change_id":0,"contacts":{},"dirs":{},"id":"2ad78599-66a8-4eae-bcb6-34f4325fe5a8","sd_dirs":{},"speed_dials":{}}',
                },
            );

            const op = new Phonebook.Operation({
                patch: [{ op: "add", path: "/name", value: "A Name" }],
                inv_patch: [{ op: "remove", path: "/nonexistent" }],
            });

            await t.throwsAsync(async () => {
                await phonebook_db.update_phonebook_t(
                    tx,
                    "2ad78599-66a8-4eae-bcb6-34f4325fe5a8",
                    op,
                    phonebook_db.empty_meta(),
                );
            });
        });
    });

    test("update_phonebook_t add bad attribute", async t => {
        await rollback_tx(async tx => {
            await tx.none(
                "INSERT INTO phonebook_changes(id, change_id, patch, inv_patch, state_hash, state) VALUES($(id), $(change_id), $(patch), $(inv_patch), $(state_hash), $(state))",
                {
                    id: "2ad78599-66a8-4eae-bcb6-34f4325fe5a8",
                    change_id: 0,
                    patch: [],
                    inv_patch: [],
                    state_hash:
                        "65afd1693eb3fa81478b42d1a509c4252b3f50d2459eca20c48943cfe56bfe9e",
                    state:
                        '{"change_id":0,"contacts":{},"dirs":{},"id":"2ad78599-66a8-4eae-bcb6-34f4325fe5a8","sd_dirs":{},"speed_dials":{}}',
                },
            );

            const op = new Phonebook.Operation({
                patch: [{ op: "add", path: "/nonexistent", value: "Value" }],
                inv_patch: [{ op: "remove", path: "/nonexistent" }],
            });

            await t.throwsAsync(async () => {
                await phonebook_db.update_phonebook_t(
                    tx,
                    "2ad78599-66a8-4eae-bcb6-34f4325fe5a8",
                    op,
                    phonebook_db.empty_meta(),
                );
            });
        });
    });

    test("update_phonebook_t add bad value", async t => {
        await rollback_tx(async tx => {
            await tx.none(
                "INSERT INTO phonebook_changes(id, change_id, patch, inv_patch, state_hash, state) VALUES($(id), $(change_id), $(patch), $(inv_patch), $(state_hash), $(state))",
                {
                    id: "2ad78599-66a8-4eae-bcb6-34f4325fe5a8",
                    change_id: 0,
                    patch: [],
                    inv_patch: [],
                    state_hash:
                        "65afd1693eb3fa81478b42d1a509c4252b3f50d2459eca20c48943cfe56bfe9e",
                    state:
                        '{"change_id":0,"contacts":{},"dirs":{},"id":"2ad78599-66a8-4eae-bcb6-34f4325fe5a8","sd_dirs":{},"speed_dials":{}}',
                },
            );

            const op = new Phonebook.Operation({
                patch: [{ op: "add", path: "/name", value: 33 }],
                inv_patch: [{ op: "remove", path: "/name" }],
            });

            await t.throwsAsync(async () => {
                await phonebook_db.update_phonebook_t(
                    tx,
                    "2ad78599-66a8-4eae-bcb6-34f4325fe5a8",
                    op,
                    phonebook_db.empty_meta(),
                );
            });
        });
    });

    test("get_state_t should go back one", async t => {
        await rollback_tx(async tx => {
            await tx.none(
                "INSERT INTO phonebook_changes(id, change_id, patch, inv_patch, state_hash, state) VALUES($(id), $(change_id), $(patch), $(inv_patch), $(state_hash), $(state))",
                {
                    id: "2ad78599-66a8-4eae-bcb6-34f4325fe5a8",
                    change_id: 0,
                    patch: [],
                    inv_patch: [],
                    state_hash:
                        "65afd1693eb3fa81478b42d1a509c4252b3f50d2459eca20c48943cfe56bfe9e",
                    state:
                        '{"change_id":0,"contacts":{},"dirs":{},"id":"2ad78599-66a8-4eae-bcb6-34f4325fe5a8","sd_dirs":{},"speed_dials":{}}',
                },
            );

            const op = new Phonebook.Operation({
                patch: [{ op: "add", path: "/name", value: "A Name" }],
                inv_patch: [{ op: "remove", path: "/name" }],
            });

            await phonebook_db.update_phonebook_t(
                tx,
                "2ad78599-66a8-4eae-bcb6-34f4325fe5a8",
                op,
                phonebook_db.empty_meta(),
            );

            await tx.none(
                "UPDATE phonebook_changes SET state = NULL WHERE change_id = 0",
            );

            const state = await phonebook_db.get_state_t(
                tx,
                "2ad78599-66a8-4eae-bcb6-34f4325fe5a8",
                0,
            );
            t.assert(
                canonify(state.state) ===
                    '{"change_id":0,"contacts":{},"dirs":{},"id":"2ad78599-66a8-4eae-bcb6-34f4325fe5a8","sd_dirs":{},"speed_dials":{}}',
                "state 0 must be the same",
            );
        });
    });

    test("get_state_t should go back two", async t => {
        await rollback_tx(async tx => {
            await tx.none(
                "INSERT INTO phonebook_changes(id, change_id, patch, inv_patch, state_hash, state) VALUES($(id), $(change_id), $(patch), $(inv_patch), $(state_hash), $(state))",
                {
                    id: "2ad78599-66a8-4eae-bcb6-34f4325fe5a8",
                    change_id: 0,
                    patch: [],
                    inv_patch: [],
                    state_hash:
                        "65afd1693eb3fa81478b42d1a509c4252b3f50d2459eca20c48943cfe56bfe9e",
                    state:
                        '{"change_id":0,"contacts":{},"dirs":{},"id":"2ad78599-66a8-4eae-bcb6-34f4325fe5a8","sd_dirs":{},"speed_dials":{}}',
                },
            );

            const op = new Phonebook.Operation({
                patch: [{ op: "add", path: "/name", value: "A Name" }],
                inv_patch: [{ op: "remove", path: "/name" }],
            });

            await phonebook_db.update_phonebook_t(
                tx,
                "2ad78599-66a8-4eae-bcb6-34f4325fe5a8",
                op,
                phonebook_db.empty_meta(),
            );

            const op2 = new Phonebook.Operation({
                patch: [{ op: "replace", path: "/name", value: "Newer Name" }],
                inv_patch: [{ op: "replace", path: "/name", value: "A Name" }],
            });

            await phonebook_db.update_phonebook_t(
                tx,
                "2ad78599-66a8-4eae-bcb6-34f4325fe5a8",
                op2,
                phonebook_db.empty_meta(),
            );

            await tx.none(
                "UPDATE phonebook_changes SET state = NULL WHERE change_id <> 2",
            );

            const state = await phonebook_db.get_state_t(
                tx,
                "2ad78599-66a8-4eae-bcb6-34f4325fe5a8",
                0,
            );
            t.assert(
                canonify(state.state) ===
                    '{"change_id":0,"contacts":{},"dirs":{},"id":"2ad78599-66a8-4eae-bcb6-34f4325fe5a8","sd_dirs":{},"speed_dials":{}}',
                "state 0 must be the same",
            );
        });
    });

    test("update_phonebook_t large import", async t => {
        t.timeout(10000);
        await rollback_tx(async tx => {
            await tx.none(
                "INSERT INTO phonebook_changes(id, change_id, patch, inv_patch, state_hash, state) VALUES($(id), $(change_id), $(patch), $(inv_patch), $(state_hash), $(state))",
                {
                    id: "2ad78599-66a8-4eae-bcb6-34f4325fe5a8",
                    change_id: 0,
                    patch: [],
                    inv_patch: [],
                    state_hash:
                        "65afd1693eb3fa81478b42d1a509c4252b3f50d2459eca20c48943cfe56bfe9e",
                    state:
                        '{"change_id":0,"contacts":{},"dirs":{},"id":"2ad78599-66a8-4eae-bcb6-34f4325fe5a8","sd_dirs":{},"speed_dials":{}}',
                },
            );

            let current = new Phonebook.Phonebook(
                (
                    await phonebook_db.get_state_t(
                        tx,
                        "2ad78599-66a8-4eae-bcb6-34f4325fe5a8",
                        "latest",
                    )
                ).state,
            );

            let op = current.dir_add(
                new Phonebook.Directory({ name: "Group", ord: "1" }),
                { id: "1fa08fd5-44fb-4fe5-a9b3-60632f20b717" },
            );

            current = current._apply(op);

            for (let i = 0; i < 2000; i++) {
                const small_op = current.contact_add(
                    new Phonebook.Contact({
                        name: "Contact " + i,
                        contact: "333",
                        ord: i,
                        parent: "1fa08fd5-44fb-4fe5-a9b3-60632f20b717",
                    }),
                    // we use the v5 UUIDs to make the UUIDs deterministic (so we can check the state_hash later)
                    {
                        id: uuid.v5({
                            namespace: "8cc10c33-03c5-400a-a49c-f7e93e250e6c",
                            name: String(i),
                        }),
                    },
                );
                op = op.concat(small_op);
            }

            await phonebook_db.update_phonebook_t(
                tx,
                "2ad78599-66a8-4eae-bcb6-34f4325fe5a8",
                op,
                phonebook_db.empty_meta(),
            );

            const latest = await phonebook_db.get_state_t(
                tx,
                "2ad78599-66a8-4eae-bcb6-34f4325fe5a8",
                "latest",
            );
            t.assert(latest.change_id === 1);
            t.assert(latest.id === "2ad78599-66a8-4eae-bcb6-34f4325fe5a8");
            t.assert(
                latest.state_hash ===
                    "1fc16788d0c9a7964b897b8c32faec74905420bcee9df9cf4043746d707d14e9",
            );
        });
    });

    test("create_phonebook_t should work", async t => {
        await rollback_tx(async tx => {
            await phonebook_db.create_phonebook_t(
                tx,
                "7c5f2bce-12ef-47ce-a116-188fb22b7fa7",
                "/nonexistent",
            );

            const value = await tx.one(
                "SELECT * FROM phonebook_changes WHERE id = $(id)",
                { id: "7c5f2bce-12ef-47ce-a116-188fb22b7fa7" },
            );

            t.assert(value.id === "7c5f2bce-12ef-47ce-a116-188fb22b7fa7");
            t.assert(value.state.filename === "/nonexistent");
            t.assert(value.change_id === "0");
        });
    });

    test("create_phonebook_t default values should work", async t => {
        await rollback_tx(async tx => {
            const id = await phonebook_db.create_phonebook_t(tx);

            const value = await tx.one(
                "SELECT * FROM phonebook_changes WHERE id = $(id)",
                { id },
            );

            t.assert(value.id === id);
            t.assert(typeof value.filename === "undefined");
        });
    });

    test("get_latest_t should work", async t => {
        await rollback_tx(async tx => {
            const id = await phonebook_db.create_phonebook_t(tx);
            const ph = await phonebook_db.get_latest_t(tx, id);
            t.assert(ph.id === id);
            t.assert(ph.change_id === 0);
        });
    });

    test("get_latest_t should work with a change", async t => {
        await rollback_tx(async tx => {
            const id = await phonebook_db.create_phonebook_t(tx);
            const initial = await phonebook_db.get_latest_t(tx, id);
            await phonebook_db.update_phonebook_t(
                tx,
                id,
                initial.dir_add(
                    new Phonebook.Directory({ name: "hello", ord: "3" }),
                ),
                phonebook_db.empty_meta(),
            );
            const ph = await phonebook_db.get_latest_t(tx, id);
            t.assert(ph.id === id);
            t.assert(ph.change_id === 1, "change_id should be 1");
            t.assert(ph.dirs.size === 1, "dirs.size should be 1");
        });
    });

    test("get_phonebook_list_t should work", async t => {
        await rollback_tx(async tx => {
            const list = await phonebook_db.get_phonebook_list_t(tx);

            t.assert(Array.isArray(list));

            t.assert(list.length === 0);
        });
    });

    test("get_phonebook_list_t should return one", async t => {
        await rollback_tx(async tx => {
            const id = await phonebook_db.create_phonebook_t(tx);

            const list = await phonebook_db.get_phonebook_list_t(tx);

            t.assert(list.length === 1);
            t.assert(list[0].id === id);
        });
    });

    test("get_phonebook_list_t should return multiple", async t => {
        await rollback_tx(async tx => {
            const id1 = await phonebook_db.create_phonebook_t(tx);
            const id2 = await phonebook_db.create_phonebook_t(tx);
            const id3 = await phonebook_db.create_phonebook_t(tx);

            const list = await phonebook_db.get_phonebook_list_t(tx);

            t.assert(list.length === 3);

            const list_set = Immutable.Set.of(
                list[0].id,
                list[1].id,
                list[2].id,
            );

            t.assert(list_set.size === 3);

            t.assert(Immutable.is(list_set, Immutable.Set.of(id1, id2, id3)));
        });
    });

    test("get_phonebook_list_t should work even on update", async t => {
        await rollback_tx(async tx => {
            const id = await phonebook_db.create_phonebook_t(tx);

            const op = new Phonebook.Operation({
                patch: [{ op: "add", path: "/name", value: "A Name" }],
                inv_patch: [{ op: "remove", path: "/name" }],
            });

            await phonebook_db.update_phonebook_t(
                tx,
                id,
                op,
                phonebook_db.empty_meta(),
            );

            const list = await phonebook_db.get_phonebook_list_t(tx);

            t.assert(list.length === 1);
            t.assert(list[0].id === id);
        });
    });

    test("get_phonebook_list_t should work multiple even on update", async t => {
        await rollback_tx(async tx => {
            const id1 = await phonebook_db.create_phonebook_t(tx);
            const id2 = await phonebook_db.create_phonebook_t(tx);
            const id3 = await phonebook_db.create_phonebook_t(tx);

            const op = new Phonebook.Operation({
                patch: [{ op: "add", path: "/name", value: "A Name" }],
                inv_patch: [{ op: "remove", path: "/name" }],
            });

            await phonebook_db.update_phonebook_t(
                tx,
                id1,
                op,
                phonebook_db.empty_meta(),
            );
            await phonebook_db.update_phonebook_t(
                tx,
                id3,
                op,
                phonebook_db.empty_meta(),
            );

            const list = await phonebook_db.get_phonebook_list_t(tx);

            t.assert(list.length === 3);

            const list_set = Immutable.Set.of(
                list[0].id,
                list[1].id,
                list[2].id,
            );

            t.assert(list_set.size === 3);

            t.assert(Immutable.is(list_set, Immutable.Set.of(id1, id2, id3)));
        });
    });

    {
        let dirs: Immutable.Map<string, Phonebook.Directory> = Immutable.Map();
        const id = uuid.v4();
        const gid = uuid.v4();
        dirs = dirs.set(
            gid,
            new Phonebook.Directory({
                icon: "what.png",
                name: "hello",
                ord: 1,
            }),
        );
        const phonebook = new Phonebook.Phonebook({ id, dirs });

        // eslint-disable-next-line ava/hooks-order
        test.before(t => {
            phonebook.validate();
            t.assert(phonebook.dirs.size === 1, "have one group defined");
        });

        test("should work", async t => {
            await rollback_tx(async tx => {
                await phonebook_db.create_t(tx, phonebook);

                await phonebook_db.patch_directory_t(
                    tx,
                    id,
                    gid,
                    { name: "a new name", icon: "new.ico" },
                    phonebook_db.empty_meta(),
                );

                const ph = await phonebook_db.get_latest_t(tx, id);

                t.assert(ph.dirs.get(gid)!.name === "a new name");

                t.assert(ph.dirs.get(gid)!.icon === "new.ico");

                t.assert(ph.change_id === 1, "change_id should be 1");
            });
        });

        test("set only name", async t => {
            await rollback_tx(async tx => {
                await phonebook_db.create_t(tx, phonebook);

                await phonebook_db.patch_directory_t(
                    tx,
                    id,
                    gid,
                    { name: "a new name" },
                    phonebook_db.empty_meta(),
                );

                const ph = await phonebook_db.get_latest_t(tx, id);

                t.assert(ph.dirs.get(gid)!.name === "a new name");

                t.assert(ph.dirs.get(gid)!.icon === "what.png");

                t.assert(ph.change_id === 1, "change_id should be 1");
            });
        });

        test("set only icon", async t => {
            await rollback_tx(async tx => {
                await phonebook_db.create_t(tx, phonebook);

                await phonebook_db.patch_directory_t(
                    tx,
                    id,
                    gid,
                    { icon: "hello.ico" },
                    phonebook_db.empty_meta(),
                );

                const ph = await phonebook_db.get_latest_t(tx, id);

                t.assert(ph.dirs.get(gid)!.name === "hello");

                t.assert(ph.dirs.get(gid)!.icon === "hello.ico");

                t.assert(ph.change_id === 1, "change_id should be 1");
            });
        });

        test("remove icon", async t => {
            await rollback_tx(async tx => {
                await phonebook_db.create_t(tx, phonebook);

                await phonebook_db.patch_directory_t(
                    tx,
                    id,
                    gid,
                    { icon: null },
                    phonebook_db.empty_meta(),
                );

                const ph = await phonebook_db.get_latest_t(tx, id);

                t.assert(ph.dirs.get(gid)!.name === "hello");

                t.assert(ph.dirs.get(gid)!.icon === undefined);

                t.assert(ph.change_id === 1, "change_id should be 1");
            });
        });
    }
}

for (const state_uuid of [
    migrations.mig.phonebook.state_uuid,
    migrations.mig.users.state_uuid,
]) {
    let db: IDatabase<unknown>;
    let _pgp: IMain;

    // these transactions always rollback
    const rollback_tx = async (
        f: (t: ISchemaTask<unknown>) => Promise<void>,
    ) => {
        const ROLLBACK = Symbol("rollback");
        try {
            await transactor(db, async t => {
                await f(t);
                // eslint-disable-next-line @typescript-eslint/no-throw-literal
                throw ROLLBACK;
            });
        } catch (e) {
            if (e !== ROLLBACK) {
                throw e;
            }
        }
    };

    // eslint-disable-next-line ava/hooks-order
    test.before(async () => {
        _pgp = pg_promise({ noWarnings: true });
        const uri = pg_tmp.create_temporary_database();
        db = _pgp(uri);
        await db_main.migrate(db, state_uuid);

        // delete everything that the migration might have initialized
        await db.none(
            "TRUNCATE phonebook, phonebook_groups, phonebook_numbers, phonebook_sd, phonebook_sd_groups",
        );
    });

    // eslint-disable-next-line ava/hooks-order
    test.after(() => {
        _pgp.end();
    });

    test(`create_phonebook_t should add to database (${state_uuid})`, async t => {
        await rollback_tx(async tx => {
            await phonebook_db.create_phonebook_t(
                tx,
                "7c5f2bce-12ef-47ce-a116-188fb22b7fa7",
                "/filename",
            );

            const value = await tx.one(
                "SELECT * FROM phonebook WHERE pid = $(id)",
                { id: "7c5f2bce-12ef-47ce-a116-188fb22b7fa7" },
            );

            t.assert(value.pid === "7c5f2bce-12ef-47ce-a116-188fb22b7fa7");
            t.assert(value.filename === "/filename");
        });
    });

    test(`create_phonebook_t default values should work (${state_uuid})`, async t => {
        await rollback_tx(async tx => {
            const id = await phonebook_db.create_phonebook_t(tx);

            const value = await tx.one(
                "SELECT * FROM phonebook WHERE pid = $(id)",
                { id },
            );

            t.assert(value.pid === id);
            t.assert(
                value.filename ===
                    "/datadisk1/ng911/config/ng911_phonebook.xml",
            );
        });
    });

    const create_group = async (
        t: ITask<unknown>,
        grp: {
            pid: string;
            gname: string;
            icon: string | null;
            ord: number;
        },
    ): Promise<string> => {
        const gid = uuid.v4();
        await t.none(
            "INSERT INTO phonebook_groups(gid, pid, gname, icon, ord) VALUES($(gid), $(pid), $(gname), $(icon), $(ord))",
            { ...grp, gid },
        );
        return gid;
    };

    test(`get_latest_t should work (${state_uuid})`, async t => {
        await rollback_tx(async tx => {
            const id = await phonebook_db.create_phonebook_t(tx);

            const ph = await phonebook_db.get_latest_t(tx, id);

            t.assert(ph.change_id === 0);
            t.assert(ph.id === id);
            t.assert(ph.name === undefined);
            t.assert(ph.dirs.size === 0);
            t.assert(ph.sd_dirs.size === 0);
            t.assert(ph.contacts.size === 0);
            t.assert(ph.speed_dials.size === 0);
            t.assert(
                ph.filename === "/datadisk1/ng911/config/ng911_phonebook.xml",
            );
            t.assert(ph.current_change_id === undefined);
        });
    });

    test(`should work with some groups (${state_uuid})`, async t => {
        await rollback_tx(async tx => {
            const pid = await phonebook_db.create_phonebook_t(tx);

            const g1 = await create_group(tx, {
                pid,
                gname: "hello",
                icon: null,
                ord: 1,
            });
            const g2 = await create_group(tx, {
                pid,
                gname: "hello 2",
                icon: "text.png",
                ord: 2,
            });

            const ph = await phonebook_db.get_latest_t(tx, pid);

            t.assert(ph.change_id === 0);
            t.assert(ph.id === pid);
            t.assert(ph.name === undefined);
            t.assert(ph.dirs.size === 2);

            t.assert(
                Immutable.is(
                    ph.dirs.get(g1),
                    new Phonebook.Directory({
                        icon: undefined,
                        name: "hello",
                        ord: 0,
                    }),
                ),
                "group g1 should be the same",
            );

            t.assert(
                Immutable.is(
                    ph.dirs.get(g2),
                    new Phonebook.Directory({
                        icon: "text.png",
                        name: "hello 2",
                        ord: 1,
                    }),
                ),
                "group g2 should be the same",
            );
            t.assert(ph.sd_dirs.size === 0);
            t.assert(ph.contacts.size === 0);
            t.assert(ph.speed_dials.size === 0);
            t.assert(
                ph.filename === "/datadisk1/ng911/config/ng911_phonebook.xml",
            );
            t.assert(ph.current_change_id === undefined);
        });
    });

    const create_contact = async (
        t: ITask<unknown>,
        num: {
            gid: string;
            nname: string;
            number: string;
            ng911: boolean;
            blind: boolean;
            attended: boolean;
            conference: boolean;
            outbound: boolean;
            icon: string | null;
            ord: number;
        },
    ): Promise<string> => {
        const nid = uuid.v4();
        await t.none(
            `INSERT INTO phonebook_numbers(nid, gid, nname, number, ng911, blind, attended, conference, outbound, icon, ord)
                    VALUES($(nid), $(gid), $(nname), $(number), $(ng911), $(blind), $(attended), $(conference), $(outbound), $(icon), $(ord))`,
            { ...num, nid },
        );
        return nid;
    };

    test(`should work with some entries (${state_uuid})`, async t => {
        await rollback_tx(async tx => {
            const pid = await phonebook_db.create_phonebook_t(tx);

            const g1 = await create_group(tx, {
                pid,
                gname: "hello",
                icon: null,
                ord: 1,
            });
            const g3 = await create_group(tx, {
                pid,
                gname: "empty",
                icon: null,
                ord: 4,
            });
            const g2 = await create_group(tx, {
                pid,
                gname: "hello 2",
                icon: "text.png",
                ord: 2,
            });

            // in g1
            const c1 = await create_contact(tx, {
                gid: g1,
                nname: "what",
                number: "333",
                ng911: true,
                blind: false,
                attended: true,
                conference: true,
                outbound: false,
                icon: null,
                ord: 3,
            });
            const c2 = await create_contact(tx, {
                gid: g1,
                nname: "what 2",
                number: "444",
                ng911: false,
                blind: true,
                attended: false,
                conference: false,
                outbound: true,
                icon: "big.png",
                ord: 1,
            });

            // in g2
            const c3 = await create_contact(tx, {
                gid: g2,
                nname: "what 2",
                number: "303",
                ng911: true,
                blind: true,
                attended: true,
                conference: true,
                outbound: true,
                icon: "icon.ico",
                ord: 4,
            });
            const c4 = await create_contact(tx, {
                gid: g2,
                nname: "ho ho ho",
                number: "222",
                ng911: false,
                blind: false,
                attended: false,
                conference: false,
                outbound: false,
                icon: "small.ico",
                ord: 9,
            });
            const c5 = await create_contact(tx, {
                gid: g2,
                nname: "eve",
                number: "222",
                ng911: false,
                blind: false,
                attended: false,
                conference: false,
                outbound: false,
                icon: "small.ico",
                ord: 1,
            });

            const ph = await phonebook_db.get_latest_t(tx, pid);

            t.assert(ph.change_id === 0);
            t.assert(ph.id === pid);
            t.assert(ph.name === undefined);
            t.assert(ph.dirs.size === 3);
            t.assert(ph.contacts.size === 5);

            t.assert(
                Immutable.is(
                    ph.dirs.get(g1),
                    new Phonebook.Directory({
                        name: "hello",
                        icon: undefined,
                        ord: 0,
                    }),
                ),
                "g1 should be correct",
            );
            t.assert(
                Immutable.is(
                    ph.dirs.get(g2),
                    new Phonebook.Directory({
                        name: "hello 2",
                        icon: "text.png",
                        ord: 1,
                    }),
                ),
                "g2 should be correct",
            );
            t.assert(
                Immutable.is(
                    ph.dirs.get(g3),
                    new Phonebook.Directory({
                        name: "empty",
                        icon: undefined,
                        ord: 2,
                    }),
                ),
                "g3 should be correct",
            );

            // group g1
            t.assert(
                Immutable.is(
                    ph.contacts.get(c1),
                    new Phonebook.Contact({
                        name: "what",
                        contact: "333",
                        xfer_ng911: true,
                        xfer_blind: false,
                        xfer_attended: true,
                        xfer_conference: true,
                        xfer_outbound: false,
                        icon: undefined,
                        ord: 1,
                        parent: g1,
                    }),
                ),
                "c1 should be correct",
            );

            t.assert(
                Immutable.is(
                    ph.contacts.get(c2),
                    new Phonebook.Contact({
                        name: "what 2",
                        contact: "444",
                        xfer_ng911: false,
                        xfer_blind: true,
                        xfer_attended: false,
                        xfer_conference: false,
                        xfer_outbound: true,
                        icon: "big.png",
                        ord: 0,
                        parent: g1,
                    }),
                ),
                "c2 should be correct",
            );

            // group g2
            t.assert(
                Immutable.is(
                    ph.contacts.get(c3),
                    new Phonebook.Contact({
                        name: "what 2",
                        contact: "303",
                        xfer_ng911: true,
                        xfer_blind: true,
                        xfer_attended: true,
                        xfer_conference: true,
                        xfer_outbound: true,
                        icon: "icon.ico",
                        ord: 1,
                        parent: g2,
                    }),
                ),
                "c3 should be correct",
            );

            t.assert(
                Immutable.is(
                    ph.contacts.get(c4),
                    new Phonebook.Contact({
                        name: "ho ho ho",
                        contact: "222",
                        xfer_ng911: false,
                        xfer_blind: false,
                        xfer_attended: false,
                        xfer_conference: false,
                        xfer_outbound: false,
                        icon: "small.ico",
                        ord: 2,
                        parent: g2,
                    }),
                ),
                "c4 should be correct",
            );

            t.assert(
                Immutable.is(
                    ph.contacts.get(c5),
                    new Phonebook.Contact({
                        name: "eve",
                        contact: "222",
                        xfer_ng911: false,
                        xfer_blind: false,
                        xfer_attended: false,
                        xfer_conference: false,
                        xfer_outbound: false,
                        icon: "small.ico",
                        ord: 0,
                        parent: g2,
                    }),
                ),
                "c5 should be correct",
            );

            t.assert(ph.sd_dirs.size === 0);
            t.assert(ph.speed_dials.size === 0);
            t.assert(
                ph.filename === "/datadisk1/ng911/config/ng911_phonebook.xml",
            );
            t.assert(ph.current_change_id === undefined);
        });
    });

    const create_sd_group = async (
        t: ITask<unknown>,
        sdg: {
            pid: string;
            gname: string;
            ord: number;
        },
    ): Promise<string> => {
        const sdgid = uuid.v4();
        await t.none(
            "INSERT INTO phonebook_sd_groups(sdgid, pid, gname, ord) VALUES($(sdgid), $(pid), $(gname), $(ord))",
            { ...sdg, sdgid },
        );
        return sdgid;
    };

    test(`should work with some sd groups (${state_uuid})`, async t => {
        await rollback_tx(async tx => {
            const pid = await phonebook_db.create_phonebook_t(tx);

            const g1 = await create_sd_group(tx, {
                pid,
                gname: "group 2",
                ord: 2,
            });
            const g2 = await create_sd_group(tx, {
                pid,
                gname: "group 1",
                ord: 1,
            });

            const ph = await phonebook_db.get_latest_t(tx, pid);

            t.assert(ph.change_id === 0);
            t.assert(ph.id === pid);
            t.assert(ph.name === undefined);
            t.assert(ph.dirs.size === 0);
            t.assert(ph.sd_dirs.size === 2);

            t.assert(
                Immutable.is(
                    ph.sd_dirs.get(g1),
                    new Phonebook.Directory({
                        icon: undefined,
                        name: "group 2",
                        ord: 1,
                    }),
                ),
                "group g1 should be the same",
            );

            t.assert(
                Immutable.is(
                    ph.sd_dirs.get(g2),
                    new Phonebook.Directory({
                        icon: undefined,
                        name: "group 1",
                        ord: 0,
                    }),
                ),
                "group g2 should be the same",
            );

            t.assert(ph.contacts.size === 0);
            t.assert(ph.speed_dials.size === 0);
            t.assert(
                ph.filename === "/datadisk1/ng911/config/ng911_phonebook.xml",
            );
            t.assert(ph.current_change_id === undefined);
        });
    });

    const create_sd = async (
        t: ITask<unknown>,
        sd: {
            sdgid: string;
            nid: string;
            action: Phonebook.sd_action;
            icon: string | null;
            ord: number;
        },
    ): Promise<string> => {
        const sdid = uuid.v4();
        await t.none(
            "INSERT INTO phonebook_sd(sdid, sdgid, nid, action, icon, ord) VALUES($(sdid), $(sdgid), $(nid), $(action), $(icon), $(ord))",
            { ...sd, sdid },
        );
        return sdid;
    };

    test(`should work with some sd entries (${state_uuid})`, async t => {
        await rollback_tx(async tx => {
            const pid = await phonebook_db.create_phonebook_t(tx);

            const g = await create_group(tx, {
                pid,
                gname: "group",
                icon: null,
                ord: 1,
            });
            const n1 = await create_contact(tx, {
                gid: g,
                nname: "one",
                number: "333",
                ng911: true,
                blind: true,
                attended: true,
                conference: true,
                outbound: true,
                icon: null,
                ord: 1,
            });
            const n2 = await create_contact(tx, {
                gid: g,
                nname: "two",
                number: "333",
                ng911: true,
                blind: true,
                attended: true,
                conference: true,
                outbound: true,
                icon: "hello.png",
                ord: 2,
            });

            const g1 = await create_sd_group(tx, {
                pid,
                gname: "hello",
                ord: 1,
            });
            const g3 = await create_sd_group(tx, {
                pid,
                gname: "empty",
                ord: 4,
            });
            const g2 = await create_sd_group(tx, {
                pid,
                gname: "hello 2",
                ord: 2,
            });

            // in g1
            const s1 = await create_sd(tx, {
                sdgid: g1,
                icon: null,
                ord: 3,
                action: "dial_out",
                nid: n1,
            });
            const s2 = await create_sd(tx, {
                sdgid: g1,
                icon: "big.png",
                ord: 1,
                action: "attended_transfer",
                nid: n2,
            });

            // in g2
            const s3 = await create_sd(tx, {
                sdgid: g2,
                icon: "icon.ico",
                ord: 4,
                action: "blind_transfer",
                nid: n1,
            });
            const s4 = await create_sd(tx, {
                sdgid: g2,
                icon: "small.ico",
                ord: 9,
                action: "conference_transfer",
                nid: n2,
            });
            const s5 = await create_sd(tx, {
                sdgid: g2,
                icon: "small.ico",
                ord: 1,
                action: "tandem_transfer",
                nid: n1,
            });

            const ph = await phonebook_db.get_latest_t(tx, pid);

            t.assert(ph.change_id === 0);
            t.assert(ph.id === pid);
            t.assert(ph.name === undefined);
            t.assert(ph.dirs.size === 1);
            t.assert(ph.contacts.size === 2);

            // groups
            t.assert(
                Immutable.is(
                    ph.dirs.get(g),
                    new Phonebook.Directory({
                        name: "group",
                        icon: undefined,
                        ord: 0,
                    }),
                ),
                "g should be correct",
            );

            // contacts
            t.assert(
                Immutable.is(
                    ph.contacts.get(n1),
                    new Phonebook.Contact({
                        name: "one",
                        contact: "333",
                        xfer_ng911: true,
                        xfer_blind: true,
                        xfer_attended: true,
                        xfer_conference: true,
                        xfer_outbound: true,
                        icon: undefined,
                        ord: 0,
                        parent: g,
                    }),
                ),
                "n1 should be correct",
            );

            t.assert(
                Immutable.is(
                    ph.contacts.get(n2),
                    new Phonebook.Contact({
                        name: "two",
                        contact: "333",
                        xfer_ng911: true,
                        xfer_blind: true,
                        xfer_attended: true,
                        xfer_conference: true,
                        xfer_outbound: true,
                        icon: "hello.png",
                        ord: 1,
                        parent: g,
                    }),
                ),
                "n2 should be correct",
            );

            // speed dial groups
            t.assert(
                Immutable.is(
                    ph.sd_dirs.get(g1),
                    new Phonebook.Directory({
                        name: "hello",
                        ord: 0,
                    }),
                ),
            );

            t.assert(
                Immutable.is(
                    ph.sd_dirs.get(g2),
                    new Phonebook.Directory({
                        name: "hello 2",
                        ord: 1,
                    }),
                ),
            );

            t.assert(
                Immutable.is(
                    ph.sd_dirs.get(g3),
                    new Phonebook.Directory({
                        name: "empty",
                        ord: 2,
                    }),
                ),
            );

            // speed dials
            t.assert(
                Immutable.is(
                    ph.speed_dials.get(s1),
                    new Phonebook.SpeedDial({
                        parent: g1,
                        icon: undefined,
                        ord: 1,
                        action: "dial_out",
                        contact_id: n1,
                    }),
                ),
                "s1 should be correct",
            );

            t.assert(
                Immutable.is(
                    ph.speed_dials.get(s2),
                    new Phonebook.SpeedDial({
                        parent: g1,
                        icon: "big.png",
                        ord: 0,
                        action: "attended_transfer",
                        contact_id: n2,
                    }),
                ),
                "s2 should be correct",
            );

            t.assert(
                Immutable.is(
                    ph.speed_dials.get(s3),
                    new Phonebook.SpeedDial({
                        parent: g2,
                        icon: "icon.ico",
                        ord: 1,
                        action: "blind_transfer",
                        contact_id: n1,
                    }),
                ),
                "s3 should be correct",
            );

            t.assert(
                Immutable.is(
                    ph.speed_dials.get(s4),
                    new Phonebook.SpeedDial({
                        parent: g2,
                        icon: "small.ico",
                        ord: 2,
                        action: "conference_transfer",
                        contact_id: n2,
                    }),
                ),
                "s4 should be correct",
            );

            t.assert(
                Immutable.is(
                    ph.speed_dials.get(s5),
                    new Phonebook.SpeedDial({
                        parent: g2,
                        icon: "small.ico",
                        ord: 0,
                        action: "tandem_transfer",
                        contact_id: n1,
                    }),
                ),
                "s5 should be correct",
            );

            t.assert(ph.sd_dirs.size === 3);
            t.assert(ph.speed_dials.size === 5);
            t.assert(
                ph.filename,
                "/datadisk1/ng911/config/ng911_phonebook.xml",
            );
            t.assert(ph.current_change_id === undefined);
        });
    });

    test(`get_phonebook_list_t should work (${state_uuid})`, async t => {
        await rollback_tx(async tx => {
            const list = await phonebook_db.get_phonebook_list_t(tx);

            t.assert(Array.isArray(list), "list must be an array");
            t.assert(list.length === 0, "list must be empty");
        });
    });

    test(`get_phonebook_list_t should return one when there is one (${state_uuid})`, async t => {
        await rollback_tx(async tx => {
            const id = await phonebook_db.create_phonebook_t(tx);

            const list = await phonebook_db.get_phonebook_list_t(tx);

            t.assert(list.length === 1, "list should have one item");
            t.assert(list[0].id === id, "list should have correct id");
        });
    });

    test(`get_phonebook_list_t should return multiple (${state_uuid})`, async t => {
        await rollback_tx(async tx => {
            const id1 = await phonebook_db.create_phonebook_t(tx);
            const id2 = await phonebook_db.create_phonebook_t(tx);
            const id3 = await phonebook_db.create_phonebook_t(tx);

            const list = await phonebook_db.get_phonebook_list_t(tx);

            t.assert(list.length === 3, "list should have 3 entries");

            const list_set = Immutable.Set.of(
                list[0].id,
                list[1].id,
                list[2].id,
            );

            t.assert(list_set.size === 3);

            t.assert(Immutable.is(list_set, Immutable.Set.of(id1, id2, id3)));
        });
    });

    test(`patch_directory_t should work (${state_uuid})`, async t => {
        await rollback_tx(async tx => {
            const id = await phonebook_db.create_phonebook_t(tx);

            const gid = await create_group(tx, {
                pid: id,
                gname: "hello",
                icon: "what.png",
                ord: 1,
            });

            await phonebook_db.patch_directory_t(
                tx,
                id,
                gid,
                { name: "a new name", icon: "new.ico" },
                phonebook_db.empty_meta(),
            );

            const ph = await phonebook_db.get_latest_t(tx, id);

            t.assert(ph.dirs.get(gid)!.name === "a new name");

            t.assert(ph.dirs.get(gid)!.icon === "new.ico");
        });
    });

    test(`patch_directory_t set only name (${state_uuid})`, async t => {
        await rollback_tx(async tx => {
            const id = await phonebook_db.create_phonebook_t(tx);

            const gid = await create_group(tx, {
                pid: id,
                gname: "hello",
                icon: "what.png",
                ord: 1,
            });

            await phonebook_db.patch_directory_t(
                tx,
                id,
                gid,
                { name: "a new name" },
                phonebook_db.empty_meta(),
            );

            const ph = await phonebook_db.get_latest_t(tx, id);

            t.assert(ph.dirs.get(gid)!.name === "a new name");

            t.assert(ph.dirs.get(gid)!.icon === "what.png");
        });
    });

    test(`patch_directory_t set only icon (${state_uuid})`, async t => {
        await rollback_tx(async tx => {
            const id = await phonebook_db.create_phonebook_t(tx);

            const gid = await create_group(tx, {
                pid: id,
                gname: "hello",
                icon: "what.png",
                ord: 1,
            });

            await phonebook_db.patch_directory_t(
                tx,
                id,
                gid,
                { icon: "hello.ico" },
                phonebook_db.empty_meta(),
            );

            const ph = await phonebook_db.get_latest_t(tx, id);

            t.assert(ph.dirs.get(gid)!.name === "hello");

            t.assert(ph.dirs.get(gid)!.icon === "hello.ico");
        });
    });

    test(`patch_directory_t remove icon (${state_uuid})`, async t => {
        await rollback_tx(async tx => {
            const id = await phonebook_db.create_phonebook_t(tx);

            const gid = await create_group(tx, {
                pid: id,
                gname: "hello",
                icon: "what.png",
                ord: 1,
            });

            await phonebook_db.patch_directory_t(
                tx,
                id,
                gid,
                { icon: null },
                phonebook_db.empty_meta(),
            );

            const ph = await phonebook_db.get_latest_t(tx, id);

            t.assert(ph.dirs.get(gid)!.name === "hello");

            t.assert(ph.dirs.get(gid)!.icon === undefined);
        });
    });
}
