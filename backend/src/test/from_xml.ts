/* eslint-disable @typescript-eslint/no-unsafe-call */

import test from "ava";
import uuid from "uuid-1345";

import { from_xml } from "../phonebook_xml";

test("should import old style", t => {
    const xml_string = `<?xml version="1.0" encoding="UTF-8"?>
<TransferList>
  <Transfer_Name>Bob</Transfer_Name>
  <Transfer_Group>The People</Transfer_Group>
  <Transfer_Number>4444444444</Transfer_Number>
  <Transfer_Code>62</Transfer_Code>
  <Transfer_Name>Eve</Transfer_Name>
  <Transfer_Group>The People</Transfer_Group>
  <Transfer_Number>404</Transfer_Number>
  <Transfer_Code>62</Transfer_Code>
  <Transfer_Name>Charly</Transfer_Name>
  <Transfer_Group>The People</Transfer_Group>
  <Transfer_Number>97777777</Transfer_Number>
  <Transfer_Code>62</Transfer_Code>
  <Transfer_Name>Ho</Transfer_Name>
  <Transfer_Group>The Legend</Transfer_Group>
  <Transfer_Number>404</Transfer_Number>
  <Transfer_Code>62</Transfer_Code>
  <Transfer_Name>Hi</Transfer_Name>
  <Transfer_Group>The Legend</Transfer_Group>
  <Transfer_Number>404</Transfer_Number>
  <Transfer_Code>62</Transfer_Code>
  <Display_Order>
    <Transfer_Group>The Legend</Transfer_Group>
    <List_Priority>2</List_Priority>
    <Transfer_Group>The People</Transfer_Group>
    <List_Priority>1</List_Priority>
  </Display_Order>
</TransferList>`;
    const phonebook = from_xml(xml_string);

    t.assert(phonebook.groups.size === 2);

    t.assert(phonebook.groups.get(0).name === "The People");
    t.assert(phonebook.groups.get(1).name === "The Legend");

    const people = phonebook.groups.get(0).numbers;
    const legend = phonebook.groups.get(1).numbers;

    t.assert(people.size === 3);
    t.assert(people.get(0).name === "Bob");
    t.assert(people.get(0).number === "4444444444");
    t.assert(people.get(1).name === "Eve");
    t.assert(people.get(1).number === "404");
    t.assert(people.get(2).name === "Charly");
    t.assert(people.get(2).number === "97777777");

    t.assert(legend.size === 2);
    t.assert(legend.get(0).name === "Ho");
    t.assert(legend.get(0).number === "404");
    t.assert(legend.get(1).name === "Hi");
    t.assert(legend.get(1).number === "404");
});

test("import icons", t => {
    const xml_string = `<?xml version="1.0" encoding="UTF-8"?>
<TransferList>
    <Transfer_Name>Bob</Transfer_Name>
    <Transfer_Group>The People</Transfer_Group>
    <Transfer_Number>4444444444</Transfer_Number>
    <Transfer_Code>62</Transfer_Code>
    <icon_file>http://192.168.62.11:3001/icon?pid=167d402c-b001-42cb-ba88-a7b7171c94a2&amp;icon=map.png</icon_file>
    <Transfer_Name>Eve</Transfer_Name>
    <Transfer_Group>The People</Transfer_Group>
    <Transfer_Number>404</Transfer_Number>
    <Transfer_Code>62</Transfer_Code>
    <icon_file>none</icon_file>
    <Transfer_Name>Charly</Transfer_Name>
    <Transfer_Group>The People</Transfer_Group>
    <Transfer_Number>97777777</Transfer_Number>
    <Transfer_Code>62</Transfer_Code>
    <icon_file>http://192.168.62.11:3001/icon?pid=167d402c-b001-42cb-ba88-a7b7171c94a2&amp;icon=shield.png</icon_file>
    <Transfer_Name>Ho</Transfer_Name>
    <Transfer_Group>The Legend</Transfer_Group>
    <Transfer_Number>404</Transfer_Number>
    <Transfer_Code>62</Transfer_Code>
    <icon_file>none</icon_file>
    <Transfer_Name>Hi</Transfer_Name>
    <Transfer_Group>The Legend</Transfer_Group>
    <Transfer_Number>404</Transfer_Number>
    <Transfer_Code>62</Transfer_Code>
    <icon_file>http://192.168.62.11:3001/icon?pid=167d402c-b001-42cb-ba88-a7b7171c94a2&amp;icon=person.png</icon_file>
    <Display_Order>
        <Transfer_Group>The People</Transfer_Group>
        <List_Priority>1</List_Priority>
        <icon_file>http://192.168.62.11:3001/icon?pid=167d402c-b001-42cb-ba88-a7b7171c94a2&amp;icon=folder.png</icon_file>
        <Transfer_Group>The Legend</Transfer_Group>
        <List_Priority>2</List_Priority>
        <icon_file>none</icon_file>
    </Display_Order>
</TransferList>`;

    const phonebook = from_xml(xml_string);

    t.assert(phonebook.groups.size === 2);

    t.assert(phonebook.groups.get(0).name === "The People");
    t.assert(phonebook.groups.get(0).icon === "folder.png");
    t.assert(phonebook.groups.get(1).name === "The Legend");
    t.assert(phonebook.groups.get(1).icon === null);

    const people = phonebook.groups.get(0).numbers;
    const legend = phonebook.groups.get(1).numbers;

    t.assert(people.size === 3);
    t.assert(people.get(0).name === "Bob");
    t.assert(people.get(0).number === "4444444444");
    t.assert(people.get(0).icon === "map.png");
    t.assert(people.get(1).name === "Eve");
    t.assert(people.get(1).number === "404");
    t.assert(people.get(1).icon === null);
    t.assert(people.get(2).name === "Charly");
    t.assert(people.get(2).number === "97777777");
    t.assert(people.get(2).icon === "shield.png");

    t.assert(legend.size === 2);
    t.assert(legend.get(0).name === "Ho");
    t.assert(legend.get(0).number === "404");
    t.assert(legend.get(0).icon === null);
    t.assert(legend.get(1).name === "Hi");
    t.assert(legend.get(1).number === "404");
    t.assert(legend.get(1).icon === "person.png");
});

test("import icons with missing icon_file", t => {
    const xml_string = `<?xml version="1.0" encoding="UTF-8"?>
<TransferList>
    <Transfer_Name>Bob</Transfer_Name>
    <Transfer_Group>The People</Transfer_Group>
    <Transfer_Number>4444444444</Transfer_Number>
    <Transfer_Code>62</Transfer_Code>
    <icon_file>http://192.168.62.11:3001/icon?pid=167d402c-b001-42cb-ba88-a7b7171c94a2&amp;icon=map.png</icon_file>
    <Transfer_Name>Eve</Transfer_Name>
    <Transfer_Group>The People</Transfer_Group>
    <Transfer_Number>404</Transfer_Number>
    <Transfer_Code>62</Transfer_Code>
    <Transfer_Name>Charly</Transfer_Name>
    <Transfer_Group>The People</Transfer_Group>
    <Transfer_Number>97777777</Transfer_Number>
    <Transfer_Code>62</Transfer_Code>
    <icon_file>http://192.168.62.11:3001/icon?pid=167d402c-b001-42cb-ba88-a7b7171c94a2&amp;icon=shield.png</icon_file>
    <Transfer_Name>Ho</Transfer_Name>
    <Transfer_Group>The Legend</Transfer_Group>
    <Transfer_Number>404</Transfer_Number>
    <Transfer_Code>62</Transfer_Code>
    <icon_file>none</icon_file>
    <Transfer_Name>Hi</Transfer_Name>
    <Transfer_Group>The Legend</Transfer_Group>
    <Transfer_Number>404</Transfer_Number>
    <Transfer_Code>62</Transfer_Code>
    <icon_file>http://192.168.62.11:3001/icon?pid=167d402c-b001-42cb-ba88-a7b7171c94a2&amp;icon=person.png</icon_file>
    <Display_Order>
        <Transfer_Group>The People</Transfer_Group>
        <List_Priority>1</List_Priority>
        <icon_file>http://192.168.62.11:3001/icon?pid=167d402c-b001-42cb-ba88-a7b7171c94a2&amp;icon=folder.png</icon_file>
        <Transfer_Group>The Legend</Transfer_Group>
        <List_Priority>2</List_Priority>
    </Display_Order>
</TransferList>`;

    const phonebook = from_xml(xml_string);

    t.assert(phonebook.groups.size === 2);

    t.assert(phonebook.groups.get(0).name === "The People");
    t.assert(phonebook.groups.get(0).icon === "folder.png");
    t.assert(phonebook.groups.get(1).name === "The Legend");
    t.assert(phonebook.groups.get(1).icon === null);

    const people = phonebook.groups.get(0).numbers;
    const legend = phonebook.groups.get(1).numbers;

    t.assert(people.size === 3);
    t.assert(people.get(0).name === "Bob");
    t.assert(people.get(0).number === "4444444444");
    t.assert(people.get(0).icon === "map.png");
    t.assert(people.get(1).name === "Eve");
    t.assert(people.get(1).number === "404");
    t.assert(people.get(1).icon === null);
    t.assert(people.get(2).name === "Charly");
    t.assert(people.get(2).number === "97777777");
    t.assert(people.get(2).icon === "shield.png");

    t.assert(legend.size === 2);
    t.assert(legend.get(0).name === "Ho");
    t.assert(legend.get(0).number === "404");
    t.assert(legend.get(0).icon === null);
    t.assert(legend.get(1).name === "Hi");
    t.assert(legend.get(1).number === "404");
    t.assert(legend.get(1).icon === "person.png");
});

test("a single speeddial", t => {
    const xml_string = `<?xml version="1.0" encoding="UTF-8"?>
<TransferList>
  <Transfer_Name>Bob</Transfer_Name>
  <Transfer_Group>The People</Transfer_Group>
  <Transfer_Number>4444444444</Transfer_Number>
  <Transfer_Code>62</Transfer_Code>
  <Speed_Dial_Action>none</Speed_Dial_Action>
  <icon_file>http://192.168.62.11:3001/icon?pid=167d402c-b001-42cb-ba88-a7b7171c94a2&amp;icon=map.png</icon_file>
  <Transfer_Name>Eve</Transfer_Name>
  <Transfer_Group>The People</Transfer_Group>
  <Transfer_Number>404</Transfer_Number>
  <Transfer_Code>62</Transfer_Code>
  <Speed_Dial_Action>none</Speed_Dial_Action>
  <icon_file>none</icon_file>
  <Transfer_Name>Charly</Transfer_Name>
  <Transfer_Group>The People</Transfer_Group>
  <Transfer_Number>97777777</Transfer_Number>
  <Transfer_Code>62</Transfer_Code>
  <Speed_Dial_Action>none</Speed_Dial_Action>
  <icon_file>http://192.168.62.11:3001/icon?pid=167d402c-b001-42cb-ba88-a7b7171c94a2&amp;icon=shield.png</icon_file>
  <Transfer_Name>Ho</Transfer_Name>
  <Transfer_Group>The Legend</Transfer_Group>
  <Transfer_Number>404</Transfer_Number>
  <Transfer_Code>62</Transfer_Code>
  <Speed_Dial_Action>none</Speed_Dial_Action>
  <icon_file>none</icon_file>
  <Transfer_Name>Hi</Transfer_Name>
  <Transfer_Group>The Legend</Transfer_Group>
  <Transfer_Number>404</Transfer_Number>
  <Transfer_Code>62</Transfer_Code>
  <Speed_Dial_Action>none</Speed_Dial_Action>
  <icon_file>http://192.168.62.11:3001/icon?pid=167d402c-b001-42cb-ba88-a7b7171c94a2&amp;icon=person.png</icon_file>
  <Transfer_Name>Bob</Transfer_Name>
  <icon_file>http://192.168.62.11:3001/icon?pid=167d402c-b001-42cb-ba88-a7b7171c94a2&amp;icon=shield.png</icon_file>
  <Transfer_Group>SpeedDial Fancy</Transfer_Group>
  <Transfer_Number>4444444444</Transfer_Number>
  <Transfer_Code>0</Transfer_Code>
  <Speed_Dial_Action>dial_out</Speed_Dial_Action>
  <Display_Order>
    <Transfer_Group>The People</Transfer_Group>
    <List_Priority>1</List_Priority>
    <icon_file>http://192.168.62.11:3001/icon?pid=167d402c-b001-42cb-ba88-a7b7171c94a2&amp;icon=folder.png</icon_file>
    <Transfer_Group>The Legend</Transfer_Group>
    <List_Priority>2</List_Priority>
    <icon_file>none</icon_file>
    <Transfer_Group>SpeedDial Fancy</Transfer_Group>
    <List_Priority>3</List_Priority>
  </Display_Order>
</TransferList>`;

    const phonebook = from_xml(xml_string);

    t.assert(phonebook.groups.size === 2);

    t.assert(phonebook.groups.get(0).name === "The People");
    t.assert(phonebook.groups.get(0).icon === "folder.png");
    t.assert(phonebook.groups.get(1).name === "The Legend");
    t.assert(phonebook.groups.get(1).icon === null);

    const people = phonebook.groups.get(0).numbers;
    const legend = phonebook.groups.get(1).numbers;

    t.assert(people.size === 3);
    t.assert(people.get(0).name === "Bob");
    t.assert(people.get(0).number === "4444444444");
    t.assert(people.get(0).icon === "map.png");
    t.assert(people.get(1).name === "Eve");
    t.assert(people.get(1).number === "404");
    t.assert(people.get(1).icon === null);
    t.assert(people.get(2).name === "Charly");
    t.assert(people.get(2).number === "97777777");
    t.assert(people.get(2).icon === "shield.png");

    t.assert(legend.size === 2);
    t.assert(legend.get(0).name === "Ho");
    t.assert(legend.get(0).number === "404");
    t.assert(legend.get(0).icon === null);
    t.assert(legend.get(1).name === "Hi");
    t.assert(legend.get(1).number === "404");
    t.assert(legend.get(1).icon === "person.png");

    t.assert(phonebook.sdgroups.size === 1);
    t.assert(phonebook.sdgroups.get(0).name === "Fancy");

    const fancy = phonebook.sdgroups.get(0).sds;
    t.assert(fancy.size === 1);
    t.assert(fancy.get(0).icon === "shield.png");
    t.assert(fancy.get(0).action === "dial_out");
    t.assert(fancy.get(0).nid === people.get(0).id);
});

test("two speeddials that differ only by phone number", t => {
    const xml_string = `<?xml version="1.0" encoding="UTF-8"?>
<TransferList>
  <Transfer_Name>Bob</Transfer_Name>
  <Transfer_Group>The People</Transfer_Group>
  <Transfer_Number>4444444444</Transfer_Number>
  <Transfer_Code>62</Transfer_Code>
  <icon_file>http://192.168.62.11:3001/icon?pid=167d402c-b001-42cb-ba88-a7b7171c94a2&amp;icon=map.png</icon_file>
  <Transfer_Name>Eve</Transfer_Name>
  <Transfer_Group>The People</Transfer_Group>
  <Transfer_Number>404</Transfer_Number>
  <Transfer_Code>62</Transfer_Code>
  <Speed_Dial_Action>none</Speed_Dial_Action>
  <icon_file>none</icon_file>
  <Transfer_Name>Charly</Transfer_Name>
  <Transfer_Group>The People</Transfer_Group>
  <Transfer_Number>97777777</Transfer_Number>
  <Transfer_Code>62</Transfer_Code>
  <Speed_Dial_Action>none</Speed_Dial_Action>
  <icon_file>http://192.168.62.11:3001/icon?pid=167d402c-b001-42cb-ba88-a7b7171c94a2&amp;icon=shield.png</icon_file>
  <Transfer_Name>Ho</Transfer_Name>
  <Transfer_Group>The Legend</Transfer_Group>
  <Transfer_Number>404</Transfer_Number>
  <Transfer_Code>62</Transfer_Code>
  <icon_file>none</icon_file>
  <Transfer_Name>Hi</Transfer_Name>
  <Transfer_Group>The Legend</Transfer_Group>
  <Transfer_Number>404</Transfer_Number>
  <Transfer_Code>62</Transfer_Code>
  <Speed_Dial_Action>none</Speed_Dial_Action>
  <icon_file>http://192.168.62.11:3001/icon?pid=167d402c-b001-42cb-ba88-a7b7171c94a2&amp;icon=person.png</icon_file>
  <Transfer_Name>Bob</Transfer_Name>
  <Transfer_Group>The Legend</Transfer_Group>
  <Transfer_Number>7777777</Transfer_Number>
  <Transfer_Code>62</Transfer_Code>
  <Speed_Dial_Action>none</Speed_Dial_Action>
  <icon_file>http://192.168.62.11:3001/icon?pid=167d402c-b001-42cb-ba88-a7b7171c94a2&amp;icon=badge.png</icon_file>
  <Transfer_Name>Bob</Transfer_Name>
  <icon_file>http://192.168.62.11:3001/icon?pid=167d402c-b001-42cb-ba88-a7b7171c94a2&amp;icon=map.png</icon_file>
  <Transfer_Group>SpeedDial Fancy</Transfer_Group>
  <Transfer_Number>4444444444</Transfer_Number>
  <Transfer_Code>0</Transfer_Code>
  <Speed_Dial_Action>dial_out</Speed_Dial_Action>
  <Transfer_Name>Bob</Transfer_Name>
  <icon_file>http://192.168.62.11:3001/icon?pid=167d402c-b001-42cb-ba88-a7b7171c94a2&amp;icon=badge.png</icon_file>
  <Transfer_Group>SpeedDial Fancy</Transfer_Group>
  <Transfer_Number>7777777</Transfer_Number>
  <Transfer_Code>0</Transfer_Code>
  <Speed_Dial_Action>attended_transfer</Speed_Dial_Action>
  <Display_Order>
    <Transfer_Group>The People</Transfer_Group>
    <List_Priority>1</List_Priority>
    <icon_file>http://192.168.62.11:3001/icon?pid=167d402c-b001-42cb-ba88-a7b7171c94a2&amp;icon=folder.png</icon_file>
    <Transfer_Group>The Legend</Transfer_Group>
    <List_Priority>2</List_Priority>
    <icon_file>none</icon_file>
    <Transfer_Group>SpeedDial Fancy</Transfer_Group>
    <List_Priority>3</List_Priority>
  </Display_Order>
</TransferList>`;

    const phonebook = from_xml(xml_string);

    t.assert(phonebook.groups.size === 2);

    t.assert(phonebook.groups.get(0).name === "The People");
    t.assert(phonebook.groups.get(0).icon === "folder.png");
    t.assert(phonebook.groups.get(1).name === "The Legend");
    t.assert(phonebook.groups.get(1).icon === null);

    const people = phonebook.groups.get(0).numbers;
    const legend = phonebook.groups.get(1).numbers;

    t.assert(people.size === 3);
    t.assert(people.get(0).name === "Bob");
    t.assert(people.get(0).number === "4444444444");
    t.assert(people.get(0).icon === "map.png");
    t.assert(people.get(1).name === "Eve");
    t.assert(people.get(1).number === "404");
    t.assert(people.get(1).icon === null);
    t.assert(people.get(2).name === "Charly");
    t.assert(people.get(2).number === "97777777");
    t.assert(people.get(2).icon === "shield.png");

    t.assert(legend.size === 3);
    t.assert(legend.get(0).name === "Ho");
    t.assert(legend.get(0).number === "404");
    t.assert(legend.get(0).icon === null);
    t.assert(legend.get(1).name === "Hi");
    t.assert(legend.get(1).number === "404");
    t.assert(legend.get(1).icon === "person.png");
    t.assert(legend.get(2).name === "Bob");
    t.assert(legend.get(2).number === "7777777");
    t.assert(legend.get(2).icon === "badge.png");

    t.assert(phonebook.sdgroups.size === 1);
    t.assert(phonebook.sdgroups.get(0).name === "Fancy");

    const fancy = phonebook.sdgroups.get(0).sds;
    t.assert(fancy.size === 2);
    t.assert(fancy.get(0).icon === "map.png");
    t.assert(fancy.get(0).action === "dial_out");
    t.assert(fancy.get(0).nid === people.get(0).id);
    t.assert(fancy.get(1).icon === "badge.png");
    t.assert(fancy.get(1).action === "attended_transfer");
    t.assert(fancy.get(1).nid === legend.get(2).id);
});

test("two speeddial groups", t => {
    const xml_string = `<?xml version="1.0" encoding="UTF-8"?>
<TransferList>
  <Transfer_Name>Bob</Transfer_Name>
  <Transfer_Group>The People</Transfer_Group>
  <Transfer_Number>4444444444</Transfer_Number>
  <Transfer_Code>62</Transfer_Code>
  <Speed_Dial_Action>none</Speed_Dial_Action>
  <icon_file>http://192.168.62.11:3001/icon?pid=167d402c-b001-42cb-ba88-a7b7171c94a2&amp;icon=map.png</icon_file>
  <Transfer_Name>Eve</Transfer_Name>
  <Transfer_Group>The People</Transfer_Group>
  <Transfer_Number>404</Transfer_Number>
  <Transfer_Code>62</Transfer_Code>
  <Speed_Dial_Action>none</Speed_Dial_Action>
  <icon_file>none</icon_file>
  <Transfer_Name>Charly</Transfer_Name>
  <Transfer_Group>The People</Transfer_Group>
  <Transfer_Number>97777777</Transfer_Number>
  <Transfer_Code>62</Transfer_Code>
  <Speed_Dial_Action>none</Speed_Dial_Action>
  <icon_file>http://192.168.62.11:3001/icon?pid=167d402c-b001-42cb-ba88-a7b7171c94a2&amp;icon=shield.png</icon_file>
  <Transfer_Name>Ho</Transfer_Name>
  <Transfer_Group>The Legend</Transfer_Group>
  <Transfer_Number>404</Transfer_Number>
  <Transfer_Code>62</Transfer_Code>
  <Speed_Dial_Action>none</Speed_Dial_Action>
  <icon_file>none</icon_file>
  <Transfer_Name>Hi</Transfer_Name>
  <Transfer_Group>The Legend</Transfer_Group>
  <Transfer_Number>404</Transfer_Number>
  <Transfer_Code>62</Transfer_Code>
  <Speed_Dial_Action>none</Speed_Dial_Action>
  <icon_file>http://192.168.62.11:3001/icon?pid=167d402c-b001-42cb-ba88-a7b7171c94a2&amp;icon=person.png</icon_file>
  <Transfer_Name>Bob</Transfer_Name>
  <Transfer_Group>The Legend</Transfer_Group>
  <Transfer_Number>7777777</Transfer_Number>
  <Transfer_Code>62</Transfer_Code>
  <Speed_Dial_Action>none</Speed_Dial_Action>
  <icon_file>http://192.168.62.11:3001/icon?pid=167d402c-b001-42cb-ba88-a7b7171c94a2&amp;icon=badge.png</icon_file>
  <Transfer_Name>Bob</Transfer_Name>
  <icon_file>http://192.168.62.11:3001/icon?pid=167d402c-b001-42cb-ba88-a7b7171c94a2&amp;icon=map.png</icon_file>
  <Transfer_Group>SpeedDial Fancy</Transfer_Group>
  <Transfer_Number>4444444444</Transfer_Number>
  <Transfer_Code>0</Transfer_Code>
  <Speed_Dial_Action>dial_out</Speed_Dial_Action>
  <Transfer_Name>Bob</Transfer_Name>
  <icon_file>http://192.168.62.11:3001/icon?pid=167d402c-b001-42cb-ba88-a7b7171c94a2&amp;icon=badge.png</icon_file>
  <Transfer_Group>SpeedDial Fancy</Transfer_Group>
  <Transfer_Number>7777777</Transfer_Number>
  <Transfer_Code>0</Transfer_Code>
  <Speed_Dial_Action>attended_transfer</Speed_Dial_Action>
  <Transfer_Name>Eve</Transfer_Name>
  <icon_file>none</icon_file>
  <Transfer_Group>SpeedDial Numero 2</Transfer_Group>
  <Transfer_Number>404</Transfer_Number>
  <Transfer_Code>0</Transfer_Code>
  <Speed_Dial_Action>dial_out</Speed_Dial_Action>
  <Display_Order>
    <Transfer_Group>The People</Transfer_Group>
    <List_Priority>1</List_Priority>
    <icon_file>http://192.168.62.11:3001/icon?pid=167d402c-b001-42cb-ba88-a7b7171c94a2&amp;icon=folder.png</icon_file>
    <Transfer_Group>The Legend</Transfer_Group>
    <List_Priority>2</List_Priority>
    <icon_file>none</icon_file>
    <Transfer_Group>SpeedDial Numero 2</Transfer_Group>
    <List_Priority>4</List_Priority>
    <Transfer_Group>SpeedDial Fancy</Transfer_Group>
    <List_Priority>3</List_Priority>
  </Display_Order>
</TransferList>`;

    const phonebook = from_xml(xml_string);

    t.assert(phonebook.groups.size === 2);

    t.assert(phonebook.groups.get(0).name === "The People");
    t.assert(phonebook.groups.get(0).icon === "folder.png");
    t.assert(phonebook.groups.get(1).name === "The Legend");
    t.assert(phonebook.groups.get(1).icon === null);

    const people = phonebook.groups.get(0).numbers;
    const legend = phonebook.groups.get(1).numbers;

    t.assert(people.size === 3);
    t.assert(people.get(0).name === "Bob");
    t.assert(people.get(0).number === "4444444444");
    t.assert(people.get(0).icon === "map.png");
    t.assert(people.get(1).name === "Eve");
    t.assert(people.get(1).number === "404");
    t.assert(people.get(1).icon === null);
    t.assert(people.get(2).name === "Charly");
    t.assert(people.get(2).number === "97777777");
    t.assert(people.get(2).icon === "shield.png");

    t.assert(legend.size === 3);
    t.assert(legend.get(0).name === "Ho");
    t.assert(legend.get(0).number === "404");
    t.assert(legend.get(0).icon === null);
    t.assert(legend.get(1).name === "Hi");
    t.assert(legend.get(1).number === "404");
    t.assert(legend.get(1).icon === "person.png");
    t.assert(legend.get(2).name === "Bob");
    t.assert(legend.get(2).number === "7777777");
    t.assert(legend.get(2).icon === "badge.png");

    t.assert(phonebook.sdgroups.size === 2);
    t.assert(phonebook.sdgroups.get(0).name === "Fancy");

    const fancy = phonebook.sdgroups.get(0).sds;
    t.assert(fancy.size === 2);
    t.assert(fancy.get(0).icon === "map.png");
    t.assert(fancy.get(0).action === "dial_out");
    t.assert(fancy.get(0).nid === people.get(0).id);
    t.assert(fancy.get(1).icon === "badge.png");
    t.assert(fancy.get(1).action === "attended_transfer");
    t.assert(fancy.get(1).nid === legend.get(2).id);

    const two = phonebook.sdgroups.get(1).sds;
    t.assert(two.size === 1);
    t.assert(two.get(0).icon === null);
    t.assert(two.get(0).action === "dial_out");
    t.assert(two.get(0).nid === people.get(1).id);
});

test("should interpret ids", t => {
    const xml_string = `<?xml version="1.0" encoding="UTF-8"?>
<TransferList>
  <Transfer_Name id="6cee2499-cbff-4527-9fa7-acbcb71a0214">Cat</Transfer_Name>
  <Transfer_Group>Some Group</Transfer_Group>
  <Transfer_Number>404</Transfer_Number>
  <Transfer_Code>62</Transfer_Code>
  <Speed_Dial_Action>none</Speed_Dial_Action>
  <icon_file>http://192.168.62.11:3001/icon?pid=deb02d58-eba9-4c72-a1da-d5fc7b549c91&amp;icon=red_cross_32x32.ico</icon_file>
  <Transfer_Name id="0152ea1a-3595-48c9-873d-cfcad0332a39">Cat</Transfer_Name>
  <Transfer_Group>Another Group</Transfer_Group>
  <Transfer_Number>404</Transfer_Number>
  <Transfer_Code>62</Transfer_Code>
  <Speed_Dial_Action>none</Speed_Dial_Action>
  <icon_file>http://192.168.62.11:3001/icon?pid=deb02d58-eba9-4c72-a1da-d5fc7b549c91&amp;icon=fire_hydrant_red.ico</icon_file>
  <Transfer_Name id="6bdfb1ed-1087-4a0c-9f04-9dd9f74f02da" nid="0152ea1a-3595-48c9-873d-cfcad0332a39">Cat</Transfer_Name>
  <icon_file>http://192.168.62.11:3001/icon?pid=deb02d58-eba9-4c72-a1da-d5fc7b549c91&amp;icon=911.ico</icon_file>
  <Transfer_Group>SpeedDial xfers</Transfer_Group>
  <Transfer_Number>404</Transfer_Number>
  <Transfer_Code>0</Transfer_Code>
  <Speed_Dial_Action>dial_out</Speed_Dial_Action>
  <Display_Order>
    <Transfer_Group>Some Group</Transfer_Group>
    <List_Priority>1</List_Priority>
    <icon_file>http://192.168.62.11:3001/icon?pid=deb02d58-eba9-4c72-a1da-d5fc7b549c91&amp;icon=folder.png</icon_file>
    <Transfer_Group>Another Group</Transfer_Group>
    <List_Priority>2</List_Priority>
    <icon_file>http://192.168.62.11:3001/icon?pid=deb02d58-eba9-4c72-a1da-d5fc7b549c91&amp;icon=folder.png</icon_file>
    <Transfer_Group>SpeedDial xfers</Transfer_Group>
    <List_Priority>3</List_Priority>
  </Display_Order>
</TransferList>`;

    const phonebook = from_xml(xml_string);

    t.assert(phonebook.groups.size === 2);
    t.assert(phonebook.sdgroups.size === 1);

    const one = phonebook.groups.get(0);
    const two = phonebook.groups.get(1);

    t.assert(one.numbers.size === 1);
    t.assert(two.numbers.size === 1);

    t.assert(one.name === "Some Group");
    t.assert(one.icon === "folder.png");
    t.assert(two.name === "Another Group");
    t.assert(two.icon === "folder.png");

    t.assert(one.numbers.get(0).name === "Cat");
    t.assert(one.numbers.get(0).id === "6cee2499-cbff-4527-9fa7-acbcb71a0214");
    t.assert(one.numbers.get(0).icon === "red_cross_32x32.ico");
    t.assert(two.numbers.get(0).name === "Cat");
    t.assert(two.numbers.get(0).id === "0152ea1a-3595-48c9-873d-cfcad0332a39");
    t.assert(two.numbers.get(0).icon === "fire_hydrant_red.ico");

    t.assert(phonebook.sdgroups.get(0).sds.size === 1);
    const sd = phonebook.sdgroups.get(0).sds.get(0);
    t.assert(sd.id === "6bdfb1ed-1087-4a0c-9f04-9dd9f74f02da");
    t.assert(sd.nid === "0152ea1a-3595-48c9-873d-cfcad0332a39");
    t.assert(sd.icon === "911.ico");
});

test("interpret icon attribute", t => {
    const xml_string = `<?xml version="1.0" encoding="UTF-8"?>
<TransferList>
  <Transfer_Name id="6cee2499-cbff-4527-9fa7-acbcb71a0214">Cat</Transfer_Name>
  <Transfer_Group>Some Group</Transfer_Group>
  <Transfer_Number>404</Transfer_Number>
  <Transfer_Code>62</Transfer_Code>
  <Speed_Dial_Action>none</Speed_Dial_Action>
  <icon_file icon="red_cross_32x32.ico">http://192.168.62.11:3001/icon?pid=deb02d58-eba9-4c72-a1da-d5fc7b549c91&amp;icon=not_red_cross_32x32.ico</icon_file>

  <Transfer_Name id="0152ea1a-3595-48c9-873d-cfcad0332a39">Cat</Transfer_Name>
  <Transfer_Group>Another Group</Transfer_Group>
  <Transfer_Number>404</Transfer_Number>
  <Transfer_Code>62</Transfer_Code>
  <Speed_Dial_Action>none</Speed_Dial_Action>
  <icon_file>http://192.168.62.11:3001/icon?pid=deb02d58-eba9-4c72-a1da-d5fc7b549c91&amp;icon=fire_hydrant_red.ico</icon_file>

  <Transfer_Name id="35fcb92f-b93f-4a67-b7a6-7103d478e730">Bob</Transfer_Name>
  <Transfer_Group>Another Group</Transfer_Group>
  <Transfer_Number>404</Transfer_Number>
  <Transfer_Code>62</Transfer_Code>
  <Speed_Dial_Action>none</Speed_Dial_Action>
  <icon_file>none</icon_file>

  <Transfer_Name id="6bdfb1ed-1087-4a0c-9f04-9dd9f74f02da" nid="0152ea1a-3595-48c9-873d-cfcad0332a39">Cat</Transfer_Name>
  <icon_file icon="911.ico">http://192.168.62.11:3001/icon?pid=deb02d58-eba9-4c72-a1da-d5fc7b549c91&amp;icon=not_911.ico</icon_file>
  <Transfer_Group>SpeedDial xfers</Transfer_Group>
  <Transfer_Number>404</Transfer_Number>
  <Transfer_Code>0</Transfer_Code>
  <Speed_Dial_Action>dial_out</Speed_Dial_Action>

  <!-- let's not put id attribute just to make sure nid works alone -->
  <Transfer_Name nid="0152ea1a-3595-48c9-873d-cfcad0332a39">Cat</Transfer_Name>
  <icon_file icon="">http://192.168.62.11:3001/icon?pid=deb02d58-eba9-4c72-a1da-d5fc7b549c91&amp;icon=fire_hydrant_red.ico</icon_file>
  <Transfer_Group>SpeedDial xfers</Transfer_Group>
  <Transfer_Number>404</Transfer_Number>
  <Transfer_Code>0</Transfer_Code>
  <Speed_Dial_Action>dial_out</Speed_Dial_Action>

  <Display_Order>
    <Transfer_Group>Some Group</Transfer_Group>
    <List_Priority>1</List_Priority>
    <icon_file icon="folder.png">http://192.168.62.11:3001/icon?pid=deb02d58-eba9-4c72-a1da-d5fc7b549c91&amp;icon=folder.png</icon_file>

    <Transfer_Group>Another Group</Transfer_Group>
    <List_Priority>2</List_Priority>
    <icon_file icon="">none</icon_file>

    <Transfer_Group>SpeedDial xfers</Transfer_Group>
    <List_Priority>3</List_Priority>
  </Display_Order>
</TransferList>`;

    const phonebook = from_xml(xml_string);

    t.assert(phonebook.groups.size === 2);
    t.assert(phonebook.sdgroups.size === 1);

    const one = phonebook.groups.get(0);
    const two = phonebook.groups.get(1);

    t.assert(one.name === "Some Group");
    t.assert(one.icon === "folder.png");
    t.assert(two.name === "Another Group");
    t.assert(two.icon === null);

    t.assert(one.numbers.size === 1);
    t.assert(two.numbers.size === 2);

    t.assert(one.numbers.get(0).name === "Cat");
    t.assert(one.numbers.get(0).id === "6cee2499-cbff-4527-9fa7-acbcb71a0214");
    t.assert(one.numbers.get(0).icon === "red_cross_32x32.ico");
    t.assert(two.numbers.get(0).name === "Cat");
    t.assert(two.numbers.get(0).id === "0152ea1a-3595-48c9-873d-cfcad0332a39");
    t.assert(two.numbers.get(0).icon === "fire_hydrant_red.ico");

    t.assert(two.numbers.get(1).name === "Bob");
    t.assert(uuid.check(two.numbers.get(1).id));
    t.assert(two.numbers.get(1).icon === null);

    t.assert(phonebook.sdgroups.get(0).sds.size === 2);

    const sd = phonebook.sdgroups.get(0).sds.get(0);
    t.assert(sd.id === "6bdfb1ed-1087-4a0c-9f04-9dd9f74f02da");
    t.assert(sd.nid === "0152ea1a-3595-48c9-873d-cfcad0332a39");
    t.assert(sd.icon === "911.ico");

    const sd2 = phonebook.sdgroups.get(0).sds.get(1);
    t.assert(uuid.check(sd2.id));
    t.assert(sd2.nid === "0152ea1a-3595-48c9-873d-cfcad0332a39");
    t.assert(sd2.icon === null);
});

test("empty transfer group", t => {
    const xml_string = `<?xml version="1.0" encoding="UTF-8"?>
        <TransferList>
          <Transfer_Name id="0152ea1a-3595-48c9-873d-cfcad0332a39">Cat</Transfer_Name>
          <Transfer_Group>Another Group</Transfer_Group>
          <Transfer_Number>404</Transfer_Number>
          <Transfer_Code>62</Transfer_Code>
          <Speed_Dial_Action>none</Speed_Dial_Action>
          <icon_file>http://192.168.62.11:3001/icon?pid=deb02d58-eba9-4c72-a1da-d5fc7b549c91&amp;icon=fire_hydrant_red.ico</icon_file>

          <Transfer_Name id="35fcb92f-b93f-4a67-b7a6-7103d478e730">Bob</Transfer_Name>
          <Transfer_Group>Another Group</Transfer_Group>
          <Transfer_Number>404</Transfer_Number>
          <Transfer_Code>62</Transfer_Code>
          <Speed_Dial_Action>none</Speed_Dial_Action>
          <icon_file>none</icon_file>

          <Transfer_Name id="6bdfb1ed-1087-4a0c-9f04-9dd9f74f02da" nid="0152ea1a-3595-48c9-873d-cfcad0332a39">Cat</Transfer_Name>
          <icon_file icon="911.ico">http://192.168.62.11:3001/icon?pid=deb02d58-eba9-4c72-a1da-d5fc7b549c91&amp;icon=not_911.ico</icon_file>
          <Transfer_Group>SpeedDial xfers</Transfer_Group>
          <Transfer_Number>404</Transfer_Number>
          <Transfer_Code>0</Transfer_Code>
          <Speed_Dial_Action>dial_out</Speed_Dial_Action>

          <!-- let's not put id attribute just to make sure nid works alone -->
          <Transfer_Name nid="0152ea1a-3595-48c9-873d-cfcad0332a39">Cat</Transfer_Name>
          <icon_file icon="">http://192.168.62.11:3001/icon?pid=deb02d58-eba9-4c72-a1da-d5fc7b549c91&amp;icon=fire_hydrant_red.ico</icon_file>
          <Transfer_Group>SpeedDial xfers</Transfer_Group>
          <Transfer_Number>404</Transfer_Number>
          <Transfer_Code>0</Transfer_Code>
          <Speed_Dial_Action>dial_out</Speed_Dial_Action>

          <Display_Order>
            <Transfer_Group>Some Group</Transfer_Group>
            <List_Priority>1</List_Priority>
            <icon_file icon="folder.png">http://192.168.62.11:3001/icon?pid=deb02d58-eba9-4c72-a1da-d5fc7b549c91&amp;icon=folder.png</icon_file>

            <Transfer_Group>Another Group</Transfer_Group>
            <List_Priority>2</List_Priority>
            <icon_file icon="">none</icon_file>

            <Transfer_Group>SpeedDial xfers</Transfer_Group>
            <List_Priority>3</List_Priority>
          </Display_Order>
        </TransferList>`;

    const phonebook = from_xml(xml_string);

    t.assert(phonebook.groups.size === 2);
    t.assert(phonebook.sdgroups.size === 1);

    const one = phonebook.groups.get(0);
    const two = phonebook.groups.get(1);

    t.assert(one.name === "Some Group");
    t.assert(one.icon === "folder.png");
    t.assert(two.name === "Another Group");
    t.assert(two.icon === null);

    t.assert(one.numbers.size === 0);
    t.assert(two.numbers.size === 2);

    t.assert(two.numbers.get(0).name === "Cat");
    t.assert(two.numbers.get(0).id === "0152ea1a-3595-48c9-873d-cfcad0332a39");
    t.assert(two.numbers.get(0).icon === "fire_hydrant_red.ico");

    t.assert(two.numbers.get(1).name === "Bob");
    t.assert(uuid.check(two.numbers.get(1).id));
    t.assert(two.numbers.get(1).icon === null);

    t.assert(phonebook.sdgroups.get(0).sds.size === 2);

    const sd = phonebook.sdgroups.get(0).sds.get(0);
    t.assert(sd.id === "6bdfb1ed-1087-4a0c-9f04-9dd9f74f02da");
    t.assert(sd.nid === "0152ea1a-3595-48c9-873d-cfcad0332a39");
    t.assert(sd.icon === "911.ico");

    const sd2 = phonebook.sdgroups.get(0).sds.get(1);
    t.assert(uuid.check(sd2.id));
    t.assert(sd2.nid === "0152ea1a-3595-48c9-873d-cfcad0332a39");
    t.assert(sd2.icon === null);
});

test("empty speed dial group", t => {
    const xml_string = `<?xml version="1.0" encoding="UTF-8"?>
<TransferList>
  <Transfer_Name id="6cee2499-cbff-4527-9fa7-acbcb71a0214">Cat</Transfer_Name>
  <Transfer_Group>Some Group</Transfer_Group>
  <Transfer_Number>404</Transfer_Number>
  <Transfer_Code>62</Transfer_Code>
  <Speed_Dial_Action>none</Speed_Dial_Action>
  <icon_file icon="red_cross_32x32.ico">http://192.168.62.11:3001/icon?pid=deb02d58-eba9-4c72-a1da-d5fc7b549c91&amp;icon=not_red_cross_32x32.ico</icon_file>

  <Transfer_Name id="0152ea1a-3595-48c9-873d-cfcad0332a39">Cat</Transfer_Name>
  <Transfer_Group>Another Group</Transfer_Group>
  <Transfer_Number>404</Transfer_Number>
  <Transfer_Code>62</Transfer_Code>
  <Speed_Dial_Action>none</Speed_Dial_Action>
  <icon_file>http://192.168.62.11:3001/icon?pid=deb02d58-eba9-4c72-a1da-d5fc7b549c91&amp;icon=fire_hydrant_red.ico</icon_file>

  <Transfer_Name id="35fcb92f-b93f-4a67-b7a6-7103d478e730">Bob</Transfer_Name>
  <Transfer_Group>Another Group</Transfer_Group>
  <Transfer_Number>404</Transfer_Number>
  <Transfer_Code>62</Transfer_Code>
  <Speed_Dial_Action>none</Speed_Dial_Action>
  <icon_file>none</icon_file>

  <Display_Order>
    <Transfer_Group>Some Group</Transfer_Group>
    <List_Priority>1</List_Priority>
    <icon_file icon="folder.png">http://192.168.62.11:3001/icon?pid=deb02d58-eba9-4c72-a1da-d5fc7b549c91&amp;icon=folder.png</icon_file>

    <Transfer_Group>Another Group</Transfer_Group>
    <List_Priority>2</List_Priority>
    <icon_file icon="">none</icon_file>

    <Transfer_Group>SpeedDial xfers</Transfer_Group>
    <List_Priority>3</List_Priority>
  </Display_Order>
</TransferList>`;

    const phonebook = from_xml(xml_string);

    t.assert(phonebook.groups.size === 2);
    t.assert(phonebook.sdgroups.size === 1);

    const one = phonebook.groups.get(0);
    const two = phonebook.groups.get(1);

    t.assert(one.name === "Some Group");
    t.assert(one.icon === "folder.png");
    t.assert(two.name === "Another Group");
    t.assert(two.icon === null);

    t.assert(one.numbers.size === 1);
    t.assert(two.numbers.size === 2);

    t.assert(one.numbers.get(0).name === "Cat");
    t.assert(one.numbers.get(0).id === "6cee2499-cbff-4527-9fa7-acbcb71a0214");
    t.assert(one.numbers.get(0).icon === "red_cross_32x32.ico");
    t.assert(two.numbers.get(0).name === "Cat");
    t.assert(two.numbers.get(0).id === "0152ea1a-3595-48c9-873d-cfcad0332a39");
    t.assert(two.numbers.get(0).icon === "fire_hydrant_red.ico");

    t.assert(two.numbers.get(1).name === "Bob");
    t.assert(uuid.check(two.numbers.get(1).id));
    t.assert(two.numbers.get(1).icon === null);

    t.assert(phonebook.sdgroups.size === 1);
    t.assert(phonebook.sdgroups.get(0).sds.size === 0);
});
