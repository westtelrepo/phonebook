/* eslint-disable @typescript-eslint/no-non-null-assertion */
import * as Phonebook from "@w/shared/dist/phonebook";
import test from "ava";
import canonify from "canonical-json";
import Immutable from "immutable";

test("empty phonebook should validate", t => {
    const p = new Phonebook.Phonebook({
        id: "2ad78599-66a8-4eae-bcb6-34f4325fe5a8",
    });
    p.validate();

    t.assert(
        canonify(p) ===
            '{"change_id":0,"contacts":{},"dirs":{},"id":"2ad78599-66a8-4eae-bcb6-34f4325fe5a8","sd_dirs":{},"speed_dials":{}}',
    );
});

test("can add dir", t => {
    const p = new Phonebook.Phonebook();
    p.validate();

    const op = p.dir_add(
        new Phonebook.Directory({
            name: "hello",
            ord: "0",
        }),
    );

    const p2 = p.apply(op);

    p2.validate();

    t.assert(
        p2.dirs.some(dir => dir.name === "hello"),
        "must have dir",
    );
    t.assert(p2.change_id === 1);
});

test("can add and then remove dir", t => {
    const p = new Phonebook.Phonebook();
    p.validate();

    const op = p.dir_add(
        new Phonebook.Directory({
            name: "hello",
            ord: "0.5",
        }),
    );

    const p2 = p.apply(op);
    p2.validate();
    t.assert(
        p2.dirs.some(dir => dir.name === "hello"),
        "must have dir",
    );
    t.assert(p2.change_id === 1);

    const op_del = p2.dir_del(p2.dirs.findKey(dir => dir.name === "hello")!);
    const p3 = p2.apply(op_del);
    p3.validate();

    t.assert(
        p3.dirs.every(dir => dir.name !== "hello"),
        "must not have dir",
    );
    t.assert(
        Immutable.is(p, p3.set("change_id", 0)),
        "must be equal other than for change_id",
    );
    t.assert(canonify(p) === canonify(p3.set("change_id", 0)));
    t.assert(p3.change_id === 2);
});

test("add can be removed", t => {
    const p = new Phonebook.Phonebook();
    p.validate();

    const op = p.dir_add(
        new Phonebook.Directory({
            name: "hello",
            ord: "3.8782",
        }),
    );

    const p2 = p.apply(op);
    p2.validate();
    t.assert(p2.dirs.some(dir => dir.name === "hello"));

    const p3 = p2.reverse(op);
    p3.validate();

    t.assert(Immutable.is(p, p3));
    t.assert(canonify(p) === canonify(p3));
    t.assert(!Immutable.is(p, p2));
    t.assert(!Immutable.is(p2, p3));
});

test("del can be removed", t => {
    const p = new Phonebook.Phonebook();
    const dir = p.dir_add(
        new Phonebook.Directory({ name: "hello", ord: "6.77" }),
    );
    const p2 = p.apply(dir);

    const op_2 = p2.dir_del(p2.dirs.findKey(x => x.name === "hello")!);
    const p3 = p2.apply(op_2);

    const p4 = p3.reverse(op_2);
    t.assert(Immutable.is(p4, p2));
});

test("dir can be moved", t => {
    const p = new Phonebook.Phonebook({
        id: "207c67ad-586b-43c7-a9e5-6e448afb3c8f",
    });
    const p2 = p.apply(
        p.dir_add(new Phonebook.Directory({ name: "aaa", ord: "1" }), {
            id: "08b352f4-1bb5-4566-9cb9-dc4dbac7140e",
        }),
    );
    const p3 = p2.apply(
        p2.dir_add(new Phonebook.Directory({ name: "hello 2", ord: "2" }), {
            id: "04142a92-729e-4575-ae60-2addfd4806c9",
        }),
    );

    const op = p3.dir_move(
        p3.dirs.findKey(x => x.name === "hello 2")!,
        p3.dirs.findKey(x => x.name === "aaa")!,
    );
    const fin = p3.apply(op);

    t.assert(
        canonify(fin) ===
            '{"change_id":3,"contacts":{},"dirs":{"04142a92-729e-4575-ae60-2addfd4806c9":{"name":"hello 2","ord":"2","parent":"08b352f4-1bb5-4566-9cb9-dc4dbac7140e"},"08b352f4-1bb5-4566-9cb9-dc4dbac7140e":{"name":"aaa","ord":"1"}},"id":"207c67ad-586b-43c7-a9e5-6e448afb3c8f","sd_dirs":{},"speed_dials":{}}',
    );
});

test("dir can be edited", t => {
    let p = new Phonebook.Phonebook({
        id: "d6acae96-b780-4966-af2c-665bd02dc03c",
    });
    p = p.apply(
        p.dir_add(new Phonebook.Directory({ name: "hello", ord: "1" }), {
            id: "ef4fb00b-1e2d-4d2e-858e-da6646591b2b",
        }),
    );

    const op = p.dir_edit(
        "ef4fb00b-1e2d-4d2e-858e-da6646591b2b",
        "icon",
        "hello.png",
    );
    p = p.apply(op);

    p.validate();

    p = p.apply(op.inverse());

    p.validate();

    t.pass();
});

test("can not have two dirs with same name", t => {
    let p = new Phonebook.Phonebook();

    p = p.apply(p.dir_add(new Phonebook.Directory({ name: "hello", ord: 1 })));

    p = p.apply(p.dir_add(new Phonebook.Directory({ name: "hello", ord: 2 })));

    t.throws(() => p.validate());
});

test("can have root sd dir and dir of the same name", t => {
    let p = new Phonebook.Phonebook();

    p = p.apply(p.dir_add(new Phonebook.Directory({ name: "hello", ord: 1 })));

    p = p.apply(
        p.sd_dir_add(new Phonebook.Directory({ name: "hello", ord: 1 })),
    );

    p.validate();

    t.pass();
});

test("children of root", t => {
    const p = new Phonebook.Phonebook({
        id: "31ccaa9c-89dd-4bec-b666-753274c88c43",
        change_id: 0,
        dirs: {
            "7a41a6eb-7469-4255-b840-8dcd51a2df17": {
                name: "boo",
                ord: "4.5",
            },
            "86baa755-de6c-4a81-be8e-7837269039f4": {
                name: "hello",
                ord: "3",
            },
        },
    });

    p.validate();

    const children = p.children_of(undefined);

    t.assert(children.size === 2);

    t.assert(children.get(0)![1].name === "hello");
    t.assert(children.get(1)![1].name === "boo");
});

test("children of root, same ord", t => {
    const p = new Phonebook.Phonebook({
        id: "31ccaa9c-89dd-4bec-b666-753274c88c43",
        change_id: 0,
        dirs: {
            "86baa755-de6c-4a81-be8e-7837269039f4": {
                name: "hello",
                ord: "3",
            },
            "7a41a6eb-7469-4255-b840-8dcd51a2df17": {
                name: "boo",
                ord: "3",
            },
        },
    });

    p.validate();

    const children = p.children_of(undefined);

    t.assert(children.size === 2);

    t.assert(children.get(0)![1].name === "boo");
    t.assert(children.get(1)![1].name === "hello");
});

test("sd_children of root", t => {
    const p = new Phonebook.Phonebook({
        id: "31ccaa9c-89dd-4bec-b666-753274c88c43",
        change_id: 0,
        sd_dirs: {
            "7a41a6eb-7469-4255-b840-8dcd51a2df17": {
                name: "boo",
                ord: "4.5",
            },
            "86baa755-de6c-4a81-be8e-7837269039f4": {
                name: "hello",
                ord: "3",
            },
        },
    });

    p.validate();

    const children = p.sd_children_of(undefined);

    t.assert(children.size === 2);

    t.assert((children.get(0)![1] as Phonebook.Directory).name === "hello");
    t.assert((children.get(1)![1] as Phonebook.Directory).name === "boo");
});

test("sd_children of root, same ord", t => {
    const p = new Phonebook.Phonebook({
        id: "31ccaa9c-89dd-4bec-b666-753274c88c43",
        change_id: 0,
        sd_dirs: {
            "86baa755-de6c-4a81-be8e-7837269039f4": {
                name: "hello",
                ord: "3",
            },
            "7a41a6eb-7469-4255-b840-8dcd51a2df17": {
                name: "boo",
                ord: "3",
            },
        },
    });

    p.validate();

    const children = p.sd_children_of(undefined);

    t.assert(children.size === 2);

    t.assert((children.get(0)![1] as Phonebook.Directory).name === "boo");
    t.assert((children.get(1)![1] as Phonebook.Directory).name === "hello");
});
