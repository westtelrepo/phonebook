import {
    formatPhoneNumber,
    is_sip_uri,
    normalize_phone_number,
} from "@w/shared/dist/validation";
import test from "ava";

test("is_sip_uri should match", t => {
    t.assert(is_sip_uri("sip:911@hello.com"));
    t.assert(is_sip_uri("sips:911@hello.com"));
    t.assert(is_sip_uri("SIpS:911@hi"));
    t.assert(is_sip_uri("SIP:911@HELLO.COM"));
});

test("is_sip_uri should not match", t => {
    t.assert(!is_sip_uri("303"));
    t.assert(!is_sip_uri("*23"));
    t.assert(!is_sip_uri("siphello"));
    t.assert(!is_sip_uri("!!!!!!!!!!!"));
    t.assert(!is_sip_uri(":::::::::::"));
    t.assert(!is_sip_uri("https://www.google.com/"));
    t.assert(!is_sip_uri("911@hello.com"));
    t.assert(!is_sip_uri("mailto:911@psap.invalid"));
});

test("formatPhoneNumber", t => {
    t.assert(formatPhoneNumber("91231234") === "9.123-1234");
    t.assert(formatPhoneNumber("*83") === "*83");
    t.assert(formatPhoneNumber("911231231234") === "91 (123) 123-1234");
    t.assert(formatPhoneNumber("1231231234") === "(123) 123-1234");
    t.assert(formatPhoneNumber("91231234") === "9.123-1234");
    t.assert(formatPhoneNumber("1231234") === "123-1234");
    t.assert(
        formatPhoneNumber("NOT A PHONE NUMBER AT ALL") ===
            "NOT A PHONE NUMBER AT ALL",
    );
});

test("normalize_phone_number", t => {
    t.assert(normalize_phone_number("*23") === "*23");
    t.false(normalize_phone_number("NOT A PHONE NUMBER"));
    t.assert(normalize_phone_number(" * 2 3 ") === "*23");
    t.assert(
        normalize_phone_number("sip:1234@127.0.0.1") === "sip:1234@127.0.0.1",
    );
    t.assert(normalize_phone_number(" (123) 123-1234") === "1231231234");
});
