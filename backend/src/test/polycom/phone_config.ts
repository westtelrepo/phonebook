import { PolycomTemplate } from "@w/shared/dist/phone_config";
import test from "ava";

test("constructor no arguments", t => {
    const template = new PolycomTemplate();

    t.assert(template.directory().size === 0);
    t.assert(template.lineKeys.size === 0);
});

test("single lineKey", t => {
    const template = new PolycomTemplate({
        lineKeys: [
            {
                category: "SpeedDial",
                ct: "333",
                lb: "a name",
            },
        ],
    });

    const directory = template.directory();
    t.assert(directory.size === 1);
    t.assert(directory.get(0).lb === "a name");
    t.assert(directory.get(0).ct === "333");
    t.assert(directory.get(0).sd === 1);
});

test("allow undefined lineKey", t => {
    const template = new PolycomTemplate({
        lineKeys: [
            undefined,
            {
                category: "BLF",
            },
        ],
    });

    t.assert(template.lineKeys.get(1).category === "BLF");
});

test("set lineKey on sidecar", t => {
    let template = new PolycomTemplate();
    template = template.setLineKeySideCar(
        { page: 0, column: 0, entry: 0 },
        { category: "BLF" },
    );

    t.assert(template.lineKeys.get(16).category === "BLF");

    t.assert(
        template.getLineKeySideCar({ page: 0, column: 0, entry: 0 })
            .category === "BLF",
    );
});

test("get blank lineKey is null", t => {
    const template = new PolycomTemplate();
    t.assert(
        template.getLineKeySideCar({ page: 0, column: 0, entry: 0 }) === null,
    );
});

test("setLineKeySideCar allow undefined", t => {
    let template = new PolycomTemplate();
    // eslint-disable-next-line @typescript-eslint/no-unused-vars
    template = template.setLineKeySideCar(
        { page: 0, column: 0, entry: 0 },
        null,
    );

    t.pass();
});
