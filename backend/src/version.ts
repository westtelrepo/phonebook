export const version =
    process.env.PRETEND_VERSION ??
    "DEVELOPMENTAL VERSION, NOT SUITABLE FOR PRODUCTION";
