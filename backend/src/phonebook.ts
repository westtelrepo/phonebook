/* eslint-disable @typescript-eslint/no-unsafe-call */
import Immutable from "immutable";

export class Phonebook extends Immutable.Record(
    {
        groups: Immutable.List(),
        icon_list: Immutable.List(),
        sdgroups: Immutable.List(),
        id: null,
    },
    "Phonebook",
) {
    constructor(obj: any) {
        if (obj instanceof Phonebook) {
            super(obj);
        } else {
            const o = Object.assign({}, obj);
            if (o.icon_list && !Immutable.List.isList(o.icon_list)) {
                o.icon_list = Immutable.List(o.icon_list);
            }
            if (o.groups && !Immutable.List.isList(o.groups)) {
                o.groups = Immutable.List(
                    o.groups.map((e: any) => new Group(e)),
                );
            }
            if (o.sdgroups && !Immutable.List.isList(o.sdgroups)) {
                o.sdgroups = Immutable.List(
                    o.sdgroups.map((e: any) => new SDGroup(e)),
                );
            }
            super(o);
        }
    }

    public get_group(id: string) {
        return this.groups.find(e => e.id === id);
    }

    public get_group_by_name(name: string) {
        return this.groups.find(e => e.name === name);
    }

    public get_number(id: string) {
        let ret: any;
        this.groups.forEach(g => {
            ret = g.numbers.find((n: any) => n.id === id);
            if (ret) {
                ret = ret.set("gid", g.id);
                return false;
            }
        });
        return ret;
    }

    public get_sd(id: string) {
        let ret;
        this.sdgroups.forEach(g => {
            ret = g.sds.find((n: any) => n.id === id);
            if (ret) {
                ret = ret.set("sdgid", g.id);
                return false;
            }
        });
        return ret;
    }

    public get_sdgroup(id: string) {
        return this.sdgroups.find(e => e.id === id);
    }
}

export class Group extends Immutable.Record(
    {
        icon: null,
        id: null,
        name: null,
        numbers: Immutable.List(),
        ord: null,
    },
    "Group",
) {
    constructor(obj: any) {
        if (obj instanceof Group) {
            super(obj);
        } else {
            const o = Object.assign({}, obj);
            if (o.numbers && !Immutable.List.isList(o.numbers)) {
                o.numbers = Immutable.List(
                    o.numbers.map((e: any) => new Contact(e)),
                );
            }
            super(o);
        }
    }

    public get_contact_by_name(name: string) {
        return this.numbers.find(n => n.name === name);
    }
}

export class ContactTransfer extends Immutable.Record(
    {
        ng911: false,
        blind: false,
        attended: false,
        conference: false,
        outbound: false,
    },
    "ContactTransfer",
) {}

export class Contact extends Immutable.Record(
    {
        gid: null,
        icon: null,
        id: null,
        name: null,
        number: null,
        ord: null,
        transfer: new ContactTransfer(),
    },
    "Contact",
) {
    constructor(obj: any) {
        if (obj instanceof Contact) {
            super(obj);
        } else {
            super(
                Object.assign({}, obj, {
                    transfer: new ContactTransfer(obj.transfer),
                }),
            );
        }
    }
}

export class SDGroup extends Immutable.Record(
    {
        id: null,
        name: null,
        sds: Immutable.List(),
        ord: null,
    },
    "SDGroup",
) {
    constructor(obj: any) {
        if (obj instanceof SDGroup || !obj) {
            super(obj);
        } else {
            super(
                Object.assign({}, obj, {
                    sds: Immutable.List(obj.sds.map((e: any) => new SD(e))),
                }),
            );
        }
    }
}

export class SD extends Immutable.Record(
    {
        id: null,
        nid: null,
        action: "dial_out",
        ord: null,
        icon: null,
        sdgid: null,
    },
    "SD",
) {}
