import koa_router from "@koa/router";
import { IUser } from "@w/shared/dist/db_users";
import { pdp_assert } from "@w/shared/dist/pdp";
import koa_bodyparser from "koa-bodyparser";
import { IDatabase } from "pg-promise";
import uuid from "uuid-1345";

import { Listener } from "../db/listener";
import * as users_db from "../db/users";
import { server_state_middleware } from "../ServerState";

interface IKoaCtx {
    db: IDatabase<unknown>;
    listener: Listener;
    logged_in?: boolean;
    user: IUser & { permissions: any };
    user_session: string;
    is_superadmin?: boolean;
}

const router = new koa_router<IKoaCtx>();

router.use(async (ctx, next) => {
    // eslint-disable-next-line @typescript-eslint/strict-boolean-expressions
    if (!ctx.state.logged_in) {
        ctx.throw("Must be logged in", 403);
    }

    await next();
});

router.use(
    koa_bodyparser({
        enableTypes: ["json"],
        extendTypes: {
            json: ["application/merge-patch+json"],
        },
    }),
);

router.get(
    "/list",
    server_state_middleware<IKoaCtx, unknown>(async ctx => {
        pdp_assert({
            user: ctx.state.user,
            target: { type: "USER_LIST" },
            action: { type: "READ" },
        });
        return users_db.get_user_list(ctx.state.db);
    }),
);

router.post("/upsert", async ctx => {
    ctx.assert(ctx.accepts("json"));
    ctx.assert(ctx.is("application/json"));

    ctx.body = {
        uid: await users_db.user_upsert(ctx.state.db, ctx.request.body, {
            user_to_check: ctx.state.user,
        }),
    };
});

router.delete("/:uid", async ctx => {
    ctx.assert(uuid.check(ctx.params.uid), 404, "must be a UUID");
    ctx.assert(ctx.accepts("json"), 406, "only supports JSON");

    await users_db.user_delete(
        ctx.state.db,
        { uid: ctx.params.uid },
        { user_to_check: ctx.state.user },
    );

    ctx.body = null;
});

export default router;
