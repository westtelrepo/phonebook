/* eslint-disable @typescript-eslint/strict-boolean-expressions */
import koa_router, { RouterParamContext } from "@koa/router";
import { IHistoryEvent, IMeta } from "@w/shared/dist/db_phonebook";
import { IUser } from "@w/shared/dist/db_users";
import { pdp_assert } from "@w/shared/dist/pdp";
import {
    IPatchContact,
    IPatchDirectory,
    IPatchSpeedDial,
    IPostContact,
    IPostDirectory,
    IPostSpeedDial,
} from "@w/shared/dist/phonebook_interfaces";
import { strict as assert } from "assert";
import fs from "fs";
import { ParameterizedContext } from "koa";
import koa_bodyparser from "koa-bodyparser";
import { IDatabase } from "pg-promise";
import uuid from "uuid-1345";

import { Listener } from "../db/listener";
import * as phonebook_db from "../db/phonebook";
import { get_user_info } from "../db/users";
import { from_xml } from "../phonebook/xml";
import { server_state_middleware } from "../ServerState";

interface IKoaCtx {
    db: IDatabase<unknown>;
    listener: Listener;
    logged_in?: boolean;
    user: IUser & { permissions: any };
    user_session: string;
    is_superadmin?: boolean;
}

const router: koa_router<IKoaCtx> = new koa_router();

function get_meta(ctx: ParameterizedContext<IKoaCtx>): IMeta {
    return {
        user: ctx.state.user,
        ip: ctx.ip,
        user_session: ctx.state.user_session,
    };
}

// this protects this entire router
// TODO: more fine grained permissions
router.use(async (ctx, next) => {
    if (!ctx.state.logged_in) {
        ctx.throw(403, "Must be logged in");
    }

    await next();
});

router.use(
    koa_bodyparser({
        enableTypes: ["json", "text"],
        extendTypes: {
            json: ["application/merge-patch+json"],
            text: ["text/xml"],
        },
    }),
);

router.get(
    "/list",
    server_state_middleware<IKoaCtx, RouterParamContext<IKoaCtx>>(
        async ctx => {
            return phonebook_db.get_phonebook_list(ctx.state.db);
        },
        undefined,
        30000,
    ),
);

router.get(
    "/:pid",
    server_state_middleware<IKoaCtx, RouterParamContext<IKoaCtx>>(
        async ctx => {
            const pid = ctx.params.pid;
            assert(uuid.check(pid));

            if (ctx.query.change_id) {
                assert(typeof ctx.query.change_id === "string");
                return phonebook_db.get_state(
                    ctx.state.db,
                    pid,
                    Number(ctx.query.change_id),
                );
            } else {
                return phonebook_db.get_latest(ctx.state.db, pid);
            }
        },
        async ctx => ({
            listener: ctx.state.listener,
            channels: [ctx.params.pid],
        }),
    ),
);

router.get(
    "/:pid/icon_list",
    server_state_middleware<IKoaCtx, RouterParamContext<IKoaCtx>>(
        async ctx => {
            assert(uuid.check(ctx.params.pid));
            return phonebook_db.get_icon_list(ctx.state.db, ctx.params.pid);
        },
        async ctx => ({
            listener: ctx.state.listener,
            channels: [ctx.params.pid],
        }),
        30000,
    ),
);

router.get(
    "/:pid/history",
    server_state_middleware<IKoaCtx, RouterParamContext<IKoaCtx>>(
        async ctx => {
            assert(uuid.check(ctx.params.pid));
            pdp_assert({
                user: ctx.state.user,
                target: { type: "PHONEBOOK" },
                action: { type: "READ_HISTORY" },
            });
            const list = await phonebook_db.get_history(
                ctx.state.db,
                ctx.params.pid,
            );
            const obj: { [index: number]: IHistoryEvent } = {};
            const cache = new Map<
                string,
                (IUser & { permissions: any }) | null
            >();
            for (const e of list) {
                // set username to be current username instead of historical username
                if (e.meta?.user) {
                    if (!cache.has(e.meta.user.uid)) {
                        cache.set(
                            e.meta.user.uid,
                            await get_user_info(ctx.state.db, {
                                uid: e.meta.user.uid,
                            }),
                        );
                    }
                    const user_info = cache.get(e.meta.user.uid);
                    e.meta.user.username = user_info
                        ? user_info.username
                        : e.meta.user.username + " (deleted)";
                }
                obj[e.change_id] = e;
            }
            return obj;
        },
        async ctx => ({
            listener: ctx.state.listener,
            channels: [ctx.params.pid],
        }),
    ),
);

router.get("/:pid/history/:cid", async ctx => {
    ctx.assert(uuid.check(ctx.params.pid));
    ctx.assert(ctx.accepts("application/json"), 406, "only supports JSON");

    ctx.body = await phonebook_db.get_history_one(
        ctx.state.db,
        ctx.params.pid,
        Number(ctx.params.cid),
    );
});

router.post("/:pid/update", async ctx => {
    assert(uuid.check(ctx.params.pid));
    assert(ctx.accepts("json"));
    assert(ctx.is("application/json"));
    pdp_assert({
        user: ctx.state.user,
        target: { type: "PHONEBOOK" },
        action: { type: "UPDATE" },
    });

    ctx.body = await phonebook_db.update_current_change_id(
        ctx.state.db,
        ctx.params.pid,
        get_meta(ctx),
    );
    await phonebook_db.write_phonebook(ctx.state.db, ctx.params.pid);
});

router.post("/:pid/undo", async ctx => {
    assert(uuid.check(ctx.params.pid));
    assert(ctx.accepts("json"));
    assert(ctx.is("application/json"));

    const old = await phonebook_db.get_latest(ctx.state.db, ctx.params.pid);

    // TODO: check ctx.request.body
    ctx.body = await phonebook_db.undo_changeid(
        ctx.state.db,
        ctx.params.pid,
        ctx.request.body.change_id,
        get_meta(ctx),
    );

    const after = await phonebook_db.get_latest(ctx.state.db, ctx.params.pid);

    if (after.current_change_id !== old.current_change_id) {
        await phonebook_db.write_phonebook(ctx.state.db, ctx.params.pid);
    }
});

router.post("/:pid/restore_to", async ctx => {
    assert(uuid.check(ctx.params.pid));
    assert.strictEqual(typeof ctx.request.body.change_id, "number");
    assert(ctx.accepts("json"));
    assert(ctx.is("application/json"));

    await phonebook_db.restore_changeid(
        ctx.state.db,
        ctx.params.pid,
        ctx.request.body.change_id,
        get_meta(ctx),
    );

    ctx.body = null;
});

router.post("/:pid/import", async ctx => {
    assert(uuid.check(ctx.params.pid));
    assert(ctx.accepts("json"));
    assert(ctx.is("application/json"));

    pdp_assert({
        user: ctx.state.user,
        target: { type: "PHONEBOOK" },
        action: { type: "IMPORT_EXPORT" },
    });

    ctx.body = await phonebook_db.bulk_import(
        ctx.state.db,
        ctx.params.pid,
        ctx.request.body,
        get_meta(ctx),
        ctx.state.user,
    );
});

router.post("/:pid/xml_import", async ctx => {
    if (!ctx.state.is_superadmin) {
        ctx.throw("must be superadmin to do XML import", 403);
    }

    if (!ctx.is("text/xml")) {
        ctx.throw("must be XML", 415);
    }

    ctx.assert(uuid.check(ctx.params.pid));
    ctx.assert(ctx.accepts("json"));

    const xml = ctx.request.body;
    if (typeof xml !== "string") {
        ctx.throw("xml is not a string");
        throw new Error("xml is not a string, dead code");
    }

    const ph = from_xml(xml);

    await phonebook_db.replace(ctx.state.db, ctx.params.pid, ph, get_meta(ctx));

    ctx.body = null;
});

router.post("/:pid/xml_import_disk", async ctx => {
    if (!ctx.state.is_superadmin) {
        ctx.throw("must be superadmin to do XML import", 403);
    }

    ctx.assert(uuid.check(ctx.params.pid));
    ctx.assert(ctx.accepts("json"));

    const current = await phonebook_db.get_latest(ctx.state.db, ctx.params.pid);
    if (typeof current.filename !== "string") {
        ctx.throw("filename does not exist to be imported from");
        throw new Error("dead code");
    }
    const file = fs.readFileSync(current.filename, "utf8");

    await phonebook_db.replace(
        ctx.state.db,
        ctx.params.pid,
        from_xml(file),
        get_meta(ctx),
    );

    ctx.body = null;
});

router.patch("/:pid/dirs/:did", async ctx => {
    assert(ctx.is("application/merge-patch+json"));
    assert(ctx.accepts("json"));
    assert(uuid.check(ctx.params.pid));
    assert(uuid.check(ctx.params.did));

    // TODO: check this
    const req: IPatchDirectory = ctx.request.body;

    ctx.body = await phonebook_db.patch_directory(
        ctx.state.db,
        ctx.params.pid,
        ctx.params.did,
        req,
        get_meta(ctx),
    );
});

router.delete("/:pid/dirs/:did", async ctx => {
    assert(ctx.accepts("json"));
    assert(uuid.check(ctx.params.pid));
    assert(uuid.check(ctx.params.did));

    ctx.body = await phonebook_db.delete_directory(
        ctx.state.db,
        ctx.params.pid,
        ctx.params.did,
        get_meta(ctx),
    );
});

router.post("/:pid/dirs/:did/moveup", async ctx => {
    assert(ctx.accepts("json"));
    assert(uuid.check(ctx.params.pid));
    assert(uuid.check(ctx.params.did));

    ctx.body = await phonebook_db.move_directory(
        ctx.state.db,
        ctx.params.pid,
        ctx.params.did,
        "up",
        get_meta(ctx),
    );
});

router.post("/:pid/dirs/:did/movedown", async ctx => {
    assert(ctx.accepts("json"));
    assert(uuid.check(ctx.params.pid));
    assert(uuid.check(ctx.params.did));

    ctx.body = await phonebook_db.move_directory(
        ctx.state.db,
        ctx.params.pid,
        ctx.params.did,
        "down",
        get_meta(ctx),
    );
});

router.post("/:pid/dirs/:did/sort", async ctx => {
    assert(ctx.accepts("json"));
    assert(uuid.check(ctx.params.pid));
    assert(uuid.check(ctx.params.did));

    ctx.body = await phonebook_db.sort_directory(
        ctx.state.db,
        ctx.params.pid,
        ctx.params.did,
        get_meta(ctx),
    );
});

router.post("/:pid/dirs", async ctx => {
    assert(ctx.is("application/json"));
    assert(ctx.accepts("json"));
    assert(uuid.check(ctx.params.pid));

    const req: IPostDirectory = ctx.request.body;

    const ret = await phonebook_db.post_directory(
        ctx.state.db,
        ctx.params.pid,
        req,
        get_meta(ctx),
    );

    ctx.body = ret;
});

router.patch("/:pid/sd_dirs/:did", async ctx => {
    assert(ctx.is("application/merge-patch+json"));
    assert(ctx.accepts("json"));
    assert(uuid.check(ctx.params.pid));
    assert(uuid.check(ctx.params.did));

    // TODO: check this
    const req: IPatchDirectory = ctx.request.body;

    ctx.body = await phonebook_db.patch_sd_directory(
        ctx.state.db,
        ctx.params.pid,
        ctx.params.did,
        req,
        get_meta(ctx),
    );
});

router.delete("/:pid/sd_dirs/:did", async ctx => {
    assert(ctx.accepts("json"));
    assert(uuid.check(ctx.params.pid));
    assert(uuid.check(ctx.params.did));

    ctx.body = await phonebook_db.delete_sd_directory(
        ctx.state.db,
        ctx.params.pid,
        ctx.params.did,
        get_meta(ctx),
    );
});

router.post("/:pid/sd_dirs/:did/moveup", async ctx => {
    assert(ctx.accepts("json"));
    assert(uuid.check(ctx.params.pid));
    assert(uuid.check(ctx.params.did));

    ctx.body = await phonebook_db.move_sd_directory(
        ctx.state.db,
        ctx.params.pid,
        ctx.params.did,
        "up",
        get_meta(ctx),
    );
});

router.post("/:pid/sd_dirs/:did/movedown", async ctx => {
    assert(ctx.accepts("json"));
    assert(uuid.check(ctx.params.pid));
    assert(uuid.check(ctx.params.did));

    ctx.body = await phonebook_db.move_sd_directory(
        ctx.state.db,
        ctx.params.pid,
        ctx.params.did,
        "down",
        get_meta(ctx),
    );
});

router.post("/:pid/sd_dirs", async ctx => {
    assert(ctx.is("application/json"));
    assert(ctx.accepts("json"));
    assert(uuid.check(ctx.params.pid));

    const req: IPostDirectory = ctx.request.body;

    const ret = await phonebook_db.post_sd_directory(
        ctx.state.db,
        ctx.params.pid,
        req,
        get_meta(ctx),
    );

    ctx.body = ret;
});

router.post("/:pid/contacts", async ctx => {
    assert(ctx.is("application/json"));
    assert(ctx.accepts("json"));
    assert(uuid.check(ctx.params.pid));

    const req: IPostContact = ctx.request.body;

    const ret = await phonebook_db.post_contact(
        ctx.state.db,
        ctx.params.pid,
        req,
        get_meta(ctx),
    );

    ctx.body = ret;
});

router.delete("/:pid/contacts/:cid", async ctx => {
    assert(ctx.accepts("json"));
    assert(uuid.check(ctx.params.pid));
    assert(uuid.check(ctx.params.cid));

    ctx.body = await phonebook_db.delete_contact(
        ctx.state.db,
        ctx.params.pid,
        ctx.params.cid,
        get_meta(ctx),
        ctx.state.user,
    );
});

router.patch("/:pid/contacts/:cid", async ctx => {
    assert(ctx.is("application/merge-patch+json"));
    assert(ctx.accepts("json"));
    assert(uuid.check(ctx.params.pid));
    assert(uuid.check(ctx.params.cid));

    // TODO: check this
    const req: IPatchContact = ctx.request.body;

    ctx.body = await phonebook_db.patch_contact(
        ctx.state.db,
        ctx.params.pid,
        ctx.params.cid,
        req,
        get_meta(ctx),
        ctx.state.user,
    );
});

router.post("/:pid/contacts/:cid/moveup", async ctx => {
    assert(ctx.accepts("json"));
    assert(uuid.check(ctx.params.pid));
    assert(uuid.check(ctx.params.cid));

    ctx.body = await phonebook_db.move_contact(
        ctx.state.db,
        ctx.params.pid,
        ctx.params.cid,
        "up",
        get_meta(ctx),
    );
});

router.post("/:pid/contacts/:cid/movedown", async ctx => {
    assert(ctx.accepts("json"));
    assert(uuid.check(ctx.params.pid));
    assert(uuid.check(ctx.params.cid));

    ctx.body = await phonebook_db.move_contact(
        ctx.state.db,
        ctx.params.pid,
        ctx.params.cid,
        "down",
        get_meta(ctx),
    );
});

router.post("/:pid/speed_dials", async ctx => {
    assert(ctx.is("application/json"));
    assert(ctx.accepts("json"));
    assert(uuid.check(ctx.params.pid));

    const req: IPostSpeedDial = ctx.request.body;

    const ret = await phonebook_db.post_speed_dial(
        ctx.state.db,
        ctx.params.pid,
        req,
        get_meta(ctx),
    );

    ctx.body = ret;
});

router.patch("/:pid/speed_dials/:cid", async ctx => {
    assert(ctx.is("application/merge-patch+json"));
    assert(ctx.accepts("json"));
    assert(uuid.check(ctx.params.pid));
    assert(uuid.check(ctx.params.cid));

    // TODO: check this
    const req: IPatchSpeedDial = ctx.request.body;

    ctx.body = await phonebook_db.patch_speed_dial(
        ctx.state.db,
        ctx.params.pid,
        ctx.params.cid,
        req,
        get_meta(ctx),
    );
});

router.delete("/:pid/speed_dials/:cid", async ctx => {
    assert(ctx.accepts("json"));
    assert(uuid.check(ctx.params.pid));
    assert(uuid.check(ctx.params.cid));

    ctx.body = await phonebook_db.delete_speed_dial(
        ctx.state.db,
        ctx.params.pid,
        ctx.params.cid,
        get_meta(ctx),
    );
});

router.post("/:pid/speed_dials/:cid/moveup", async ctx => {
    assert(ctx.accepts("json"));
    assert(uuid.check(ctx.params.pid));
    assert(uuid.check(ctx.params.cid));

    ctx.body = await phonebook_db.move_sd(
        ctx.state.db,
        ctx.params.pid,
        ctx.params.cid,
        "up",
        get_meta(ctx),
    );
});

router.post("/:pid/speed_dials/:cid/movedown", async ctx => {
    assert(ctx.accepts("json"));
    assert(uuid.check(ctx.params.pid));
    assert(uuid.check(ctx.params.cid));

    ctx.body = await phonebook_db.move_sd(
        ctx.state.db,
        ctx.params.pid,
        ctx.params.cid,
        "down",
        get_meta(ctx),
    );
});

export default router;
